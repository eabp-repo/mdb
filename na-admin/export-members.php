<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsMembersExportCSV = "-1";
if (isset($_GET['naid'])) {
  $colname_rsMembersExportCSV = $_GET['naid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembersExportCSV = sprintf("SELECT firstname, lastname, homeadd1, homecity, homepostcode, country, workadd1, workcity, workpostcode, email, acronym AS NA, type FROM (type INNER JOIN member ON type.tid = member.tid) INNER JOIN (national INNER JOIN country ON national.cid = country.cid) ON member.cid = national.cid WHERE naid = %s ORDER BY lastname", GetSQLValueString($colname_rsMembersExportCSV, "int"));
$rsMembersExportCSV = mysql_query($query_rsMembersExportCSV, $connEABP2) or die(mysql_error());
$row_rsMembersExportCSV = mysql_fetch_assoc($rsMembersExportCSV);
$totalRows_rsMembersExportCSV = mysql_num_rows($rsMembersExportCSV);

?>
<?php 
csvToExcelDownloadFromResult($rsMembersExportCSV);

// setup the headers
function setExcelContentType() {
    if(headers_sent())
        return false;

    header('Content-type: application/vnd.ms-excel');
    return true;
}

function setDownloadAsHeader($filename) {
    if(headers_sent())
        return false;

    header('Content-disposition: attachment; filename=' . $filename);
    return true;
}
// send a CSV to a stream using a mysql result
function csvFromResult($stream, $rsMembersExportCSV, $showColumnHeaders = true) {
    if($showColumnHeaders) {
        $columnHeaders = array();
        $nfields = mysql_num_fields($rsMembersExportCSV);
        for($i = 0; $i < $nfields; $i++) {
            $field = mysql_fetch_field($rsMembersExportCSV, $i);
            $columnHeaders[] = $field->name;
        }
        fputcsv($stream, $columnHeaders);
    }

    $nrows = 0;
    while($row = mysql_fetch_row($rsMembersExportCSV)) {
        fputcsv($stream, $row);
        $nrows++;
    }

    return $nrows;
}
// use the above function to write a CSV to a file, given by $filename
function csvFileFromResult($filename, $rsMembersExportCSV, $showColumnHeaders = true) {
    $fp = fopen($filename, 'w');
    $rc = csvFromResult($fp, $rsMembersExportCSV, $showColumnHeaders);
    fclose($fp);
    return $rc;
}
// output
function csvToExcelDownloadFromResult($rsMembersExportCSV, $showColumnHeaders = true, $asFilename = 'members.csv') {
    setExcelContentType();
    setDownloadAsHeader($asFilename);
    return csvFileFromResult('php://output', $rsMembersExportCSV, $showColumnHeaders);
}

?>
<?php
mysql_free_result($rsMembersExportCSV);
?>