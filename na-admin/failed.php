<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>National Association admin</title>
<link href="../sitecms/cms.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-224881-50']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
<p><img src="../images/logo-20.gif" alt="" width="47" height="20" align="absmiddle" /> - <a href="../index.php">website</a></p>
<p>National Association login</p>
<p>Sorry but there is a problem with your login.</p>
<p>Please <a href="login.php">try again</a>.</p>
<p>If you still have problems please <a href="mailto:secretariat@eabp.org">email the EABP Secretariat</a>.</p>
<p>&nbsp;</p>
</body>
</html>
