<?php require_once('../Connections/connEABP2.php'); ?><?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?><?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsNA = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsNA = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = sprintf("SELECT naid, acronym, fullname FROM `national` WHERE username = %s", GetSQLValueString($colname_rsNA, "text"));
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);

$colname_rsMembers = $row_rsNA['naid'];

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT mid, firstname, lastname, type, school FROM member INNER JOIN type ON member.tid = type.tid WHERE nid = %s AND type.tid <> 9 ORDER BY type DESC, lastname", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);
?>
<?php  $lastTFM_nest = "";?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>National Association admin</title>
<link href="../sitecms/cms.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-224881-50']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
<p><img src="../images/logo-20.gif" alt="" width="47" height="20" align="absmiddle" /> - <a href="../index.php">website</a></p>
<p>National Association admin | <a href="logout.php">logout</a></p>
<p>&nbsp;</p>
<h1><?php echo $row_rsNA['acronym']; ?> - <?php echo $row_rsNA['fullname']; ?></h1>
<p><a href="national-edit.php?naid=<?php echo $row_rsNA['naid']; ?>">Update  association details</a></p>
<hr size="1" noshade="noshade" />
<h2>Organisations </h2>
<form id="frmOrgs" name="frmOrgs" method="post" action="orgs.php">
  <input name="nid" type="hidden" id="nid" value="<?php echo $row_rsNA['naid']; ?>" />
  <input name="btnEdit" type="submit" class="btnAdd" id="btnEdit" value="View list to edit" />
</form>
<p><a href="../sitecms/all-orgs-add.php?na=0">Add a new organisation</a></p>
<hr size="1" noshade="noshade" />
<h2>Members</h2>
<p><a href="export-members.php?naid=<?php echo $row_rsNA['naid']; ?>">Address List</a> (save as csv file)</p>
<p><a href="member-add.php?naid=<?php echo $row_rsNA['naid']; ?>">Add a member</a></p>
<h3>Update/Delete a member:</h3>
<table width="100%" border="0" cellpadding="5" cellspacing="0">
  <?php do { ?>
    <tr>
      <td width="200"><?php $TFM_nest = $row_rsMembers['type'];
if ($lastTFM_nest != $TFM_nest) { 
	$lastTFM_nest = $TFM_nest; ?>
        <?php echo $row_rsMembers['type']; ?>s: 
      <?php } //End of Basic-UltraDev Simulated Nested Repeat?></td>
      <td><a href="member-edit.php?mid=<?php echo $row_rsMembers['mid']; ?>"><strong><?php echo $row_rsMembers['lastname']; ?></strong>, <?php echo $row_rsMembers['firstname']; ?></a> <?php echo $row_rsMembers['school']; ?></td>
    </tr>
    <?php } while ($row_rsMembers = mysql_fetch_assoc($rsMembers)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($rsNA);

mysql_free_result($rsMembers);
?>
