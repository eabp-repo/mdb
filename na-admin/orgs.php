<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsOrgs = "-1";
if (isset($_POST['nid'])) {
  $colname_rsOrgs = $_POST['nid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgs = sprintf("SELECT id, orgname FROM allorgs WHERE nid = %s ORDER BY orgname ASC", GetSQLValueString($colname_rsOrgs, "int"));
$rsOrgs = mysql_query($query_rsOrgs, $connEABP2) or die(mysql_error());
$row_rsOrgs = mysql_fetch_assoc($rsOrgs);
$totalRows_rsOrgs = mysql_num_rows($rsOrgs);

$colname_rsNA = "-1";
if (isset($_POST['nid'])) {
  $colname_rsNA = $_POST['nid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = sprintf("SELECT fullname FROM `national` WHERE naid = %s", GetSQLValueString($colname_rsNA, "int"));
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>NA organisations</title>
<link href="../sitecms/cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>EABP organisations - <?php echo $row_rsNA['fullname']; ?></h1>
<ul>
  <?php do { ?>
    <li>
      <h3><a href="../sitecms/all-orgs-edit.php?id=<?php echo $row_rsOrgs['id']; ?>&amp;na=1"><?php echo $row_rsOrgs['orgname']; ?></a></h3>
    </li>
    <?php } while ($row_rsOrgs = mysql_fetch_assoc($rsOrgs)); ?>
</ul>
</body>
</html>
<?php
mysql_free_result($rsOrgs);

mysql_free_result($rsNA);
?>
