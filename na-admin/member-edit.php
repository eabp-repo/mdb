<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/memberphotos";
$ppu->extensions = "GIF,JPG,JPEG,PNG";
$ppu->formName = "frmUpdate";
$ppu->storeType = "file";
$ppu->sizeLimit = "50";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmUpdate")) {
  $updateSQL = sprintf("UPDATE member SET doa=%s, dob=%s, doj=%s, dor=%s, ecp=%s, email=%s, fax=%s, firstname=%s, forum=%s, gender=%s, homecity=%s, homephone=%s, homepostcode=%s, homeadd1=%s, lastname=%s, mobile=%s, modality=%s, speciality=%s, notes=%s, post=%s, school=%s, skype=%s, title=%s, webpage=%s, workphone=%s, cid=%s, nid=%s, tid=%s, workadd1=%s, workcity=%s, workpostcode=%s, workcid=%s, showworkinfo=%s, showhomeinfo=%s, imagefile=IFNULL(%s,imagefile), education=%s, currentwork=%s, employmenthistory=%s, bio=%s, blogaddress=%s, workshops=%s, showonsite=%s WHERE mid=%s",
                       GetSQLValueString($_POST['doa'], "date"),
                       GetSQLValueString($_POST['dob'], "date"),
                       GetSQLValueString($_POST['doj'], "date"),
                       GetSQLValueString($_POST['dor'], "date"),
                       GetSQLValueString($_POST['ecp'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['fax'], "text"),
                       GetSQLValueString($_POST['firstname'], "text"),
                       GetSQLValueString($_POST['forum'], "text"),
                       GetSQLValueString($_POST['gender'], "text"),
                       GetSQLValueString($_POST['homecity'], "text"),
                       GetSQLValueString($_POST['homephone'], "text"),
                       GetSQLValueString($_POST['homepostcode'], "text"),
                       GetSQLValueString($_POST['homeadd1'], "text"),
                       GetSQLValueString($_POST['lastname'], "text"),
                       GetSQLValueString($_POST['mobile'], "text"),
                       GetSQLValueString($_POST['modality'], "text"),
                       GetSQLValueString($_POST['speciality'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString($_POST['post'], "text"),
                       GetSQLValueString($_POST['school'], "text"),
                       GetSQLValueString($_POST['skype'], "text"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['webpage'], "text"),
                       GetSQLValueString($_POST['workphone'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['naid'], "int"),
                       GetSQLValueString($_POST['tid'], "int"),
                       GetSQLValueString($_POST['workadd1'], "text"),
                       GetSQLValueString($_POST['workcity'], "text"),
                       GetSQLValueString($_POST['workpostcode'], "text"),
                       GetSQLValueString($_POST['workcid'], "int"),
                       GetSQLValueString(isset($_POST['showworkinfo']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString(isset($_POST['showhomeinfo']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['imagefile'], "text"),
                       GetSQLValueString($_POST['education'], "text"),
                       GetSQLValueString($_POST['currentwork'], "text"),
                       GetSQLValueString($_POST['employmenthistory'], "text"),
                       GetSQLValueString($_POST['bio'], "text"),
                       GetSQLValueString($_POST['blogaddress'], "text"),
                       GetSQLValueString($_POST['workshops'], "text"),
                       GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9 AND tid <> 11 AND tid <> 6";
$rsMemTypes = mysql_query($query_rsMemTypes, $connEABP2) or die(mysql_error());
$row_rsMemTypes = mysql_fetch_assoc($rsMemTypes);
$totalRows_rsMemTypes = mysql_num_rows($rsMemTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9";
$rsTypes = mysql_query($query_rsTypes, $connEABP2) or die(mysql_error());
$row_rsTypes = mysql_fetch_assoc($rsTypes);
$totalRows_rsTypes = mysql_num_rows($rsTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = "SELECT naid, acronym FROM `national`";
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>EABP individual membership database update</title>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>

<link href="../sitecms/cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-224881-50']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php">Admin home</a></p>
<h1>Individual member  update
  <?php if (isset($_POST['processed'])) { ?> - <img src="../images/tick.gif" alt="tick" width="16" height="16" /><?php } ?></h1>
<p><a href="member-delete.php?mid=<?php echo $row_rsMembers['mid']; ?>">Go to delete this member</a> (you will be asked to confirm)</p>
<hr size="1" noshade="noshade" />
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmUpdate" id="frmUpdate" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,PNG',false,50,'','','','','','');return document.MM_returnValue">
  <table border="0" cellpadding="3" cellspacing="0" id="tblDetails">
    <tr>
      <td colspan="2" class="smallText">Filemaker Pro membership ID:<strong> <?php echo $row_rsMembers['membershipid']; ?></strong></td>
      <td class="smallText"><span class="redText">red fields</span> are new</td>
    </tr>
    <?php if ($_GET['tid'] == 11) { ?>
    <?php } ?>
    <tr>
      <td class="redText">Show on website</td>
      <td><input <?php if (!(strcmp($row_rsMembers['showonsite'],1))) {echo "checked=\"checked\"";} ?> type="checkbox" name="showonsite" id="showonsite" /></td>
      <td rowspan="7" align="right"><input name="mid" type="hidden" id="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
          <input type="submit" name="btnUpdate" id="btnUpdate" value="Update" />
          <input name="MM_update" type="hidden" id="MM_update" value="frmUpdate" /></td>
    </tr>
    <tr>
      <td width="190">Last Name</td>
      <td><input name="lastname" type="text" class="largerText" id="lastname" value="<?php echo $row_rsMembers['lastname']; ?>" /></td>
    </tr>
    <tr>
      <td>First Name</td>
      <td><input name="firstname" type="text" id="firstname" value="<?php echo $row_rsMembers['firstname']; ?>" /></td>
    </tr>
    <tr>
      <td>Title</td>
      <td><input name="post" type="text" id="post" value="<?php echo $row_rsMembers['post']; ?>" /></td>
    </tr>
    <tr>
      <td>Letters after name</td>
      <td><input name="title" type="text" id="title" value="<?php echo $row_rsMembers['title']; ?>" /></td>
    </tr>
    <tr>
      <td>Gender</td>
      <td><select name="gender" id="gender">
          <option value="Female" <?php if (!(strcmp("Female", $row_rsMembers['gender']))) {echo "selected=\"selected\"";} ?>>Female</option>
          <option value="Male" <?php if (!(strcmp("Male", $row_rsMembers['gender']))) {echo "selected=\"selected\"";} ?>>Male</option>
      </select></td>
    </tr>
    <tr>
      <td>Type of membership</td>
      <td><select name="tid" id="tid">
          <?php
do {  
?>
          <option value="<?php echo $row_rsTypes['tid']?>"<?php if (!(strcmp($row_rsTypes['tid'], $row_rsMembers['tid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsTypes['type']?></option>
          <?php
} while ($row_rsTypes = mysql_fetch_assoc($rsTypes));
  $rows = mysql_num_rows($rsTypes);
  if($rows > 0) {
      mysql_data_seek($rsTypes, 0);
	  $row_rsTypes = mysql_fetch_assoc($rsTypes);
  }
?>
      </select></td>
    </tr>
    <tr>
      <td>Modality</td>
      <td><input name="modality" type="text" id="modality" value="<?php echo $row_rsMembers['modality']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td class="redText">Speciality</td>
      <td><input name="speciality" type="text" id="speciality" value="<?php echo $row_rsMembers['speciality']; ?>" /></td>
      <td class="smallText">&nbsp;</td>
    </tr>
    <tr>
      <td>Organisation</td>
      <td><input name="school" type="text" id="school" value="<?php echo $row_rsMembers['school']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td class="redText">Web password</td>
      <td><input name="webpassword" type="text" id="webpassword" value="<?php echo $row_rsMembers['webpassword']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td><span class="redText">Photo</span>
          <?php if ($row_rsMembers['imagefile'] !="") { ?>
        <span class="smallText"> - currently</span> <a href="../images/memberphotos/<?php echo $row_rsMembers['imagefile']; ?>"><img src="../images/memberphotos/<?php echo $row_rsMembers['imagefile']; ?>" alt="Click to view full size" width="24" height="24" border="0" align="absmiddle" /></a>
      <?php } ?></td>
      <td><input name="imagefile" type="file" id="imagefile" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,PNG',false,50,'','','','','','')" /></td>
      <td align="center" class="smallText">Optional - 50KB max</td>
    </tr>
    <tr>
      <td>Date of Birth</td>
      <td><input name="dob" type="text" id="dob" value="<?php echo $row_rsMembers['dob']; ?>" /></td>
      <td align="center" class="smallText">enter as yyyy-mm-dd</td>
    </tr>
    <tr>
      <td>Email</td>
      <td><input name="email" type="text" id="email" value="<?php echo $row_rsMembers['email']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Fax</td>
      <td><input name="fax" type="text" id="fax" value="<?php echo $row_rsMembers['fax']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Mobile Phone</td>
      <td><input name="mobile" type="text" id="mobile" value="<?php echo $row_rsMembers['mobile']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Work Phone</td>
      <td><input name="workphone" type="text" id="workphone" value="<?php echo $row_rsMembers['workphone']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td class="redText">Work Street</td>
      <td><input name="workadd1" type="text" id="workadd1" value="<?php echo $row_rsMembers['workadd1']; ?>" /></td>
      <td rowspan="10" align="right"><input name="mid" type="hidden" id="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
          <input type="submit" name="btnUpdate" id="btnUpdate" value="Update" />
          <input name="MM_update" type="hidden" id="MM_update" value="frmUpdate" /></td>
    </tr>
    <tr>
      <td class="redText">Work City</td>
      <td><input name="workcity" type="text" id="workcity" value="<?php echo $row_rsMembers['workcity']; ?>" /></td>
    </tr>
    <tr>
      <td class="redText">Work Postal Code</td>
      <td><input name="workpostcode" type="text" id="workpostcode" value="<?php echo $row_rsMembers['workpostcode']; ?>" /></td>
    </tr>
    <tr>
      <td class="redText">Work Country</td>
      <td><select name="workcid" id="workcid">
          <?php
do {  
?><option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsMembers['workcid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option><?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
      </select></td>
    </tr>
    <tr>
      <td class="redText">Show work info on website</td>
      <td><input <?php if (!(strcmp($row_rsMembers['showworkinfo'],1))) {echo "checked=\"checked\"";} ?> name="showworkinfo" type="checkbox" id="showworkinfo" value="1" /></td>
    </tr>
    <tr>
      <td>Home Phone</td>
      <td><input name="homephone" type="text" id="homephone" value="<?php echo $row_rsMembers['homephone']; ?>" /></td>
    </tr>
    <tr>
      <td>Home Street</td>
      <td><input name="homeadd1" type="text" id="homeadd1" value="<?php echo $row_rsMembers['homeadd1']; ?>" /></td>
    </tr>
    <tr>
      <td>Home City</td>
      <td><input name="homecity" type="text" id="homecity" value="<?php echo $row_rsMembers['homecity']; ?>" /></td>
    </tr>
    <tr>
      <td>Home Postal Code</td>
      <td><input name="homepostcode" type="text" id="homepostcode" value="<?php echo $row_rsMembers['homepostcode']; ?>" /></td>
    </tr>
    <tr>
      <td>Home Country</td>
      <td><select name="cid" id="cid">
          <?php
do {  
?>
          <option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsMembers['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option>
          <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
      </select></td>
    </tr>
    <tr>
      <td class="redText">Show home info on website</td>
      <td><input <?php if (!(strcmp($row_rsMembers['showhomeinfo'],1))) {echo "checked=\"checked\"";} ?> name="showhomeinfo" type="checkbox" id="showhomeinfo" value="1" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Skype</td>
      <td><input name="skype" type="text" id="skype" value="<?php echo $row_rsMembers['skype']; ?>" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" class="redText">Education</td>
      <td><textarea name="education" id="education" cols="45" rows="5"><?php echo $row_rsMembers['education']; ?></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" class="redText">Current work</td>
      <td><textarea name="currentwork" id="currentwork" cols="45" rows="5"><?php echo $row_rsMembers['currentwork']; ?></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" class="redText">Employment history</td>
      <td><textarea name="employmenthistory" id="employmenthistory" cols="45" rows="5"><?php echo $row_rsMembers['employmenthistory']; ?></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" class="redText">Bio</td>
      <td><textarea name="bio" id="bio" cols="45" rows="5"><?php echo $row_rsMembers['bio']; ?></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" class="redText">Workshops</td>
      <td><textarea name="workshops" id="workshops" cols="45" rows="5"><?php echo $row_rsMembers['workshops']; ?></textarea></td>
      <td><input name="mid" type="hidden" id="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
        <input name="btnUpdate" type="submit" id="btnUpdate" value="Update" />
        <input name="MM_update" type="hidden" id="MM_update" value="frmUpdate" /></td>
    </tr>
    <tr>
      <td class="redText">Blog address</td>
      <td><input name="blogaddress" type="text" id="blogaddress" value="<?php echo $row_rsMembers['blogaddress']; ?>" /></td>
      <td class="smallText">NO http://</td>
    </tr>
    <tr>
      <td>Website address</td>
      <td><input name="webpage" type="text" id="webpage" value="<?php echo $row_rsMembers['webpage']; ?>" /></td>
      <td class="smallText">NO http://</td>
    </tr>
    <tr>
      <td>ECP</td>
      <td><input name="ecp" type="text" id="ecp" value="<?php echo $row_rsMembers['ecp']; ?>" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Forum</td>
      <td><input name="forum" type="text" id="forum" value="<?php echo $row_rsMembers['forum']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>National association</td>
      <td><select name="naid" id="naid">
          <?php
do {  
?>
          <option value="<?php echo $row_rsNA['naid']?>"<?php if (!(strcmp($row_rsNA['naid'], $row_rsMembers['nid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsNA['acronym']?></option>
          <?php
} while ($row_rsNA = mysql_fetch_assoc($rsNA));
  $rows = mysql_num_rows($rsNA);
  if($rows > 0) {
      mysql_data_seek($rsNA, 0);
	  $row_rsNA = mysql_fetch_assoc($rsNA);
  }
?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td valign="top">Notes</td>
      <td><textarea name="notes" id="notes" cols="45" rows="3"><?php echo $row_rsMembers['notes']; ?></textarea></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Membership Declaration</td>
      <td><input name="declaration" type="text" id="declaration" value="<?php echo $row_rsMembers['declaration']; ?>" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Date of Application</td>
      <td><input name="doa" type="text" id="doa" value="<?php echo $row_rsMembers['doa']; ?>" /></td>
      <td align="center" class="smallText">enter as yyyy-mm-dd</td>
    </tr>
    <tr>
      <td>Date of Joining</td>
      <td><input name="doj" type="text" id="doj" value="<?php echo $row_rsMembers['doj']; ?>" /></td>
      <td align="center" class="smallText">enter as yyyy-mm-dd</td>
    </tr>
    <tr>
      <td>Date of resignation</td>
      <td><input name="dor" type="text" id="dor" value="<?php echo $row_rsMembers['dor']; ?>" /></td>
      <td align="center" class="smallText">enter as yyyy-mm-dd</td>
    </tr>

    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Redundant or little used:</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td>Phones</td>
      <td><?php echo $row_rsMembers['phones']; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Send NA material</td>
      <td><?php echo $row_rsMembers['sendNAmaterial']; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Sent invoice</td>
      <td><?php echo $row_rsMembers['sentinvoice']; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Webpage school</td>
      <td><?php echo $row_rsMembers['webpageschool']; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><span class="smallText"><?php echo $row_rsMembers['mid']; ?></span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <input name="processed" type="hidden" id="processed" value="1" />
  <input type="hidden" name="MM_update" value="frmUpdate" />
</form>
</body>
</html>
<?php
mysql_free_result($rsMembers);

mysql_free_result($rsMemTypes);

mysql_free_result($rsTypes);

mysql_free_result($rsCountries);

mysql_free_result($rsNA);
?>
