<?php require_once('Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsMember = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMember = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMember = sprintf("SELECT mid, email, firstname, lastname FROM member WHERE mid = %s", GetSQLValueString($colname_rsMember, "int"));
$rsMember = mysql_query($query_rsMember, $connEABP2) or die(mysql_error());
$row_rsMember = mysql_fetch_assoc($rsMember);
$totalRows_rsMember = mysql_num_rows($rsMember);
?>
<?php require_once("webassist/email/mail_php.php"); ?>
<?php require_once("webassist/email/mailformatting_php.php"); ?>
<?php
if (!isset($_SESSION))session_start();
if ((($_SERVER["REQUEST_METHOD"] == "POST") && (isset($_SERVER["HTTP_REFERER"]) && strpos(urldecode($_SERVER["HTTP_REFERER"]), urldecode($_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"])) > 0) && isset($_POST)))     {
  //WA Universal Email object="mail"
  set_time_limit(0);
  $EmailRef = "waue_email-member_1";
  $BurstSize = 200;
  $BurstTime = 1;
  $WaitTime = 1;
  $GoToPage = "";
  $RecipArray = array();
  $StartBurst = time();
  $LoopCount = 0;
  $TotalEmails = 0;
  $RecipIndex = 0;
  // build up recipients array
  $CurIndex = sizeof($RecipArray);
  $RecipArray[$CurIndex] = array();
  $RecipArray[$CurIndex ][] = "".$row_rsMember['email']  ."";
  $TotalEmails += sizeof($RecipArray[$CurIndex]);
  $RealWait = ($WaitTime<0.25)?0.25:($WaitTime+0.1);
  $TimeTracker = Array();
  $TotalBursts = floor($TotalEmails/$BurstSize);
  $AfterBursts = $TotalEmails % $BurstSize;
  $TimeRemaining = ($TotalBursts * $BurstTime) + ($AfterBursts*$RealWait);
  if ($TimeRemaining < ($TotalEmails*$RealWait) )  {
    $TimeRemaining = $TotalEmails*$RealWait;
  }
  $_SESSION[$EmailRef."_Total"] = $TotalEmails;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = $TimeRemaining;
  while ($RecipIndex < sizeof($RecipArray))  {
    $EnteredValue = is_string($RecipArray[$RecipIndex][0]);
    $CurIndex = 0;
    while (($EnteredValue && $CurIndex < sizeof($RecipArray[$RecipIndex])) || (!$EnteredValue && $RecipArray[$RecipIndex][0])) {
      $starttime = microtime_float();
      if ($EnteredValue)  {
        $RecipientEmail = $RecipArray[$RecipIndex][$CurIndex];
      }  else  {
        $RecipientEmail = $RecipArray[$RecipIndex][0][$RecipArray[$RecipIndex][2]];
      }
      $EmailsRemaining = ($TotalEmails- $LoopCount);
      $BurstsRemaining = ceil(($EmailsRemaining-$AfterBursts)/$BurstSize);
      $IntoBurst = ($EmailsRemaining-$AfterBursts) % $BurstSize;
      if ($AfterBursts<$EmailsRemaining) $IntoBurst = 0;
      $TimeRemaining = ($BurstsRemaining * $BurstTime * 60) + ((($AfterBursts<$EmailsRemaining)?$AfterBursts:$EmailsRemaining)*$RealWait) - (($AfterBursts>$EmailsRemaining)?0:($IntoBurst*$RealWait));
      if ($TimeRemaining < ($EmailsRemaining*$RealWait) )  {
        $TimeRemaining = $EmailsRemaining*$RealWait;
      }
      $CurIndex ++;
      $LoopCount ++;
      session_commit();
      session_start();
      $_SESSION[$EmailRef."_Index"] = $LoopCount;
      $_SESSION[$EmailRef."_Remaining"] = round($TimeRemaining);
      session_commit();
      wa_sleep($WaitTime);
      include("webassist/email/waue_email-member_1.php");
      $endtime = microtime_float();
      $TimeTracker[] =$endtime - $starttime;
      $RealWait = array_sum($TimeTracker)/sizeof($TimeTracker);
      if ($LoopCount % $BurstSize == 0 && $CurIndex < sizeof($RecipArray[$RecipIndex]))  {
        $TimePassed = (time() - $StartBurst);
        if ($TimePassed < ($BurstTime*60))  {
          $WaitBurst = ($BurstTime*60) -$TimePassed;
          wa_sleep($WaitBurst);
        }
        else  {
          $TimeRemaining = ($TotalEmails- $LoopCount)*$RealWait;
        }
        $StartBurst = time();
      }
      if (!$EnteredValue)  {
        $RecipArray[$RecipIndex][0] =  mysql_fetch_assoc($RecipArray[$RecipIndex][1]);
      }
    }
    $RecipIndex ++;
  }
  $_SESSION[$EmailRef."_Total"] = 0;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = 0;
  session_commit();
  session_start();
  if ($GoToPage!="")     {
    header("Location: ".$GoToPage);
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Email an EABP member</title>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 100%;
}

.smallTxt {	font-size: 12px;
}
</style>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php if ($_POST['processed'] !=1) { ?>
<p><strong>Email <?php echo $row_rsMember['firstname']; ?> <?php echo $row_rsMember['lastname']; ?> </strong></p>
<form id="frmEmail" name="frmEmail" method="post" action="">
  <table border="0" cellpadding="3" cellspacing="0">
    <tr>
      <td align="right">Your name</td>
      <td><span id="sprytextfield1">
        <input name="name" type="text" id="name" size="35" />
        <span class="textfieldRequiredMsg">required</span></span></td>
</tr>
    <tr>
      <td align="right">Your email address</td>
      <td><span id="sprytextfield2">
        <input name="email" type="text" id="email" size="35" />
        <span class="textfieldRequiredMsg">required</span></span></td>
</tr>
    <tr>
      <td align="right">Your phone number</td>
      <td><input type="text" name="phone" id="phone" />
        optional</td>
    </tr>
    <tr>
      <td align="right">Subject</td>
      <td><span id="sprytextfield3">
        <input name="subject" type="text" id="subject" size="35" />
        <span class="textfieldRequiredMsg">required</span></span></td>
</tr>
    <tr>
      <td align="right" valign="top">Message</td>
      <td width="320"><span id="sprytextarea1">
        <textarea name="message" id="message" cols="28" rows="6"></textarea>
        <span class="textareaRequiredMsg">required</span></span></td>
</tr>
    <tr>
      <td><input name="processed" type="hidden" id="processed" value="1" /></td>
      <td><input type="submit" name="button" id="button" value="Send" /> 
      (you will receive a copy)</td>
    </tr>
    <tr>
      <td colspan="2" class="smallTxt">By clicking the send button above you confirm that you are not using this messaging service for advertising or marketing purposes.</td>
    </tr>
  </table>
  <table border="0" cellpadding="3" cellspacing="0">
    <tr> </tr>
    <tr> </tr>
    <tr> </tr>
    <tr> </tr>
  </table>
</form>
<?php } else { ?>
<p>Thank you. Your message has been sent to  <?php echo $row_rsMember['firstname']; ?> <?php echo $row_rsMember['lastname']; ?>.</p>
<?php } ?>
<p><a href="javascript:window.close()" class="close">Close</a></p>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
</script>
</body>
</html>
<?php
mysql_free_result($rsMember);
?>
