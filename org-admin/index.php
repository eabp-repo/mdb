<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsOrg = "-1";
if (isset($_GET['id'])) {
  $colname_rsOrg = $_GET['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrg = sprintf("SELECT orgname FROM allorgs WHERE id = %s", GetSQLValueString($colname_rsOrg, "int"));
$rsOrg = mysql_query($query_rsOrg, $connEABP2) or die(mysql_error());
$row_rsOrg = mysql_fetch_assoc($rsOrg);
$totalRows_rsOrg = mysql_num_rows($rsOrg);
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['username'])) {
  $loginUsername=$_POST['username'];
  $password=$_POST['password'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "edit.php";
  $MM_redirectLoginFailed = "failed.html";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_connEABP2, $connEABP2);
  
  $LoginRS__query=sprintf("SELECT username, password FROM allorgs WHERE username=%s AND password=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $connEABP2) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $loginStrGroup = "";
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP organisation login</title>
<link href="../sitecms/cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p>EABP organisation member </p>
<h1><?php echo $row_rsOrg['orgname']; ?></h1>
<h2>Login to edit the details of your organisation</h2>
<form ACTION="<?php echo $loginFormAction; ?>" METHOD="POST" id="frmLogin">
<table width="500" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td bgcolor="#CCCCCC" scope="col">Username: </td>
    <td bgcolor="#CCCCCC" scope="col"><input name="username" type="text" class="input400" id="username" /></td>
  </tr>
  <tr>
    <td bgcolor="#CCCCCC">Password:</td>
    <td bgcolor="#CCCCCC"><input name="password" type="password" class="input400" id="password" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="btnLogin" type="submit" class="btnAdd" id="btnLogin" value="Login" /></td>
  </tr>
</table>

</form>
</body>
</html>
<?php
mysql_free_result($rsOrg);
?>
