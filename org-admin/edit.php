<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE allorgs SET orgname=%s, logofile=%s, address=%s, postcode=%s, city=%s, contact=%s, contact2=%s, contact3=%s, telephone=%s, email=%s, email2=%s, email3=%s, fax=%s, website=%s, skype=%s, legalstructure=%s, typeid=%s, ethics=%s, trainers=%s, cid=%s, nid=%s, EABPreps=%s, username=%s, password=%s WHERE id=%s",
                       GetSQLValueString($_POST['orgname'], "text"),
                       GetSQLValueString($_POST['logofile'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['postcode'], "text"),
                       GetSQLValueString($_POST['city'], "text"),
                       GetSQLValueString($_POST['contact'], "text"),
                       GetSQLValueString($_POST['contact2'], "text"),
                       GetSQLValueString($_POST['contact3'], "text"),
                       GetSQLValueString($_POST['telephone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['email2'], "text"),
                       GetSQLValueString($_POST['email3'], "text"),
                       GetSQLValueString($_POST['fax'], "text"),
                       GetSQLValueString($_POST['website'], "text"),
                       GetSQLValueString($_POST['skype'], "text"),
                       GetSQLValueString($_POST['legalstructure'], "text"),
                       GetSQLValueString($_POST['typeid'], "int"),
                       GetSQLValueString($_POST['ethics'], "text"),
                       GetSQLValueString($_POST['trainers'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['nid'], "int"),
                       GetSQLValueString($_POST['EABPreps'], "text"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['password'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsType = "SELECT * FROM forumtypes WHERE forumtypeid <= 3";
$rsType = mysql_query($query_rsType, $connEABP2) or die(mysql_error());
$row_rsType = mysql_fetch_assoc($rsType);
$totalRows_rsType = mysql_num_rows($rsType);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNAs = "SELECT naid, acronym FROM `national` WHERE fullname <> '' OR naid = 12 ORDER BY acronym";
$rsNAs = mysql_query($query_rsNAs, $connEABP2) or die(mysql_error());
$row_rsNAs = mysql_fetch_assoc($rsNAs);
$totalRows_rsNAs = mysql_num_rows($rsNAs);

$colname_rsOrg = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsOrg = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrg = sprintf("SELECT * FROM allorgs WHERE username = %s", GetSQLValueString($colname_rsOrg, "text"));
$rsOrg = mysql_query($query_rsOrg, $connEABP2) or die(mysql_error());
$row_rsOrg = mysql_fetch_assoc($rsOrg);
$totalRows_rsOrg = mysql_num_rows($rsOrg);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>organisation member edit</title>
<link href="../sitecms/cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<style type="text/css">
input, select {
	background-color: #FFFFFF;
	width: 700px;
	font-size: 1em;
}
#logofile {
	width: 250px;
}
#form1 table {
	background-color: #EFEFEF;
}
.btnAdd {
	background-color: #CCFF99;
	border: 1px solid #006600;
	font-weight: normal;
	width: 120px;
	float: left;
	margin-top: 200px;
}
#tick {
	float: left;
	width: 80px;
}
</style>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="../index.php">EABP website</a> | <a href="logout.php">logout</a>.</p>
<hr />
<h1>Edit organisation member details</h1>
<form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
  <table align="left" cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Username:</td>
      <td bgcolor="#CCCCCC"><input name="username" type="text" class="input400" id="username" value="<?php echo $row_rsOrg['username']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Password:</td>
      <td bgcolor="#CCCCCC"><input name="password" type="text" class="input400" id="password" value="<?php echo $row_rsOrg['password']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Name:</td>
      <td><h2>
        <input name="orgname" type="text" value="<?php echo $row_rsOrg['orgname']; ?>" size="32" />
      </h2></td>
    </tr>
    <tr>
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Logo file:</td>
      <td bgcolor="#FFFFFF"><input name="logofile" type="file" id="logofile" value="<?php echo $row_rsOrg['logofile']; ?>" />
        <span class="smallText">
        <?php if ($row_rsOrg['logofile'] !="") { ?>
        <a href="../images/logos/<?php echo $row_rsOrg['logofile']; ?>" target="_blank">currently</a>
        <?php } else { ?>
        You do not have a logo showing on the site.
        <?php } ?>
        </span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Address:</td>
      <td><input name="address" type="text" value="<?php echo $row_rsOrg['address']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Post code: </td>
      <td bgcolor="#FFFFFF"><input name="postcode" type="text" class="input100" id="postcode" value="<?php echo $row_rsOrg['postcode']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">City:</td>
      <td><input name="city" type="text" class="input400" value="<?php echo $row_rsOrg['city']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Country:</td>
      <td bgcolor="#FFFFFF"><select name="cid" class="input100">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsOrg['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
      </select>      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact:</td>
      <td><input name="contact" type="text" value="<?php echo $row_rsOrg['contact']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Contact 2:</td>
      <td bgcolor="#FFFFFF"><input name="contact2" type="text" id="contact2" value="<?php echo $row_rsOrg['contact2']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact 3:</td>
      <td><input name="contact3" type="text" id="contact3" value="<?php echo $row_rsOrg['contact3']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Telephone:</td>
      <td bgcolor="#FFFFFF"><input name="telephone" type="text" value="<?php echo $row_rsOrg['telephone']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email:</td>
      <td><input name="email" type="text" value="<?php echo $row_rsOrg['email']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Email2:</td>
      <td bgcolor="#FFFFFF"><input name="email2" type="text" value="<?php echo $row_rsOrg['email2']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email3:</td>
      <td><input name="email3" type="text" value="<?php echo $row_rsOrg['email3']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Fax:</td>
      <td bgcolor="#FFFFFF"><input name="fax" type="text" class="input400" value="<?php echo $row_rsOrg['fax']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Website:</td>
      <td><input name="website" type="text" class="input400" value="<?php echo $row_rsOrg['website']; ?>" size="32" /> 
        NO http://</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Skype:</td>
      <td valign="baseline" bgcolor="#FFFFFF"><input name="skype" type="text" class="input400" id="skype" value="<?php echo $row_rsOrg['skype']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Legal structure:</td>
      <td><input name="legalstructure" type="text" value="<?php echo $row_rsOrg['legalstructure']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Type:</td>
      <td bgcolor="#FFFFFF"><select name="typeid" class="input400">
        <?php
do {  
?>
        <option value="<?php echo $row_rsType['forumtypeid']?>"<?php if (!(strcmp($row_rsType['forumtypeid'], $row_rsOrg['typeid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsType['forumtype']?></option>
        <?php
} while ($row_rsType = mysql_fetch_assoc($rsType));
  $rows = mysql_num_rows($rsType);
  if($rows > 0) {
      mysql_data_seek($rsType, 0);
	  $row_rsType = mysql_fetch_assoc($rsType);
  }
?>
      </select>      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Ethics module:</td>
      <td><input name="ethics" type="text" value="<?php echo $row_rsOrg['ethics']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Number trainers:</td>
      <td bgcolor="#FFFFFF"><input name="trainers" type="text" value="<?php echo $row_rsOrg['numbertrainees']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">EABP reps:</td>
      <td><input name="EABPreps" type="text" class="input500" value="<?php echo $row_rsOrg['EABPreps']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">National Association:</td>
      <td bgcolor="#FFFFFF"><select name="nid" class="input400" id="nid">
        <option value="0" <?php if (!(strcmp(0, $row_rsOrg['nid']))) {echo "selected=\"selected\"";} ?>>Please choose:</option>
        <?php
do {  
?>
<option value="<?php echo $row_rsNAs['naid']?>"<?php if (!(strcmp($row_rsNAs['naid'], $row_rsOrg['nid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsNAs['acronym']?></option>
        <?php
} while ($row_rsNAs = mysql_fetch_assoc($rsNAs));
  $rows = mysql_num_rows($rsNAs);
  if($rows > 0) {
      mysql_data_seek($rsNAs, 0);
	  $row_rsNAs = mysql_fetch_assoc($rsNAs);
  }
?>
      </select></td>
    </tr>
  </table>
    <?php if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) { ?>
  <img src="../images/icon_tick.png" alt="" name="tick" width="80" height="80" id="tick" />
  <?php } ?>  
  <input type="submit" class="btnAdd" value="Update" />

  <input name="id" type="hidden" id="id" value="<?php echo $row_rsOrg['id']; ?>" />
  <input type="hidden" name="MM_update" value="form1" />  
</form>
<hr class="clear" />
</body>
</html>
<?php
mysql_free_result($rsType);

mysql_free_result($rsCountries);

mysql_free_result($rsNAs);

mysql_free_result($rsOrg);
?>
