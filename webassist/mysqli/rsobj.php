<?php
// version 2.18
class WA_MySQLi_RS  {
	public function __construct($name,$conn,$maxRows=0) {
	  $this->Connection = $conn;
	  $this->CurrentPage = $_SERVER['PHP_SELF'];
	  $this->Debug = true;
	  $this->FilterValues = array();
	  $this->Index = 0;
	  $this->LastRow = 0;
	  $this->MaxRows = $maxRows;
	  $this->Name = $name;
	  $this->NextPage = 0;
	  $this->PageNum = isset($_GET['pageNum_'.$name])?$_GET['pageNum_'.$name]:0;
	  $this->ParamTypes = array();
	  $this->ParamValues= array();
	  $this->ParamDefaults= array();
	  $this->PrevPage = max(0, $this->PageNum - 1);
	  $this->QueryString = "";
	  $this->Results = array();
	  $this->StartLimit = $this->PageNum * $this->MaxRows;
	  $this->StartRow = 0;
	  $this->Statement = "";
	  $this->Table = "";
	  $this->TotalPages = 0;
	  $this->TotalRows = 0;
	}
	
	public function addFilter($filterColumn, $filterComparison, $filterType, $filterValue, $filterJoin = "AND") {
      $this->FilterValues[] = array($filterColumn, $filterComparison, $filterType, $filterValue);
	}
	
	public function addRow($row) {
	  $this->Results[] = $row;
	}
	
	public function atEnd() {
	  return $this->Index == sizeof($this->Results); 
	}
	
	public function bindParam($paramType,$paramValue,$paramDefault="",$paramPosition=false) {
	  if ($paramValue === 0 || $paramValue === NULL) $paramValue = $paramDefault;
	  $paramArray = array($paramValue);
	  $isList = false;
	  if (strpos($paramType,"l")) {
		$paramType = substr($paramType,0,1);
		$paramArray = preg_split("/\s*\,\s*/", $paramValue);
		$isList = true;
	  }
	  for ($x=0; $x<sizeof($paramArray); $x++) {
		$paramValue = $paramArray[$x];
		if ($paramValue === "") {
		  switch ($paramDefault) {
			case "WA_BLANK":
			case "WA_DEFAULT":
			  $paramValue = "";
			  break;
			case "WA_NULL":
			  $paramValue = null;
			  break;
			case "WA_CURRENT_TIMESTAMP":
			  $paramValue = date("Y-m-d H:i:s");
			  break;
			case "WA_ZERO":
			  $paramValue = "0";
			  break;
			case "WA_NO":
			  $paramValue = "N";
			  break;
			default:
			  $paramValue = $paramDefault;
		  }
		}
		if ($paramType == "t") {
		  $paramType = "s";
		  if ($paramValue) {
		    $hasTime = strpos($paramValue," ") !== false;
		    $paramValue = strtotime($paramValue);
		    if ($hasTime) {
			  $paramValue = date('Y-m-d H:i:s',$paramValue);
		    } else {
			  $paramValue = date('Y-m-d',$paramValue);
		    }
		  }
		} else if ($paramType == "c") {
		  $paramType = "s";
		  $paramValue = "%" . $paramValue . "%";
		} else if ($paramType == "b") {
		  $paramType = "s";
		  $paramValue = $paramValue . "%";
		} else if ($paramType == "e") {
		  $paramType = "s";
		  $paramValue = "%" . $paramValue;
		} 
		if (($isList || sizeof($paramArray) > 1) && $x == 0) {
		  $sqlParts = explode("?",$this->Statement);
		  if (!preg_match("/\(\s*$/",$sqlParts[sizeof($this->ParamValues)]) && !preg_match("/^\s*\)/",$sqlParts[sizeof($this->ParamValues)+1])) {
			$sqlParts[sizeof($this->ParamValues)] =  $sqlParts[sizeof($this->ParamValues)] . "(";
			$sqlParts[sizeof($this->ParamValues)+1] = ")" . $sqlParts[sizeof($this->ParamValues)+1];
		  }
		  $this->Statement = implode("?",$sqlParts);
		}
		if ($x>0) {
		  $sqlParts = explode("?",$this->Statement);
		  $sqlParts[sizeof($this->ParamValues)] = ", ?" . $sqlParts[sizeof($this->ParamValues)];
		  $this->Statement = implode("?",$sqlParts);
		}
		if ($paramPosition == false) {
		  $this->ParamTypes[] = $paramType;	
		  $this->ParamValues[] = $paramValue;
		  $this->ParamDefaults[] = $paramDefault;
		} else {
		  array_splice($this->ParamTypes, $paramPosition, 0, $paramType);
		  array_splice($this->ParamValues, $paramPosition, 0, $paramValue);
		  array_splice($this->ParamDefaults, $paramPosition, 0, $paramDefault);
		}
	  }
	}
	
	public function debugSQL() {
	  $debugStatement = $this->Statement;
	  $paramTypes = $this->ParamTypes;
	  for ($x=0; $x<sizeof($this->ParamValues); $x++) {
	    $pos = strpos($debugStatement,"?");
		$thisType = $paramTypes[$x];
        if ($pos !== false) {
          $debugStatement = substr_replace($debugStatement,(($thisType=="s")?"'":"") . $this->ParamValues[$x] . (($thisType=="s")?"'":""),$pos,strlen("?"));
        }
	  }
	  return $debugStatement;
	}
	
	public function execute() {
	  if (!$this->Statement && $this->Table) {
		 $this->Statement = "SELECT * FROM " . $this->Table; 
	  }
	  $this->setFilter();
	  $this->setSort();
	  $this->setSearch();
	  $statement = $this->Statement;
	  if ($this->MaxRows && !isset($_GET['totalRows_'.$this->Name])) {
	    $statement = preg_replace('/^select /i',"SELECT SQL_CALC_FOUND_ROWS ",$statement);
	  }
	  if ($this->MaxRows) $statement .= " LIMIT ".$this->StartLimit.",".$this->MaxRows;
	  $query = $this->Connection->Prepare($statement);
	  if ($query == false) {
		if ($this->Debug) {
		  die($this->Statement . "<BR><BR>" . mysqli_error($this->Connection));
		} else {
		  die("There is an error in your SQL syntax.");
		}
	  }
	  $rsParams = $this->getParams();
	  if ($rsParams[0]) call_user_func_array(array($query, "bind_param"),$this->paramRefs($rsParams));
	  $query->execute();
	  if (method_exists($query,'get_result')) {
        $result = $query->get_result();
        while ($rows = $result->fetch_array(MYSQLI_ASSOC)) {
          $this->addRow($rows);
        }
	  } else {
	    $result = $this->waMySQLiStmtGetResult($query);
	    while ($rows = $this->waMySQLiResultFetchAssoc($result)) {
		  $this->addRow($rows);
	    }
	  }
	  if ($this->MaxRows) {
		if (isset($_GET['totalRows_'.$this->Name])) {
		  $this->TotalRows = intval($_GET['totalRows_'.$this->Name]);
		} else {
		  $stmt = $this->Connection->prepare("SELECT FOUND_ROWS()");
		  $stmt->execute();
		  $stmt->bind_result($num);
		  $stmt->fetch();
		  $this->TotalRows = $num;
		  $stmt->close();
		}
		$this->TotalPages = ceil($this->TotalRows/$this->MaxRows)-1;
	  } else {
        $this->TotalRows = sizeof($this->Results);
	  }
	  if ($this->TotalRows < $this->StartRow) {
		  $this->StartRow = $this->TotalRows;
	  } else if ($this->TotalRows > 0) {
		  $this->StartRow = ($this->PageNum * $this->MaxRows) + 1;
	  }
	  $this->LastRow = min($this->StartRow + $this->MaxRows - 1, $this->TotalRows);
	  if ($this->MaxRows == 0) $this->LastRow = $this->TotalRows;
	  $this->NextPage = min($this->TotalPages, $this->PageNum + 1); 
	  $this->setQueryString();
      $query->close();
	}
	
	public function findRow($column,$value) {
	  for ($x=0; $x<sizeof($this->Results); $x++) {
		if ($this->Results[$x][$col] == $val) {
			$this->Index = $x;
			return true;
		}
	  }
	  return false;
	}
	
	public function getColumnVal($col) {
	  $colVal = "";
	  if (isset($this->Results[$this->Index]) && isset($this->Results[$this->Index][$col])) $colVal = $this->Results[$this->Index][$col];
	  return $colVal;
	}
	
	public function getFirstPageLink() {
	  return $this->CurrentPage . "?pageNum_" . $this->Name . "=0" . $this->QueryString;	
	}
	
	public function getLastPageLink() {
	  return $this->CurrentPage . "?pageNum_" . $this->Name . "=" . $this->TotalPages . $this->QueryString;	
	}
	
	public function getNextPageLink() {
	  return $this->CurrentPage . "?pageNum_" . $this->Name . "=" . $this->NextPage . $this->QueryString;
	}
	
	private function getParams() {
	  $useParams = array();
	  $useTypes = array();
	  for ($x=0; $x<sizeof($this->ParamValues); $x++) {
		$paramVal = $this->ParamValues[$x];
		if (!(($paramVal === "" || $paramVal === null) && ($this->ParamDefaults[$x] == "WA_DEFAULT" || $this->ParamDefaults[$x] == "WA_IGNORE"))) {
		  $useParams[] = $paramVal;
		  $useTypes[] = $this->ParamTypes[$x];
		}
	  }
	  return array_merge(array(implode("",$useTypes)), $useParams);
	}
	
	public function getPrevPageLink() {	  	
	  return $this->CurrentPage . "?pageNum_" . $this->Name . "=" . $this->PrevPage . $this->QueryString;
	}
	
	private function getSQLPiece($thePiece) {
	  $pieces = array("SELECT","FROM","WHERE","GROUP BY","HAVING","ORDER BY","HAVING","LIMIT");
	  $sqlBefore = "";
	  $sqlMatch = "";
	  $sqlAfter = $this->Statement;
	  $searchAfter = $sqlAfter;
	  $endSearch = false;
	  for ($x=0; $x<sizeof($pieces); $x++) {
		$pattern = "/[\s\(]".(($x!=0)?"":"?").$pieces[$x]."[\s\)].*/i";
		$allMatches = array();
		$allSearch = $searchAfter;
		while ($match = preg_match($pattern,$allSearch,$found)) {
		  $allMatches = array_merge($allMatches, $found);
		  $allSearch = substr($allSearch, strpos($allSearch,trim($found[0]))+1);
		}
	    if (sizeof($allMatches)>0) {
		  $matchIndex = 0;
		  if ($pieces[$x] != "SELECT") $matchIndex = sizeof($allMatches)-1;
		  if ($endSearch === false || $x < $endSearch) {
			$sqlBefore .= substr($sqlAfter,0,strpos($sqlAfter,trim($allMatches[$matchIndex])));
		    $sqlAfter = substr($sqlAfter,strpos($sqlAfter,trim($allMatches[$matchIndex])));
		  }
		  $searchAfter =  substr($sqlAfter,strpos($sqlAfter,trim($allMatches[$matchIndex])));
		  if ($thePiece == $pieces[$x]) {
		    $sqlMatch = $sqlAfter;
			$endSearch = $x;
		  }
		  if ($endSearch!== false && $endSearch < $x) {
		    if ($sqlMatch) {
			  $sqlAfter = substr($sqlMatch,strpos($sqlMatch,$allMatches[$matchIndex]));
		      $sqlMatch = substr($sqlMatch,0,strpos($sqlMatch,$allMatches[$matchIndex]));
			} else {
			  $sqlBefore .= substr($sqlAfter,0,strpos($sqlAfter,trim($allMatches[$matchIndex])));
		      $sqlAfter = substr($sqlAfter,strpos($sqlAfter,trim($allMatches[$matchIndex])));
			}
			return array($sqlBefore,$sqlMatch,$sqlAfter);
		  }
		} else {
		  if ($thePiece == $pieces[$x]) {
			$endSearch = $x;
		  }
		}
	  }
	  if ($endSearch !== false) {
		if ($sqlMatch) {
		  $sqlAfter = ""; 
		} else {
		  $sqlBefore .= $sqlAfter;
		  $sqlAfter = ""; 
		}
	  }
	  return array($sqlBefore,$sqlMatch,$sqlAfter);
	}
	
	public function inResult($col,$val) {
	  for ($x=0; $x<sizeof($this->Results); $x++) {
		if ($this->Results[$x][$col] == $val) return true;
	  }
	  return false;
	}
	
	public function moveFirst() {
	  $this->Index = 0;
	}
	
	public function moveNext() {
	  $this->Index++;
	  if (sizeof($this->Results) > $this->Index) {
	    return true;
	  }
	  $this->Index = sizeof($this->Results);
	  return false;
	}
	
	public function movePrevious() {
	  if ($this->Index>0) {
	    $this->Index--;
	    return true;
	  }
	  return false;
	}
	
	public function paramRefs($arr) {
	  if (strnatcmp(phpversion(),'5.3') >= 0)
	  {
		$refs = array();
		foreach($arr as $key => $value)
		  $refs[$key] = &$arr[$key];
		return $refs;
	  }
	  return $arr;
	}
	
	public function setFilter() {
	  if (sizeof($this->FilterValues) > 0) {
		$addFilter = "";
		$pieces = $this->getSQLPiece("WHERE");
        for ($x=0; $x<sizeof($this->FilterValues); $x++) {
		  if (!is_array($this->FilterValues[$x][3])) {
			$filterValues = array($this->FilterValues[$x][3]);
		  } else {
			$filterValues = $this->FilterValues[$x][3];
		  }
		  for ($y=0; $y<sizeof($filterValues); $y++) {
			if ($x>0 && $y==0) $addFilter .= " AND ";
		    if (sizeof($filterValues)>1 && $y==0) $addFilter .= "(";
			if ($y>0) $addFilter .= ($this->FilterValues[$x][1] == "<>" || strtoupper($this->FilterValues[$x][1]) == "IS NOT")?" AND ":" OR ";
			if (sizeof($this->FilterValues[$x]) > 4) $this->RepeatedParams[] = array(sizeof($this->ParamValues),$this->FilterValues[$x][4],true);
			$addFilter .= $this->FilterValues[$x][0] . " " . $this->FilterValues[$x][1] . " ?";
			$this->bindParam($this->FilterValues[$x][2], strval($filterValues[$y]), "");
		  }
		  if (sizeof($filterValues)>1) $addFilter .= ")";
		}
        $this->Statement = $pieces[0] . ($pieces[1]? $pieces[1] . " AND ":" WHERE ") . $addFilter . $pieces[2];
	  }
	}
	
	public function setQuery($statement) {
	  $this->Statement = $statement;	
	}
	
	private function setQueryString() {
	  $queryString = "";
	  if (!empty($_SERVER['QUERY_STRING'])) {
		$params = explode("&", $_SERVER['QUERY_STRING']);
		$newParams = array();
		foreach ($params as $param) {
		  if (stristr($param, "pageNum_".$this->Name) == false && 
			  stristr($param, "totalRows_".$this->Name) == false) {
			array_push($newParams, $param);
		  }
		}
		if (count($newParams) != 0) {
		  $queryString = "&" . htmlentities(implode("&", $newParams));
		}
	  }
	  $queryString = sprintf("&totalRows_".$this->Name."=%d%s", $this->TotalRows, $queryString);
	  $this->QueryString = $queryString;
	}
	
	public function setSearch() {
	  $search = "";
	  if (isset($GLOBALS['WA_RSSearch'])) {
		for ($x=sizeof($GLOBALS['WA_RSSearch'])-1; $x>=0; $x--) {
		  if ($GLOBALS['WA_RSSearch'][$x]->Recordset == $this->Name) {
			$searchVals = $GLOBALS['WA_RSSearch'][$x]->getSearch();
			$addSearch = $searchVals[0];
			if ($addSearch) {
		      $pieces = $this->getSQLPiece("WHERE");
			  $paramPos = sizeof($this->ParamValues) - substr_count($pieces[2],"?");
			  for ($y=0; $y<sizeof($searchVals[1]); $y++) {
			    $this->bindParam($searchVals[1][$y][0],$searchVals[1][$y][1],"",$paramPos+$y);
			  }
              $this->Statement = $pieces[0] . ($pieces[1]? $pieces[1] . " AND ":" WHERE ") . $addSearch . " " . $pieces[2];
			}
		  }
		}
	  }
	  if ($search) {
		$pieces = $this->getSQLPiece("WHERE");
	    $this->Statement = trim($pieces[0]) . " WHERE " . $search . " " . trim($pieces[2]);
	  }
	}
	
	public function setSort() {
	  $sort = "";
	  if (isset($GLOBALS['WA_RSSorts'])) {
		for ($x=sizeof($GLOBALS['WA_RSSorts'])-1; $x>=0; $x--) {
		  if ($GLOBALS['WA_RSSorts'][$x]->Recordset == $this->Name) {
			$sort = $GLOBALS['WA_RSSorts'][$x]->getSort();
		    break;
		  }
		}
	  }
	  if ($sort) {
		$pieces = $this->getSQLPiece("ORDER BY");
	    $this->Statement = trim($pieces[0]) . " ORDER BY " . $sort . " " . trim($pieces[2]);
	  }
	}
	
	private function waMySQLiResultFetchAssoc(&$result) {
	  $ret = array();
	  $code = "mysqli_stmt_store_result(\$result->stmt); return mysqli_stmt_bind_result(\$result->stmt ";
	  for ($i=0; $i<$result->nCols; $i++) {
		$ret[$result->fields[$i]->name] = NULL;
		$code .= ", \$ret['" .$result->fields[$i]->name ."']";
	  };
	  $code .= ");";
	  if (!eval($code)) { return NULL; };
	  if (!mysqli_stmt_fetch($result->stmt)) { return NULL; };
	  return $ret;
	}
	
	private function waMySQLiStmtGetResult($stmt)  {
	  $metadata = mysqli_stmt_result_metadata($stmt);
	  $ret = (object) array('nCols'=>'0', 'fields'=>array(), 'stmt'=>'');
	  $ret->nCols = mysqli_num_fields($metadata);
	  $ret->fields = mysqli_fetch_fields($metadata);
	  $ret->stmt = $stmt;
	  mysqli_free_result($metadata);
	  return $ret;
	}
	
}
class WA_MySQLi_Sort  {
	public function __construct($recordset,$default="") {
	  if (!isset($GLOBALS['WA_RSSorts'])) $GLOBALS['WA_RSSorts'] = array();
	  $GLOBALS['WA_RSSorts'][] = $this;
	  $this->Recordset = $recordset;
	  $this->Default = $default;
	  $this->SortArray = isset($_SESSION["WASort_".$this->Recordset])?$_SESSION["WASort_".$this->Recordset]:array();
	}
	
	public function clearSort() {
	  $this->SortArray = array();	
	  $this->saveInSession();
	}
	
	public function getSort() {
	  $sort = "";
	  for ($x=0; $x<sizeof($this->SortArray); $x++) {
		if ($x!=0) $sort .= ",";
		$sort .= ' ' . $this->SortArray[$x][0] . " " . $this->SortArray[$x][1];
	  }
	  if (!$sort) {
		 $sort = $this->Default;
		 if (strpos(strtoupper($sort),"ORDER BY ") !== false) {
			$sort = substr($sort,strpos(strtoupper($sort),"ORDER BY ")+9); 
		 }
	  }
	  return $sort;
	}
	
	public function saveInSession() {
	  @session_start();
	  $_SESSION["WASort_".$this->Recordset] = $this->SortArray;
	}
	
	public function setSort($column,$direction="ASC",$toggle=true,$clear=true) {
	  $direction = strtoupper($direction);
	  $found = false;
	  for ($x=0; $x<sizeof($this->SortArray); $x++) {
		if ($this->SortArray[$x][0] == $column) {
		  $found = true;
		  if (strtoupper($this->SortArray[$x][1]) == $direction) {
			if ($toggle == true) {
			  array_splice($this->SortArray,$x,1);
		      array_unshift($this->SortArray, array($column,($direction=="ASC")?"DESC":"ASC"));
			} else {
			  if ($clear == true) {
				array_splice($this->SortArray,$x,1);  
			  }
			}
		  } else {
			if ($clear == true) {
			  array_splice($this->SortArray,$x,1);  
			} else {
			  array_splice($this->SortArray,$x,1); 
		      array_unshift($this->SortArray, $direction);
			}
		  }
		}
	  }
	  if (!$found) {
		array_unshift($this->SortArray, array($column,$direction));
	  }
	  $this->saveInSession();
	}
}

class WA_MySQLi_Search  {
	public function __construct($recordset,$default="") {
	  if (!isset($GLOBALS['WA_RSSearch'])) $GLOBALS['WA_RSSearch'] = array();
	  $GLOBALS['WA_RSSearch'][] = $this;
	  $this->Recordset = $recordset;
	  $this->Default = $default;
	  $this->SearchArray = isset($_SESSION["WASearch_".$this->Recordset])?$_SESSION["WASearch_".$this->Recordset]:array();
	}
	
	public function clearSearch() {
	  $this->SearchArray = array();	
	  $this->saveInSession();
	}
	
	public function getSearch() {
	  $searchStr = "";
	  $searchParams = array();
	  for ($x=0; $x<sizeof($this->SearchArray); $x++) {
		$searchType = strtolower((isset($this->SearchArray[$x][0]["type"]) && $this->SearchArray[$x][0]["type"])?$this->SearchArray[$x][0]["type"]:"v");
		$searchColType = $this->SearchArray[$x][2];
		$searchJoin = strtolower((isset($this->SearchArray[$x][0]["join"]) && $this->SearchArray[$x][0]["join"])?$this->SearchArray[$x][0]["join"]:"AND");
		$searchValues = (isset($this->SearchArray[$x][3]) && $this->SearchArray[$x][3])?$this->SearchArray[$x][3]:"";
		$searchComparison = (isset($this->SearchArray[$x][0]["comparison"]) && $this->SearchArray[$x][0]["comparison"])?$this->SearchArray[$x][0]["comparison"]:"=";
		$searchColumns = $this->SearchArray[$x][1];
		if (!is_array($searchValues)) $searchValues = array($searchValues);
		$newGroup = true;
		for ($s=0; $s<sizeof($searchValues); $s++) {
		  $searchValue = $searchValues[$s];
		  if (strpos($searchType ,"e") === 0 || strpos($searchType ,"l") === 0) {
			if (isset($_POST[$searchValue])) {
			  $searchValue = $_POST[$searchValue];
			} else if (isset($_GET[$searchValue])) {
			  $searchValue = $_GET[$searchValue];
			} else {
			  $searchValue = "";	
			}
		  }
		  if (strpos($searchType ,"c") === 0) {
			if (isset($_POST[$searchValue]) || isset($_GET[$searchValue])) {
			  if (isset($_POST[$searchValue])) $searchValue = $_POST[$searchValue];
			  if (isset($_POST[$searchValue])) $searchValue = $_GET[$searchValue];
			  if (isset($this->SearchArray[$x][0]["checked_value"])) $searchValue = $this->SearchArray[$x][0]["checked_value"];
			} else {
			  if (isset($this->SearchArray[$x][0]["unchecked_value"])) {
				  $searchValue = $this->SearchArray[$x][0]["unchecked_value"];
			  } else {
				  $searchValue = "";
			  }
			  if (isset($this->SearchArray[$x][0]["unchecked_comparison"])) $searchComparison = $this->SearchArray[$x][0]["unchecked_comparison"];
			}
		  }
		  if (is_array($searchValue)) {
			for ($y=0; $y<sizeof($searchValue); $y++) {
			  if ($searchValue[$y] !== "") {
				$hasSearch = true;
				break; 
			  }
			}
		  } else {
			$hasSearch = ($searchValue !== "");
		  }
		  if ($hasSearch) {
			if ($newGroup == true) {
			  if (sizeof($searchValues) > 1) {
			    if ($searchStr != "") {
				  $searchStr .= " " . $searchJoin . " (";
				} else {
				  $searchStr .= "(";
				}
			  } else {
			    if ($searchStr != "") $searchStr .= " " . $searchJoin . " ";
			  }	
			  $newGroup = false;
			} else {
			  if (strlen($searchStr) > 4 && substr($searchStr, -4) != " OR ")  $searchStr .= " OR ";
			}
			if (strpos($searchType ,"k") === 0) {
			  $valueArray = array();
			  $valueArray[] = array($searchValue,"a");
			  $keyAnd = ((isset($this->SearchArray[$x][0]["and"]) && $this->SearchArray[$x][0]["and"])?$this->SearchArray[$x][0]["and"]:"");
			  $keyOr = ((isset($this->SearchArray[$x][0]["or"]) && $this->SearchArray[$x][0]["or"])?$this->SearchArray[$x][0]["or"]:"");
			  $keyStartEnc = ((isset($this->SearchArray[$x][0]["start_encap"]) && $this->SearchArray[$x][0]["start_encap"])?$this->SearchArray[$x][0]["start_encap"]:"");
			  $keyEndEnc = ((isset($this->SearchArray[$x][0]["end_encap"]) && $this->SearchArray[$x][0]["or"])?$this->SearchArray[$x][0]["end_encap"]:"");
			  if ($keyStartEnc !== "" && $keyEndEnc !== "") {
				for ($z=0; $z<sizeof($valueArray); $z++) {
				  $thisVal = $valueArray[$z][0];
				  if (strpos($thisVal,$keyStartEnc) !== false && strpos($thisVal,$keyEndEnc) !== false) {
					$beforeStart = substr($thisVal,0,strpos($thisVal,$keyStartEnc));
					$afterStart = substr($thisVal,strpos($thisVal,$keyStartEnc)+strlen($keyStartEnc));
					if (strpos($afterStart,$keyEndEnc) !== false) {
					  $exactMatch = substr($afterStart,0,strpos($afterStart,$keyEndEnc));
					  $afterStart = substr($afterStart,strpos($afterStart,$keyEndEnc)+strlen($keyEndEnc));
					  
					  if ($beforeStart) {
						$valueArray[$z][0] = $beforeStart;
						$valueArray[] = array($exactMatch,"e");
					  } else {
						$valueArray[$z][0] = $exactMatch;
						$valueArray[$z][1] = "e";
					  }
					  if ($afterStart) $valueArray[] = array($afterStart,"a");
					  $z++;
					}
				  }
				}
			  }
			  $orFirst = false;
			  if ($keyAnd !== "") {
				for ($z=0; $z<sizeof($valueArray); $z++) {
				  if (strpos($keyOr,$keyAnd) !== false) {
					$orFirst = true;
					break;
				  }
				  if ($valueArray[$z][1] == "e") continue;
				  $thisVal = $valueArray[$z][0];
				  if (strpos($thisVal,$keyAnd) !== false) {
					$beforeAnd = substr($thisVal,0,strpos($thisVal,$keyAnd));
					$afterAnd = substr($thisVal,strpos($thisVal,$keyAnd)+strlen($keyAnd));
					$valueArray[$z][0] = $beforeAnd;
					array_splice($valueArray, $z+1, 0, array(array($afterAnd,"a")));
				  }
				}
			  }
			  if ($keyOr !== "") {
				for ($z=0; $z<sizeof($valueArray); $z++) {
				  if ($valueArray[$z][1] == "e") continue;
				  $thisVal = $valueArray[$z][0];
				  if (strpos($thisVal,$keyOr) !== false) {
					$beforeOr = substr($thisVal,0,strpos($thisVal,$keyOr));
					$afterOr = substr($thisVal,strpos($thisVal,$keyOr)+strlen($keyOr));
					$valueArray[$z][0] = $beforeOr;
					array_splice($valueArray, $z+1, 0, array(array($afterOr,"o")));
				  }
				}
			  }
			  if ($keyAnd !== "" && $orFirst == true) {
				for ($z=0; $z<sizeof($valueArray); $z++) {
				  if ($valueArray[$z][1] == "e") continue;
				  $thisVal = $valueArray[$z][0];
				  if (strpos($thisVal,$keyAnd) !== false) {
					$beforeAnd = substr($thisVal,0,strpos($thisVal,$keyAnd));
					$afterAnd = substr($thisVal,strpos($thisVal,$keyAnd)+strlen($keyAnd));
					$valueArray[$z][0] = $beforeAnd;
					array_splice($valueArray, $z+1, 0, array(array($afterAnd,"a")));
				  }
				}
			  }
			  $searchValue = $valueArray;
			}
			$searchStr .= "(";
			for ($y=0; $y<sizeof($searchColumns); $y++) {
			  if ($y!=0) $searchStr .= " OR ";
			  if (is_array($searchValue)) {
				$searchStr .= "(";
				for ($z=0; $z<sizeof($searchValue); $z++) {
				  if (is_array($searchValue[$z])) {
					$strValue = $searchValue[$z][0];
					$strJoin = ($searchValue[$z][1] == "a")?" AND ":" OR ";
				  } else {
					$strValue = $searchValue[$z];
					$strJoin = " OR ";
				  }
				  if ($z!=0) $searchStr .= $strJoin;
				  if ($searchColType == "t") {
					$searchStr .= "date(`".str_replace(".","`.`",$searchColumns[$y])."`)" . " " . $searchComparison . " ?";
				  } else {
					$searchStr .= "`".str_replace(".","`.`",$searchColumns[$y])."`" . " " . $searchComparison . " ?";
				  }
				  $searchParams[] = array($searchColType,$strValue);
				}
				$searchStr .= ")";
			  } else {
				if ($searchColType == "t") {
				  $searchStr .= "date(`".str_replace(".","`.`",$searchColumns[$y])."`)" . " " . $searchComparison . " ?";
				} else {
				  $searchStr .= "`".str_replace(".","`.`",$searchColumns[$y])."`" . " " . $searchComparison . " ?";
				}
				
		        $hasTime = strpos($searchValue," ") !== false;
				if (!$hasTime) {
				  	if (strpos($searchComparison,"<")!==false && strpos($searchComparison,">")===false) {
						$searchValue .= " 23:59:59";
					} else if (strpos($searchComparison,">")!==false && strpos($searchComparison,"<")===false) {
						$searchValue .= " 00:00:00";
					}
				}
				
				$searchParams[] = array($searchColType,$searchValue);
			  }
			}
			$searchStr .= ")";
		  }
		}
		if ($newGroup == false && sizeof($searchValues) > 1) $searchStr .= ")";
	  }
	  return array($searchStr,$searchParams);
	}
	
	public function saveInSession() {
	  @session_start();
	  $_SESSION["WASearch_".$this->Recordset] = $this->SearchArray;
	}
	
	public function setSearch($options,$columns,$datatype,$value,$repeat=false) {
	  $searchType = strtolower((isset($options["type"]) && $options["type"])?$options["type"]:"v");
	  if ($repeat)  {
		$value = array();
		$repeatIndex = 1;
		while (isset($_POST['WA_RepeatID' . $repeatIndex]) || isset($_GET['WA_RepeatID' . $repeatIndex])) {
		  if (strpos($searchType ,"e") === 0 || strpos($searchType ,"l") === 0 || strpos($searchType ,"c") === 0) $searchValues[] = $repeat.$repeatIndex;
		  else if (isset($_POST['WA_RepeatID' . $repeatIndex])) {
			$value[] = $_POST[$repeat.$repeatIndex];
		  } else {
			$value[] = $_GET[$repeat.$repeatIndex];
		  }
		  $repeatIndex++;
		}
	  }
	  array_push($this->SearchArray, array($options,$columns,$datatype,$value));
	  $this->saveInSession();
	}
}
?>