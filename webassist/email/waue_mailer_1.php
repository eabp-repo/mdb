<?php
$MailAttachments = "";
$MailBCC         = "";
$MailCC          = "";
$MailTo          = "";
$MailBodyFormat  = "";
$MailBody        = "";
$MailImportance  = "";
$MailFrom        = "secretariat@eabp.org";
$MailSubject     = "EABP Membership Fee for 2013 - Invoice";
$_SERVER["QUERY_STRING"] = "";

//Global Variables

  $WA_MailObject = WAUE_Definition("localhost","","","","","");

if ($RecipientEmail)     {
  $WA_MailObject = WAUE_AddRecipient($WA_MailObject,$RecipientEmail);
}
else      {
  //To Entries
}

//Additional Headers

//Attachment Entries

//BCC Entries
  $WA_MailObject = WAUE_AddBCC($WA_MailObject,"luis.ferreira@gmail.com");

//CC Entries
  $WA_MailObject = WAUE_AddCC($WA_MailObject,"secretariat@eabp.org");

//Body Format
  $WA_MailObject = WAUE_BodyFormat($WA_MailObject,0);

//Set Importance
  $WA_MailObject = WAUE_SetImportance($WA_MailObject,"3");

//Start Mail Body
$MailBody = $MailBody . "<html><head></head><body>\r\n";
$MailBody = $MailBody . "\r\n";
$MailBody = $MailBody . "<p>Hello ";
$MailBody = $MailBody .  $row_rsMembers['firstname'];
$MailBody = $MailBody . " ";
$MailBody = $MailBody .  $row_rsMembers['lastname'];
$MailBody = $MailBody . " </p>\r\n";
$MailBody = $MailBody . "<p>Your membership fee for 2013 is now due.</p>\r\n";
$MailBody = $MailBody . "<p>The amount is &#8364;";
$MailBody = $MailBody .  $row_rsMembers['fee'];
$MailBody = $MailBody . " (";
$MailBody = $MailBody .  $row_rsMembers['type'];
$MailBody = $MailBody . ").</p>\r\n";
$MailBody = $MailBody . "<p>Please <a href=\"http://www.eabp.org/payment.php?mid=";
$MailBody = $MailBody .  $row_rsMembers['mid'];
$MailBody = $MailBody . "\">click here</a> to go to the EABP website and start the payment process.</p>\r\n";
$MailBody = $MailBody . "<p>If you have already paid please let me know so that I can confirm receipt of payment.</p>\r\n";
$MailBody = $MailBody . "<p>Christine Hayes<br />\r\n";
$MailBody = $MailBody . "  EABP Secretariat<br />\r\n";
$MailBody = $MailBody . "Leidsestraat 106-108/1<br />\r\n";
$MailBody = $MailBody . "1017 PG Amsterdam<br />\r\n";
$MailBody = $MailBody . "The Netherlands</p>\r\n";
$MailBody = $MailBody . "<p>tel +31- [0]20 - 3302703<br />\r\n";
$MailBody = $MailBody . "fax +31- [0]20 - 6257312<br />\r\n";
$MailBody = $MailBody . "<a href=\"mailto:secretariat@eabp.org\">secretariat@eabp.org</a><br />\r\n";
$MailBody = $MailBody . "<a href=\"http://www.eabp.org\">www.eabp.org</a></p>\r\n";
$MailBody = $MailBody . "</body></html>";
//End Mail Body

$WA_MailObject = WAUE_SendMail($WA_MailObject,$MailAttachments,$MailBCC,$MailCC,$MailTo,$MailImportance,$MailFrom,$MailSubject,$MailBody,"waue_mailer_1");

if (isset($_SESSION["waue_mailer_1_Status"])) {
  $MailLogBindings = new WAUE_Log_Bindings();
  //Start Log Bindings
  //Success Or Failure
  $MailLogBindings->SuccessOrFailure->ToDo = "none";
  $MailLogBindings->SuccessOrFailure->Connection = "";
  $MailLogBindings->SuccessOrFailure->TableName = "";
  $MailLogBindings->SuccessOrFailure->EmailColumn = "";
  $MailLogBindings->SuccessOrFailure->ColumnList = array();
  $MailLogBindings->SuccessOrFailure->TypeList = array();
  $MailLogBindings->SuccessOrFailure->ValueList = array();
  //Success Only
  $MailLogBindings->Success->ToDo = "none";
  $MailLogBindings->Success->Connection = "";
  $MailLogBindings->Success->TableName = "";
  $MailLogBindings->Success->EmailColumn = "";
  $MailLogBindings->Success->ColumnList = array();
  $MailLogBindings->Success->TypeList = array();
  $MailLogBindings->Success->ValueList = array();
  //Failure Only
  $MailLogBindings->Failure->ToDo = "none";
  $MailLogBindings->Failure->Connection = "";
  $MailLogBindings->Failure->TableName = "";
  $MailLogBindings->Failure->EmailColumn = "";
  $MailLogBindings->Failure->ColumnList = array();
  $MailLogBindings->Failure->TypeList = array();
  $MailLogBindings->Failure->ValueList = array();
  //End Log Bindings
  $MailLogBindings->SuccessOrFailure->MailRef = "waue_mailer_1";
  $MailLogBindings->Success->MailRef = "waue_mailer_1";
  $MailLogBindings->Failure->MailRef = "waue_mailer_1";
  $MailLogBindings->processLog(($_SESSION["waue_mailer_1_Status"] == "Failure"));
}
$WA_MailObject = null;
?>