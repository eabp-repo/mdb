<?php
$MailAttachments = "";
$MailBCC         = "";
$MailCC          = "";
$MailTo          = "";
$MailBodyFormat  = "";
$MailBody        = "";
$MailImportance  = "";
$MailFrom        = "secretariat@eabp.org";
$MailSubject     = "EABP Subscription for 2013 - Invoice";
$_SERVER["QUERY_STRING"] = "";

//Global Variables

  $WA_MailObject = WAUE_Definition("","","","","","");

if ($RecipientEmail)     {
  $WA_MailObject = WAUE_AddRecipient($WA_MailObject,$RecipientEmail);
}
else      {
  //To Entries
}

//Additional Headers

//Attachment Entries

//BCC Entries

//CC Entries

//Body Format
  $WA_MailObject = WAUE_BodyFormat($WA_MailObject,0);

//Set Importance
  $WA_MailObject = WAUE_SetImportance($WA_MailObject,"3");

//Start Mail Body
$MailBody = $MailBody . "<html><head></head><body>\r\n";
$MailBody = $MailBody . "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\r\n";
$MailBody = $MailBody . "  <tr>\r\n";
$MailBody = $MailBody . "    <td>Invoice number: 2013-";
$MailBody = $MailBody .  $row_rsInvoice['invoiceid'];
$MailBody = $MailBody . "</td>\r\n";
$MailBody = $MailBody . "    <td width=\"200\" align=\"right\">Invoice date: ";
$MailBody = $MailBody .  date("d M Y",strtotime($row_rsInvoice['invoicedate']));
$MailBody = $MailBody . "</td>\r\n";
$MailBody = $MailBody . "  </tr>\r\n";
$MailBody = $MailBody . "  <tr>\r\n";
$MailBody = $MailBody . "    <td valign=\"top\">For:</td>\r\n";
$MailBody = $MailBody . "    <td align=\"right\" valign=\"top\">Contact: </td>\r\n";
$MailBody = $MailBody . "  </tr>\r\n";
$MailBody = $MailBody . "  <tr>\r\n";
$MailBody = $MailBody . "    <td valign=\"top\"><strong>";
$MailBody = $MailBody .  $row_rsInvoice['orgname'];
$MailBody = $MailBody . "<br />\r\n";
$MailBody = $MailBody . "      </strong>";
$MailBody = $MailBody .  $row_rsInvoice['address'];
$MailBody = $MailBody . "<br />\r\n";
$MailBody = $MailBody . "      ";
$MailBody = $MailBody .  $row_rsInvoice['city'];
$MailBody = $MailBody . " <br />\r\n";
$MailBody = $MailBody . "      ";
$MailBody = $MailBody .  $row_rsInvoice['postcode'];
$MailBody = $MailBody . "</td>\r\n";
$MailBody = $MailBody . "    <td align=\"right\" valign=\"top\">";
$MailBody = $MailBody .  $row_rsInvoice['contact'];
$MailBody = $MailBody . "</td>\r\n";
$MailBody = $MailBody . "  </tr>\r\n";
$MailBody = $MailBody . "</table>\r\n";
$MailBody = $MailBody . "<hr align=\"left\" width=\"600\" />\r\n";
$MailBody = $MailBody . "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\r\n";
$MailBody = $MailBody . "  <tr>\r\n";
$MailBody = $MailBody . "    <td><strong>Details</strong></td>\r\n";
$MailBody = $MailBody . "    <td>&nbsp;</td>\r\n";
$MailBody = $MailBody . "  </tr>\r\n";
$MailBody = $MailBody . "  ";
 do { 
$MailBody = $MailBody . "\r\n";
$MailBody = $MailBody . "  <tr>\r\n";
$MailBody = $MailBody . "    <td valign=\"top\">";
$MailBody = $MailBody .  $row_rsDetails['description'];
$MailBody = $MailBody . "<br />\r\n";
$MailBody = $MailBody . "      ";
$MailBody = $MailBody .  $row_rsDetails['notes'];
$MailBody = $MailBody . "</td>\r\n";
$MailBody = $MailBody . "    <td align=\"right\" valign=\"top\">";
$MailBody = $MailBody .  $row_rsDetails['amount'];
$MailBody = $MailBody . "</td>\r\n";
$MailBody = $MailBody . "  </tr>\r\n";
$MailBody = $MailBody . "  ";
 } while ($row_rsDetails = mysql_fetch_assoc($rsDetails)); 
$MailBody = $MailBody . "\r\n";
$MailBody = $MailBody . "  <tr>\r\n";
$MailBody = $MailBody . "    <td align=\"right\">Total: </td>\r\n";
$MailBody = $MailBody . "    <td align=\"right\">&#8364;";
$MailBody = $MailBody .  $row_rsTotal['SumOfamount'];
$MailBody = $MailBody . "</td>\r\n";
$MailBody = $MailBody . "  </tr>\r\n";
$MailBody = $MailBody . "</table>\r\n";
$MailBody = $MailBody . "<p>Please <a href=\"http://www.eabp.org/payment-org.php?id=";
$MailBody = $MailBody .  $row_rsInvoice['id'];
$MailBody = $MailBody . "\">click here</a> to go to the EABP website and start the payment process.</p>\r\n";
$MailBody = $MailBody . "<p>EABP Secretariat<br />\r\n";
$MailBody = $MailBody . "  Leidsestraat 106-108/1<br />\r\n";
$MailBody = $MailBody . "  1017 PG Amsterdam<br />\r\n";
$MailBody = $MailBody . "  The Netherlands</p>\r\n";
$MailBody = $MailBody . "<p>tel +31- [0]20 - 3302703<br />\r\n";
$MailBody = $MailBody . "  fax +31- [0]20 - 6257312<br />\r\n";
$MailBody = $MailBody . "  <a href=\"mailto:secretariat@eabp.org\">secretariat@eabp.org</a><br />\r\n";
$MailBody = $MailBody . "  <a href=\"http://www.eabp.org\">www.eabp.org</a></p>\r\n";
$MailBody = $MailBody . "</body></html>";
//End Mail Body

$WA_MailObject = WAUE_SendMail($WA_MailObject,$MailAttachments,$MailBCC,$MailCC,$MailTo,$MailImportance,$MailFrom,$MailSubject,$MailBody,"waue_mailer-organisations_1");

if (isset($_SESSION["waue_mailer-organisations_1_Status"])) {
  $MailLogBindings = new WAUE_Log_Bindings();
  //Start Log Bindings
  //Success Or Failure
  $MailLogBindings->SuccessOrFailure->ToDo = "none";
  $MailLogBindings->SuccessOrFailure->Connection = "";
  $MailLogBindings->SuccessOrFailure->TableName = "";
  $MailLogBindings->SuccessOrFailure->EmailColumn = "";
  $MailLogBindings->SuccessOrFailure->ColumnList = array();
  $MailLogBindings->SuccessOrFailure->TypeList = array();
  $MailLogBindings->SuccessOrFailure->ValueList = array();
  //Success Only
  $MailLogBindings->Success->ToDo = "none";
  $MailLogBindings->Success->Connection = "";
  $MailLogBindings->Success->TableName = "";
  $MailLogBindings->Success->EmailColumn = "";
  $MailLogBindings->Success->ColumnList = array();
  $MailLogBindings->Success->TypeList = array();
  $MailLogBindings->Success->ValueList = array();
  //Failure Only
  $MailLogBindings->Failure->ToDo = "none";
  $MailLogBindings->Failure->Connection = "";
  $MailLogBindings->Failure->TableName = "";
  $MailLogBindings->Failure->EmailColumn = "";
  $MailLogBindings->Failure->ColumnList = array();
  $MailLogBindings->Failure->TypeList = array();
  $MailLogBindings->Failure->ValueList = array();
  //End Log Bindings
  $MailLogBindings->SuccessOrFailure->MailRef = "waue_mailer-organisations_1";
  $MailLogBindings->Success->MailRef = "waue_mailer-organisations_1";
  $MailLogBindings->Failure->MailRef = "waue_mailer-organisations_1";
  $MailLogBindings->processLog(($_SESSION["waue_mailer-organisations_1_Status"] == "Failure"));
}
$WA_MailObject = null;
?>