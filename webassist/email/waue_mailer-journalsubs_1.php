<?php
$MailAttachments = "";
$MailBCC         = "";
$MailCC          = "";
$MailTo          = "";
$MailBodyFormat  = "";
$MailBody        = "";
$MailImportance  = "";
$MailFrom        = "secretariat@eabp.org";
$MailSubject     = "Subscription to International Body Psychotherapy Journal";
$_SERVER["QUERY_STRING"] = "";

//Global Variables

  $WA_MailObject = WAUE_Definition("","","","","","");

if ($RecipientEmail)     {
  $WA_MailObject = WAUE_AddRecipient($WA_MailObject,$RecipientEmail);
}
else      {
  //To Entries
}

//Additional Headers

//Attachment Entries

//BCC Entries

//CC Entries

//Body Format
  $WA_MailObject = WAUE_BodyFormat($WA_MailObject,0);

//Set Importance
  $WA_MailObject = WAUE_SetImportance($WA_MailObject,"3");

//Start Mail Body
$MailBody = $MailBody . "<html><head></head><body>\r\n";
$MailBody = $MailBody . "\r\n";
$MailBody = $MailBody . "<p>Thank you.</p>\r\n";
$MailBody = $MailBody . "<p>To verify your email address and download the pdf of the latest issue of the International Body Psychotherapy Journal please <a href=\"http://www.eabp.org/ibpj-download.php?id=";
$MailBody = $MailBody .  $row_rsSubs['sessionid'];
$MailBody = $MailBody . "\">click here</a>.</p>\r\n";
$MailBody = $MailBody . "<p>EABP Secretariat<br />\r\n";
$MailBody = $MailBody . "  Leidsestraat 106-108/1<br />\r\n";
$MailBody = $MailBody . "  NL-1017 PG<br />\r\n";
$MailBody = $MailBody . "  Amsterdam<br />\r\n";
$MailBody = $MailBody . "  The Netherlands</p>\r\n";
$MailBody = $MailBody . "</body></html>";
//End Mail Body

$WA_MailObject = WAUE_SendMail($WA_MailObject,$MailAttachments,$MailBCC,$MailCC,$MailTo,$MailImportance,$MailFrom,$MailSubject,$MailBody,"waue_mailer-journalsubs_1");

if (isset($_SESSION["waue_mailer-journalsubs_1_Status"])) {
  $MailLogBindings = new WAUE_Log_Bindings();
  //Start Log Bindings
  //Success Or Failure
  $MailLogBindings->SuccessOrFailure->ToDo = "none";
  $MailLogBindings->SuccessOrFailure->Connection = "";
  $MailLogBindings->SuccessOrFailure->TableName = "";
  $MailLogBindings->SuccessOrFailure->EmailColumn = "";
  $MailLogBindings->SuccessOrFailure->ColumnList = array();
  $MailLogBindings->SuccessOrFailure->TypeList = array();
  $MailLogBindings->SuccessOrFailure->ValueList = array();
  //Success Only
  $MailLogBindings->Success->ToDo = "none";
  $MailLogBindings->Success->Connection = "";
  $MailLogBindings->Success->TableName = "";
  $MailLogBindings->Success->EmailColumn = "";
  $MailLogBindings->Success->ColumnList = array();
  $MailLogBindings->Success->TypeList = array();
  $MailLogBindings->Success->ValueList = array();
  //Failure Only
  $MailLogBindings->Failure->ToDo = "none";
  $MailLogBindings->Failure->Connection = "";
  $MailLogBindings->Failure->TableName = "";
  $MailLogBindings->Failure->EmailColumn = "";
  $MailLogBindings->Failure->ColumnList = array();
  $MailLogBindings->Failure->TypeList = array();
  $MailLogBindings->Failure->ValueList = array();
  //End Log Bindings
  $MailLogBindings->SuccessOrFailure->MailRef = "waue_mailer-journalsubs_1";
  $MailLogBindings->Success->MailRef = "waue_mailer-journalsubs_1";
  $MailLogBindings->Failure->MailRef = "waue_mailer-journalsubs_1";
  $MailLogBindings->processLog(($_SESSION["waue_mailer-journalsubs_1_Status"] == "Failure"));
}
$WA_MailObject = null;
?>