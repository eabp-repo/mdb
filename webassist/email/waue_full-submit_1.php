<?php
$MailAttachments = "";
$MailBCC         = "";
$MailCC          = "";
$MailTo          = "";
$MailBodyFormat  = "";
$MailBody        = "";
$MailImportance  = "";
$MailFrom        = "Online application|WA|no-reply@eabp.org";
$MailSubject     = "EABP application - ".$row_rsApp['firstname']  ." ".$row_rsApp['lastname']  ."";
$_SERVER["QUERY_STRING"] = "";

//Global Variables

  $WA_MailObject = WAUE_Definition("","","","","","");

if ($RecipientEmail)     {
  $WA_MailObject = WAUE_AddRecipient($WA_MailObject,$RecipientEmail);
}
else      {
  //To Entries
}

//Additional Headers
  $WA_MailObject->ReplyTo = "".$row_rsApp['email']  ."";

//Attachment Entries

//BCC Entries
  $WA_MailObject = WAUE_AddBCC($WA_MailObject,"luis.ferreira@gmail.com");

//CC Entries
  $WA_MailObject = WAUE_AddCC($WA_MailObject,"secretariatasst@eabp.org");

//Body Format
  $WA_MailObject = WAUE_BodyFormat($WA_MailObject,0);

//Set Importance
  $WA_MailObject = WAUE_SetImportance($WA_MailObject,"3");

//Start Mail Body
$MailBody = $MailBody . "<html><head></head><body>\r\n";
$MailBody = $MailBody . "<p>";
$MailBody = $MailBody .  $row_rsApp['firstname'];
$MailBody = $MailBody . " ";
$MailBody = $MailBody .  $row_rsApp['lastname'];
$MailBody = $MailBody . " has submitted their application.</p>\r\n";
$MailBody = $MailBody . "<p><a href=\"http://mdb.eabp.org/sitecms/applications/application.php?aid=";
$MailBody = $MailBody .  $row_rsApp['aid'];
$MailBody = $MailBody . "\">Click here to review the application</a>.</p>\r\n";
$MailBody = $MailBody . "</body></html>";
//End Mail Body

$WA_MailObject = WAUE_SendMail($WA_MailObject,$MailAttachments,$MailBCC,$MailCC,$MailTo,$MailImportance,$MailFrom,$MailSubject,$MailBody,"waue_full-submit_1");

if (isset($_SESSION["waue_full-submit_1_Status"])) {
  $MailLogBindings = new WAUE_Log_Bindings();
  //Start Log Bindings
  //Success Or Failure
  $MailLogBindings->SuccessOrFailure->ToDo = "none";
  $MailLogBindings->SuccessOrFailure->Connection = "";
  $MailLogBindings->SuccessOrFailure->TableName = "";
  $MailLogBindings->SuccessOrFailure->EmailColumn = "";
  $MailLogBindings->SuccessOrFailure->ColumnList = array();
  $MailLogBindings->SuccessOrFailure->TypeList = array();
  $MailLogBindings->SuccessOrFailure->ValueList = array();
  //Success Only
  $MailLogBindings->Success->ToDo = "none";
  $MailLogBindings->Success->Connection = "";
  $MailLogBindings->Success->TableName = "";
  $MailLogBindings->Success->EmailColumn = "";
  $MailLogBindings->Success->ColumnList = array();
  $MailLogBindings->Success->TypeList = array();
  $MailLogBindings->Success->ValueList = array();
  //Failure Only
  $MailLogBindings->Failure->ToDo = "none";
  $MailLogBindings->Failure->Connection = "";
  $MailLogBindings->Failure->TableName = "";
  $MailLogBindings->Failure->EmailColumn = "";
  $MailLogBindings->Failure->ColumnList = array();
  $MailLogBindings->Failure->TypeList = array();
  $MailLogBindings->Failure->ValueList = array();
  //End Log Bindings
  $MailLogBindings->SuccessOrFailure->MailRef = "waue_full-submit_1";
  $MailLogBindings->Success->MailRef = "waue_full-submit_1";
  $MailLogBindings->Failure->MailRef = "waue_full-submit_1";
  $MailLogBindings->processLog(($_SESSION["waue_full-submit_1_Status"] == "Failure"));
}
$WA_MailObject = null;
?>