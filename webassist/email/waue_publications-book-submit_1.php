<?php
$MailAttachments = "";
$MailBCC         = "";
$MailCC          = "";
$MailTo          = "";
$MailBodyFormat  = "";
$MailBody        = "";
$MailImportance  = "";
$MailFrom        = "EABP website user|WA|website@eabp.org";
$MailSubject     = "EABP website - book submission - ".$_POST['title']  ."";
$_SERVER["QUERY_STRING"] = "";

//Global Variables

  $WA_MailObject = WAUE_Definition("","","","","","");

if ($RecipientEmail)     {
  $WA_MailObject = WAUE_AddRecipient($WA_MailObject,$RecipientEmail);
}
else      {
  //To Entries
}

//Additional Headers
  $WA_MailObject->ReplyTo = "".$_POST['submitemail']  ."";

//Attachment Entries

//BCC Entries

//CC Entries
  $WA_MailObject = WAUE_AddCC($WA_MailObject,"secretariatasst@eabp.org");

//Body Format
  $WA_MailObject = WAUE_BodyFormat($WA_MailObject,0);

//Set Importance
  $WA_MailObject = WAUE_SetImportance($WA_MailObject,"3");

//Start Mail Body
$MailBody = $MailBody . "<html><head></head><body><p>";
$MailBody = $MailBody .  $_POST['submitby'];
$MailBody = $MailBody . "\r\n";
$MailBody = $MailBody . "has submitted a book - ";
$MailBody = $MailBody .  $_POST['title'];
$MailBody = $MailBody . "</p>\r\n";
$MailBody = $MailBody . "<p><a href=\"http://www.eabp.org/sitecms/book-edit.php?bookid=";
$MailBody = $MailBody .  $connEABP2_i->insert_id;
$MailBody = $MailBody . "\">Click here</a> to review.</p>\r\n";
$MailBody = $MailBody . "</body></html>";
//End Mail Body

$WA_MailObject = WAUE_SendMail($WA_MailObject,$MailAttachments,$MailBCC,$MailCC,$MailTo,$MailImportance,$MailFrom,$MailSubject,$MailBody,"waue_publications-book-submit_1");

if (isset($_SESSION["waue_publications-book-submit_1_Status"])) {
  $MailLogBindings = new WAUE_Log_Bindings();
  //Start Log Bindings
  //Success Or Failure
  $MailLogBindings->SuccessOrFailure->ToDo = "none";
  $MailLogBindings->SuccessOrFailure->Connection = "";
  $MailLogBindings->SuccessOrFailure->TableName = "";
  $MailLogBindings->SuccessOrFailure->EmailColumn = "";
  $MailLogBindings->SuccessOrFailure->ColumnList = array();
  $MailLogBindings->SuccessOrFailure->TypeList = array();
  $MailLogBindings->SuccessOrFailure->ValueList = array();
  //Success Only
  $MailLogBindings->Success->ToDo = "none";
  $MailLogBindings->Success->Connection = "";
  $MailLogBindings->Success->TableName = "";
  $MailLogBindings->Success->EmailColumn = "";
  $MailLogBindings->Success->ColumnList = array();
  $MailLogBindings->Success->TypeList = array();
  $MailLogBindings->Success->ValueList = array();
  //Failure Only
  $MailLogBindings->Failure->ToDo = "none";
  $MailLogBindings->Failure->Connection = "";
  $MailLogBindings->Failure->TableName = "";
  $MailLogBindings->Failure->EmailColumn = "";
  $MailLogBindings->Failure->ColumnList = array();
  $MailLogBindings->Failure->TypeList = array();
  $MailLogBindings->Failure->ValueList = array();
  //End Log Bindings
  $MailLogBindings->SuccessOrFailure->MailRef = "waue_publications-book-submit_1";
  $MailLogBindings->Success->MailRef = "waue_publications-book-submit_1";
  $MailLogBindings->Failure->MailRef = "waue_publications-book-submit_1";
  $MailLogBindings->processLog(($_SESSION["waue_publications-book-submit_1_Status"] == "Failure"));
}
$WA_MailObject = null;
?>