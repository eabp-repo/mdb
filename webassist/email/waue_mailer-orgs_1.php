<?php
$MailAttachments = "";
$MailBCC         = "";
$MailCC          = "";
$MailTo          = "";
$MailBodyFormat  = "";
$MailBody        = "";
$MailImportance  = "";
$MailFrom        = "EABP|WA|secretariat@eabp.org";
$MailSubject     = "EABP Subscription for 2012 - Invoice";
$_SERVER["QUERY_STRING"] = "";

//Global Variables

  $WA_MailObject = WAUE_Definition("","","","","","");

if ($RecipientEmail)     {
  $WA_MailObject = WAUE_AddRecipient($WA_MailObject,$RecipientEmail);
}
else      {
  //To Entries
}

//Additional Headers

//Attachment Entries

//BCC Entries
  $WA_MailObject = WAUE_AddBCC($WA_MailObject,"luis.ferreira@gmail.com");

//CC Entries

//Body Format
  $WA_MailObject = WAUE_BodyFormat($WA_MailObject,0);

//Set Importance
  $WA_MailObject = WAUE_SetImportance($WA_MailObject,"3");

//Start Mail Body
$MailBody = $MailBody . "<html><head></head><body>\r\n";

global $RecipArray;
global $RecipIndex;

$MailBody = $MailBody . "<p>";
$MailBody = $MailBody .  ($RecipArray[$RecipIndex][0]["orgname"]);
$MailBody = $MailBody . "<br />\r\n";
$MailBody = $MailBody .  ($RecipArray[$RecipIndex][0]["address"]);
$MailBody = $MailBody . "<br />\r\n";
$MailBody = $MailBody .  ($RecipArray[$RecipIndex][0]["city"]);
$MailBody = $MailBody . "";
$MailBody = $MailBody .  ($RecipArray[$RecipIndex][0]["postcode"]);
$MailBody = $MailBody . "\r\n";
$MailBody = $MailBody . "</p>\r\n";
$MailBody = $MailBody . "<p>15 May 2012</p>\r\n";
$MailBody = $MailBody . "<p>Dear ";
$MailBody = $MailBody .  ($RecipArray[$RecipIndex][0]["contact"]);
$MailBody = $MailBody . ",</p>\r\n";
$MailBody = $MailBody . "<p>The following is your invoice for your 2012 EABP Organisational Membership fee:</p>\r\n";
$MailBody = $MailBody . "<p>INVOICE No. OF-";
$MailBody = $MailBody .  ($RecipArray[$RecipIndex][0]["id"]);
$MailBody = $MailBody . "-2012</p>\r\n";
$MailBody = $MailBody . "<p>For EABP Organisational Membership Fee 2012 - &#8364;190</p>\r\n";
$MailBody = $MailBody . "<p>Please <a href=\"http://www.eabp.org/payment-org.php?id=";
$MailBody = $MailBody .  ($RecipArray[$RecipIndex][0]["id"]);
$MailBody = $MailBody . "\">click here</a> to go to the EABP website and start the payment process.</p>\r\n";
$MailBody = $MailBody . "<p>EABP Secretariat<br />\r\n";
$MailBody = $MailBody . "Leidsestraat 106-108/1<br />\r\n";
$MailBody = $MailBody . "1017 PG Amsterdam<br />\r\n";
$MailBody = $MailBody . "The Netherlands</p>\r\n";
$MailBody = $MailBody . "<p>tel +31- [0]20 - 3302703<br />\r\n";
$MailBody = $MailBody . "fax +31- [0]20 - 6257312<br />\r\n";
$MailBody = $MailBody . "<a href=\"mailto:secretariat@eabp.org\">secretariat@eabp.org</a><br />\r\n";
$MailBody = $MailBody . "<a href=\"http://www.eabp.org\">www.eabp.org</a></p>\r\n";
$MailBody = $MailBody . "</body></html>";
//End Mail Body

$WA_MailObject = WAUE_SendMail($WA_MailObject,$MailAttachments,$MailBCC,$MailCC,$MailTo,$MailImportance,$MailFrom,$MailSubject,$MailBody,"waue_mailer-orgs_1");

if (isset($_SESSION["waue_mailer-orgs_1_Status"])) {
  $MailLogBindings = new WAUE_Log_Bindings();
  //Start Log Bindings
  //Success Or Failure
  $MailLogBindings->SuccessOrFailure->ToDo = "none";
  $MailLogBindings->SuccessOrFailure->Connection = "";
  $MailLogBindings->SuccessOrFailure->TableName = "";
  $MailLogBindings->SuccessOrFailure->EmailColumn = "";
  $MailLogBindings->SuccessOrFailure->ColumnList = array();
  $MailLogBindings->SuccessOrFailure->TypeList = array();
  $MailLogBindings->SuccessOrFailure->ValueList = array();
  //Success Only
  $MailLogBindings->Success->ToDo = "none";
  $MailLogBindings->Success->Connection = "";
  $MailLogBindings->Success->TableName = "";
  $MailLogBindings->Success->EmailColumn = "";
  $MailLogBindings->Success->ColumnList = array();
  $MailLogBindings->Success->TypeList = array();
  $MailLogBindings->Success->ValueList = array();
  //Failure Only
  $MailLogBindings->Failure->ToDo = "none";
  $MailLogBindings->Failure->Connection = "";
  $MailLogBindings->Failure->TableName = "";
  $MailLogBindings->Failure->EmailColumn = "";
  $MailLogBindings->Failure->ColumnList = array();
  $MailLogBindings->Failure->TypeList = array();
  $MailLogBindings->Failure->ValueList = array();
  //End Log Bindings
  $MailLogBindings->SuccessOrFailure->MailRef = "waue_mailer-orgs_1";
  $MailLogBindings->Success->MailRef = "waue_mailer-orgs_1";
  $MailLogBindings->Failure->MailRef = "waue_mailer-orgs_1";
  $MailLogBindings->processLog(($_SESSION["waue_mailer-orgs_1_Status"] == "Failure"));
}
$WA_MailObject = null;
?>