<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "docs";
$ppu->extensions = "pdf,doc,docx";
$ppu->formName = "frmApplication";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "";
$ppu->progressHeight = "";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmApplication")) {
  $updateSQL = sprintf("UPDATE applications SET doa=%s, dob=%s, email=%s, firstname=%s, gender=%s, homecity=%s, homephone=%s, homepostcode=%s, homeadd1=%s, lastname=%s, webpage=%s, nationality=%s, cid=%s, tid=%s, proofdoc=IFNULL(%s,proofdoc), langs=%s, reasonapply=%s, submitted=%s, datesubmitted=%s WHERE aid=%s",
                       GetSQLValueString($_POST['doa'], "text"),
                       GetSQLValueString($_POST['dob'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['firstname'], "text"),
                       GetSQLValueString($_POST['gender'], "text"),
                       GetSQLValueString($_POST['homecity'], "text"),
                       GetSQLValueString($_POST['homephone'], "text"),
                       GetSQLValueString($_POST['homepostcode'], "text"),
                       GetSQLValueString($_POST['homeadd1'], "text"),
                       GetSQLValueString($_POST['lastname'], "text"),
                       GetSQLValueString($_POST['webpage'], "text"),
                       GetSQLValueString($_POST['nationality'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['tid'], "int"),
                       GetSQLValueString($_POST['proofdoc'], "text"),
                       GetSQLValueString($_POST['langs'], "text"),
                       GetSQLValueString($_POST['reasonapply'], "text"),
                       GetSQLValueString($_POST['submitted'], "int"),
                       GetSQLValueString($_POST['datesubmitted'], "date"),
                       GetSQLValueString($_POST['aid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

$colname_rsApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT * FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);
?>
<?php require_once("../webassist/email/mail_php.php"); ?>
<?php require_once("../webassist/email/mailformatting_php.php"); ?>
<?php
if (!isset($_SESSION))session_start();
if ((isset($_POST["btnSubmit"])))     {
  //WA Universal Email object="mail"
  set_time_limit(0);
  $EmailRef = "waue_student_1";
  $BurstSize = 200;
  $BurstTime = 1;
  $WaitTime = 1;
  $GoToPage = "../apply-thanks.php?tid=13&aid=".$row_rsApp['aid'];
  $RecipArray = array();
  $StartBurst = time();
  $LoopCount = 0;
  $TotalEmails = 0;
  $RecipIndex = 0;
  // build up recipients array
  $CurIndex = sizeof($RecipArray);
  $RecipArray[$CurIndex] = array();
  $RecipArray[$CurIndex ][] = "secretariatasst@eabp.org";
  $TotalEmails += sizeof($RecipArray[$CurIndex]);
  $RealWait = ($WaitTime<0.25)?0.25:($WaitTime+0.1);
  $TimeTracker = Array();
  $TotalBursts = floor($TotalEmails/$BurstSize);
  $AfterBursts = $TotalEmails % $BurstSize;
  $TimeRemaining = ($TotalBursts * $BurstTime) + ($AfterBursts*$RealWait);
  if ($TimeRemaining < ($TotalEmails*$RealWait) )  {
    $TimeRemaining = $TotalEmails*$RealWait;
  }
  $_SESSION[$EmailRef."_Total"] = $TotalEmails;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = $TimeRemaining;
  while ($RecipIndex < sizeof($RecipArray))  {
    $EnteredValue = is_string($RecipArray[$RecipIndex][0]);
    $CurIndex = 0;
    while (($EnteredValue && $CurIndex < sizeof($RecipArray[$RecipIndex])) || (!$EnteredValue && $RecipArray[$RecipIndex][0])) {
      $starttime = microtime_float();
      if ($EnteredValue)  {
        $RecipientEmail = $RecipArray[$RecipIndex][$CurIndex];
      }  else  {
        $RecipientEmail = $RecipArray[$RecipIndex][0][$RecipArray[$RecipIndex][2]];
      }
      $EmailsRemaining = ($TotalEmails- $LoopCount);
      $BurstsRemaining = ceil(($EmailsRemaining-$AfterBursts)/$BurstSize);
      $IntoBurst = ($EmailsRemaining-$AfterBursts) % $BurstSize;
      if ($AfterBursts<$EmailsRemaining) $IntoBurst = 0;
      $TimeRemaining = ($BurstsRemaining * $BurstTime * 60) + ((($AfterBursts<$EmailsRemaining)?$AfterBursts:$EmailsRemaining)*$RealWait) - (($AfterBursts>$EmailsRemaining)?0:($IntoBurst*$RealWait));
      if ($TimeRemaining < ($EmailsRemaining*$RealWait) )  {
        $TimeRemaining = $EmailsRemaining*$RealWait;
      }
      $CurIndex ++;
      $LoopCount ++;
      session_commit();
      session_start();
      $_SESSION[$EmailRef."_Index"] = $LoopCount;
      $_SESSION[$EmailRef."_Remaining"] = round($TimeRemaining);
      session_commit();
      wa_sleep($WaitTime);
      include("../webassist/email/waue_student_1.php");
      $endtime = microtime_float();
      $TimeTracker[] =$endtime - $starttime;
      $RealWait = array_sum($TimeTracker)/sizeof($TimeTracker);
      if ($LoopCount % $BurstSize == 0 && $CurIndex < sizeof($RecipArray[$RecipIndex]))  {
        $TimePassed = (time() - $StartBurst);
        if ($TimePassed < ($BurstTime*60))  {
          $WaitBurst = ($BurstTime*60) -$TimePassed;
          wa_sleep($WaitBurst);
        }
        else  {
          $TimeRemaining = ($TotalEmails- $LoopCount)*$RealWait;
        }
        $StartBurst = time();
      }
      if (!$EnteredValue)  {
        $RecipArray[$RecipIndex][0] =  mysql_fetch_assoc($RecipArray[$RecipIndex][1]);
      }
    }
    $RecipIndex ++;
  }
  $_SESSION[$EmailRef."_Total"] = 0;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = 0;
  session_commit();
  session_start();
  if ($GoToPage!="")     {
    header("Location: ".$GoToPage);
  }
}
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - online application - student member</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-224881-50']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<link rel="stylesheet" type="text/css" href="../datetimepicker-master/jquery.datetimepicker.css"/>
<script language='javascript' src='../ScriptLibrary/incPureUpload.js'></script>
</head>

<body>
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1>Online application:<br />
    EABP <strong>student</strong> membership</h1>
  <div id="leftSidebar">
    <p>Please inform yourself about the <a href="../membership-student.php">Student Membership Criteria  and Conditions</a> and use the instructions for completing this application  form.</p>
    <p>Once your application has been accepted you will be sent an invoice for the yearly membership fee of 30 Euros. After paying the fee you will be entered in the membership database on the EABP (and/or National Association) website(s).</p>
    <p><span class="crimsonText">*</span> = required</p>
    <form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmApplication" id="frmApplication" onSubmit="checkFileUpload(this,'pdf,doc,docx',false,'','','','','','','');return document.MM_returnValue">
    <table border="0" cellpadding="0" cellspacing="2" id="tblApplication">
      <tr>
        <th scope="row">First name <span class="crimsonText">*</span></th>
        <td><input name="firstname" type="text" required id="firstname" value="<?php echo $row_rsApp['firstname']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Last name <span class="crimsonText">*</span></th>
        <td><input name="lastname" type="text" required id="lastname" value="<?php echo $row_rsApp['lastname']; ?>" /></td>
      </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">Contact details</td>
        </tr>
      <tr>
        <th scope="row">Address <span class="crimsonText">*</span></th>
        <td><input name="homeadd1" type="text" required id="homeadd1" value="<?php echo $row_rsApp['homeadd1']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Town/City <span class="crimsonText">*</span></th>
        <td><input name="homecity" type="text" required id="homecity" value="<?php echo $row_rsApp['homecity']; ?>"  /></td>
      </tr>
      <tr>
        <th scope="row">Post code <span class="crimsonText">*</span></th>
        <td><input name="homepostcode" type="text" required id="homepostcode" value="<?php echo $row_rsApp['homepostcode']; ?>"  /></td>
      </tr>
      <tr>
        <th scope="row">Country <span class="crimsonText">*</span></th>
        <td><select name="cid" id="cid">
          <?php
do {  
?>
          <option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsApp['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option>
          <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>

        </select></td>
      </tr>
      <tr>
        <th scope="row">Telephone <span class="crimsonText">*</span></th>
        <td><input name="homephone" type="text" required id="homephone" value="<?php echo $row_rsApp['homephone']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Email <span class="crimsonText">*</span></th>
        <td><input name="email" type="email" required id="email" value="<?php echo $row_rsApp['email']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Website<br />
          <span class="normal">no http://</span></th>
        <td><input name="webpage" type="text" id="webpage" value="<?php echo $row_rsApp['webpage']; ?>" /></td>
      </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">Personal details</td>
        </tr>
      <tr>
        <th scope="row">Date of birth <span class="crimsonText">*</span></th>
        <td><input name="dob" type="text" required id="dob" value="<?php echo $row_rsApp['dob']; ?>"  /></td>
      </tr>
      <tr>
        <th scope="row">Gender <span class="crimsonText">*</span></th>
        <td>
          <label>
            <input <?php if (!(strcmp($row_rsApp['gender'],"Male"))) {echo "checked=\"checked\"";} ?> name="gender" type="radio" class="radioButton" id="gender_0" value="Male" />
            male</label>
          <label>
            <input <?php if (!(strcmp($row_rsApp['gender'],"Female"))) {echo "checked=\"checked\"";} ?> name="gender" type="radio" class="radioButton" id="gender_1" value="Female" />
            female</label>
              <label>
            <input <?php if (!(strcmp($row_rsApp['gender'],"Other"))) {echo "checked=\"checked\"";} ?> name="gender" type="radio" class="radioButton" id="gender_2" value="Other" />
            other</label>
        </td>
      </tr>
      <tr>
        <th scope="row">Nationality <span class="crimsonText">*</span></th>
        <td><input name="nationality" type="text" required id="nationality" value="<?php echo $row_rsApp['nationality']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Languages spoken</th>
        <td><input name="langs" type="text" id="langs" value="<?php echo $row_rsApp['langs']; ?>" /></td>
      </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">Reasons for applying</td>
      </tr>
      <tr>
        <td colspan="2" scope="row"><ul>
          <li>If you are in training in Body-Psychotherapy please give the name of the training institute and its website address.</li>
          <li>If you are a university student (e.g. social science, psychology, medicine, social worker) please give the name of the faculty and university and its website address.</li>
          <li>Please supply proof from the Institution where you are studying.</li>
        </ul></td>
      </tr>
      <tr>
        <td colspan="2" scope="row"><textarea name="reasonapply" class="textAreaLong" id="reasonapply"><?php echo $row_rsApp['reasonapply']; ?></textarea></td>
        </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">Proof of registration with a training school or university</td>
      </tr>
      <tr>
        <th scope="row">Document (scan or original) <span class="crimsonText">*</span></th>
        <td><input name="proofdoc" type="file" id="proofdoc" onChange="checkOneFileUpload(this,'pdf,doc,docx',false,'','','','','','','')"></td>
      </tr>
      <?php if ($row_rsApp['submitted'] !=1) { ?>
      <tr>
        <th scope="row"><input name="tid" type="hidden" id="tid" value="13" />
          <input name="showonsite" type="hidden" id="showonsite" value="0" />
          <input name="doa" type="hidden" id="doa" value="<?php echo date('Y-m-d') ; ?>">
          <input name="nid" type="hidden" id="nid" value="12">
          <input name="aid" type="hidden" id="aid" value="<?php echo $row_rsApp['aid']; ?>">
          <input name="submitted" type="hidden" id="submitted" value="1">
          <input name="datesubmitted" type="hidden" id="datesubmitted" value="<?php echo date('Y-m-d H:i:s') ; ?>"></th>
        <td><input type="submit" name="btnSubmit" id="btnSubmit" value="Submit Application" /></td>
      </tr>
      <?php } else { ?>
      <tr>
        <th colspan="2"><span class="allCaps">You have already submitted your application</span>.</th></tr>
      <?php } ?>
    </table>
    <input type="hidden" name="MM_update" value="frmApplication">
    </form>
    <p>&nbsp;</p>
    <p><a href="#top" class="top">Back to top</a></p>
  </div>
  <div id="rightSidebar">
<p><a href="logout.php">Logout</a></p>
<p><?php echo $_SESSION['MM_Username']; ?></p>

  </div>
  </div>
<div id="contentBottom">&nbsp;</div>
</div>


</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../datetimepicker-master/build/jquery.datetimepicker.full.js"></script>
<script>/*
window.onerror = function(errorMsg) {
	$('#console').html($('#console').html()+'<br>'+errorMsg)
}*/

$.datetimepicker.setLocale('en');


$('#dob').datetimepicker({
	yearOffset:0,
	yearStart:1915,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
});

</script>
</html>
<?php

mysql_free_result($rsCountries);

mysql_free_result($rsApp);

?>
