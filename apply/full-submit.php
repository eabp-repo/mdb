<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmApplication")) {
  $updateSQL = sprintf("UPDATE applications SET submitted=%s, datesubmitted=%s WHERE aid=%s",
                       GetSQLValueString($_POST['submitted'], "int"),
                       GetSQLValueString($_POST['datesubmitted'], "date"),
                       GetSQLValueString($_POST['aid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT aid, email, firstname, lastname, submitted FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);
?>
<?php
// DO NOT SEND IF ALREADY SUBMITTED
// if($row_rsApp['submitted'] !=1) {?>
<?php require_once("../webassist/email/mail_php.php"); ?>
<?php require_once("../webassist/email/mailformatting_php.php"); ?>
<?php
if (!isset($_SESSION))session_start();
if ((isset($_POST["btnSubmit"])))     {
  //WA Universal Email object="mail"
  set_time_limit(0);
  $EmailRef = "waue_full-submit_1";
  $BurstSize = 200;
  $BurstTime = 1;
  $WaitTime = 1;
  $GoToPage = "";
  $RecipArray = array();
  $StartBurst = time();
  $LoopCount = 0;
  $TotalEmails = 0;
  $RecipIndex = 0;
  // build up recipients array
  $CurIndex = sizeof($RecipArray);
  $RecipArray[$CurIndex] = array();
  $RecipArray[$CurIndex ][] = "officemanager@eabp.org";
  $TotalEmails += sizeof($RecipArray[$CurIndex]);
  $RealWait = ($WaitTime<0.25)?0.25:($WaitTime+0.1);
  $TimeTracker = Array();
  $TotalBursts = floor($TotalEmails/$BurstSize);
  $AfterBursts = $TotalEmails % $BurstSize;
  $TimeRemaining = ($TotalBursts * $BurstTime) + ($AfterBursts*$RealWait);
  if ($TimeRemaining < ($TotalEmails*$RealWait) )  {
    $TimeRemaining = $TotalEmails*$RealWait;
  }
  $_SESSION[$EmailRef."_Total"] = $TotalEmails;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = $TimeRemaining;
  while ($RecipIndex < sizeof($RecipArray))  {
    $EnteredValue = is_string($RecipArray[$RecipIndex][0]);
    $CurIndex = 0;
    while (($EnteredValue && $CurIndex < sizeof($RecipArray[$RecipIndex])) || (!$EnteredValue && $RecipArray[$RecipIndex][0])) {
      $starttime = microtime_float();
      if ($EnteredValue)  {
        $RecipientEmail = $RecipArray[$RecipIndex][$CurIndex];
      }  else  {
        $RecipientEmail = $RecipArray[$RecipIndex][0][$RecipArray[$RecipIndex][2]];
      }
      $EmailsRemaining = ($TotalEmails- $LoopCount);
      $BurstsRemaining = ceil(($EmailsRemaining-$AfterBursts)/$BurstSize);
      $IntoBurst = ($EmailsRemaining-$AfterBursts) % $BurstSize;
      if ($AfterBursts<$EmailsRemaining) $IntoBurst = 0;
      $TimeRemaining = ($BurstsRemaining * $BurstTime * 60) + ((($AfterBursts<$EmailsRemaining)?$AfterBursts:$EmailsRemaining)*$RealWait) - (($AfterBursts>$EmailsRemaining)?0:($IntoBurst*$RealWait));
      if ($TimeRemaining < ($EmailsRemaining*$RealWait) )  {
        $TimeRemaining = $EmailsRemaining*$RealWait;
      }
      $CurIndex ++;
      $LoopCount ++;
      session_commit();
      session_start();
      $_SESSION[$EmailRef."_Index"] = $LoopCount;
      $_SESSION[$EmailRef."_Remaining"] = round($TimeRemaining);
      session_commit();
      wa_sleep($WaitTime);
      include("../webassist/email/waue_full-submit_1.php");
      $endtime = microtime_float();
      $TimeTracker[] =$endtime - $starttime;
      $RealWait = array_sum($TimeTracker)/sizeof($TimeTracker);
      if ($LoopCount % $BurstSize == 0 && $CurIndex < sizeof($RecipArray[$RecipIndex]))  {
        $TimePassed = (time() - $StartBurst);
        if ($TimePassed < ($BurstTime*60))  {
          $WaitBurst = ($BurstTime*60) -$TimePassed;
          wa_sleep($WaitBurst);
        }
        else  {
          $TimeRemaining = ($TotalEmails- $LoopCount)*$RealWait;
        }
        $StartBurst = time();
      }
      if (!$EnteredValue)  {
        $RecipArray[$RecipIndex][0] =  mysql_fetch_assoc($RecipArray[$RecipIndex][1]);
      }
    }
    $RecipIndex ++;
  }
  $_SESSION[$EmailRef."_Total"] = 0;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = 0;
  session_commit();
  session_start();
  if ($GoToPage!="")     {
    header("Location: ".$GoToPage);
  }
}
?>

<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - online application - full member - Submit application</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />
</head>

<body>
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1><span class="normalText">Online application:</span><br />
    EABP <strong><?php if (($_COOKIE['type']) =="full") { ?>full<?php } else { ?>candidate<?php }?></strong> membership</h1>
  <div id="leftSidebar">
    <p>I wish to apply to EABP for <?php if (($_COOKIE['type']) =="full") { ?>full<?php } else { ?>candidate<?php }?> membership and confirm that the information I have given here is correct.</p>
    <p>I herewith declare that I have read and understood the Ethics Guidelines and will endorse them for my professional practice.</p>
    <p>As a <?php if (($_COOKIE['type']) =="full") { ?>full<?php } else { ?>candidate<?php }?> individual member of the EABP I commit to taking care of my own continued professional development (CPD) in order to maintain and develop my professional competency. </p>
   <?php if (($_COOKIE['type']) =="full") { ?>
    <p><strong>By submitting my full membership application <a href="https://www.eabp.org/docs/2016EABPEthicsGuidelines.pdf" target="_blank"> I agree to apply the EABP ethical code in my practice</a></strong>.</p>
    <?php }?>
     <?php if($row_rsApp['submitted'] ==0) {?>    
    <div class="terms">By submitting your application you agree to the terms stated above</div>

    <form action="<?php echo $editFormAction; ?>" method="POST" name="frmApplication" id="frmApplication">
    <table border="0" cellpadding="0" cellspacing="2" id="tblApplication">
      <tr>
        <td align="center" valign="middle" scope="row"><input name="aid" type="hidden" id="aid" value="<?php echo $row_rsApp['aid']; ?>">          <input name="submitted" type="hidden" id="submitted" value="1">        <input name="datesubmitted" type="hidden" id="datesubmitted" value="<?php echo date('Y-m-d H:i:s') ; ?>">        <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit application for <?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?>" /></td>
        </tr>
    </table>
    <input type="hidden" name="MM_update" value="frmApplication">
    </form>
    <?php }?>    
    <?php if($row_rsApp['submitted'] ==1) {?>
    <p class="alertRed">Thank you. Your application has been submitted. We will send you an email to acknowledge receipt.</p>
    <?php }?>
</div>
  <div id="rightSidebar">
  <p><a href="logout.php">Logout</a></p>

    <h3 class="allCaps alert"><?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?></h3>
    <ul>
      <li><a href="full.php">Section 1 - Personal Data</a></li>
      <li><a href="full2.php">Section 2 - Training</a></li>
      <li><a href="full3.php">Section 3 - Personal Psychotherapy</a></li>
      <li><a href="full4.php">Section 4 - Professional Supervision </a></li>
      <li><a href="full5.php">Section 5 - Professional Practice</a></li>
      <li><a href="full6.php">Section 6 - Membership of other Professional Bodies</a></li>
      <li><a href="full7.php">Section 7 - References</a></li>
      <li><a href="full8.php">Section 8 - Documents</a></li>
      <li><a href="full-review.php">Review Application</a></li>
      <li><strong>Submit Application</strong></li>
    </ul>
  </div>
  </div>
<div id="contentBottom">&nbsp;</div>
</div>

</div>
</body>

</html>
<?php
mysql_free_result($rsApp);
?>
