<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmApplication")) {
  $updateSQL = sprintf("UPDATE applications SET trainbp=%s, trainbp2=%s, trainbp3=%s, trainbptrainers=%s, trainbpyears=%s, trainbphours=%s, trainother=%s, trainothertrainers=%s, trainotheryears=%s, trainotherhours=%s, traincourses=%s, traincourseshours=%s, traintotal=%s WHERE aid=%s",
                       GetSQLValueString($_POST['trainbp'], "text"),
                       GetSQLValueString($_POST['trainbp2'], "text"),
                       GetSQLValueString($_POST['trainbp3'], "text"),
                       GetSQLValueString($_POST['trainbptrainers'], "text"),
                       GetSQLValueString($_POST['trainbpyears'], "text"),
                       GetSQLValueString($_POST['trainbphours'], "text"),
                       GetSQLValueString($_POST['trainother'], "text"),
                       GetSQLValueString($_POST['trainothertrainers'], "text"),
                       GetSQLValueString($_POST['trainotheryears'], "text"),
                       GetSQLValueString($_POST['trainotherhours'], "text"),
                       GetSQLValueString($_POST['traincourses'], "text"),
                       GetSQLValueString($_POST['traincourseshours'], "text"),
					   GetSQLValueString($_POST['traintotal'], "text"),
                       GetSQLValueString($_POST['aid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "full3.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT * FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - online application</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />
</head>

<body>
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1><span class="normalText">Online application:</span><br />
    EABP <strong><?php if (($_COOKIE['type']) =="full") { ?>full<?php } else { ?>candidate<?php }?></strong> membership</h1>
  <div id="leftSidebar">
    <p>You can <strong>save each section</strong> and return at a later date to complete your application.</p>
    <p><span class="crimsonText">*</span> = required</p>
    <h2>Section 2 - Training</h2>
    <p>Please note:</p>
    <ul>
      <li>600 hours required over at least four years</li>
      <li>At least 400 hours  in body-psychotherapy</li>
    </ul>
    <form action="<?php echo $editFormAction; ?>" method="POST" name="frmApplication" id="frmApplication">
      <table border="0" cellpadding="0" cellspacing="2" id="tblApplication">
      <tr>
        <td colspan="2" class="rowHeader" scope="row">2.1.  Training in body psychotherapy</td>
      </tr>
      <tr>
        <td colspan="2" scope="row">There is space for <strong> three</strong> institutes and modalities of body   psychotherapy. <br>
          <span class="text11px">Please include <em>for each</em>: <br>
Contact person<br>
Email address<br>
Website<br>
Modality of body psychotherapy</span> <br></td>
      </tr>
      <tr>
        <td colspan="2" valign="top" scope="row"><h4>Training institute 1</h4>
         
            <textarea name="trainbp" class="textAreaLong" id="trainbp"><?php echo $row_rsApp['trainbp']; ?></textarea>
          </td>
        </tr>
<tr>
        <td colspan="2" valign="top" scope="row"><h4>Training institute 2 (optional)</h4>
<textarea name="trainbp2" class="textAreaLong" id="trainbp2"><?php echo $row_rsApp['trainbp2']; ?></textarea></td>
        </tr>
        <tr>
        <td colspan="2" valign="top" scope="row"><h4>Training institute 3 (optional)</h4>
<textarea name="trainbp3" class="textAreaLong" id="trainbp3"><?php echo $row_rsApp['trainbp3']; ?></textarea></td>
        </tr>
      <tr>
        <th scope="row">Main trainers <span class="crimsonText">*</span></th>
        <td><input name="trainbptrainers" type="text" id="trainbptrainers" value="<?php echo $row_rsApp['trainbptrainers']; ?>"></td>
      </tr>
      <tr>
        <th scope="row">How many years did you train? (give dates)</th>
        <td><textarea name="trainbpyears" id="trainbpyears"><?php echo $row_rsApp['trainbpyears']; ?></textarea></td>
      </tr>
      <tr>
        <th colspan="2" scope="row"><span class="hoursLabel">Total number of hours of professional training in body psychotherapy.</span>          <input name="trainbphours" type="text" class="input20" id="trainbphours" value="<?php echo $row_rsApp['trainbphours']; ?>"  onkeyup="sum();" ></th>
        </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">2.2. Training in other modalities of psychotherapy</td>
        </tr>
      <tr class="highlighted">
        <td colspan="2" scope="row">Training institute(s): <br>
          <span class="text11px"> Please include <em>for each</em>: <br>
Contact person<br>
Email address<br>
Website<br>
Modality of  psychotherapy</span></td>
      </tr>
      <tr class="highlighted">
        <td colspan="2" valign="top" scope="row"><textarea name="trainother" class="highlighted textAreaLong" id="trainother"><?php echo $row_rsApp['trainother']; ?></textarea></td>
        </tr>
      <tr class="highlighted">
        <th valign="top" scope="row">Trainers: </th>
        <td>          <input name="trainothertrainers" type="text" id="trainothertrainers" value="<?php echo $row_rsApp['trainothertrainers']; ?>">        </td>
      </tr>
      <tr class="highlighted">
        <th valign="top" scope="row">How many years did you train? (give dates)</th>
        <td>
          <textarea name="trainotheryears" id="trainotheryears"><?php echo $row_rsApp['trainotheryears']; ?></textarea>        </td>
      </tr>
      <tr class="highlighted">
        <th colspan="2" scope="row"><span class="hoursLabel">Total number of hours training?</span>
<input name="trainotherhours" type="text" class="input20" id="trainotherhours" value="<?php echo $row_rsApp['trainotherhours']; ?>"  onkeyup="sum();" > 
(enter 0 if not applicable)   </th>
        </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">2.3  Courses/workshops for professional therapists </td>
        </tr>
      <tr class="highlighted">
        <th valign="top" scope="row">Which other training activities for  psychotherapists did you attend? </th>
        <td>
          <textarea name="traincourses" id="traincourses"><?php echo $row_rsApp['traincourses']; ?></textarea>        </td>
      </tr>
      <tr class="highlighted">
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel">How many hours did these courses/workshops       include?</span>          <input name="traincourseshours" type="text" class="input20" id="traincourseshours" value="<?php echo $row_rsApp['traincourseshours']; ?>"  onkeyup="sum();" >
(enter 0 if not applicable) </th>
        </tr>
      <tr class="rowHeader">
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel allCaps">Total number of hours</span>
          <input name="traintotal" type="text" class="input20" id="traintotal" value="<?php echo $row_rsApp['traintotal']; ?>"   onkeyup="sum();" ></th>
      </tr>
      <tr>
        <th scope="row"><input name="aid" type="hidden" id="aid" value="<?php echo $row_rsApp['aid']; ?>"></th>
        <td><input type="submit" name="btnSubmit" id="btnSubmit" value="Save and continue" /></td>
      </tr>
    </table>
      <input type="hidden" name="MM_update" value="frmApplication">
    </form>
    <p>&nbsp;</p>
    <p><a href="#top" class="top">Back to top</a></p>
  </div>
  <div id="rightSidebar">
  <p><a href="logout.php">Logout</a></p>
  <h3 class="allCaps alert"><?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?></h3>
    <ul>
      <li><a href="full.php">Section 1 - Personal Data</a></li>
      <li><strong>Section 2 - Training</strong></li>
      <li><a href="full3.php">Section 3 - Personal Psychotherapy</a></li>
      <li><a href="full4.php">Section 4 - Professional Supervision </a></li>
      <li><a href="full5.php">Section 5 - Professional Practice</a></li>
      <li><a href="full6.php">Section 6 - Membership of other Professional Bodies</a></li>
      <li><a href="full7.php">Section 7 - References</a></li>
      <li><a href="full8.php">Section 8 - Documents</a></li>
      <li><a href="full-review.php">Review Application</a></li>
<li><a href="full-submit.php">Submit Application</a></li>
    </ul>
    <hr>
    <h3>Graduates of <span class="allCaps">Forum</span> Training Institutes</h3>
    <p><span class="highlighted">Highlighted</span> questions are <span class="allCaps"><strong>not</strong></span> required if you are a graduate of an <a href="../forum.php">EABP <span class="allCaps">forum</span> training institute</a>.</p>
    </div>
  </div>
<div id="contentBottom">&nbsp;</div>
</div>


</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
function sum() {
            var txtFirstNumberValue = document.getElementById('trainbphours').value;
            var txtSecondNumberValue = document.getElementById('trainotherhours').value;
            var txtThirdNumberValue = document.getElementById('traincourseshours').value;
            var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue) + parseFloat(txtThirdNumberValue);
            if (!isNaN(result)) {
                document.getElementById('traintotal').value = result;
            }
        }
</script>
</html>
<?php
mysql_free_result($rsApp);
?>
