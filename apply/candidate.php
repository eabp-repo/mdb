<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmApplication")) {
  $updateSQL = sprintf("UPDATE applications SET doa=%s, dob=%s, email=%s, firstname=%s, gender=%s, homecity=%s, homephone=%s, homepostcode=%s, homeadd1=%s, lastname=%s, professions=%s, webpage=%s, nationality=%s, cid=%s, tid=%s, education=%s, langs=%s, reasonapply=%s, submitted=%s, datesubmitted=%s WHERE aid=%s",
                       GetSQLValueString($_POST['doa'], "text"),
                       GetSQLValueString($_POST['dob'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['firstname'], "text"),
                       GetSQLValueString($_POST['gender'], "text"),
                       GetSQLValueString($_POST['homecity'], "text"),
                       GetSQLValueString($_POST['homephone'], "text"),
                       GetSQLValueString($_POST['homepostcode'], "text"),
                       GetSQLValueString($_POST['homeadd1'], "text"),
                       GetSQLValueString($_POST['lastname'], "text"),
                       GetSQLValueString($_POST['professions'], "text"),
                       GetSQLValueString($_POST['webpage'], "text"),
                       GetSQLValueString($_POST['nationality'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['tid'], "int"),
                       GetSQLValueString($_POST['education'], "text"),
                       GetSQLValueString($_POST['langs'], "text"),
                       GetSQLValueString($_POST['reasonapply'], "text"),
                       GetSQLValueString($_POST['submitted'], "int"),
                       GetSQLValueString($_POST['datesubmitted'], "date"),
                       GetSQLValueString($_POST['aid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "../apply-thanks.php?tid=4";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNAs = "SELECT acronym, country, national.naid, flag FROM national INNER JOIN country ON national.cid = country.cid WHERE live = 1 AND committee <> 1 AND naid <> 13 ORDER BY country";
$rsNAs = mysql_query($query_rsNAs, $connEABP2) or die(mysql_error());
$row_rsNAs = mysql_fetch_assoc($rsNAs);
$totalRows_rsNAs = mysql_num_rows($rsNAs);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

$colname_rsApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT * FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - online application - candidate member</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-224881-50']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<link rel="stylesheet" type="text/css" href="../datetimepicker-master/jquery.datetimepicker.css"/>
</head>

<body>
<div id="wrapper"></div>
<div id="colMiddle">
  <div id="contentTop">
    <h1>Online application:<br />
    EABP <strong>candidate</strong> membership</h1>
  <div id="leftSidebar">
    <p>Please inform yourself about the <a href="http://www.eabp.org/membership-categories.php">EABP Membership Criteria  and Conditions</a> and use the instructions for completing this application  form.</p>
    <p>Once your application has been accepted you will be sent an invoice for the yearly membership fee of 50 Euros. After paying the fee you will be entered in the membership database on the EABP (and/or National Association) website(s).</p>
    <p><span class="crimsonText">*</span> = required</p>
          <?php if ($row_rsApp['submitted'] !=0) { ?>

      <p class="highlighted"><strong>You have already submitted your application.</strong></p>
      <?php } ?>
    <form action="<?php echo $editFormAction; ?>" method="POST" name="frmApplication" id="frmApplication">
    <table border="0" cellpadding="0" cellspacing="2" id="tblApplication">
      <tr>
        <th scope="row">First name <span class="crimsonText">*</span></th>
        <td><input name="firstname" type="text" required id="firstname" value="<?php echo $row_rsApp['firstname']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Last name <span class="crimsonText">*</span></th>
        <td><input name="lastname" type="text" required id="lastname" value="<?php echo $row_rsApp['lastname']; ?>" /></td>
      </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">Contact details</td>
        </tr>
      <tr>
        <th scope="row">Address <span class="crimsonText">*</span></th>
        <td><input name="homeadd1" type="text" required id="homeadd1" value="<?php echo $row_rsApp['homeadd1']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Town/City <span class="crimsonText">*</span></th>
        <td><input name="homecity" type="text" required id="homecity" value="<?php echo $row_rsApp['homecity']; ?>"  /></td>
      </tr>
      <tr>
        <th scope="row">Post code <span class="crimsonText">*</span></th>
        <td><input name="homepostcode" type="text" required id="homepostcode" value="<?php echo $row_rsApp['homepostcode']; ?>"  /></td>
      </tr>
      <tr>
        <th scope="row">Country <span class="crimsonText">*</span></th>
        <td><select name="cid" id="cid">
          <?php
do {  
?>
          <option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsApp['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option>
          <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>

        </select></td>
      </tr>
      <tr>
        <th scope="row">Telephone <span class="crimsonText">*</span></th>
        <td><input name="homephone" type="text" required id="homephone" value="<?php echo $row_rsApp['homephone']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Email <span class="crimsonText">*</span></th>
        <td><input name="email" type="email" required id="email" value="<?php echo $row_rsApp['email']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Website<br />
          <span class="normal">no http://</span></th>
        <td><input name="webpage" type="text" id="webpage" value="<?php echo $row_rsApp['webpage']; ?>" /></td>
      </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">Personal details</td>
        </tr>
      <tr>
        <th scope="row">Date of birth <span class="crimsonText">*</span></th>
        <td><input name="dob" type="text" required id="dob" value="<?php echo $row_rsApp['dob']; ?>"  /></td>
      </tr>
      <tr>
        <th scope="row">Gender <span class="crimsonText">*</span></th>
        <td>
          <label>
            <input <?php if (!(strcmp($row_rsApp['gender'],"Male"))) {echo "checked=\"checked\"";} ?> name="gender" type="radio" class="radioButton" id="gender_0" value="Male" />
            male</label>
          <label>
            <input <?php if (!(strcmp($row_rsApp['gender'],"Female"))) {echo "checked=\"checked\"";} ?> name="gender" type="radio" class="radioButton" id="gender_1" value="Female" />
            female</label>
             <label>
            <input <?php if (!(strcmp($row_rsApp['gender'],"Other"))) {echo "checked=\"checked\"";} ?> name="gender" type="radio" class="radioButton" id="gender_2" value="Other" />
            other</label>
    
        </td>
      </tr>
      <tr>
        <th scope="row">Nationality <span class="crimsonText">*</span></th>
        <td><input name="nationality" type="text" required id="nationality" value="<?php echo $row_rsApp['nationality']; ?>" /></td>
      </tr>
      <tr>
        <th scope="row">Languages spoken</th>
        <td><input name="langs" type="text" id="langs" value="<?php echo $row_rsApp['langs']; ?>" /></td>
      </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">Professional/Education</td>
        </tr>
      <tr>
        <th scope="row">Profession(s)</th>
        <td><textarea name="professions" cols="40" rows="3" id="professions"><?php echo $row_rsApp['professions']; ?></textarea></td>
      </tr>
      <tr>
        <th scope="row">Other education</th>
        <td><textarea name="education" cols="40" rows="3" id="education"><?php echo $row_rsApp['education']; ?></textarea></td>
      </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">Reasons for applying</td>
        </tr>
      <tr>
        <th scope="row">Please say why you want to join the EABP</th>
        <td><textarea name="reasonapply" id="reasonapply" cols="40" rows="8"><?php echo $row_rsApp['reasonapply']; ?></textarea></td>
      </tr>
      <tr>
        <th scope="row"><input name="tid" type="hidden" id="tid" value="4" />
          <input name="showonsite" type="hidden" id="showonsite" value="0" />
          <input name="doa" type="hidden" id="doa" value="<?php echo date('Y-m-d') ; ?>">
          <input name="nid" type="hidden" id="nid" value="12">
          <input name="aid" type="hidden" id="aid" value="<?php echo $row_rsApp['aid']; ?>">
          <input name="submitted" type="hidden" id="submitted" value="1">
          <input name="datesubmitted" type="hidden" id="datesubmitted" value="<?php echo date('Y-m-d H:i:s') ; ?>"></th>
        <td>
              <?php if ($row_rsApp['submitted'] !=1) { ?>
        <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit application" />
        <?php } else { ?>
        <p class="highlighted">You have already submitted your application.</p>
        <?php } ?>

        </td>
      </tr>
    </table>
    <input type="hidden" name="MM_update" value="frmApplication">
    </form>
    <p>&nbsp;</p>
    <p><a href="#top" class="top">Back to top</a></p>
  </div>
  <div id="rightSidebar">
    <p><a href="logout.php">Logout</a></p>
  </div>
  </div>
<div id="contentBottom">&nbsp;</div>
</div>

</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../datetimepicker-master/build/jquery.datetimepicker.full.js"></script>
<script>/*
window.onerror = function(errorMsg) {
	$('#console').html($('#console').html()+'<br>'+errorMsg)
}*/

$.datetimepicker.setLocale('en');


$('#dob').datetimepicker({
	yearOffset:0,
	yearStart:1915,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
});

</script>
</html>
<?php
mysql_free_result($rsNAs);

mysql_free_result($rsCountries);

mysql_free_result($rsApp);
?>
