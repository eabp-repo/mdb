<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['email'])) {
  $loginUsername=$_POST['email'];
  $password=$_POST['webpassword'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "type.php";
  $MM_redirectLoginFailed = "login-failed.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_connEABP2, $connEABP2);
  
  $LoginRS__query=sprintf("SELECT email, webpassword FROM applications WHERE email=%s AND webpassword=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $connEABP2) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $loginStrGroup = "";
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>

<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - login - applying for membership</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />
<script>
if (navigator.cookieEnabled == 0) {
  alert("You need to enable cookies in your browser before you login.");
}
</script>
</head>

<body onLoad="myFunction()">
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1>Applying for membership</h1>
  <?php if (isset($_GET['v'])) { ?>
  <p class="alertRed">Thank you, your email address has been verified. Now please login to continue with your application.</p>
  <?php }?>
   <?php if (isset($_GET['p'])) { ?>
  <p class="alert">Your password has been sent to you. Please check your email.</p>
    <?php }?>
  <div id="leftSidebar">
    <h2>Login</h2>
<form ACTION="<?php echo $loginFormAction; ?>" method="POST" name="frmLogin" id="frmLogin">
  <table width="100%" border="0" cellspacing="5" id="tblApplication">
      <tr>
        <th scope="row">Email:</th>
        <td><input type="email" name="email" id="email" required></td>
      </tr>
      <tr>
        <th scope="row">Password:</th>
        <td><input type="password" name="webpassword" id="webpassword" required></td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <td><input type="submit" name="btnSubmit" id="btnSubmit" value="Login"></td>
      </tr>
    </table>
  </form>
  </div>
  <div id="rightSidebar">
    <h3>Forgotten password</h3>
    <p>Enter you email address below and we will send you a reminder.</p>
    <form action="password-mailer.php" method="post" id="frmForgotten">
      <p>
        <input type="email" name="email" id="email" required>
      </p>
      <p>
        <input type="submit" name="btnForgotten" id="btnForgotten" value="Submit">
      </p>
    </form>
    <hr>
    <p>Not yet registered to apply for membership? <a href="index.php">Register now &gt;</a></p>
  </div>
  </div>
</div>


</div>
</body>
</html>