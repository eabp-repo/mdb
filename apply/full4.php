<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmApplication")) {
  $updateSQL = sprintf("UPDATE applications SET superbp=%s, superbphoursind=%s, superbphoursgroup=%s, superbpnames=%s, superother=%s, superotherhoursind=%s, superotherhoursgroup=%s, superothernames=%s, supertotal=%s WHERE aid=%s",
                       GetSQLValueString($_POST['superbp'], "text"),
                       GetSQLValueString($_POST['superbphoursind'], "text"),
                       GetSQLValueString($_POST['superbphoursgroup'], "text"),
                       GetSQLValueString($_POST['superbpnames'], "text"),
                       GetSQLValueString($_POST['superother'], "text"),
                       GetSQLValueString($_POST['superotherhoursind'], "text"),
                       GetSQLValueString($_POST['superotherhoursgroup'], "text"),
                       GetSQLValueString($_POST['superothernames'], "text"),
                       GetSQLValueString($_POST['supertotal'], "text"),
                       GetSQLValueString($_POST['aid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "full5.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT * FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - online application - full member - Professional practice</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />

</head>

<body>
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1><span class="normalText">Online application:</span><br />
    EABP <strong><?php if (($_COOKIE['type']) =="full") { ?>full<?php } else { ?>candidate<?php }?></strong> membership</h1>
  <div id="leftSidebar">
    <p>You can <strong>save each section</strong> and return at a later date to complete your application.</p>
    <p><span class="crimsonText">*</span> = required</p>
    <h2>Section 4 - Professional Supervision</h2>
    <p>Please note: 75 hours required for full membership</p>
    <form action="<?php echo $editFormAction; ?>" method="POST" name="frmApplication" id="frmApplication">
      <table border="0" cellpadding="0" cellspacing="2" id="tblApplication">
      <tr>
        <td colspan="2" class="rowHeader" scope="row">4.1  In body psychotherapy outside the setting of the    training </td>
      </tr>
      <tr>
        <th valign="top" scope="row">In which modality (ies)?</th>
        <td><textarea name="superbp" id="superbp"><?php echo $row_rsApp['superbp']; ?></textarea></td>
      </tr>
      <tr>
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel">Number of hours of individual sessions</span>          <input name="superbphoursind" type="text" class="input20" id="superbphoursind" value="<?php echo $row_rsApp['superbphoursind']; ?>"   onkeyup="sum();" /></th>
        </tr>
      <tr>
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel">Number of hours of group sessions</span>          <input name="superbphoursgroup" type="text" class="input20" id="superbphoursgroup" value="<?php echo $row_rsApp['superbphoursgroup']; ?>"  onkeyup="sum();" /></th>
        </tr>
      <tr>
        <th valign="top" scope="row">Name of supervisors</th>
        <td><textarea name="superbpnames" id="superbpnames"><?php echo $row_rsApp['superbpnames']; ?></textarea></td>
      </tr>
      <tr>
        <td colspan="2" class="rowHeader" scope="row">4.2  In other  modalities</td>
      </tr>
      <tr>
        <th valign="top" scope="row">In which modality (ies)?</th>
        <td><textarea name="superother" id="superother"><?php echo $row_rsApp['superother']; ?></textarea></td>
      </tr>
      <tr>
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel">Number of hours of individual sessions</span>          <input name="superotherhoursind" type="text" class="input20" id="superotherhoursind" value="<?php echo $row_rsApp['superotherhoursind']; ?>"  onkeyup="sum();" /></th>
        </tr>
      <tr>
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel">Number of hours of group sessions</span>          <input name="superotherhoursgroup" type="text" class="input20" id="superotherhoursgroup" value="<?php echo $row_rsApp['superotherhoursgroup']; ?>"  onkeyup="sum();" /></th>
        </tr>
      <tr>
        <th scope="row">Name of supervisors</th>
        <td><textarea name="superothernames" id="superothernames"><?php echo $row_rsApp['superothernames']; ?></textarea></td>
      </tr>
      <tr>
        <th colspan="2" scope="row"><span class="hoursLabel allCaps">Total number of hours<br>
          <span class="normalText">(must be at least 75 hours for full membership)</span>        </span>
          <input name="supertotal" type="text" class="input20" id="supertotal" value="<?php echo $row_rsApp['supertotal']; ?>"></th>
        </tr>
      <tr>
        <th scope="row"><input name="aid" type="hidden" id="aid" value="<?php echo $row_rsApp['aid']; ?>"></th>
        <td><input type="submit" name="btnSubmit" id="btnSubmit" value="Save and continue" /></td>
      </tr>
    </table>
      <input type="hidden" name="MM_update" value="frmApplication">
    </form>
    <p>&nbsp;</p>
    <p><a href="#top" class="top">Back to top</a></p>
  </div>
  <div id="rightSidebar">
  <p><a href="logout.php">Logout</a></p>

    <h3 class="allCaps alert"><?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?></h3>
    <ul>
      <li><a href="full.php">Section 1 - Personal Data</a></li>
      <li><a href="full2.php">Section 2 - Training</a></li>
      <li><a href="full3.php">Section 3 - Personal Psychotherapy</a></li>
      <li><strong>Section 4 - Professional Supervision </strong></li>
      <li><a href="full5.php">Section 5 - Professional Practice</a></li>
      <li><a href="full6.php">Section 6 - Membership of other Professional Bodies</a></li>
      <li><a href="full7.php">Section 7 - References</a></li>
      <li><a href="full8.php">Section 8 - Documents</a></li>
      <li><a href="full-review.php">Review Application</a></li>
<li><a href="full-submit.php">Submit Application</a></li>
    </ul>
    <hr>
  </div>
  </div>
<div id="contentBottom">&nbsp;</div>
</div>

</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
function sum() {
            var txtFirstNumberValue = document.getElementById('superbphoursind').value;
            var txtSecondNumberValue = document.getElementById('superbphoursgroup').value;
            var txtThirdNumberValue = document.getElementById('superotherhoursind').value;
            var txtFourthNumberValue = document.getElementById('superotherhoursgroup').value;
            var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue) + parseFloat(txtThirdNumberValue) + parseFloat(txtFourthNumberValue);
            if (!isNaN(result)) {
                document.getElementById('supertotal').value = result;
            }
        }
</script>
</html>
<?php
mysql_free_result($rsApp);
?>
