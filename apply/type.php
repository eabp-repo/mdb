<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsApp = "-1";
if (isset($_SESSION['email'])) {
  $colname_rsApp = $_SESSION['email'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT aid, email, firstname, lastname FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);$colname_rsApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT aid, firstname, lastname, doa, submitted FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);

$locked = false;
if (!is_null($row_rsApp['doa']) or $row_rsApp['submitted'] == 1) {
  $locked=true;
} 

if (!$locked){

  if ($_POST['type'] == 'f') {
  header("Location:full.php");
  setcookie( "type", "full", time()+(86400 * 30));
  } else if ($_POST['type'] == 'a') {
  header("Location:associate.php");	
  } else if ($_POST['type'] == 'c') {
  header("Location:full.php");	
  setcookie( "type", "candidate", time()+(86400 * 30));
  } else if ($_POST['type'] == 's') {
  header("Location:student.php"); 
  } else if ($_POST['type'] == 'o') {
  header("Location:organisation.php"); 
  }

}
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - apply for membership</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application.css" rel="stylesheet" type="text/css" />
</head>

<body>
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1>Apply for membership</h1>
  <div id="leftSidebar">
<?php  if ($locked):?>
  <p>Your application has already been submitted.</p>
<?php else:?>
    <p>Choose the type of membership you are applying for:</p>
<form action="" method="POST" name="frmType" id="frmType">
  <table width="100%" border="0" cellspacing="5" id="tblApplication">
      <tr>
        <td width="299"><p>
          <label>
            <input name="type" type="radio" class="rb16" id="type_0" value="f">
            Full</label>
          <br>
                <label>
            <input name="type" type="radio" class="rb16" id="type_1" value="c">
            Candidate</label>
          <br>
          <label>
            <input name="type" type="radio" class="rb16" id="type_2" value="a">
            Associate</label>
          <br>
          <label>
            <input name="type" type="radio" class="rb16" id="type_3" value="s">
            Student</label>
          <br>
          <label>
            <input name="type" type="radio" class="rb16" id="type_4" value="o">
            Organisation</label>
        </p>
        </td>
      </tr>
      <tr>
        <td><input type="submit" name="btnSubmit" id="btnSubmit" value="Continue">
          <input name="email" type="hidden" id="email" value="<?php echo $_SESSION['MM_Username']; ?>"></td>
      </tr>
    </table>
  </form>
<?php endif;?>

  </div>
  <div id="rightSidebar">
    <p><a href="logout.php">Logout</a></p>
    <h3 class="allCaps alert"><?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?></h3>
  </div>
  </div>
<div id="contentBottom">&nbsp;</div>
</div>

</div>
</body>

</html>
<?php
mysql_free_result($rsApp);
?>
