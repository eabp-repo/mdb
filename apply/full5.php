<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmApplication")) {
  $updateSQL = sprintf("UPDATE applications SET practicebp=%s, practicebpind=%s, practicebpgroup=%s, practicebptotal=%s, practicebptherapy=%s, practicebpdetails=%s, practiceotherhours=%s, practiceotherind=%s, practiceothergroup=%s, practiceothertotal=%s, practiceotherdetails=%s, whenfulfil=%s WHERE aid=%s",
                       GetSQLValueString($_POST['practicebp'], "text"),
                       GetSQLValueString($_POST['practicebpind'], "text"),
                       GetSQLValueString($_POST['practicebpgroup'], "text"),
                       GetSQLValueString($_POST['practicebptotal'], "text"),
                       GetSQLValueString($_POST['practicebptherapy'], "text"),
                       GetSQLValueString($_POST['practicebpdetails'], "text"),
                       GetSQLValueString($_POST['practiceotherhours'], "text"),
                       GetSQLValueString($_POST['practiceotherind'], "text"),
                       GetSQLValueString($_POST['practiceothergroup'], "text"),
                       GetSQLValueString($_POST['practiceothertotal'], "text"),
                       GetSQLValueString($_POST['practiceotherdetails'], "text"),
					   GetSQLValueString($_POST['whenfulfil'], "text"),
                       GetSQLValueString($_POST['aid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "full6.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT * FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - online application - full member - Professional practice</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />

</head>

<body>
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1><span class="normalText">Online application:</span><br />
    EABP <strong><?php if (($_COOKIE['type']) =="full") { ?>full<?php } else { ?>candidate<?php }?></strong> membership</h1>
  <div id="leftSidebar">
    <p>You can <strong>save each section</strong> and return at a later date to complete your application.</p>
    <p><span class="crimsonText">*</span> = required</p>
    <h2>Section 5 - Professional Practice</h2>
    <p>Please note:</p>
    <ul>
      <li>400 hours over at least 2 years for full membership</li>
    </ul>
    <form action="<?php echo $editFormAction; ?>" method="POST" name="frmApplication" id="frmApplication">
      <table width="100%" border="0" cellpadding="0" cellspacing="2" id="tblApplication">
      <tr>
        <td colspan="2" class="rowHeader" scope="row">5.1 Professional practice as a body psychotherapist</td>
      </tr>
      <tr>
        <th colspan="2" valign="top" scope="row">Please describe your current work (any special client      groups and work load: e.g. clinic. individual practice, groups etc.; ie working with children, disabled persons) <span class="crimsonText">*</span></th>
        </tr>
      <tr>
        <td colspan="2" valign="top" scope="row"><textarea name="practicebp" class="textAreaLong" id="practicebp"><?php echo $row_rsApp['practicebp']; ?></textarea></td>
        </tr>
      <tr>
        <th colspan="2" scope="row"><span class="hoursLabel">Hours of individual sessions *
        </span>
          <input name="practicebpind" type="text" class="input20" id="practicebpind" value="<?php echo $row_rsApp['practicebpind']; ?>"  onkeyup="sum();" ></th>
        </tr>
      <tr>
        <th colspan="2" scope="row"><span class="hoursLabel">Hours of group sessions *</span>          <input name="practicebpgroup" type="text" class="input20" id="practicebpgroup" value="<?php echo $row_rsApp['practicebpgroup']; ?>"  onkeyup="sum();" ></th>
        </tr>
      <tr>
        <th colspan="2" scope="row"><span class="hoursLabel allCaps">Total hours *</span>          <input name="practicebptotal" type="text" class="input20" id="practicebptotal" value="<?php echo $row_rsApp['practicebptotal']; ?>"></th>
      </tr>
      <tr>
        <th colspan="2" scope="row"><span class="hoursLabel">What do you call the therapy that you practice?  *</span>
          <textarea name="practicebptherapy" id="practicebptherapy"><?php echo $row_rsApp['practicebptherapy']; ?></textarea></th>
        </tr>
      <tr>
        <th colspan="2" valign="top" scope="row">Please give dates and details about the setting (e.g.              as an individual practitioner, to trainees on training courses, in a health clinic, etc.) <span class="crimsonText">*</span></th>
        </tr>
      <tr>
        <td colspan="2" valign="top" scope="row"><textarea name="practicebpdetails" class="textAreaLong" id="practicebpdetails"><?php echo $row_rsApp['practicebpdetails']; ?></textarea></td>
      </tr>
      <tr class="highlighted">
        <td colspan="2" class="rowHeader" scope="row">5.2  In other psychotherapy modalities</td>
      </tr>
      <tr class="highlighted">
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel">How many hours of paid professional practice in other modalities of psychotherapy have you given?</span>          <input name="practiceotherhours" type="text" class="input20" id="practiceotherhours" value="<?php echo $row_rsApp['practiceotherhours']; ?>"  onkeyup="sum2();" ></th>
        </tr>
      <tr class="highlighted">
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel">Number of hours of individual sessions</span>          <input name="practiceotherind" type="text" class="input20" id="practiceotherind" value="<?php echo $row_rsApp['practiceotherind']; ?>"  onkeyup="sum2();" ></th>
        </tr>
      <tr class="highlighted">
        <th colspan="2" valign="top" scope="row"><span class="hoursLabel">Number of hours of group sessions</span>          <input name="practiceothergroup" type="text" class="input20" id="practiceothergroup" value="<?php echo $row_rsApp['practiceothergroup']; ?>"  onkeyup="sum2();" ></th>
        </tr>
      <tr class="highlighted">
        <th colspan="2" scope="row"><span class="hoursLabel allCaps">Total hours *</span>
          <input name="practiceothertotal" type="text" class="input20" id="practiceothertotal" value="<?php echo $row_rsApp['practiceothertotal']; ?>"></th>
      </tr>
      <tr class="highlighted">
        <th colspan="2" valign="top" scope="row">Please give dates and details about the setting (e.g.        in a health clinic, as an individual practitioner, to       trainees on training courses etc.)</th>
        </tr>
      <tr class="highlighted">
        <td colspan="2" valign="top" scope="row"><textarea name="practiceotherdetails" class="textAreaLong" id="practiceotherdetails"><?php echo $row_rsApp['practiceotherdetails']; ?></textarea></td>
        </tr>
 <?php if (($_COOKIE['type']) =="full") { ?> 
 <input name="whenfulfil" type="text" id="whenfulfil" value="<?php echo $row_rsApp['whenfulfil']; ?>" class="whenfulfil">
 
 <?php } else { ?>      
        
      <tr>
        <th colspan="2" class="rowHeader" scope="row">When do you expect to fulfil the criteria for full membership?</th>
        </tr>
      <tr>
        <th colspan="2" scope="row"><textarea name="whenfulfil" required id="whenfulfil"><?php echo $row_rsApp['whenfulfil']; ?></textarea></th>
        </tr>
      <tr>
 <?php }?>  
    
        <th width="221" scope="row"><input name="aid" type="hidden" id="aid" value="<?php echo $row_rsApp['aid']; ?>"></th>
        <td width="275"><input type="submit" name="btnSubmit" id="btnSubmit" value="Save and continue" /></td>
      </tr>
    </table>
      <input type="hidden" name="MM_update" value="frmApplication">
    </form>
    <p>&nbsp;</p>
    <p><a href="#top" class="top">Back to top</a></p>
  </div>
  <div id="rightSidebar">
  <p><a href="logout.php">Logout</a></p>

    <h3 class="allCaps alert"><?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?></h3>
    <ul>
      <li><a href="full.php">Section 1 - Personal Data</a></li>
      <li><a href="full2.php">Section 2 - Training</a></li>
      <li><a href="full3.php">Section 3 - Personal Psychotherapy</a></li>
      <li><a href="full4.php">Section 4 - Professional Supervision </a></li>
      <li><strong>Section 5 - Professional Practice</strong></li>
      <li><a href="full6.php">Section 6 - Membership of other Professional Bodies</a></li>
      <li><a href="full7.php">Section 7 - References</a></li>
      <li><a href="full8.php">Section 8 - Documents</a></li>
      <li><a href="full-review.php">Review Application</a></li>
<li><a href="full-submit.php">Submit Application</a></li>
    </ul>
    <hr>
    <h3>Graduates of <span class="allCaps">Forum</span> Training Institutes</h3>
    <p><span class="highlighted">Highlighted</span> questions are <span class="allCaps"><strong>not</strong></span> required if you are a graduate of an <a href="../forum.php">EABP <span class="allCaps">forum</span> training institute</a>.</p>
  </div>
  </div>
<div id="contentBottom">&nbsp;</div>
</div>


</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
function sum() {
            var txtFirstNumberValue = document.getElementById('practicebpind').value;
            var txtSecondNumberValue = document.getElementById('practicebpgroup').value;

            var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue);
            if (!isNaN(result)) {
                document.getElementById('practicebptotal').value = result;
            }
        }
function sum2() {
            var txtFirstNumberValue = document.getElementById('practiceotherhours').value;
            var txtSecondNumberValue = document.getElementById('practiceotherind').value;
			var txtThirdNumberValue = document.getElementById('practiceothergroup').value;
            var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue) + parseFloat(txtThirdNumberValue);
            if (!isNaN(result)) {
                document.getElementById('practiceothertotal').value = result;
            }
        }		
</script>
</html>
<?php
mysql_free_result($rsApp);
?>
