<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsOrgApp = "-1";
if (isset($_GET['orgid'])) {
  $colname_rsOrgApp = $_GET['orgid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgApp = sprintf("SELECT * FROM applicationorgs WHERE orgid = %s", GetSQLValueString($colname_rsOrgApp, "int"));
$rsOrgApp = mysql_query($query_rsOrgApp, $connEABP2) or die(mysql_error());
$row_rsOrgApp = mysql_fetch_assoc($rsOrgApp);
$totalRows_rsOrgApp = mysql_num_rows($rsOrgApp);

$colname_rsDocs = "-1";

  $colname_rsDocs = $row_rsOrgApp['orgid'];

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocs = sprintf("SELECT appdocid, doctitle, docfile FROM apporgdocs WHERE orgid = %s", GetSQLValueString($colname_rsDocs, "int"));
$rsDocs = mysql_query($query_rsDocs, $connEABP2) or die(mysql_error());
$row_rsDocs = mysql_fetch_assoc($rsDocs);
$totalRows_rsDocs = mysql_num_rows($rsDocs);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>EABP application - <?php echo $row_rsOrgApp['orgname']; ?></title>
<style type="text/css">
body {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 14px;
	width: 80%;
	margin-right: auto;
	margin-left: auto;
	margin-top: 20px;
	margin-bottom: 20px;
}
h1{
	color: #AC3556;

}
h2{
	color: #AC3556;
	padding-top: 10px;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #AC3556;
	margin-top: 20px;
}
h3    {
	margin: 0px;
	padding: 5px;
	background-color: #E8E8E8;
	font-weight: normal;

}

th {
	font-weight: normal;
	text-align: left;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #CCCCCC;
	width: 300px;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC;
}
td {
	padding: 4px;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #CCCCCC;
	font-weight: normal;
	font-style: italic;
}
.highlighted {
	background-color: #FFFF66;
}
.close {
	display: block;
	float: right;
	text-transform: uppercase;
	text-decoration: none;
}
</style>
</head>

<body id="top">
<p>EABP  membership application for <strong><?php echo $row_rsOrgApp['orgname']; ?></strong><span class="close"><a href="javascript:window.close()">Close</a></span></p>
</form>
    <?php if($row_rsOrgApp['submitted'] ==1) {?>
<p><strong>Submitted on <?php echo date("j M Y",strtotime($row_rsOrgApp['datesubmitted'])); ?> at <?php echo date("g:i a",strtotime($row_rsOrgApp['datesubmitted'])); ?> (GMT).</strong></p>
<?php } ?>
<h1>Review application </h1>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblApplication">
  <tr>
    <th colspan="2" scope="row"><h3>Contact details</h3></th>
  </tr>
  <tr>
    <th scope="row">Contact</th>
    <td><?php echo $row_rsOrgApp['contact']; ?></td>
  </tr>
  <tr>
    <th scope="row">Contact function</th>
    <td><?php echo $row_rsOrgApp['contactfunction']; ?></td>
  </tr>
  <tr>
    <th scope="row">Email</th>
    <td><?php echo $row_rsOrgApp['email']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>Organisation details</h3></th>
  </tr>
  <tr>
    <th scope="row">Organisation name</th>
    <td><?php echo $row_rsOrgApp['orgname']; ?></td>
  </tr>
  <tr>
    <th scope="row">Address</th>
    <td><?php echo $row_rsOrgApp['address']; ?></td>
  </tr>
  <tr>
    <th scope="row">City</th>
    <td><?php echo $row_rsOrgApp['city']; ?></td>
  </tr>
  <tr>
    <th scope="row">Post code</th>
    <td><?php echo $row_rsOrgApp['postcode']; ?></td>
  </tr>
  <tr>
    <th scope="row">Telephone</th>
    <td><?php echo $row_rsOrgApp['telephone']; ?></td>
  </tr>
  <tr>
    <th scope="row">Website<br /></th>
    <td><?php echo $row_rsOrgApp['website']; ?></td>
  </tr>
  </table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th align="right">Sub-modality</th>
    <td><?php echo $row_rsOrgApp['submodality']; ?></td>
  </tr>
  <tr>
    <th align="right">How many years have you been in existence?</th>
    <td><?php echo $row_rsOrgApp['yearsexistence']; ?></td>
  </tr>
  <tr>
    <th align="right">What is your legal organisational (or corporate) structure?</th>
    <td><?php echo $row_rsOrgApp['legalstructure']; ?></td>
  </tr>
  <tr>
    <th align="right">Do you have an appropriate code of ethics and complaints procedure?</th>
    <td valign="middle"><?php echo $row_rsOrgApp['codeofethics']; ?></td>
  </tr>
  <tr>
    <th align="right">Type of organisation</th>
    <td><?php echo $row_rsOrgApp['type']; ?></td>
  </tr>
  <tr>
    <th align="right">EABP members</th>
    <td><?php echo $row_rsOrgApp['eabpmembers']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right"><strong>Training organisations only:</strong></th>
  </tr>
  <tr>
    <th align="right">About how many people  have you trained?  </th>
    <td><?php echo $row_rsOrgApp['totaltrained']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right">How many people are currently involved in the  organisation?</th>
  </tr>
  <tr>
    <td align="right">Trainers:</td>
    <td><?php echo $row_rsOrgApp['notrainers']; ?></td>
  </tr>
  <tr>
    <td align="right">Supervisors:</td>
    <td><?php echo $row_rsOrgApp['nosupervisors']; ?></td>
  </tr>
  <tr>
    <td align="right">Admin staff:</td>
    <td><?php echo $row_rsOrgApp['noadminstaff']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right">How many trainees do  you have in the:</th>
  </tr>
  <tr>
    <td align="right">First year?</td>
    <td><?php echo $row_rsOrgApp['notraineesfirst']; ?></td>
  </tr>
  <tr>
    <td align="right">Second year?</td>
    <td><?php echo $row_rsOrgApp['notraineessecond']; ?></td>
  </tr>
  <tr>
    <td align="right"> Third year?</td>
    <td><?php echo $row_rsOrgApp['notraineesthird']; ?></td>
  </tr>
  <tr>
    <td align="right">Fourth year?</td>
    <td><?php echo $row_rsOrgApp['notraineesfourth']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right"><strong>Professional  associations only:</strong></th>
  </tr>
  <tr>
    <th align="right">How many members do  you have?</th>
    <td><?php echo $row_rsOrgApp['totalmembers']; ?></td>
  </tr>
  <tr>
    <th align="right">Among your members how many practitioners do you  have who fulfil the membership criteria of EABP or something similar?</th>
    <td valign="middle"><?php echo $row_rsOrgApp['nofulfilcriteria']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right"><strong>Other organisations only:</strong></th>
  </tr>
  <tr>
    <th align="right">Details:</th>
    <td><?php echo $row_rsOrgApp['otherorgdetails']; ?></td>
  </tr>
</table>

<h2>Documentation </h2>
<?php if ($totalRows_rsDocs > 0) { // Show if recordset not empty ?>
  <h3>Documents uploaded and saved:</h3>
      <ul id="docList">
        <?php do { ?>
        <li><a href="docs/<?php echo $row_rsDocs['docfile']; ?>"><?php echo $row_rsDocs['doctitle']; ?></a><br>
          </li>
          <?php } while ($row_rsDocs = mysql_fetch_assoc($rsDocs)); ?>
      </ul>
      <?php } // Show if recordset not empty ?>
<p><a href="#top">Back to top </a></p>
</body>
</html>
<?php
mysql_free_result($rsOrgApp);

mysql_free_result($rsDocs);
?>
