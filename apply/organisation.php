<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "docs";
$ppu->extensions = "";
$ppu->formName = "frmUploadDoc";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = true;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "";
$ppu->progressHeight = "";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO apporgdocs (orgid, doctitle, docfile) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['orgid'], "int"),
                       GetSQLValueString($_POST['doctitle'], "text"),
                       GetSQLValueString($_POST['docfile'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmSubmit")) {
  $updateSQL = sprintf("UPDATE applicationorgs SET submitted=%s, datesubmitted=%s WHERE orgid=%s",
                       GetSQLValueString($_POST['submitted'], "int"),
                       GetSQLValueString($_POST['datesubmitted'], "date"),
                       GetSQLValueString($_POST['orgid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmApplication")) {
  $updateSQL = sprintf("UPDATE applicationorgs SET orgname=%s, address=%s, postcode=%s, city=%s, country=%s, contact=%s, contactfunction=%s, submodality=%s, telephone=%s, website=%s, legalstructure=%s, type=%s, eabpmembers=%s, totaltrained=%s, totalmembers=%s, nofulfilcriteria=%s, yearsexistence=%s, codeofethics=%s, notrainers=%s, nosupervisors=%s, noadminstaff=%s, notraineesfirst=%s, notraineessecond=%s, notraineesthird=%s, notraineesfourth=%s, otherorgdetails=%s WHERE orgid=%s",
                       GetSQLValueString($_POST['orgname'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['postcode'], "text"),
                       GetSQLValueString($_POST['city'], "text"),
                       GetSQLValueString($_POST['country'], "text"),
                       GetSQLValueString($_POST['contact'], "text"),
                       GetSQLValueString($_POST['contactfunction'], "text"),
                       GetSQLValueString($_POST['submodality'], "text"),
                       GetSQLValueString($_POST['telephone'], "text"),
                       GetSQLValueString($_POST['website'], "text"),
                       GetSQLValueString($_POST['legalstructure'], "text"),
                       GetSQLValueString($_POST['type'], "text"),
					   GetSQLValueString($_POST['eabpmembers'], "text"),
                       GetSQLValueString($_POST['totaltrained'], "text"),
                       GetSQLValueString($_POST['totalmembers'], "text"),
                       GetSQLValueString($_POST['nofulfilcriteria'], "int"),
                       GetSQLValueString($_POST['yearsexistence'], "int"),
                       GetSQLValueString($_POST['codeofethics'], "int"),
                       GetSQLValueString($_POST['notrainers'], "int"),
                       GetSQLValueString($_POST['nosupervisors'], "int"),
                       GetSQLValueString($_POST['noadminstaff'], "int"),
                       GetSQLValueString($_POST['notraineesfirst'], "int"),
                       GetSQLValueString($_POST['notraineessecond'], "int"),
                       GetSQLValueString($_POST['notraineesthird'], "int"),
                       GetSQLValueString($_POST['notraineesfourth'], "int"),
                       GetSQLValueString($_POST['otherorgdetails'], "text"),
                       GetSQLValueString($_POST['orgid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsOrgApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsOrgApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgApp = sprintf("SELECT * FROM applicationorgs WHERE email = %s", GetSQLValueString($colname_rsOrgApp, "text"));
$rsOrgApp = mysql_query($query_rsOrgApp, $connEABP2) or die(mysql_error());
$row_rsOrgApp = mysql_fetch_assoc($rsOrgApp);
$totalRows_rsOrgApp = mysql_num_rows($rsOrgApp);

$colname_rsDocs = "-1";
$colname_rsDocs = $row_rsOrgApp['orgid'];
  

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocs = sprintf("SELECT appdocid, doctitle, docfile FROM apporgdocs WHERE orgid = %s", GetSQLValueString($colname_rsDocs, "int"));
$rsDocs = mysql_query($query_rsDocs, $connEABP2) or die(mysql_error());
$row_rsDocs = mysql_fetch_assoc($rsDocs);
$totalRows_rsDocs = mysql_num_rows($rsDocs);
?>
<?php require_once("../webassist/email/mail_php.php"); ?>
<?php require_once("../webassist/email/mailformatting_php.php"); ?>
<?php
if (!isset($_SESSION))session_start();
if ((isset($_POST["btnSubmit"])))     {
  //WA Universal Email object="mail"
  set_time_limit(0);
  $EmailRef = "waue_organisation_1";
  $BurstSize = 200;
  $BurstTime = 1;
  $WaitTime = 1;
  $GoToPage = "";
  $RecipArray = array();
  $StartBurst = time();
  $LoopCount = 0;
  $TotalEmails = 0;
  $RecipIndex = 0;
  // build up recipients array
  $CurIndex = sizeof($RecipArray);
  $RecipArray[$CurIndex] = array();
  $RecipArray[$CurIndex ][] = "officemanager@eabp.org";
  $TotalEmails += sizeof($RecipArray[$CurIndex]);
  $RealWait = ($WaitTime<0.25)?0.25:($WaitTime+0.1);
  $TimeTracker = Array();
  $TotalBursts = floor($TotalEmails/$BurstSize);
  $AfterBursts = $TotalEmails % $BurstSize;
  $TimeRemaining = ($TotalBursts * $BurstTime) + ($AfterBursts*$RealWait);
  if ($TimeRemaining < ($TotalEmails*$RealWait) )  {
    $TimeRemaining = $TotalEmails*$RealWait;
  }
  $_SESSION[$EmailRef."_Total"] = $TotalEmails;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = $TimeRemaining;
  while ($RecipIndex < sizeof($RecipArray))  {
    $EnteredValue = is_string($RecipArray[$RecipIndex][0]);
    $CurIndex = 0;
    while (($EnteredValue && $CurIndex < sizeof($RecipArray[$RecipIndex])) || (!$EnteredValue && $RecipArray[$RecipIndex][0])) {
      $starttime = microtime_float();
      if ($EnteredValue)  {
        $RecipientEmail = $RecipArray[$RecipIndex][$CurIndex];
      }  else  {
        $RecipientEmail = $RecipArray[$RecipIndex][0][$RecipArray[$RecipIndex][2]];
      }
      $EmailsRemaining = ($TotalEmails- $LoopCount);
      $BurstsRemaining = ceil(($EmailsRemaining-$AfterBursts)/$BurstSize);
      $IntoBurst = ($EmailsRemaining-$AfterBursts) % $BurstSize;
      if ($AfterBursts<$EmailsRemaining) $IntoBurst = 0;
      $TimeRemaining = ($BurstsRemaining * $BurstTime * 60) + ((($AfterBursts<$EmailsRemaining)?$AfterBursts:$EmailsRemaining)*$RealWait) - (($AfterBursts>$EmailsRemaining)?0:($IntoBurst*$RealWait));
      if ($TimeRemaining < ($EmailsRemaining*$RealWait) )  {
        $TimeRemaining = $EmailsRemaining*$RealWait;
      }
      $CurIndex ++;
      $LoopCount ++;
      session_commit();
      session_start();
      $_SESSION[$EmailRef."_Index"] = $LoopCount;
      $_SESSION[$EmailRef."_Remaining"] = round($TimeRemaining);
      session_commit();
      wa_sleep($WaitTime);
      include("../webassist/email/waue_organisation_1.php");
      $endtime = microtime_float();
      $TimeTracker[] =$endtime - $starttime;
      $RealWait = array_sum($TimeTracker)/sizeof($TimeTracker);
      if ($LoopCount % $BurstSize == 0 && $CurIndex < sizeof($RecipArray[$RecipIndex]))  {
        $TimePassed = (time() - $StartBurst);
        if ($TimePassed < ($BurstTime*60))  {
          $WaitBurst = ($BurstTime*60) -$TimePassed;
          wa_sleep($WaitBurst);
        }
        else  {
          $TimeRemaining = ($TotalEmails- $LoopCount)*$RealWait;
        }
        $StartBurst = time();
      }
      if (!$EnteredValue)  {
        $RecipArray[$RecipIndex][0] =  mysql_fetch_assoc($RecipArray[$RecipIndex][1]);
      }
    }
    $RecipIndex ++;
  }
  $_SESSION[$EmailRef."_Total"] = 0;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = 0;
  session_commit();
  session_start();
  if ($GoToPage!="")     {
    header("Location: ".$GoToPage);
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - Organisational Membership Apply</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<style type="text/css">
#frmApplication #tblApplication tr td ul li {
	font-size: 13px;
}
</style>
<script type="text/javascript">
var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-224881-50']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
function MM_popupMsg(msg) { //v1.0
  alert(msg);
}
</script>
<script language='javascript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body <?php if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmApplication")) {?>onload="MM_popupMsg('Application form updated')"<?php }?>>
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1><span class="normalText">Online application:</span><br />
    Organisational Membership</h1>
  <div id="leftSidebar">
    <h3>Before you start:</h3>
    <ul>
      <li>Please inform yourself about the <a href="../membership-organisation.php">Organisational Membership criteria</a> and conditions and use the instructions for filling in this application form.</li>
      <li>You are required to have <strong>at least one full individual member</strong> of EABP.</li>
      <li>You can <strong>save </strong> and return at a later date to complete your application.</li>
      <li>For security reasons you will need to login again <strong>after 30 minutes of idle activity</strong>.</li>
      <li>Please answer all questions in <strong>English</strong>.</li>
      <li>When finished click the button on the right to submit your application.</li>
    </ul>
    <form action="<?php echo $editFormAction; ?>" method="POST" name="frmApplication" id="frmApplication">
      <table width="100%" cellpadding="0" cellspacing="3" id="tblApplication">
        <tr>
          <th colspan="2" align="right" class="rowHeader">Contact details:</th>
        </tr>
        <tr>
          <th align="right">Name of contact person:</th>
          <td><input name="contact" type="text" id="contact" value="<?php echo $row_rsOrgApp['contact']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">Contact's function within the  organisation:</th>
          <td><input type="text" name="contactfunction" value="<?php echo $row_rsOrgApp['contactfunction']; ?>"  /></td>
        </tr>
        <tr>
          <th colspan="2" align="right" class="rowHeader">Organisation details:</th>
        </tr>
        <tr>
          <th align="right">Organisation name:</th>
          <td><input type="text" name="orgname" value="<?php echo $row_rsOrgApp['orgname']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">Address:</th>
          <td><input type="text" name="address" value="<?php echo $row_rsOrgApp['address']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">Postcode:</th>
          <td><input type="text" name="postcode" value="<?php echo $row_rsOrgApp['postcode']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">City:</th>
          <td><input type="text" name="city" value="<?php echo $row_rsOrgApp['city']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">Country:</th>
          <td>
          <select name="country" id="country">
       <option value="-1" selected="selected" <?php if (!(strcmp(-1, $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Choose:</option>
      <option value="Afghanistan" <?php if (!(strcmp("Afghanistan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Afghanistan</option>
      <option value="Albania" <?php if (!(strcmp("Albania", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Albania</option>
      <option value="Algeria" <?php if (!(strcmp("Algeria", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Algeria</option>
      <option value="American Samoa" <?php if (!(strcmp("American Samoa", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>American Samoa</option>
      <option value="Andorra" <?php if (!(strcmp("Andorra", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Andorra</option>
      <option value="Angola" <?php if (!(strcmp("Angola", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Angola</option>
      <option value="Anguilla" <?php if (!(strcmp("Anguilla", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Anguilla</option>
      <option value="Antarctica" <?php if (!(strcmp("Antarctica", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Antarctica</option>
      <option value="Antigua, Barbuda" <?php if (!(strcmp("Antigua, Barbuda", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Antigua, Barbuda</option>
      <option value="Argentina" <?php if (!(strcmp("Argentina", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Argentina</option>
      <option value="Armenia" <?php if (!(strcmp("Armenia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Armenia</option>
      <option value="Australia" <?php if (!(strcmp("Australia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Australia</option>
      <option value="Austria" <?php if (!(strcmp("Austria", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Austria</option>
      <option value="Azerbaijan" <?php if (!(strcmp("Azerbaijan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Azerbaijan</option>
      <option value="Bahamas" <?php if (!(strcmp("Bahamas", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Bahamas</option>
      <option value="Bahrain" <?php if (!(strcmp("Bahrain", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Bahrain</option>
      <option value="Bangladesh" <?php if (!(strcmp("Bangladesh", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Bangladesh</option>
      <option value="Barbados" <?php if (!(strcmp("Barbados", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Barbados</option>
      <option value="Belarus" <?php if (!(strcmp("Belarus", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Belarus</option>
      <option value="Belgium" <?php if (!(strcmp("Belgium", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Belgium</option>
      <option value="Belize" <?php if (!(strcmp("Belize", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Belize</option>
      <option value="Benin" <?php if (!(strcmp("Benin", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Benin</option>
      <option value="Bermuda" <?php if (!(strcmp("Bermuda", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Bermuda</option>
      <option value="Bhutan" <?php if (!(strcmp("Bhutan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Bhutan</option>
      <option value="Bolivia" <?php if (!(strcmp("Bolivia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Bolivia</option>
      <option value="Bosnia-Herzegovina" <?php if (!(strcmp("Bosnia-Herzegovina", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Bosnia-Herzegovina</option>
      <option value="Botswana" <?php if (!(strcmp("Botswana", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Botswana</option>
      <option value="Brazil" <?php if (!(strcmp("Brazil", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Brazil</option>
      <option value="British Indian Ocean Territory" <?php if (!(strcmp("British Indian Ocean Territory", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>British Indian Ocean Territory</option>
      <option value="British Virgin Islands" <?php if (!(strcmp("British Virgin Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>British Virgin Islands</option>
      <option value="Brunei" <?php if (!(strcmp("Brunei", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Brunei</option>
      <option value="Bulgaria" <?php if (!(strcmp("Bulgaria", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Bulgaria</option>
      <option value="Burkina Faso" <?php if (!(strcmp("Burkina Faso", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Burkina Faso</option>
      <option value="Burma" <?php if (!(strcmp("Burma", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Burma</option>
      <option value="Burundi" <?php if (!(strcmp("Burundi", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Burundi</option>
      <option value="Cambodia" <?php if (!(strcmp("Cambodia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Cambodia</option>
      <option value="Cameroon" <?php if (!(strcmp("Cameroon", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Cameroon</option>
      <option value="Canada" <?php if (!(strcmp("Canada", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Canada</option>
      <option value="Canary Islands" <?php if (!(strcmp("Canary Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Canary Islands</option>
      <option value="Cape Verde" <?php if (!(strcmp("Cape Verde", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Cape Verde</option>
      <option value="Cayman Islands" <?php if (!(strcmp("Cayman Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Cayman Islands</option>
      <option value="Central Africa" <?php if (!(strcmp("Central Africa", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Central Africa</option>
      <option value="Chad" <?php if (!(strcmp("Chad", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Chad</option>
      <option value="Chile" <?php if (!(strcmp("Chile", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Chile</option>
      <option value="China" <?php if (!(strcmp("China", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>China</option>
      <option value="Christmas Island" <?php if (!(strcmp("Christmas Island", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Christmas Island</option>
      <option value="Cocos Island" <?php if (!(strcmp("Cocos Island", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Cocos Island</option>
      <option value="Colombia" <?php if (!(strcmp("Colombia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Colombia</option>
      <option value="Comoros" <?php if (!(strcmp("Comoros", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Comoros</option>
      <option value="Congo" <?php if (!(strcmp("Congo", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Congo</option>
      <option value="Cook Islands" <?php if (!(strcmp("Cook Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Cook Islands</option>
      <option value="Costa Rica" <?php if (!(strcmp("Costa Rica", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Costa Rica</option>
      <option value="Croatia" <?php if (!(strcmp("Croatia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Croatia</option>
      <option value="Cuba" <?php if (!(strcmp("Cuba", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Cuba</option>
      <option value="Cyprus" <?php if (!(strcmp("Cyprus", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Cyprus</option>
      <option value="Czech Republic" <?php if (!(strcmp("Czech Republic", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Czech Republic</option>
      <option value="Democratic Republic of Congo" <?php if (!(strcmp("Democratic Republic of Congo", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Democratic Republic of Congo</option>
      <option value="Denmark" <?php if (!(strcmp("Denmark", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Denmark</option>
      <option value="Djibouti" <?php if (!(strcmp("Djibouti", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Djibouti</option>
      <option value="Dominica" <?php if (!(strcmp("Dominica", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Dominica</option>
      <option value="Dominican Republic" <?php if (!(strcmp("Dominican Republic", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Dominican Republic</option>
      <option value="East Timor" <?php if (!(strcmp("East Timor", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>East Timor</option>
      <option value="Ecuador" <?php if (!(strcmp("Ecuador", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Ecuador</option>
      <option value="Egypt" <?php if (!(strcmp("Egypt", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Egypt</option>
      <option value="El Salvador" <?php if (!(strcmp("El Salvador", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>El Salvador</option>
      <option value="Equatorial Guinea" <?php if (!(strcmp("Equatorial Guinea", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Equatorial Guinea</option>
      <option value="Eritrea" <?php if (!(strcmp("Eritrea", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Eritrea</option>
      <option value="Estonia" <?php if (!(strcmp("Estonia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Estonia</option>
      <option value="Ethiopia" <?php if (!(strcmp("Ethiopia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Ethiopia</option>
      <option value="FYRO Macedonia" <?php if (!(strcmp("FYRO Macedonia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>FYRO Macedonia</option>
      <option value="Falkland Islands" <?php if (!(strcmp("Falkland Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Falkland Islands</option>
      <option value="Faroe Islands" <?php if (!(strcmp("Faroe Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Faroe Islands</option>
      <option value="Fiji" <?php if (!(strcmp("Fiji", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Fiji</option>
      <option value="Finland" <?php if (!(strcmp("Finland", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Finland</option>
      <option value="France" <?php if (!(strcmp("France", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>France</option>
      <option value="Gabon" <?php if (!(strcmp("Gabon", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Gabon</option>
      <option value="Gambia" <?php if (!(strcmp("Gambia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Gambia</option>
      <option value="Georgia" <?php if (!(strcmp("Georgia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Georgia</option>
      <option value="Germany" <?php if (!(strcmp("Germany", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Germany</option>
      <option value="Ghana" <?php if (!(strcmp("Ghana", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Ghana</option>
      <option value="Gibraltar" <?php if (!(strcmp("Gibraltar", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Gibraltar</option>
      <option value="Greece" <?php if (!(strcmp("Greece", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Greece</option>
      <option value="Greenland" <?php if (!(strcmp("Greenland", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Greenland</option>
      <option value="Grenada" <?php if (!(strcmp("Grenada", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Grenada</option>
      <option value="Guadeloupe" <?php if (!(strcmp("Guadeloupe", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Guadeloupe</option>
      <option value="Guam" <?php if (!(strcmp("Guam", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Guam</option>
      <option value="Guatemala" <?php if (!(strcmp("Guatemala", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Guatemala</option>
      <option value="Guinea" <?php if (!(strcmp("Guinea", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Guinea</option>
      <option value="Guinea-Bissau" <?php if (!(strcmp("Guinea-Bissau", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Guinea-Bissau</option>
      <option value="Guyana" <?php if (!(strcmp("Guyana", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Guyana</option>
      <option value="Haiti" <?php if (!(strcmp("Haiti", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Haiti</option>
      <option value="Honduras" <?php if (!(strcmp("Honduras", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Honduras</option>
      <option value="Hong Kong" <?php if (!(strcmp("Hong Kong", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Hong Kong</option>
      <option value="Hungary" <?php if (!(strcmp("Hungary", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Hungary</option>
      <option value="Iceland" <?php if (!(strcmp("Iceland", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Iceland</option>
      <option value="India" <?php if (!(strcmp("India", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>India</option>
      <option value="Indonesia" <?php if (!(strcmp("Indonesia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Indonesia</option>
      <option value="Iran" <?php if (!(strcmp("Iran", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Iran</option>
      <option value="Iraq" <?php if (!(strcmp("Iraq", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Iraq</option>
      <option value="Ireland" <?php if (!(strcmp("Ireland", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Ireland</option>
      <option value="Israel" <?php if (!(strcmp("Israel", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Israel</option>
      <option value="Italy" <?php if (!(strcmp("Italy", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Italy</option>
      <option value="Ivory Coast" <?php if (!(strcmp("Ivory Coast", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Ivory Coast</option>
      <option value="Jamaica" <?php if (!(strcmp("Jamaica", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Jamaica</option>
      <option value="Japan" <?php if (!(strcmp("Japan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Japan</option>
      <option value="Jordan" <?php if (!(strcmp("Jordan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Jordan</option>
      <option value="Kazachstan" <?php if (!(strcmp("Kazachstan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Kazachstan</option>
      <option value="Kenya" <?php if (!(strcmp("Kenya", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Kenya</option>
      <option value="Kiribati" <?php if (!(strcmp("Kiribati", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Kiribati</option>
      <option value="Kuwait" <?php if (!(strcmp("Kuwait", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Kuwait</option>
      <option value="Kyrgyzstan" <?php if (!(strcmp("Kyrgyzstan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Kyrgyzstan</option>
      <option value="Lao PDR" <?php if (!(strcmp("Lao PDR", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Lao PDR</option>
      <option value="Latvia" <?php if (!(strcmp("Latvia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Latvia</option>
      <option value="Lebanon" <?php if (!(strcmp("Lebanon", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Lebanon</option>
      <option value="Lesotho" <?php if (!(strcmp("Lesotho", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Lesotho</option>
      <option value="Liberia" <?php if (!(strcmp("Liberia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Liberia</option>
      <option value="Libya" <?php if (!(strcmp("Libya", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Libya</option>
      <option value="Liechtenstein" <?php if (!(strcmp("Liechtenstein", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Liechtenstein</option>
      <option value="Lithuania" <?php if (!(strcmp("Lithuania", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Lithuania</option>
      <option value="Luxembourg" <?php if (!(strcmp("Luxembourg", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Luxembourg</option>
      <option value="Madagascar" <?php if (!(strcmp("Madagascar", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Madagascar</option>
      <option value="Malawi" <?php if (!(strcmp("Malawi", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Malawi</option>
      <option value="Malaysia" <?php if (!(strcmp("Malaysia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Malaysia</option>
      <option value="Maldives" <?php if (!(strcmp("Maldives", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Maldives</option>
      <option value="Mali" <?php if (!(strcmp("Mali", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Mali</option>
      <option value="Malta" <?php if (!(strcmp("Malta", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Malta</option>
      <option value="Marshall Islands" <?php if (!(strcmp("Marshall Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Marshall Islands</option>
      <option value="Martinique" <?php if (!(strcmp("Martinique", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Martinique</option>
      <option value="Mauritania" <?php if (!(strcmp("Mauritania", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Mauritania</option>
      <option value="Mauritius" <?php if (!(strcmp("Mauritius", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Mauritius</option>
      <option value="Mexico" <?php if (!(strcmp("Mexico", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Mexico</option>
      <option value="Micronesia" <?php if (!(strcmp("Micronesia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Micronesia</option>
      <option value="Moldova" <?php if (!(strcmp("Moldova", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Moldova</option>
      <option value="Monaco" <?php if (!(strcmp("Monaco", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Monaco</option>
      <option value="Mongolia" <?php if (!(strcmp("Mongolia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Mongolia</option>
      <option value="Montserrat" <?php if (!(strcmp("Montserrat", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Montserrat</option>
      <option value="Morocco" <?php if (!(strcmp("Morocco", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Morocco</option>
      <option value="Mozambique" <?php if (!(strcmp("Mozambique", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Mozambique</option>
      <option value="Namibia" <?php if (!(strcmp("Namibia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Namibia</option>
      <option value="Nauru" <?php if (!(strcmp("Nauru", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Nauru</option>
      <option value="Nepal" <?php if (!(strcmp("Nepal", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Nepal</option>
      <option value="Netherlands" <?php if (!(strcmp("Netherlands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Netherlands</option>
      <option value="New Caledonia" <?php if (!(strcmp("New Caledonia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>New Caledonia</option>
      <option value="New Zealand" <?php if (!(strcmp("New Zealand", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>New Zealand</option>
      <option value="Nicaragua" <?php if (!(strcmp("Nicaragua", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Nicaragua</option>
      <option value="Niger" <?php if (!(strcmp("Niger", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Niger</option>
      <option value="Nigeria" <?php if (!(strcmp("Nigeria", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Nigeria</option>
      <option value="Niue" <?php if (!(strcmp("Niue", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Niue</option>
      <option value="Norfolk Island" <?php if (!(strcmp("Norfolk Island", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Norfolk Island</option>
      <option value="North Korea" <?php if (!(strcmp("North Korea", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>North Korea</option>
      <option value="Norway" <?php if (!(strcmp("Norway", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Norway</option>
      <option value="Oman" <?php if (!(strcmp("Oman", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Oman</option>
      <option value="Pakistan" <?php if (!(strcmp("Pakistan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Pakistan</option>
      <option value="Palau" <?php if (!(strcmp("Palau", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Palau</option>
      <option value="Palestine National Authority" <?php if (!(strcmp("Palestine National Authority", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Palestine National Authority</option>
      <option value="Panama" <?php if (!(strcmp("Panama", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Panama</option>
      <option value="Papua New Guinea" <?php if (!(strcmp("Papua New Guinea", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Papua New Guinea</option>
      <option value="Paraguay" <?php if (!(strcmp("Paraguay", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Paraguay</option>
      <option value="Peru" <?php if (!(strcmp("Peru", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Peru</option>
      <option value="Philippines" <?php if (!(strcmp("Philippines", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Philippines</option>
      <option value="Pitcairn Islands" <?php if (!(strcmp("Pitcairn Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Pitcairn Islands</option>
      <option value="Poland" <?php if (!(strcmp("Poland", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Poland</option>
      <option value="Polynesia" <?php if (!(strcmp("Polynesia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Polynesia</option>
      <option value="Portugal" <?php if (!(strcmp("Portugal", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Portugal</option>
      <option value="Puerto Rico" <?php if (!(strcmp("Puerto Rico", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Puerto Rico</option>
      <option value="Qatar" <?php if (!(strcmp("Qatar", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Qatar</option>
      <option value="Reunion" <?php if (!(strcmp("Reunion", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Reunion</option>
      <option value="Romania" <?php if (!(strcmp("Romania", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Romania</option>
      <option value="Russia" <?php if (!(strcmp("Russia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Russia</option>
      <option value="Rwanda" <?php if (!(strcmp("Rwanda", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Rwanda</option>
      <option value="Saint Kitts and Nevis" <?php if (!(strcmp("Saint Kitts and Nevis", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Saint Kitts and Nevis</option>
      <option value="Saint Lucia" <?php if (!(strcmp("Saint Lucia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Saint Lucia</option>
      <option value="Saint Vincent and the Grenadines" <?php if (!(strcmp("Saint Vincent and the Grenadines", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Saint Vincent and the Grenadines</option>
      <option value="Samoa" <?php if (!(strcmp("Samoa", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Samoa</option>
      <option value="San Marino" <?php if (!(strcmp("San Marino", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>San Marino</option>
      <option value="Sao Tome" <?php if (!(strcmp("Sao Tome", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Sao Tome</option>
      <option value="Saudi Arabia" <?php if (!(strcmp("Saudi Arabia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Saudi Arabia</option>
      <option value="Senegal" <?php if (!(strcmp("Senegal", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Senegal</option>
      <option value="Serbia and Montenegro" <?php if (!(strcmp("Serbia and Montenegro", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Serbia and Montenegro</option>
      <option value="Seychelles" <?php if (!(strcmp("Seychelles", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Seychelles</option>
      <option value="Sierra Leone" <?php if (!(strcmp("Sierra Leone", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Sierra Leone</option>
      <option value="Singapore" <?php if (!(strcmp("Singapore", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Singapore</option>
      <option value="Slovakia" <?php if (!(strcmp("Slovakia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Slovakia</option>
      <option value="Slovenia" <?php if (!(strcmp("Slovenia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Slovenia</option>
      <option value="Solomon Islands" <?php if (!(strcmp("Solomon Islands", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Solomon Islands</option>
      <option value="Somalia" <?php if (!(strcmp("Somalia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Somalia</option>
      <option value="South Africa" <?php if (!(strcmp("South Africa", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>South Africa</option>
      <option value="South Korea" <?php if (!(strcmp("South Korea", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>South Korea</option>
      <option value="Spain" <?php if (!(strcmp("Spain", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Spain</option>
      <option value="Sri Lanka" <?php if (!(strcmp("Sri Lanka", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Sri Lanka</option>
      <option value="St. Helena" <?php if (!(strcmp("St. Helena", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>St. Helena</option>
      <option value="Sudan" <?php if (!(strcmp("Sudan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Sudan</option>
      <option value="Suriname" <?php if (!(strcmp("Suriname", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Suriname</option>
      <option value="Swaziland" <?php if (!(strcmp("Swaziland", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Swaziland</option>

      <option value="Sweden" <?php if (!(strcmp("Sweden", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Sweden</option>
      <option value="Switzerland" <?php if (!(strcmp("Switzerland", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Switzerland</option>
      <option value="Syria" <?php if (!(strcmp("Syria", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Syria</option>
      <option value="Tadjikistan" <?php if (!(strcmp("Tadjikistan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Tadjikistan</option>
      <option value="Taiwan" <?php if (!(strcmp("Taiwan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Taiwan</option>
      <option value="Tanzania" <?php if (!(strcmp("Tanzania", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Tanzania</option>
      <option value="Thailand" <?php if (!(strcmp("Thailand", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Thailand</option>
      <option value="Togo" <?php if (!(strcmp("Togo", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Togo</option>
      <option value="Tokelau" <?php if (!(strcmp("Tokelau", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Tokelau</option>
      <option value="Tonga" <?php if (!(strcmp("Tonga", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Tonga</option>
      <option value="Trinidad and Tobago" <?php if (!(strcmp("Trinidad and Tobago", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Trinidad and Tobago</option>
      <option value="Tunisia" <?php if (!(strcmp("Tunisia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Tunisia</option>
      <option value="Turkey" <?php if (!(strcmp("Turkey", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Turkey</option>
      <option value="Turkmenistan" <?php if (!(strcmp("Turkmenistan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Turkmenistan</option>
      <option value="Tuvalu" <?php if (!(strcmp("Tuvalu", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Tuvalu</option>
      <option value="Uganda" <?php if (!(strcmp("Uganda", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Uganda</option>
      <option value="Ukraine" <?php if (!(strcmp("Ukraine", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Ukraine</option>
      <option value="United Arab Emirates" <?php if (!(strcmp("United Arab Emirates", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>United Arab Emirates</option>
      <option value="United Kingdom" <?php if (!(strcmp("United Kingdom", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>United Kingdom</option>
      <option value="United States" <?php if (!(strcmp("United States", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>United States</option>
      <option value="Uruguay" <?php if (!(strcmp("Uruguay", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Uruguay</option>
      <option value="Uzbekistan" <?php if (!(strcmp("Uzbekistan", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Uzbekistan</option>
      <option value="Vanuatu" <?php if (!(strcmp("Vanuatu", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Vanuatu</option>
      <option value="Vatican City State" <?php if (!(strcmp("Vatican City State", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Vatican City State</option>
      <option value="Venezuela" <?php if (!(strcmp("Venezuela", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Venezuela</option>
      <option value="Vietnam" <?php if (!(strcmp("Vietnam", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Vietnam</option>
      <option value="Virgin Islands, British" <?php if (!(strcmp("Virgin Islands, British", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Virgin Islands, British</option>
      <option value="Virgin Islands, US" <?php if (!(strcmp("Virgin Islands, US", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Virgin Islands, US</option>
      <option value="Western Sahara" <?php if (!(strcmp("Western Sahara", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Western Sahara</option>
      <option value="Yemen" <?php if (!(strcmp("Yemen", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Yemen</option>
      <option value="Zambia" <?php if (!(strcmp("Zambia", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Zambia</option>
      <option value="Zimbabwe" <?php if (!(strcmp("Zimbabwe", $row_rsOrgApp['country']))) {echo "selected=\"selected\"";} ?>>Zimbabwe</option>
    </select>
          </td>
        </tr>
        <tr>
          <th align="right">Telephone (including country code):</th>
          <td><input type="text" name="telephone" value="<?php echo $row_rsOrgApp['telephone']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">Website:</th>
          <td><input type="text" name="website" value="<?php echo $row_rsOrgApp['website']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">Sub-modality:</th>
          <td><input name="submodality" type="text" id="submodality" value="<?php echo $row_rsOrgApp['submodality']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">How many years have you been in existence?</th>
          <td><input type="text" name="yearsexistence" value="<?php echo $row_rsOrgApp['yearsexistence']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">What is your legal organisational (or corporate) structure?</th>
          <td><textarea name="legalstructure"><?php echo $row_rsOrgApp['legalstructure']; ?></textarea></td>
        </tr>
        <tr>
          <th align="right">Do you have an appropriate code of ethics and complaints procedure?</th>
          <td valign="middle"><label>
            <input <?php if (!(strcmp($row_rsOrgApp['codeofethics'],"1"))) {echo "checked=\"checked\"";} ?> name="codeofethics" type="radio" class="radioButton" id="codeofethics_0" value="1" />
            Yes</label>
            <label>
              <input <?php if (!(strcmp($row_rsOrgApp['codeofethics'],"0"))) {echo "checked=\"checked\"";} ?> name="codeofethics" type="radio" class="radioButton" id="codeofethics_1" value="0" />
            No</label></td>
        </tr>
        <tr>
          <th align="right">Type of organisation:</th>
          <td>
          <select name="type">
      <option value="Training Organisation" <?php if (!(strcmp("Training Organisation", $row_rsOrgApp['type']))) {echo "selected=\"selected\"";} ?>>Training Organisation</option>
      <option value="Professional Association" <?php if (!(strcmp("Professional Association", $row_rsOrgApp['type']))) {echo "selected=\"selected\"";} ?>>Professional Association</option>
      <option value="Other" <?php if (!(strcmp("Other", $row_rsOrgApp['type']))) {echo "selected=\"selected\"";} ?>>Other</option>
    </select>
          </td>
        </tr>
        <tr>
          <th align="right" valign="top">You are required to have at least one full individual member of EABP.  Who is/are your individual member(s)? </th>
          <td><textarea name="eabpmembers" id="eabpmembers" cols="45" rows="5"><?php echo $row_rsOrgApp['eabpmembers']; ?></textarea></td>
        </tr>
        <tr>
          <td colspan="2" align="left" class="crimsonText"><hr />
<strong>Please complete either section A or B.</strong></td>
          </tr>
        <tr>
          <th colspan="2" align="right" class="rowHeader">(A) Training organisation</th>
        </tr>
        <tr>
          <th align="right">&nbsp;</th>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th align="right">About how many people  have you trained?  </th>
          <td><input type="text" name="totaltrained" value="<?php echo $row_rsOrgApp['totaltrained']; ?>"  /></td>
        </tr>
        <tr>
          <th colspan="2" align="right">How many people are currently involved in the  organisation?</th>
        </tr>
        <tr>
          <td align="right">Trainers:</td>
          <td><input name="notrainers" type="text" class="input100" value="<?php echo $row_rsOrgApp['notrainers']; ?>"  /></td>
        </tr>
        <tr>
          <td align="right">Supervisors:</td>
          <td><input name="nosupervisors" type="text" class="input100" value="<?php echo $row_rsOrgApp['nosupervisors']; ?>"  /></td>
        </tr>
        <tr>
          <td align="right">Admin staff:</td>
          <td><input name="noadminstaff" type="text" class="input100" value="<?php echo $row_rsOrgApp['noadminstaff']; ?>"  /></td>
        </tr>
        <tr>
          <th colspan="2" align="right">How many trainees do  you have in the:</th>
        </tr>
        <tr>
          <td align="right">First year?</td>
          <td><input name="notraineesfirst" type="text" class="input100" value="<?php echo $row_rsOrgApp['notraineesfirst']; ?>"  /></td>
        </tr>
        <tr>
          <td align="right">Second year?</td>
          <td><input name="notraineessecond" type="text" class="input100" value="<?php echo $row_rsOrgApp['notraineessecond']; ?>"  /></td>
        </tr>
        <tr>
          <td align="right"> Third year?</td>
          <td><input name="notraineesthird" type="text" class="input100" value="<?php echo $row_rsOrgApp['notraineesthird']; ?>"  /></td>
        </tr>
        <tr>
          <td align="right">Fourth year?</td>
          <td><input name="notraineesfourth" type="text" class="input100" value="<?php echo $row_rsOrgApp['notraineesfourth']; ?>"  /></td>
        </tr>
        <tr>
          <td colspan="2">Please attach (top right):
            <ul>
              <li>your training curriculum</li>
              <li>a list of individuals within the training structure indicating their function(s): trainer, therapist, supervisor etc.</li>
              <li>Scanned registration of your organisation in the Chamber of Commerce </li>
            </ul>            </td>
          </tr>
        <tr>
          <th colspan="2" align="right" class="rowHeader">(B) Professional  association</th>
        </tr>
        <tr>
          <th align="right">How many members do  you have?</th>
          <td><input type="text" name="totalmembers" value="<?php echo $row_rsOrgApp['totalmembers']; ?>"  /></td>
        </tr>
        <tr>
          <th align="right">Among your members how many practitioners do you  have who fulfil the membership criteria of EABP or something similar?</th>
          <td valign="middle"><input type="text" name="nofulfilcriteria" value="<?php echo $row_rsOrgApp['nofulfilcriteria']; ?>"  /></td>
        </tr>
        <tr>
          <td colspan="2">Please attach (top right) a scan of the registration of your organisation in the Chamber of Commerce </td>
        </tr>
        <tr>
          <th colspan="2" align="right" class="rowHeader">Other organisations only:</th>
        </tr>
        <tr>
          <th colspan="2" align="right">Please give details:</th>
          </tr>
        <tr>
          <th colspan="2" align="right"><textarea name="otherorgdetails" rows="5" class="textAreaWide" id="otherorgdetails"><?php echo $row_rsOrgApp['otherorgdetails']; ?></textarea></th>
          </tr>
        <tr>
          <td colspan="2" align="center"><input name="orgid" type="hidden" id="orgid" value="<?php echo $row_rsOrgApp['orgid']; ?>" />            <input type="submit" class="btnSave" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="MM_update" value="frmApplication" />
    </form>
    <p>&nbsp;</p>
  </div>
  <div id="rightSidebar">
    <h3>Logged in</h3>
    <p>username: <strong><?php echo $row_rsOrgApp['email']; ?></strong><br />
    </p>
    <p><a href="logout.php">Logout</a></p>
    <hr size="1" noshade="noshade" />
    <h3>Documents    </h3>
    <form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmUploadDoc" id="frmUploadDoc" onsubmit="checkFileUpload(this,'',true,'','','','','','','');return document.MM_returnValue">
      <table align="center">
        <tr>
          <td nowrap="nowrap" align="right">Title:</td>
          <td><input type="text" name="doctitle" value="" /></td>
        </tr>
        <tr>
          <td nowrap="nowrap" align="right">File:</td>
          <td><input name="docfile" type="file" id="docfile" onchange="checkOneFileUpload(this,'',true,'','','','','','','')" /></td>
        </tr>
        <tr>
          <td nowrap="nowrap" align="right">&nbsp;</td>
          <td><input type="submit" value="Upload document" /></td>
        </tr>
      </table>
      <input type="hidden" name="orgid" value="<?php echo $row_rsOrgApp['orgid']; ?>" />
      <input type="hidden" name="MM_insert" value="form1" />
</form>
  <p>Files uploaded:
    <ul>
      <?php do { ?>
        <li><a href="docs/<?php echo $row_rsDocs['docfile']; ?>"><?php echo $row_rsDocs['doctitle']; ?></a></li>
        <?php } while ($row_rsDocs = mysql_fetch_assoc($rsDocs)); ?>
    </ul>

    <hr size="1" noshade="noshade" />
    <?php if ($totalRows_rsDocs > 0 && $row_rsOrgApp['eabpmembers'] !="" ) { // Show if recordset not empty ?>

    <p>Form completed and documents uploaded. </p>
    <p><strong>By submitting your application you <a href="https://www.eabp.org/docs/2016EABPEthicsGuidelines.pdf" target="_blank">agree to apply the EABP ethical code</a></strong>.</p>
      <form id="frmSubmit" name="frmSubmit" method="POST" action="<?php echo $editFormAction; ?>">
        <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit application" />
        <input name="orgid" type="hidden" id="orgid" value="<?php echo $row_rsOrgApp['orgid']; ?>" />
        <input name="submitted" type="hidden" id="submitted" value="1" />
        <input name="datesubmitted" type="hidden" id="datesubmitted" value="<?php echo date('Y-m-d H:i:s') ; ?>" />
        <input type="hidden" name="MM_update" value="frmSubmit" />
      </form>
      <?php } // Show if recordset not empty ?>
      <?php if ($totalRows_rsDocs == 0) { // Show if recordset empty ?>
  <p class="redText"> You need to upload supporting documents before submitting your application.</p>
  <?php } // Show if recordset empty ?>
  
  <?php if ($row_rsOrgApp['eabpmembers'] =="") { ?>
  <p class="redText">You are required to have <strong>at least one full individual member of EABP</strong> before submitting your application.</p>
  <?php } ?>
  
<hr size="1" noshade="noshade" />
    <?php if($row_rsOrgApp['submitted'] ==1) {?>
    <div class="alertRed">Thank you. Your application has been submitted. We will send you an email to acknowledge receipt.</div>
    <?php }?>
  </div>
</div>
<div id="contentBottom">&nbsp;</div>
</div>


</div>
</body>
</html>
<?php
mysql_free_result($rsOrgApp);

mysql_free_result($rsDocs);
?>
