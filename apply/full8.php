<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "docs";
$ppu->extensions = "doc,docx,pdf,jpg,jpeg";
// choose form to upload from
if (isset($_POST["btnUploadForum"])) {
$ppu->formName = "frmForumDocs";
} else {
$ppu->formName = "frmDocs";
}
//
$ppu->storeType = "file";
$ppu->sizeLimit = "5000";
$ppu->nameConflict = "uniq";
$ppu->nameToLower = false;
$ppu->requireUpload = true;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "2000";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmForumDocs")) {
  $insertSQL = sprintf("INSERT INTO appdocs (aid, doctypeid, docfile, notes) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['aid'], "int"),
                       GetSQLValueString($_POST['doctypeid'], "int"),
					   GetSQLValueString($_POST['docfile'], "text"),
                       GetSQLValueString($_POST['notes'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmDocs")) {
  $insertSQL = sprintf("INSERT INTO appdocs (aid, doctypeid, docfile, notes) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['aid'], "int"),
                       GetSQLValueString($_POST['doctypeid'], "int"),
                       GetSQLValueString($_POST['docfile'], "text"),
                       GetSQLValueString($_POST['notes'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmNotes")) {
  $updateSQL = sprintf("UPDATE appdocs SET notes=%s WHERE appdocid=%s",
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString($_POST['appdocid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsApp = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsApp = $_SESSION['MM_Username'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT * FROM applications WHERE email = %s", GetSQLValueString($colname_rsApp, "text"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocTypesForum = "SELECT doctypeid, doctype FROM appdoctypes WHERE forum = 1";
$rsDocTypesForum = mysql_query($query_rsDocTypesForum, $connEABP2) or die(mysql_error());
$row_rsDocTypesForum = mysql_fetch_assoc($rsDocTypesForum);
$totalRows_rsDocTypesForum = mysql_num_rows($rsDocTypesForum);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocTypesOther = "SELECT doctypeid, doctype FROM appdoctypes WHERE forum = 0";
$rsDocTypesOther = mysql_query($query_rsDocTypesOther, $connEABP2) or die(mysql_error());
$row_rsDocTypesOther = mysql_fetch_assoc($rsDocTypesOther);
$totalRows_rsDocTypesOther = mysql_num_rows($rsDocTypesOther);

$colname_rsDocs = "-1";

  $colname_rsDocs = $row_rsApp['aid'];

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocs = sprintf("SELECT appdocid, notes, doctype, docfile FROM appdocs INNER JOIN appdoctypes ON appdocs.doctypeid=appdoctypes.doctypeid WHERE aid = %s", GetSQLValueString($colname_rsDocs, "int"));
$rsDocs = mysql_query($query_rsDocs, $connEABP2) or die(mysql_error());
$row_rsDocs = mysql_fetch_assoc($rsDocs);
$totalRows_rsDocs = mysql_num_rows($rsDocs);
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - online application - full member - References</title>
<link href="../css/sitemain2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="../css/application2.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>

<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css">
<style type="text/css">
#TabbedPanels1 .TabbedPanelsTabGroup .TabbedPanelsTab.TabbedPanelsTab, #TabbedPanels1 .TabbedPanelsTabGroup .TabbedPanelsTab.TabbedPanelsTabSelected {
	font-size: 12px;
}
</style>
<script language='javascript' src='../ScriptLibrary/incPureUpload.js'></script>
</head>

<body>
<img src="../images/logo.png" alt="EABP" name="logo" width="216" height="81" id="logo">
<div id="colMiddle">
  <div id="contentTop">
    <h1><span class="normalText">Online application:</span><br />
    EABP <strong><?php if (($_COOKIE['type']) =="full") { ?>full<?php } else { ?>candidate<?php }?></strong> membership</h1>
  <div id="leftSidebar">
    <p>You can <strong>save each section</strong> and return at a later date to complete your application.    </p>
    <h2>Section 8 - Documentation</h2>
    <p><strong>Please choose the type of applicant you are:</strong></p>
    <div id="TabbedPanels1" class="TabbedPanels">
      <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0">Graduates of EABP <span class="allCaps">Forum</span> Training Institutes</li>
        <li class="TabbedPanelsTab" tabindex="0">Other Applicants</li>
      </ul>
      <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent">
 <p>Please add the following scanned documents to this application form (all are required):</p>
    <ul>
      <li>Previous education certificate</li>
      <li>FORUM Training Institute certificate</li>
      <li>Letter from the Training Institute confirming the certification</li>
      <li>Letter from the supervisor confirming the professional practice</li>
      <li>Documentation of professional practice after training: at least 600 hours of supervised professional practice as a body psychotherapist over a period of at least two years </li>
      <li>Professional Curriculm Vitae</li>
      </ul>
      <fieldset>
      <legend>Graduates of Forum Training Institutes</legend>
    <form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmForumDocs" id="frmForumDocs" onSubmit="checkFileUpload(this,'doc,docx,pdf,jpg,jpeg',true,5000,'','','','','','');return document.MM_returnValue">
    <label class="doc">Choose document type *:</label>
<select name="doctypeid" id="doctypeid">
  
  <?php
do {  
?>
  <option value="<?php echo $row_rsDocTypesForum['doctypeid']?>"><?php echo $row_rsDocTypesForum['doctype']?></option>
        <?php
} while ($row_rsDocTypesForum = mysql_fetch_assoc($rsDocTypesForum));
  $rows = mysql_num_rows($rsDocTypesForum);
  if($rows > 0) {
      mysql_data_seek($rsDocTypesForum, 0);
	  $row_rsDocTypesForum = mysql_fetch_assoc($rsDocTypesForum);
  }
?>
  </select>
      <input name="docfile" type="file" class="clear" id="docfile" onChange="checkOneFileUpload(this,'doc,docx,pdf,jpg,jpeg',true,5000,'','','','','','')">
      <span class="crimsonText">! 5MB max file size !</span><br>
      <label>
        <input name="notes" type="text" class="input100pc" id="notes" placeholder="Notes for this document (optional)"></label>
      <br>
<input type="submit" name="btnUploadForum" id="btnUploadForum" value="Upload">
      <input name="aid" type="hidden" id="aid" value="<?php echo $row_rsApp['aid']; ?>">
      <input type="hidden" name="MM_insert" value="frmForumDocs">
    </form>
    </fieldset>       
        </div>
        <div class="TabbedPanelsContent">
         <p>Please add the following scanned documents to this application form (all are required):</p>
    <ul>
      <li>Training certificate(s)</li>
      <li>Certificates from other professional training courses,      workshops etc.</li>
      <li>Signed statement(s) of your psychotherapists</li>
      <li>Signed statement(s) of your supervisor(s)</li>
      <li>Reference letters of two body psychotherapists      who sponsor your  EABP membership</li>
      <li>Training curriculum </li>
      <li>Professional Curriculm Vitae</li>
    </ul>
     <fieldset>
     <legend>Other Applicants</legend>
    <form action="<?php echo $editFormAction; ?>" method="post"  enctype="multipart/form-data"  name="frmDocs" id="frmDocs"  onSubmit="checkFileUpload(this,'doc,docx,pdf,jpg,jpeg',true,5000,'','','','','','');return document.MM_returnValue">
     <label class="doc">Choose document type *:</label>
<select name="doctypeid" id="doctypeid">
  
  <?php
do {  
?>
  <option value="<?php echo $row_rsDocTypesOther['doctypeid']?>"><?php echo $row_rsDocTypesOther['doctype']?></option>
        <?php
} while ($row_rsDocTypesOther = mysql_fetch_assoc($rsDocTypesOther));
  $rows = mysql_num_rows($rsDocTypesOther);
  if($rows > 0) {
      mysql_data_seek($rsDocTypesOther, 0);
	  $row_rsDocTypesOther = mysql_fetch_assoc($rsDocTypesOther);
  }
?>
    </select>
      <input name="docfile" type="file" class="clear" id="docfile">
           ! 5MB max file size !
      <br>
            <label>
              <input name="notes" type="text" class="input100pc" id="notes" placeholder="Notes for this document (optional)">
            </label>
            <br>
      <input type="submit" name="btnUploadOther" id="btnUploadOther" value="Upload">
      <input name="aid" type="hidden" id="aid" value="<?php echo $row_rsApp['aid']; ?>">
      <input type="hidden" name="MM_insert" value="frmDocs">
    </form>
    </fieldset>     
        
        
        </div>
      </div>
    </div>

     
     <form action="full-review.php" method="POST" name="frmApplication" id="frmApplication">
       <table border="0" cellpadding="0" cellspacing="2" id="tblApplication">
      <tr>
        <td align="center" valign="middle" scope="row"><input type="submit" name="btnSubmit" id="btnSubmit" value="Continue" /></td>
        </tr>
    </table>
</form>
    <p>&nbsp;</p>
    <p><a href="#top" class="top">Back to top</a></p>
</div>
  <div id="rightSidebar">
  <p><a href="logout.php">Logout</a></p>

  <h3 class="allCaps alert"><?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?></h3>
<ul>
  <li><a href="full.php">Section 1 - Personal Data</a></li>
      <li><a href="full2.php">Section 2 - Training</a></li>
      <li><a href="full3.php">Section 3 - Personal Psychotherapy</a></li>
      <li><a href="full4.php">Section 4 - Professional Supervision </a></li>
      <li><a href="full5.php">Section 5 - Professional Practice</a></li>
      <li><a href="full6.php">Section 6 - Membership of other Professional Bodies</a></li>
      <li><a href="full7.php">Section 7 - References</a></li>
      <li><strong>Section 8 - Documents</strong></li>
      <li><a href="full-review.php">Review Application</a></li>
      <li><a href="full-submit.php">Submit Application</a></li>
    </ul>
    <hr>
    <?php if ($totalRows_rsDocs > 0) { // Show if recordset not empty ?>
  <h3>Documents uploaded and saved:</h3>
      <ul id="docList">
        <?php do { ?>
          <li><a href="docs/<?php echo $row_rsDocs['docfile']; ?>"><?php echo $row_rsDocs['doctype']; ?></a><br>
          <form method="POST" action="<?php echo $editFormAction; ?>" name="frmNotes" id="frmNotes"><br>
Note:
            <input name="notes" type="text" id="notes" value="<?php echo $row_rsDocs['notes']; ?>">
            <input name="appdocid" type="hidden" id="appdocid" value="<?php echo $row_rsDocs['appdocid']; ?>">
<input type="submit" name="btnNoteUpdate" id="btnNoteUpdate" value="Update note">
<input type="hidden" name="MM_update" value="frmNotes">
          </form>
          
            <a href="doc-delete.php?appdocid=<?php echo $row_rsDocs['appdocid']; ?>" class="docDelete">Delete document
          X</a></li>
          <?php } while ($row_rsDocs = mysql_fetch_assoc($rsDocs)); ?>
      </ul>
      <?php } // Show if recordset not empty ?>
  </div>
  </div>
<div id="contentBottom">&nbsp;</div>
</div>

</div>
<script type="text/javascript">
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>
</body>

</html>
<?php
mysql_free_result($rsCountries);

mysql_free_result($rsApp);

mysql_free_result($rsDocTypesForum);

mysql_free_result($rsDocTypesOther);

mysql_free_result($rsDocs);
?>
