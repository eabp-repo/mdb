<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['appdocid'])) && ($_GET['appdocid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM appdocs WHERE appdocid=%s",
                       GetSQLValueString($_GET['appdocid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());
}

$colname_rsDoc = "-1";
if (isset($_GET['appdocid'])) {
  $colname_rsDoc = $_GET['appdocid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDoc = sprintf("SELECT * FROM appdocs WHERE appdocid = %s", GetSQLValueString($colname_rsDoc, "int"));
$rsDoc = mysql_query($query_rsDoc, $connEABP2) or die(mysql_error());
$row_rsDoc = mysql_fetch_assoc($rsDoc);
$totalRows_rsDoc = mysql_num_rows($rsDoc);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Document deleted ...</title>
<meta http-equiv="refresh" content="1;URL=full8.php">
</head>

<body>
Document deleted ...
</body>
</html>
<?php
mysql_free_result($rsDoc);
?>
