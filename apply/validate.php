<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Makes "field" always required. Nothing and blanks are invalid.</title>

 
</head>
<body>
<form id="myform">
<label for="field">Required: </label>
<input type="text" class="left" id="id" name="id">
<br/>
<input type="submit" value="Validate!">
</form>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>

$( "#myform" ).validate({
  rules: {
    id: {
      required: true
    }
  }
});
</script>
</body>
</html>
