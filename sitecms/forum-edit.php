<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/logos";
$ppu->extensions = "GIF,JPG,JPEG,PNG";
$ppu->formName = "form1";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE forum SET orgname=%s, logofile=IFNULL(%s,logofile), address=%s, city=%s, accredited=%s, reaccredited=%s, contact=%s, contact2=%s, contact3=%s, telephone=%s, email=%s, email2=%s, email3=%s, fax=%s, website=%s, skype=%s, invoiceto=%s, invoiceemail=%s, legalstructure=%s, typeid=%s, totaltrained=%s, ethics=%s, trainers=%s, listtrainees=%s, numbertrainees=%s, cid=%s, doa=%s, EABPreps=%s, orgfee=%s, notes=%s, showonsite=%s, username=%s, password=%s WHERE id=%s",
                       GetSQLValueString($_POST['orgname'], "text"),
                       GetSQLValueString($_POST['logofile'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['city'], "text"),
                       GetSQLValueString($_POST['accredited'], "text"),
                       GetSQLValueString($_POST['reaccredited'], "text"),
                       GetSQLValueString($_POST['contact'], "text"),
                       GetSQLValueString($_POST['contact2'], "text"),
                       GetSQLValueString($_POST['contact3'], "text"),
                       GetSQLValueString($_POST['telephone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['email2'], "text"),
                       GetSQLValueString($_POST['email3'], "text"),
                       GetSQLValueString($_POST['fax'], "text"),
                       GetSQLValueString($_POST['website'], "text"),
                       GetSQLValueString($_POST['skype'], "text"),
                       GetSQLValueString($_POST['invoiceto'], "text"),
                       GetSQLValueString($_POST['invoiceemail'], "text"),
                       GetSQLValueString($_POST['legalstructure'], "text"),
                       GetSQLValueString($_POST['typeid'], "int"),
                       GetSQLValueString($_POST['totaltrained'], "text"),
                       GetSQLValueString($_POST['ethics'], "text"),
                       GetSQLValueString($_POST['trainers'], "text"),
                       GetSQLValueString($_POST['listtrainees'], "text"),
                       GetSQLValueString($_POST['numbertrainees'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['doa'], "text"),
                       GetSQLValueString($_POST['EABPreps'], "text"),
                       GetSQLValueString($_POST['orgfee'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['password'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsForum = "-1";
if (isset($_GET['id'])) {
  $colname_rsForum = $_GET['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsForum = sprintf("SELECT * FROM forum WHERE id = %s", GetSQLValueString($colname_rsForum, "int"));
$rsForum = mysql_query($query_rsForum, $connEABP2) or die(mysql_error());
$row_rsForum = mysql_fetch_assoc($rsForum);
$totalRows_rsForum = mysql_num_rows($rsForum);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsType = "SELECT * FROM forumtypes";
$rsType = mysql_query($query_rsType, $connEABP2) or die(mysql_error());
$row_rsType = mysql_fetch_assoc($rsType);
$totalRows_rsType = mysql_num_rows($rsType);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Forum member edit</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>Edit Forum member</h1>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,PNG',false,'','','','','','','');return document.MM_returnValue">
  <table cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Login username:</td>
      <td><input name="username" type="text" class="input400" id="username" value="<?php echo $row_rsForum['username']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Login password:</td>
      <td><input name="password" type="text" class="input400" id="password" value="<?php echo $row_rsForum['password']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Show on site:</td>
      <td><input name="showonsite" type="checkbox" class="input50" value=""  <?php if (!(strcmp(htmlentities($row_rsForum['showonsite'], ENT_COMPAT, 'utf-8'),1))) {echo "checked=\"checked\"";} ?> />
      - <a href="forum-delete.php?id=<?php echo $row_rsForum['id']; ?>">delete this forum member</a> (you will be asked to confirm)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Name:</td>
      <td><input name="orgname" type="text" class="input400" value="<?php echo ($row_rsForum['orgname']); ?>" /></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Logo file:</td>
      <td><input name="logofile" type="file" id="logofile" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,PNG',false,'','','','','','','')" />
        <span class="smallText">current:</span> <img src="../images/logos/<?php echo $row_rsForum['logofile']; ?>" alt="" align="absmiddle" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Address:</td>
      <td><input type="text" name="address" value="<?php echo htmlentities($row_rsForum['address'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">City:</td>
      <td><input type="text" name="city" value="<?php echo htmlentities($row_rsForum['city'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Country:</td>
      <td><select name="cid">
        <?php
do {  
?><option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsForum['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
              </select>      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Accredited:</td>
      <td><input name="accredited" type="text" id="accredited" value="<?php echo htmlentities($row_rsForum['accredited'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Re-accredited:</td>
      <td><input name="reaccredited" type="text" id="reaccredited" value="<?php echo htmlentities($row_rsForum['reaccredited']); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact:</td>
      <td><input type="text" name="contact" value="<?php echo htmlentities($row_rsForum['contact'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact 2:</td>
      <td><input name="contact2" type="text" id="contact2" value="<?php echo $row_rsForum['contact2']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact 3:</td>
      <td><input name="contact3" type="text" id="contact3" value="<?php echo $row_rsForum['contact3']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Telephone:</td>
      <td><input type="text" name="telephone" value="<?php echo htmlentities($row_rsForum['telephone'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email:</td>
      <td><input type="text" name="email" value="<?php echo htmlentities($row_rsForum['email'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email2:</td>
      <td><input type="text" name="email2" value="<?php echo htmlentities($row_rsForum['email2'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email3:</td>
      <td><input type="text" name="email3" value="<?php echo htmlentities($row_rsForum['email3'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Fax:</td>
      <td><input type="text" name="fax" value="<?php echo htmlentities($row_rsForum['fax'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Website:</td>
      <td><input type="text" name="website" value="<?php echo htmlentities($row_rsForum['website'], ENT_COMPAT, 'utf-8'); ?>" size="32" /> 
        NO http://</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Skype:</td>
      <td valign="baseline"><input type="text" name="skype" id="skype" /></td>
    </tr>
    <tr valign="baseline" bgcolor="#CCCCCC">
      <td nowrap="nowrap">Invoices</td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Invoice to:</td>
      <td bgcolor="#CCCCCC"><input name="invoiceto" type="text" class="input400" id="invoiceto" value="<?php echo $row_rsForum['invoiceto']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Email to:</td>
      <td bgcolor="#CCCCCC"><input name="invoiceemail" type="text" class="input400" id="invoiceemail" value="<?php echo $row_rsForum['invoiceemail']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Legal structure:</td>
      <td><input type="text" name="legalstructure" value="<?php echo htmlentities($row_rsForum['legalstructure'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Type:</td>
      <td><select name="typeid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsType['forumtypeid']?>"<?php if (!(strcmp($row_rsType['forumtypeid'], $row_rsForum['typeid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsType['forumtype']?></option>
        <?php
} while ($row_rsType = mysql_fetch_assoc($rsType));
  $rows = mysql_num_rows($rsType);
  if($rows > 0) {
      mysql_data_seek($rsType, 0);
	  $row_rsType = mysql_fetch_assoc($rsType);
  }
?>
      </select>      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Total trained:</td>
      <td><input type="text" name="totaltrained" value="<?php echo htmlentities($row_rsForum['totaltrained'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Ethics:</td>
      <td><input type="text" name="ethics" value="<?php echo htmlentities($row_rsForum['ethics'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Trainers:</td>
      <td><input type="text" name="trainers" value="<?php echo htmlentities($row_rsForum['trainers'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">List trainees:</td>
      <td><input type="text" name="listtrainees" value="<?php echo htmlentities($row_rsForum['listtrainees'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Number trainees:</td>
      <td><input type="text" name="numbertrainees" value="<?php echo htmlentities($row_rsForum['numbertrainees'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Doa:</td>
      <td><input type="text" name="doa" value="<?php echo htmlentities($row_rsForum['doa'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">EABP reps:</td>
      <td><input name="EABPreps" type="text" class="input500" value="<?php echo htmlentities($row_rsForum['EABPreps'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Org fee:</td>
      <td><input type="text" name="orgfee" value="<?php echo htmlentities($row_rsForum['orgfee'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Notes:</td>
      <td><input type="text" name="notes" value="<?php echo htmlentities($row_rsForum['notes'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id" value="<?php echo $row_rsForum['id']; ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsForum);

mysql_free_result($rsType);

mysql_free_result($rsCountries);
?>
