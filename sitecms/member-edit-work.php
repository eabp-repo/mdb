<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../member/cvs";
$ppu->extensions = "";
$ppu->formName = "frmCV";
$ppu->storeType = "file";
$ppu->sizeLimit = "100";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE membertrainingwork SET previousexperience=%s, bio=%s, professionoutsidebp=%s WHERE mid=%s",
                       GetSQLValueString($_POST['previousexperience'], "text"),
                       GetSQLValueString($_POST['bio'], "text"),
                       GetSQLValueString($_POST['professionoutsidebp'], "text"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmCV")) {
  $updateSQL = sprintf("UPDATE membercpd SET cv=IFNULL(%s,cv), showcv=%s WHERE mid=%s",
                       GetSQLValueString($_POST['cv'], "text"),
                       GetSQLValueString(isset($_POST['showcv']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_GET['delwmid'])) && ($_GET['delwmid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM workmembers WHERE wmid=%s",
                       GetSQLValueString($_GET['delwmid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmPros")) {
  $insertSQL = sprintf("INSERT INTO profmembers (mid, profid) VALUES (%s, %s)",
                       GetSQLValueString($_POST['mid'], "int"),
                       GetSQLValueString($_POST['profid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmWorkTypes")) {
  $insertSQL = sprintf("INSERT INTO workmembers (mid, wtid) VALUES (%s, %s)",
                       GetSQLValueString($_POST['mid'], "int"),
                       GetSQLValueString($_POST['wtid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT mid, firstname, lastname FROM member WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsWorkTypes = "SELECT * FROM worktypes";
$rsWorkTypes = mysql_query($query_rsWorkTypes, $connEABP2) or die(mysql_error());
$row_rsWorkTypes = mysql_fetch_assoc($rsWorkTypes);
$totalRows_rsWorkTypes = mysql_num_rows($rsWorkTypes);

$colname_rsWork = "-1";
if (isset($_GET['mid'])) {
  $colname_rsWork = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsWork = sprintf("SELECT worktype,wmid FROM workmembers INNER JOIN worktypes ON worktypes.worktypeid = workmembers.wtid WHERE mid = %s", GetSQLValueString($colname_rsWork, "int"));
$rsWork = mysql_query($query_rsWork, $connEABP2) or die(mysql_error());
$row_rsWork = mysql_fetch_assoc($rsWork);
$totalRows_rsWork = mysql_num_rows($rsWork);

$colname_rsProsWith = "-1";
if (isset($_GET['mid'])) {
  $colname_rsProsWith = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsProsWith = sprintf("SELECT profession FROM profmembers INNER JOIN professions ON profmembers.profid = professions.profid WHERE mid = %s", GetSQLValueString($colname_rsProsWith, "int"));
$rsProsWith = mysql_query($query_rsProsWith, $connEABP2) or die(mysql_error());
$row_rsProsWith = mysql_fetch_assoc($rsProsWith);
$totalRows_rsProsWith = mysql_num_rows($rsProsWith);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPros = "SELECT * FROM professions";
$rsPros = mysql_query($query_rsPros, $connEABP2) or die(mysql_error());
$row_rsPros = mysql_fetch_assoc($rsPros);
$totalRows_rsPros = mysql_num_rows($rsPros);

$colname_rsTrainWork = "-1";
if (isset($_GET['mid'])) {
  $colname_rsTrainWork = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTrainWork = sprintf("SELECT * FROM membertrainingwork WHERE mid = %s", GetSQLValueString($colname_rsTrainWork, "int"));
$rsTrainWork = mysql_query($query_rsTrainWork, $connEABP2) or die(mysql_error());
$row_rsTrainWork = mysql_fetch_assoc($rsTrainWork);
$totalRows_rsTrainWork = mysql_num_rows($rsTrainWork);

$colname_rsCPD = "-1";
if (isset($_GET['mid'])) {
  $colname_rsCPD = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCPD = sprintf("SELECT * FROM membercpd WHERE mid = %s", GetSQLValueString($colname_rsCPD, "int"));
$rsCPD = mysql_query($query_rsCPD, $connEABP2) or die(mysql_error());
$row_rsCPD = mysql_fetch_assoc($rsCPD);
$totalRows_rsCPD = mysql_num_rows($rsCPD);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP individual membership database update</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='javascript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='javascript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p class="leftAlign"><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" />  admin home</a></p>
<!-- Google Translate -->
<div id="google_translate_element"></div>
<p class="googleTranslateText">
  <script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    includedLanguages: 'bg,da,nl,en,fi,fr,de,el,iw,it,ja,no,pl,pt,ru,sr,sl,es,sv',
    gaTrack: true,
    gaId: 'UA-224881-50',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script>
  <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  The Google Translator is here to give you some help but it is not perfect!
  <!-- End -->
</p>
<p class="clear">EABP individual member update:</p>
<p><a href="member-edit-login.php?mid=<?php echo $row_rsMembers['mid']; ?>">Login details</a> | <a href="member-edit-general.php?mid=<?php echo $row_rsMembers['mid']; ?>">General</a> | <a href="member-edit-contact.php?mid=<?php echo $row_rsMembers['mid']; ?>">Contact details</a> | <a href="member-edit-membership.php?mid=<?php echo $row_rsMembers['mid']; ?>">Membership</a> | <a href="member-edit-training.php?mid=<?php echo $row_rsMembers['mid']; ?>">Training</a> | Current Work | <a href="member-edit-cpd.php?mid=<?php echo $row_rsMembers['mid']; ?>">Congresses attended</a> | <a href="member-delete.php?mid=<?php echo $row_rsMembers['mid']; ?>">Delete this member</a> (you will be asked to confirm)</p>
<hr size="1" noshade="noshade" />
<h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
<h2>Current work</h2>
<hr size="1" noshade="noshade" />
<?php if ($totalRows_rsTrainWork == 0) { // Show if recordset empty ?>
  <h2><a href="member-work-add.php?mid=<?php echo $row_rsMembers['mid']; ?>">To activate this section click here</a></h2>
  <?php } // Show if recordset empty ?>
<?php if ($totalRows_rsTrainWork > 0) { // Show if recordset not empty ?>
  <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
    <table align="left" cellpadding="3" cellspacing="0">
      <tr valign="baseline">
        <td nowrap="nowrap" align="right" valign="top">Previous experience:</td>
        <td><textarea name="previousexperience" rows="10" class="input500"><?php echo $row_rsTrainWork['previousexperience']; ?></textarea>      </td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right" valign="top">Biography:</td>
        <td><textarea name="bio" rows="10" class="input500"><?php echo $row_rsTrainWork['bio']; ?></textarea>      </td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">Profession outside BP:</td>
        <td><input type="text" name="professionoutsidebp" value="<?php echo $row_rsTrainWork['professionoutsidebp']; ?>" /></td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">&nbsp;</td>
        <td><input type="submit" class="btnAdd" value="Update" /></td>
      </tr>
    </table>
    <input type="hidden" name="MM_update" value="form1" />
    <input type="hidden" name="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
  </form>
<?php } // Show if recordset not empty ?>
<h2>Curriculum Vitae
      <?php if (isset($_POST['cvprocessed'])) { ?>
        <img src="../images/tick.gif" alt="tick" width="16" height="16" />
        <?php } ?>
</h2>
    <p>Can be a Word document or pdf. 100kb limit.</p>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data"  name="frmCV" id="frmCV" onsubmit="checkFileUpload(this,'',false,100,'','','','','','');return document.MM_returnValue" >
      <input name="cv" type="file" class="input200" id="cv" onchange="checkOneFileUpload(this,'',false,100,'','','','','','')"  value="<?php echo $row_rsCPD['cv']; ?>" />
      
      <input name="mid" type="hidden" id="mid" value="<?php echo $_GET['mid']; ?>" />
      <input name="cvprocessed" type="hidden" id="cvprocessed" value="1" />
  <label><br />
  Show on website:</label>
      <input <?php if (!(strcmp($row_rsCPD['showcv'],1))) {echo "checked=\"checked\"";} ?> name="showcv" type="checkbox" class="input20" id="showcv" value="1" />
      <input name="btnUpload" type="submit" class="btnGo clear" id="btnUpload" value="Go" />
      <input type="hidden" name="MM_update" value="frmCV" />
</form>
    <hr />
<?php if ($totalRows_rsTrainWork > 0) { // Show if recordset not empty ?>
  <div id="rightBar">
    <h3>Current work: <?php if ($totalRows_rsWork == 0) { // Show if recordset empty ?>no entry<?php } // Show if recordset empty ?></h3>
    
    <?php if ($totalRows_rsWork > 0) { // Show if recordset not empty ?>
      <ul>
        <?php do { ?>
          <li><?php echo $row_rsWork['worktype']; ?> - <a href="member-edit-work.php?mid=<?php echo $_GET['mid']; ?>&amp;delwmid=<?php echo $row_rsWork['wmid']; ?>">delete</a></li>
          <?php } while ($row_rsWork = mysql_fetch_assoc($rsWork)); ?>
      </ul>
      <?php } // Show if recordset not empty ?>
    <form id="frmWorkTypes" name="frmWorkTypes" method="POST" action="<?php echo $editFormAction; ?>">
      <select name="wtid" id="wtid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsWorkTypes['worktypeid']?>"><?php echo $row_rsWorkTypes['worktype']?></option>
        <?php
} while ($row_rsWorkTypes = mysql_fetch_assoc($rsWorkTypes));
  $rows = mysql_num_rows($rsWorkTypes);
  if($rows > 0) {
      mysql_data_seek($rsWorkTypes, 0);
	  $row_rsWorkTypes = mysql_fetch_assoc($rsWorkTypes);
  }
?>
      </select>
      <br />
      <input name="button" type="submit" class="btnGo" id="button" value="Add" />
      <input name="mid" type="hidden" id="mid" value="<?php echo $_GET['mid']; ?>" />
      <input type="hidden" name="MM_insert" value="frmWorkTypes" />
    </form>
    
    <hr size="1" noshade="noshade" />
    <h3>Professionals working with: <?php if ($totalRows_rsProsWith == 0) { // Show if recordset empty ?>no entry<?php } // Show if recordset empty ?></h3>
    
    
    <?php if ($totalRows_rsProsWith > 0) { // Show if recordset not empty ?>  
      <ul>
        
        <?php do { ?>
          <li><?php echo $row_rsProsWith['profession']; ?></li>
          <?php } while ($row_rsProsWith = mysql_fetch_assoc($rsProsWith)); ?>
      </ul>        
      <?php } // Show if recordset not empty ?>
    
    <form id="frmPros" name="frmPros" method="POST" action="<?php echo $editFormAction; ?>">
      <select name="profid" id="profid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsPros['profid']?>"><?php echo $row_rsPros['profession']?></option>
        <?php
} while ($row_rsPros = mysql_fetch_assoc($rsPros));
  $rows = mysql_num_rows($rsPros);
  if($rows > 0) {
      mysql_data_seek($rsPros, 0);
	  $row_rsPros = mysql_fetch_assoc($rsPros);
  }
?>
      </select>
      <br />
      <input name="button2" type="submit" class="btnGo" id="button2" value="Add" />
      <input name="mid" type="hidden" id="mid" value="<?php echo $_GET['mid']; ?>" />
      <input type="hidden" name="MM_insert" value="frmPros" />
    </form>
    <p>&nbsp;</p>
  </div>
  <?php } // Show if recordset not empty ?>
</body>
</html>
<?php
mysql_free_result($rsMembers);

mysql_free_result($rsWorkTypes);

mysql_free_result($rsWork);

mysql_free_result($rsProsWith);

mysql_free_result($rsPros);

mysql_free_result($rsTrainWork);

mysql_free_result($rsCPD);
?>
