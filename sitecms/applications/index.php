<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApplications = "SELECT DISTINCTROW aid, applications.firstname, applications.lastname, applications.tid, applications.submitted, applications.doa, applications.email, approved, member.doj FROM applications LEFT JOIN member ON applications.email = member.email WHERE applications.lastname <> '' AND submitted = 1 ORDER BY doa DESC";
$rsApplications = mysql_query($query_rsApplications, $connEABP2) or die(mysql_error());
$row_rsApplications = mysql_fetch_assoc($rsApplications);
$totalRows_rsApplications = mysql_num_rows($rsApplications);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgs = "SELECT applicationorgs.orgid, applicationorgs.orgname, applicationorgs.submitted, applicationorgs.datesubmitted, allorgs.approved, allorgs.doj, allorgs.id FROM applicationorgs INNER JOIN allorgs ON applicationorgs.email=allorgs.email WHERE submitted = 1 ORDER BY datesubmitted DESC";
$rsOrgs = mysql_query($query_rsOrgs, $connEABP2) or die(mysql_error());
$row_rsOrgs = mysql_fetch_assoc($rsOrgs);
$totalRows_rsOrgs = mysql_num_rows($rsOrgs);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Applications</title>
<link href="../cms.css" rel="stylesheet" type="text/css">
<style type="text/css">
.col1 {
	float: left;
	width: 45%;
	margin-right: 5%;
}
.col2 {
	float: left;
	width: 45%;
	margin-right: 0%;
}
ul li {
	margin-bottom: 2px;
}
</style>
</head>

<body>
<p><img src="../../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> <a href="../index.php">admin home</a></p>
<h1>Applications</h1>
<p><a href="search.php">Search</a>, or click the name below, to review application (and delete option)</p>
<div class="col1">
<h2>Individuals</h2>
<ul>
  <?php do { ?>
    <li><a href="application.php?aid=<?php echo $row_rsApplications['aid']; ?>"><?php echo $row_rsApplications['firstname']; ?>&nbsp;<?php echo $row_rsApplications['lastname']; ?></a><br>
    <em>
<?php
switch($row_rsApplications['tid'])
{
	case 3 : echo("Associate"); break;
	case 4 : echo("Candidate"); break;
	case 7 : echo("Full"); break;
	case 13 : echo("Student"); break;
}
?>	
</em>	
	<?php if ($row_rsApplications['submitted'] ==1) { ?> - submitted <?php echo date("j M Y",strtotime($row_rsApplications['doa'])); ?> <?php if ($row_rsApplications['approved'] ==1) { ?>- approved and joined <?php echo date("j M Y",strtotime($row_rsApplications['doj'])); ?><?php }?>
      <?php } ?>
    </li>
    <?php } while ($row_rsApplications = mysql_fetch_assoc($rsApplications)); ?>
</ul>
</div>
<div class="col2">
<h2>Organisations</h2>
<ul>
  <?php do { ?>
    <li><a href="application-org.php?orgid=<?php echo $row_rsOrgs['orgid']; ?>"><?php echo $row_rsOrgs['orgname']; ?></a> 
      <?php if ($row_rsOrgs['submitted'] ==1) { ?>
      <br>
      submitted <?php echo date("j M Y",strtotime($row_rsOrgs['datesubmitted'])); }?>
      <?php if ($row_rsOrgs['approved'] ==1) { ?>- approved and joined <?php echo date("j M Y",strtotime($row_rsOrgs['doj'])); ?><?php }?>
     
      
      </li>
    <?php } while ($row_rsOrgs = mysql_fetch_assoc($rsOrgs)); ?>
</ul>
</div>
</body>
</html>
<?php
mysql_free_result($rsApplications);

mysql_free_result($rsOrgs);

?>
