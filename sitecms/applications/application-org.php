<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "../login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmJoin")) {
  $insertSQL = sprintf("INSERT INTO allorgs (approved, orgname, address, postcode, city, contact, telephone, email, website, type, doa) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['approved'], "int"),
					   GetSQLValueString($_POST['orgname'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['postcode'], "text"),
                       GetSQLValueString($_POST['city'], "text"),
                       GetSQLValueString($_POST['contact'], "text"),
                       GetSQLValueString($_POST['telephone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['website'], "text"),
                       GetSQLValueString($_POST['type'], "text"),
                       GetSQLValueString($_POST['doa'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "../confirmed-member.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

$colname_rsOrgApp = "-1";
if (isset($_GET['orgid'])) {
  $colname_rsOrgApp = $_GET['orgid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgApp = sprintf("SELECT * FROM applicationorgs WHERE orgid = %s", GetSQLValueString($colname_rsOrgApp, "int"));
$rsOrgApp = mysql_query($query_rsOrgApp, $connEABP2) or die(mysql_error());
$row_rsOrgApp = mysql_fetch_assoc($rsOrgApp);
$totalRows_rsOrgApp = mysql_num_rows($rsOrgApp);

$colname_rsDocs = "-1";

  $colname_rsDocs = $row_rsOrgApp['orgid'];

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocs = sprintf("SELECT appdocid, doctitle, docfile FROM apporgdocs WHERE orgid = %s", GetSQLValueString($colname_rsDocs, "int"));
$rsDocs = mysql_query($query_rsDocs, $connEABP2) or die(mysql_error());
$row_rsDocs = mysql_fetch_assoc($rsDocs);
$totalRows_rsDocs = mysql_num_rows($rsDocs);

  $colname_rsAllorgs = $row_rsOrgApp['email'];
  
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsAllorgs = sprintf("SELECT * FROM allorgs WHERE email = %s", GetSQLValueString($colname_rsAllorgs, "text"));
$rsAllorgs = mysql_query($query_rsAllorgs, $connEABP2) or die(mysql_error());
$row_rsAllorgs = mysql_fetch_assoc($rsAllorgs);
$totalRows_rsAllorgs = mysql_num_rows($rsAllorgs);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>EABP application - <?php echo $row_rsOrgApp['orgname']; ?></title>
<style type="text/css">
body {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 14px;
	width: 80%;
	margin-right: auto;
	margin-left: auto;
	margin-top: 20px;
	margin-bottom: 20px;
}
h1{
	color: #AC3556;

}
h2{
	color: #AC3556;
	padding-top: 10px;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #AC3556;
	margin-top: 20px;
}
h3    {
	margin: 0px;
	padding: 5px;
	background-color: #E8E8E8;
	font-weight: normal;

}

th {
	font-weight: normal;
	text-align: left;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #CCCCCC;
	width: 300px;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC;
}
td {
	padding: 4px;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #CCCCCC;
	font-weight: normal;
	font-style: italic;
}
.highlighted {
	background-color: #FFFF66;
}
.close {
	display: block;
	float: right;
	text-transform: uppercase;
	text-decoration: none;
}
#btnJoin {
	padding: 1%;
	color: #FFFFFF;
	background-color: #009900;
	font-size: 1.6em;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	cursor: pointer;
}
.allCaps {
	text-transform: uppercase;
}
</style>
</head>

<body id="top">
<p><img src="../../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> <a href="../index.php">admin home</a> | <a href="index.php">applications</a></p>
<p>EABP  membership application for <strong><?php echo $row_rsOrgApp['orgname']; ?></strong></p>

<p><a href="application-org-delete.php?orgid=<?php echo $row_rsOrgApp['orgid']; ?>">Delete application &gt;</a> (you will be asked to confirm)</p>

<?php if ($totalRows_rsAllorgs == 0) { // Show if recordset empty ?>
<?php if ($_SESSION['MM_Username'] == 'officemanager@eabp.org') { ?>
  <form action="<?php echo $editFormAction; ?>" method="POST" name="frmJoin" id="frmJoin">
    <input type="submit" name="btnJoin" id="btnJoin" value="If the application has been approved CLICK HERE to add <?php echo $row_rsOrgApp['orgname']; ?> to the EABP membership database">
    <input name="orgname" type="hidden" id="orgname" value="<?php echo $row_rsOrgApp['orgname']; ?>">
    <input name="address" type="hidden" id="address" value="<?php echo $row_rsOrgApp['address']; ?>">
    <input name="city" type="hidden" id="city" value="<?php echo $row_rsOrgApp['city']; ?>">
    <input name="postcode" type="hidden" id="postcode" value="<?php echo $row_rsOrgApp['postcode']; ?>">
    <input name="telephone" type="hidden" id="telephone" value="<?php echo $row_rsOrgApp['telephone']; ?>">
    <input name="email" type="hidden" id="email" value="<?php echo $row_rsOrgApp['email']; ?>">
    <input name="website" type="hidden" id="website" value="<?php echo $row_rsOrgApp['website']; ?>">
    <input name="doa" type="hidden" id="doa" value="<?php echo $row_rsOrgApp['doa']; ?>">
    <input name="contact" type="hidden" id="contact" value="<?php echo $row_rsOrgApp['contact']; ?>">
    <input name="email" type="hidden" id="email" value="<?php echo $row_rsOrgApp['email']; ?>">
    <input name="type" type="hidden" id="type" value="<?php echo $row_rsOrgApp['type']; ?>">
    <input type="hidden" name="MM_insert" value="frmJoin">
    <input name="approved" type="hidden" id="approved" value="1">
  </form>
  <?php } ?>
  <?php } // Show if recordset empty ?>
  
  <?php if ($row_rsAllorgs['approved'] ==1) { ?>
<p class="allCaps"><strong>Approved</strong></p>
<?php } ?>
    <?php if($row_rsOrgApp['submitted'] ==1) {?>
<p><strong>Submitted on <?php echo date("j M Y",strtotime($row_rsOrgApp['datesubmitted'])); ?> at <?php echo date("g:i a",strtotime($row_rsOrgApp['datesubmitted'])); ?> (GMT).</strong></p>
<?php } ?>
<h1>Review application </h1>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblApplication">
  <tr>
    <th colspan="2" scope="row"><h3>Contact details</h3></th>
  </tr>
  <tr>
    <th scope="row">Contact</th>
    <td><?php echo $row_rsOrgApp['contact']; ?></td>
  </tr>
  <tr>
    <th scope="row">Contact function</th>
    <td><?php echo $row_rsOrgApp['contactfunction']; ?></td>
  </tr>
  <tr>
    <th scope="row">Email</th>
    <td><?php echo $row_rsOrgApp['email']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>Organisation details</h3></th>
  </tr>
  <tr>
    <th scope="row">Organisation name</th>
    <td><?php echo $row_rsOrgApp['orgname']; ?></td>
  </tr>
  <tr>
    <th scope="row">Address</th>
    <td><?php echo $row_rsOrgApp['address']; ?></td>
  </tr>
  <tr>
    <th scope="row">City</th>
    <td><?php echo $row_rsOrgApp['city']; ?></td>
  </tr>
  <tr>
    <th scope="row">Post code</th>
    <td><?php echo $row_rsOrgApp['postcode']; ?></td>
  </tr>
  <tr>
    <th scope="row">Telephone</th>
    <td><?php echo $row_rsOrgApp['telephone']; ?></td>
  </tr>
  <tr>
    <th scope="row">Website<br /></th>
    <td><?php echo $row_rsOrgApp['website']; ?></td>
  </tr>
  </table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th align="right">Sub-modality</th>
    <td><?php echo $row_rsOrgApp['submodality']; ?></td>
  </tr>
  <tr>
    <th align="right">How many years have you been in existence?</th>
    <td><?php echo $row_rsOrgApp['yearsexistence']; ?></td>
  </tr>
  <tr>
    <th align="right">What is your legal organisational (or corporate) structure?</th>
    <td><?php echo $row_rsOrgApp['legalstructure']; ?></td>
  </tr>
  <tr>
    <th align="right">Do you have an appropriate code of ethics and complaints procedure?</th>
    <td valign="middle"><?php echo $row_rsOrgApp['codeofethics']; ?></td>
  </tr>
  <tr>
    <th align="right">Type of organisation</th>
    <td><?php echo $row_rsOrgApp['type']; ?></td>
  </tr>
  <tr>
    <th align="right">EABP members</th>
    <td><?php echo $row_rsOrgApp['eabpmembers']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right">&nbsp;</th>
  </tr>
  <tr>
    <th colspan="2" align="right"><h3>Training organisations only:</h3></th>
  </tr>
  <tr>
    <th align="right">About how many people  have you trained?  </th>
    <td><?php echo $row_rsOrgApp['totaltrained']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right">How many people are currently involved in the  organisation?</th>
  </tr>
  <tr>
    <td align="right">Trainers:</td>
    <td><?php echo $row_rsOrgApp['notrainers']; ?></td>
  </tr>
  <tr>
    <td align="right">Supervisors:</td>
    <td><?php echo $row_rsOrgApp['nosupervisors']; ?></td>
  </tr>
  <tr>
    <td align="right">Admin staff:</td>
    <td><?php echo $row_rsOrgApp['noadminstaff']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right">How many trainees do  you have in the:</th>
  </tr>
  <tr>
    <td align="right">First year?</td>
    <td><?php echo $row_rsOrgApp['notraineesfirst']; ?></td>
  </tr>
  <tr>
    <td align="right">Second year?</td>
    <td><?php echo $row_rsOrgApp['notraineessecond']; ?></td>
  </tr>
  <tr>
    <td align="right"> Third year?</td>
    <td><?php echo $row_rsOrgApp['notraineesthird']; ?></td>
  </tr>
  <tr>
    <td align="right">Fourth year?</td>
    <td><?php echo $row_rsOrgApp['notraineesfourth']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right"><h3>Professional  associations only:</h3></th>
  </tr>
  <tr>
    <th align="right">How many members do  you have?</th>
    <td><?php echo $row_rsOrgApp['totalmembers']; ?></td>
  </tr>
  <tr>
    <th align="right">Among your members how many practitioners do you  have who fulfil the membership criteria of EABP or something similar?</th>
    <td valign="middle"><?php echo $row_rsOrgApp['nofulfilcriteria']; ?></td>
  </tr>
  <tr>
    <th colspan="2" align="right"><h3>Other organisations only:</h3></th>
  </tr>
  <tr>
    <th align="right">Details:</th>
    <td><?php echo $row_rsOrgApp['otherorgdetails']; ?></td>
  </tr>
</table>

<h2>Documentation </h2>
<?php if ($totalRows_rsDocs > 0) { // Show if recordset not empty ?>
  <h3>Documents uploaded and saved:</h3>
      <ul id="docList">
        <?php do { ?>
        <li><a href="../../apply/docs/<?php echo $row_rsDocs['docfile']; ?>"><?php echo $row_rsDocs['doctitle']; ?></a><br>
          </li>
          <?php } while ($row_rsDocs = mysql_fetch_assoc($rsDocs)); ?>
      </ul>
      <?php } // Show if recordset not empty ?>
<p><a href="#top">Back to top </a></p>
</body>
</html>
<?php
mysql_free_result($rsOrgApp);

mysql_free_result($rsDocs);

mysql_free_result($rsAllorgs);
?>
