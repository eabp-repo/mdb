<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsApplications = "-1";
if (isset($_GET['q'])) {
  $colname_rsApplications = $_GET['q'];
}
$colname2_rsApplications = "-1";
if (isset($_GET['q'])) {
  $colname2_rsApplications = $_GET['q'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApplications = sprintf("SELECT DISTINCTROW aid, applications.firstname, applications.lastname, applications.tid, applications.submitted, applications.doa, applications.email, approved, member.doj FROM applications LEFT JOIN member ON applications.email = member.email WHERE (applications.firstname LIKE %s OR applications.lastname LIKE %s) AND applications.lastname <> '' AND submitted = 1 ORDER BY doa DESC ", GetSQLValueString("%" . $colname_rsApplications . "%", "text"),GetSQLValueString("%" . $colname2_rsApplications . "%", "int"));
$rsApplications = mysql_query($query_rsApplications, $connEABP2) or die(mysql_error());
$row_rsApplications = mysql_fetch_assoc($rsApplications);
$totalRows_rsApplications = mysql_num_rows($rsApplications);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Applications Seach</title>
<link href="../cms.css" rel="stylesheet" type="text/css">
<style type="text/css">
.col1 {
	float: left;
	width: 45%;
	margin-right: 5%;
}
.col2 {
	float: left;
	width: 45%;
	margin-right: 0%;
}
ul li {
	margin-bottom: 2px;
}
</style>
</head>

<body>
<p><img src="../../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> <a href="../index.php">admin home</a></p>
<h1>Application Search</h1>
<p>Enter part of first name or last name:</p>
<form action="" method="get" name="frmSearch" id="frmSearch">
  <input name="q" type="text" class="input200" id="q">
  <input name="btnSearch" type="submit" class="btnAdd" id="btnSearch" value="Search">
</form>
<p>&nbsp;</p>
<div class="col1">
<?php if ($totalRows_rsApplications > 0) { // Show if recordset not empty ?>

    <p>Click the name below, to review application (and delete option)</p>
    <ul>
      <?php do { ?>
        <li><a href="application.php?aid=<?php echo $row_rsApplications['aid']; ?>"><?php echo $row_rsApplications['firstname']; ?>&nbsp;<?php echo $row_rsApplications['lastname']; ?></a><br>
          <em>
          <?php
switch($row_rsApplications['tid'])
{
	case 3 : echo("Associate"); break;
	case 4 : echo("Candidate"); break;
	case 7 : echo("Full"); break;
	case 13 : echo("Student"); break;
}
?>	
          </em>	
          <?php if ($row_rsApplications['submitted'] ==1) { ?> - submitted <?php echo date("j M Y",strtotime($row_rsApplications['doa'])); ?> <?php if ($row_rsApplications['approved'] ==1) { ?>- approved and joined <?php echo date("j M Y",strtotime($row_rsApplications['doj'])); ?><?php }?>
          <?php } ?>
        </li>
        <?php } while ($row_rsApplications = mysql_fetch_assoc($rsApplications)); ?>
    </ul>

  <?php } // Show if recordset not empty ?>
  <?php if  ($totalRows_rsApplications == 0) { // Show if recordset empty ?>
  <?php if (isset($_GET['q'])) { ?>
  <p>No name found for <em><?php echo $_GET['q']; ?></em></p>
  <?php } ?>
  <?php } // Show if recordset empty ?>
</div>  
</body>
</html>
<?php
mysql_free_result($rsApplications);
?>
