<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "../login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmJoin")) {
  $insertSQL = sprintf("INSERT INTO member (approved, doa, dob, doj, email, firstname, gender, homecity, homephone, homepostcode, homeadd1, lastname, title, webpage, nationality, cid, nid, tid, langs, reasonapply) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,  %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['approved'], "int"),
                       GetSQLValueString($_POST['doa'], "text"),
                       GetSQLValueString($_POST['dob'], "text"),
                       GetSQLValueString($_POST['doj'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['firstname'], "text"),
                       GetSQLValueString($_POST['gender'], "text"),
                       GetSQLValueString($_POST['homecity'], "text"),
                       GetSQLValueString($_POST['homephone'], "text"),
                       GetSQLValueString($_POST['homepostcode'], "text"),
                       GetSQLValueString($_POST['homeadd1'], "text"),
                       GetSQLValueString($_POST['lastname'], "text"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['webpage'], "text"),
                       GetSQLValueString($_POST['nationality'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['nid'], "int"),
                       GetSQLValueString($_POST['tid'], "int"),
                       GetSQLValueString($_POST['langs'], "text"),
                       GetSQLValueString($_POST['reasonapply'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "../confirmed-member.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

$colname_rsApp = "-1";
if (isset($_GET['aid'])) {
  $colname_rsApp = $_GET['aid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApp = sprintf("SELECT * FROM applications WHERE aid = %s", GetSQLValueString($colname_rsApp, "int"));
$rsApp = mysql_query($query_rsApp, $connEABP2) or die(mysql_error());
$row_rsApp = mysql_fetch_assoc($rsApp);
$totalRows_rsApp = mysql_num_rows($rsApp);

$colname_rsCountry = "-1";
if (isset($_GET['aid'])) {
  $colname_rsCountry = $_GET['aid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountry = sprintf("SELECT country FROM country INNER JOIN applications ON applications.cid=country.cid WHERE aid = %s", GetSQLValueString($colname_rsCountry, "int"));
$rsCountry = mysql_query($query_rsCountry, $connEABP2) or die(mysql_error());
$row_rsCountry = mysql_fetch_assoc($rsCountry);
$totalRows_rsCountry = mysql_num_rows($rsCountry);

$colname_rsDocs = "-1";

  $colname_rsDocs = $row_rsApp['aid'];

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocs = sprintf("SELECT appdocid, notes, doctype, docfile FROM appdocs INNER JOIN appdoctypes ON appdocs.doctypeid=appdoctypes.doctypeid WHERE aid = %s", GetSQLValueString($colname_rsDocs, "int"));
$rsDocs = mysql_query($query_rsDocs, $connEABP2) or die(mysql_error());
$row_rsDocs = mysql_fetch_assoc($rsDocs);
$totalRows_rsDocs = mysql_num_rows($rsDocs);

  $colname_rsMember = $row_rsApp['email'];

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMember = sprintf("SELECT mid, approved FROM member WHERE email = %s AND approved IS NOT NULL", GetSQLValueString($colname_rsMember, "text"));
$rsMember = mysql_query($query_rsMember, $connEABP2) or die(mysql_error());
$row_rsMember = mysql_fetch_assoc($rsMember);
$totalRows_rsMember = mysql_num_rows($rsMember);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>EABP application - <?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?></title>
<style type="text/css">
body {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 14px;
	width: 80%;
	margin-right: auto;
	margin-left: auto;
	margin-top: 20px;
	margin-bottom: 20px;
}
h1{
	color: #AC3556;

}
h2{
	color: #AC3556;
	padding-top: 10px;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #AC3556;
	margin-top: 20px;
}
h3    {
	margin: 0px;
	padding: 5px;
	background-color: #E8E8E8;
	font-weight: normal;

}

th {
	font-weight: normal;
	text-align: left;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #CCCCCC;
	width: 300px;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC;
}
td {
	padding: 4px;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #CCCCCC;
	font-weight: normal;
	font-style: italic;
}
.highlighted {
	background-color: #FFFF66;
}
.close {
	display: block;
	float: right;
	text-transform: uppercase;
	text-decoration: none;
}
#btnJoin {
	padding: 1%;
	color: #FFFFFF;
	background-color: #009900;
	font-size: 1.6em;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	cursor: pointer;
}
.allCaps {
	text-transform: uppercase;
}
</style>
</head>

<body id="top">
<p><img src="../../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> <a href="../index.php">admin home</a> | <a href="index.php">applications</a></p>
<p><a href="application-delete.php?aid=<?php echo $row_rsApp['aid']; ?>">Delete application &gt;</a> (you will be asked to confirm)</p>
<?php if ($totalRows_rsMember == 0) { // Show if recordset empty ?>
<?php if ($_SESSION['MM_Username'] == 'officemanager@eabp.org') { ?>
  <form action="<?php echo $editFormAction; ?>" method="POST" name="frmJoin" id="frmJoin">
    <input type="submit" name="btnJoin" id="btnJoin" value="If the application has been approved CLICK HERE to add <?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?> to the EABP membership database">
    <input name="firstname" type="hidden" id="firstname" value="<?php echo $row_rsApp['firstname']; ?>">
    <input name="lastname" type="hidden" id="lastname" value="<?php echo $row_rsApp['lastname']; ?>">
    <input name="title" type="hidden" id="title" value="<?php echo $row_rsApp['title']; ?>">
    <input name="homeadd1" type="hidden" id="homeadd1" value="<?php echo $row_rsApp['homeadd1']; ?>">
    <input name="homecity" type="hidden" id="homecity" value="<?php echo $row_rsApp['homecity']; ?>">
    <input name="homepostcode" type="hidden" id="homepostcode" value="<?php echo $row_rsApp['homepostcode']; ?>">
    <input name="cid" type="hidden" id="cid" value="<?php echo $row_rsApp['cid']; ?>">
    <input name="homephone" type="hidden" id="homephone" value="<?php echo $row_rsApp['homephone']; ?>">
    <input name="email" type="hidden" id="email" value="<?php echo $row_rsApp['email']; ?>">
    <input name="webpage" type="hidden" id="webpage" value="<?php echo $row_rsApp['webpage']; ?>">
    <input name="dob" type="hidden" id="dob" value="<?php echo $row_rsApp['dob']; ?>">
    <input name="doa" type="hidden" id="doa" value="<?php echo $row_rsApp['doa']; ?>">
    <input name="doj" type="hidden" id="doj" value="<?php echo date('Y-m-d') ; ?>">
    <input name="gender" type="hidden" id="gender" value="<?php echo $row_rsApp['gender']; ?>">
    <input name="nationality" type="hidden" id="nationality" value="<?php echo $row_rsApp['nationality']; ?>">
    <input name="langs" type="hidden" id="langs" value="<?php echo $row_rsApp['langs']; ?>">
    <input name="reasonapply" type="hidden" id="reasonapply" value="<?php echo $row_rsApp['reasonapply']; ?>">
    <input name="nid" type="hidden" id="nid" value="12">
    <input name="tid" type="hidden" id="tid" value="<?php echo $row_rsApp['tid']; ?>">
    <input type="hidden" name="MM_insert" value="frmJoin">
    <input name="approved" type="hidden" id="approved" value="1">
  </form>
  <?php } ?>
  <?php } // Show if recordset empty ?>
<h1><em>
<?php
switch($row_rsApp['tid'])
{
	case 3 : echo("Associate"); break;
	case 4 : echo("Candidate"); break;
	case 7 : echo("Full"); break;
	case 13 : echo("Student"); break;
}
?></em>  membership application for <strong><?php echo $row_rsApp['firstname']; ?> <?php echo $row_rsApp['lastname']; ?></strong></h1>
<?php if ($row_rsMember['approved'] ==1) { ?>
<p class="allCaps"><strong>Approved</strong></p>
<?php } ?>

<!-- FULL --> 
<p><span class="highlighted">Highlighted</span> questions are <span class="allCaps"><strong>not</strong></span> required if the applicant is a graduate of an EABP <span class="allCaps">forum</span> training institute.</p>
<!-- FULL --> 


<?php if($row_rsApp['submitted'] ==1) {?>
<p><strong>Submitted on <?php echo date("j M Y",strtotime($row_rsApp['datesubmitted'])); ?> at <?php echo date("g:i a",strtotime($row_rsApp['datesubmitted'])); ?> (GMT).</strong></p>
<?php } ?>

<!-- FULL --> 
<h2>Section 1 - Personal Data</h2>
<!-- FULL --> 

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tblApplication">
  <tr>
    <th scope="row">First name</th>
    <td><?php echo $row_rsApp['firstname']; ?></td>
  </tr>
  <tr>
    <th scope="row">Last name</th>
    <td><?php echo $row_rsApp['lastname']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>Contact details</h3></th>
  </tr>
  <tr>
    <th scope="row">Home address</th>
    <td><?php echo $row_rsApp['homeadd1']; ?></td>
  </tr>
  <tr>
    <th scope="row">Town/City</th>
    <td><?php echo $row_rsApp['homecity']; ?></td>
  </tr>
  <tr>
    <th scope="row">Post code</th>
    <td><?php echo $row_rsApp['homepostcode']; ?></td>
  </tr>
  <tr>
    <th scope="row">Country</th>
    <td><?php echo $row_rsCountry['country']; ?></td>
  </tr>
  <tr>
    <th scope="row">Telephone</th>
    <td><?php echo $row_rsApp['homephone']; ?></td>
  </tr>
  <tr>
    <th scope="row">Email</th>
    <td><a href="mailto:<?php echo $row_rsApp['email']; ?>"><?php echo $row_rsApp['email']; ?></a></td>
  </tr>
  <tr>
    <th scope="row">Website<br /></th>
    <td><?php echo $row_rsApp['webpage']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>Personal details</h3></th>
  </tr>
  <tr>
    <th scope="row">Date of birth</th>
    <td><?php if($row_rsApp['dob'] !="0000-00-00") echo date("j M Y",strtotime($row_rsApp['dob'])); ?></td>
  </tr>
  <tr>
    <th scope="row">Gender</th>
    <td><?php echo $row_rsApp['gender']; ?></td>
  </tr>
  <tr>
    <th scope="row">Nationality</th>
    <td><?php echo $row_rsApp['nationality']; ?></td>
  </tr>
  <tr>
    <th scope="row">Languages spoken</th>
    <td><?php echo $row_rsApp['langs']; ?></td>
  </tr>
   <?php if ($row_rsApp['tid'] !=13) { ?>
  <tr>
    <th colspan="2" class="rowHeader" scope="row"><h3>Pre-education</h3></th>
  </tr>
  <tr>
    <th scope="row">Profession(s)</th>
    <td><?php echo $row_rsApp['professions']; ?></td>
  </tr>
  <tr>
    <th scope="row">Other education</th>
    <td><?php echo $row_rsApp['education']; ?></td>
  </tr>
  <?php }?>
  <?php if ($row_rsApp['tid'] ==13) { ?>
  <tr>
    <th colspan="2" scope="row"><h3>Proof of registration with a training school or university</h3></th>
  </tr>
  <tr>
    <th scope="row">Document (scan or original)</th>
    <td><a href="../../apply/docs/<?php echo $row_rsApp['proofdoc']; ?>"><?php echo $row_rsApp['proofdoc']; ?></a></td>
  </tr>
  <?php } ?>
  <tr>
  
    <th colspan="2" scope="row"><h3>Reasons for applying</h3></th>
  </tr>
  <tr>
    <th scope="row">Please say why you want    to join the EABP</th>
    <td><?php echo $row_rsApp['reasonapply']; ?></td>
  </tr>

  <tr>
  </tr>
</table>
<?php if($row_rsApp['tid'] == 7 || $row_rsApp['tid'] == 4) {?>
<!-- FULL -->  
<h2>Section 2 - Training 
</h2>
<table width="100%" cellpadding="0" cellspacing="0" class="tblApplication">
  <tr>
    <th colspan="2" scope="row"><h3>2.1.  Training in body psychotherapy</h3></th>
  </tr>
  <tr>
    <th scope="row">Training institute(s) and modality of body   psychotherapy<br>
      <br>
      <span class="text11px">Please include: <br>
        Contact person<br>
        Email address<br>
        Website<br>
        Modality of body psychotherapy</span></th>
    <td><?php echo $row_rsApp['trainbp']; ?></td>
  </tr>
  <tr>
    <th scope="row">Main trainers</th>
    <td><?php echo $row_rsApp['trainbptrainers']; ?></td>
  </tr>
  <tr>
<th class="highlighted" scope="row">How many years did you train? (give dates)</th>
    <td><?php echo $row_rsApp['trainbpyears']; ?></td>
  </tr>
  <tr>
    <th class="highlighted" scope="row">Total number of hours of professional training in body psychotherapy.</th>
    <td><?php echo $row_rsApp['trainbphours']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>2.2. Training in other modalities of psychotherapy</h3></th>
  </tr>
  <tr>
    <th class="highlighted" scope="row">Training institute(s): <br>
      <span class="text11px"><br>
        Please include: <br>
        Contact person<br>
        Email address<br>
        Website<br>
        Modality of body psychotherapy</span></th>
    <td><?php echo $row_rsApp['trainother']; ?></td>
  </tr>
  <tr>
    <th class="highlighted" scope="row">Trainers: </th>
    <td><?php echo $row_rsApp[' trainothertrainers']; ?></td>
  </tr>
  <tr>
    <th class="highlighted" scope="row">How many years did you train? (give dates)</th>
    <td><?php echo $row_rsApp['trainotheryears']; ?></td>
  </tr>
  <tr>
    <th class="highlighted" scope="row">Total number of hours training? </th>
    <td><?php echo $row_rsApp['trainotherhours']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>2.3  Courses/workshops for professional therapists </h3></th>
  </tr>
  <tr>
<th class="highlighted" scope="row">Which other training activities for  psychotherapists did you attend? </th>
    <td><?php echo $row_rsApp['traincourses']; ?></td>
  </tr>
  <tr>
    <th class="highlighted" scope="row">How many hours did these courses/workshops       include? </th>
    <td><?php echo $row_rsApp['traincourseshours']; ?></td>
  </tr>
</table>
<h2>Section 3  - Personal Psychotherapy
</h2>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th colspan="2" scope="row"><h3>3.1 In body psychotherapy</h3></th>
  </tr>
  <tr>
    <th scope="row">Number of hours of individual sessions</th>
    <td><?php echo $row_rsApp['personalbphoursind']; ?></td>
  </tr>
  <tr>
    <th scope="row">Number of hours of group sessions</th>
    <td><?php echo $row_rsApp['personalbphoursgroup']; ?></td>
  </tr>
  <tr>
    <th scope="row">Name of psychotherapist(s)</th>
    <td><?php echo $row_rsApp['personalbpnames']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>3.2  In other psychotherapy modalities</h3></th>
  </tr>
  <tr>
    <th scope="row">In which modality (ies)?</th>
    <td><?php echo $row_rsApp['personalother']; ?></td>
  </tr>
  <tr>
    <th scope="row">Number of hours of individual sessions</th>
    <td><?php echo $row_rsApp['personalotherhoursind']; ?></td>
  </tr>
  <tr>
    <th scope="row">Number of hours of group sessions</th>
    <td><?php echo $row_rsApp['personalotherhoursgroup']; ?></td>
  </tr>
  <tr>
    <th scope="row">Name of psychotherapist(s)</th>
    <td><?php echo $row_rsApp['personalothernames']; ?></td>
  </tr>
</table>
<p><a href="#top">Back to top </a></p>
<h2>Section 4 - Professional Supervision</h2>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th colspan="2" scope="row"><h3>4.1  In body psychotherapy outside the setting of the    training </h3></th>
  </tr>
  <tr>
    <th scope="row">Number of hours of individual sessions</th>
    <td><?php echo $row_rsApp['superbphoursind']; ?></td>
  </tr>
  <tr>
    <th scope="row">Number of hours of group sessions</th>
    <td><?php echo $row_rsApp['superbphoursgroup']; ?></td>
  </tr>
  <tr>
    <th scope="row">Name of supervisors</th>
    <td><?php echo $row_rsApp['superbpnames']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>4.2  In other  modalities</h3></th>
  </tr>
  <tr>
    <th scope="row">In which modality (ies)?</th>
    <td><?php echo $row_rsApp['superother']; ?></td>
  </tr>
  <tr>
    <th scope="row">Number of hours of individual sessions</th>
    <td><?php echo $row_rsApp['superotherhoursind']; ?></td>
  </tr>
  <tr>
    <th scope="row">Number of hours of group sessions</th>
    <td><?php echo $row_rsApp['superotherhoursgroup']; ?></td>
  </tr>
  <tr>
    <th scope="row">Name of supervisors</th>
    <td><?php echo $row_rsApp['superothernames']; ?></td>
  </tr>
  <tr>
    <th scope="row">Total number of hours</th>
    <td><?php echo $row_rsApp['supertotal']; ?></td>
  </tr>
</table>

<h2>Section 5 - Professional Practice </h2>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th colspan="2" scope="row"><h3>5.1 Professional practice as a body psychotherapist</h3></th>
  </tr>
  <tr>
    <th scope="row">Please describe your current work (any special client      groups and work load: e.g. clinic. individual practice, groups etc.; ie working with children, disabled persons)</th>
    <td scope="row"><?php echo $row_rsApp['practicebp']; ?></td>
  </tr>
  <tr>
    <th scope="row">Hours of individual sessions</th>
    <td><?php echo $row_rsApp['practicebpind']; ?></td>
  </tr>
  <tr>
    <th scope="row">Hours of group sessions</th>
    <td><?php echo $row_rsApp['practicebpgroup']; ?></td>
  </tr>
  <tr>
    <th scope="row">What do you call the therapy that you practice?</th>
    <td><?php echo $row_rsApp['practicebptherapy']; ?></td>
  </tr>
  <tr>
    <th scope="row">Please give dates and details about the setting (e.g. as an individual practitioner, to trainees on training courses, in a health clinic, etc.)</th>
    <td scope="row"><?php echo $row_rsApp['practicebpdetails']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>5.2  In other psychotherapy modalities</h3></th>
  </tr>
  <tr>
    <th class="highlighted" scope="row">How many hours of paid professional practice in other modalities of psychotherapy have you given?</th>
    <td><?php echo $row_rsApp['practiceother']; ?></td>
  </tr>
  <tr>
    <th class="highlighted" scope="row">Number of hours of individual sessions</th>
    <td><?php echo $row_rsApp['practiceotherind']; ?></td>
  </tr>
  <tr>
    <th class="highlighted" scope="row">Number of hours of group sessions</th>
    <td><?php echo $row_rsApp['practiceothergroup']; ?></td>
  </tr>
  <tr>
    <th class="highlighted" scope="row">Please give dates and details about the setting (e.g.        in a health clinic, as an individual practitioner, to       trainees on training courses etc.)</th>
    <td scope="row"><?php echo $row_rsApp['practiceotherdetails']; ?></td>
  </tr>
</table>
<h2>Section 6 - Membership of other Professional Bodies </h2>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th valign="top" scope="row">Are you registered with any other professional bodies in psychotherapy? If so, which?</th>
    <td><?php echo $row_rsApp['profbodiespsych']; ?></td>
  </tr>
  <tr>
    <th valign="top" scope="row">Are you registered with any other professional    body? If so, which?</th>
    <td><?php echo $row_rsApp['profbodiesother']; ?></td>
  </tr>
</table>
<h2>Section 7 - References</h2>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th colspan="2" scope="row"><h3>First reference</h3></th>
  </tr>
  <tr>
    <th scope="row">Name</th>
    <td><?php echo $row_rsApp['ref1name']; ?></td>
  </tr>
  <tr>
    <th scope="row">Email</th>
    <td><?php echo $row_rsApp['ref1email']; ?></td>
  </tr>
  <tr>
    <th colspan="2" scope="row"><h3>Second reference</h3></th>
  </tr>
  <tr>
    <th scope="row">Name</th>
    <td><?php echo $row_rsApp['ref2name']; ?></td>
  </tr>
  <tr>
    <th scope="row">Email</th>
    <td><?php echo $row_rsApp['ref2email']; ?></td>
  </tr>
</table>
<h2>Section 8 - Documentation </h2>
<?php if ($totalRows_rsDocs == 0) { // Show if recordset empty ?>
  <p>No documents uploaded.</p>
  <?php } // Show if recordset empty ?>
<?php if ($totalRows_rsDocs > 0) { // Show if recordset not empty ?>
  <h3>Documents uploaded and saved:</h3>
      <ul id="docList">
        <?php do { ?>
        <li><a href="../../apply/docs/<?php echo $row_rsDocs['docfile']; ?>"><?php echo $row_rsDocs['doctype']; ?></a><br>
            <?php echo $row_rsDocs['notes']; ?>      <br>
        </li>
          <?php } while ($row_rsDocs = mysql_fetch_assoc($rsDocs)); ?>
      </ul>
      <?php } // Show if recordset not empty ?>
 
 <?php }?>     
<p><a href="#top">Back to top </a></p>
</body>
</html>
<?php
mysql_free_result($rsApp);

mysql_free_result($rsCountry);

mysql_free_result($rsDocs);

mysql_free_result($rsMember);
?>
