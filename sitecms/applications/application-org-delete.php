<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM applicationorgs WHERE orgid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "index.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsApplication = "-1";
if (isset($_GET['orgid'])) {
  $colname_rsApplication = $_GET['orgid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsApplication = sprintf("SELECT * FROM applicationorgs WHERE orgid = %s", GetSQLValueString($colname_rsApplication, "int"));
$rsApplication = mysql_query($query_rsApplication, $connEABP2) or die(mysql_error());
$row_rsApplication = mysql_fetch_assoc($rsApplication);
$totalRows_rsApplication = mysql_num_rows($rsApplication);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Delete application</title>
<link href="../cms.css" rel="stylesheet" type="text/css">
<style type="text/css">
body {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 14px;
	width: 80%;
	margin-right: auto;
	margin-left: auto;
	margin-top: 20px;
	margin-bottom: 20px;
}
</style>
</head>

<body>
<p><img src="../../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> <a href="../index.php">admin home</a> | <a href="index.php">applications</a></p>
<p>Delete application
</p>
<h1><?php echo $row_rsApplication['orgname']; ?></h1>
<p>This will remove the application completely from the system.</p>
<form action="" method="post" name="frmDelete" id="frmDelete">
  <input name="delid" type="hidden" id="delid" value="<?php echo $_GET['orgid']; ?>">
  <input type="submit" name="btnDelete" id="btnDelete" value="Yes, delete">
</form>
<p>&nbsp;</p>

</body>
</html>
<?php
mysql_free_result($rsApplication);
?>
