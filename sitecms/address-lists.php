<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT cid, country FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemType = "SELECT tid, type FROM type WHERE tid <> 1 ORDER BY type ASC";
$rsMemType = mysql_query($query_rsMemType, $connEABP2) or die(mysql_error());
$row_rsMemType = mysql_fetch_assoc($rsMemType);
$totalRows_rsMemType = mysql_num_rows($rsMemType);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNAs = "SELECT acronym, fullname FROM `national` WHERE live = 1 AND fullname <> '' AND acronym <> '' ORDER BY acronym";
$rsNAs = mysql_query($query_rsNAs, $connEABP2) or die(mysql_error());
$row_rsNAs = mysql_fetch_assoc($rsNAs);
$totalRows_rsNAs = mysql_num_rows($rsNAs);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Members address lists</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<h1>Members address lists</h1>
<form id="form1" name="form1" method="post" action="export-members.php">
<p>This will create a csv file showing the basic address info. Hopefully you will be able to use it with the program you use for address labels.</p>
<p>After choosing the country and clicking Go you will be asked to Save or Open the file.</p>
Choose the country:  <select name="cid" id="cid">
<option value="">All</option>
  <?php
do {  
?>
  <option value="<?php echo $row_rsCountries['cid']?>"><?php echo $row_rsCountries['country']?></option>
  <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
  </select>
  <input name="button" type="submit" class="btnGo" id="button" value="Go" />
</form>
<hr />
<p><a href="export-forum-members.php">FORUM members</a></p>
<p><strike>Full members with EABP as their national association</strike> - now included below</p>
<p>Or choose from list:</p>
<form id="frmType" name="frmType" method="get" action="export-members-eabp-by-type.php">
  <select name="tid" class="input100" id="tid">
    <?php
do {  
?>
    <option value="<?php echo $row_rsMemType['tid']?>"><?php echo $row_rsMemType['type']?></option>
    <?php
} while ($row_rsMemType = mysql_fetch_assoc($rsMemType));
  $rows = mysql_num_rows($rsMemType);
  if($rows > 0) {
      mysql_data_seek($rsMemType, 0);
	  $row_rsMemType = mysql_fetch_assoc($rsMemType);
  }
?>
  </select>
  <input name="btnGo" type="submit" class="btnGo" id="btnGo" value="Go" />
</form>
<hr />
<p><a href="export-members-full-honorary.php">Full and Honorary Members list</a> (for certificates)</p>
<hr />
<p>National Association members addresses (full and honorary):</p>
<form id="frmNAMembers" name="frmNAMembers" method="get" action="export-na-member-addresses.php">
  <select name="na" id="na">
    <option value="-1">Choose: </option>
    <?php
do {  
?>
    <option value="<?php echo $row_rsNAs['acronym']?>"><?php echo $row_rsNAs['acronym']?> - <?php echo $row_rsNAs['fullname']?></option>
    <?php
} while ($row_rsNAs = mysql_fetch_assoc($rsNAs));
  $rows = mysql_num_rows($rsNAs);
  if($rows > 0) {
      mysql_data_seek($rsNAs, 0);
	  $row_rsNAs = mysql_fetch_assoc($rsNAs);
  }
?>
  </select>
  <input name="btnGoNA" type="submit" class="btnGo" id="btnGoNA" value="Go" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsCountries);

mysql_free_result($rsMemType);

mysql_free_result($rsNAs);
?>
