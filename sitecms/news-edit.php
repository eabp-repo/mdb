<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/news";
$ppu->extensions = "GIF,JPG,JPEG,PNG";
$ppu->formName = "frmEdit";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "150";
$ppu->minHeight = "150";
$ppu->maxWidth = "150";
$ppu->maxHeight = "150";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();
?>
<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM news WHERE newsid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmEdit")) {
  $updateSQL = sprintf("UPDATE news SET title=%s, textblock=%s, imagefile=IFNULL(%s,imagefile), linktext=%s, linkurl=%s, live=%s WHERE newsid=%s",
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['textblock'], "text"),
                       GetSQLValueString($_POST['imagefile'], "text"),
                       GetSQLValueString($_POST['linktext'], "text"),
                       GetSQLValueString($_POST['linkurl'], "text"),
                       GetSQLValueString(isset($_POST['live']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['newsid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsNews = "-1";
if (isset($_GET['newsid'])) {
  $colname_rsNews = $_GET['newsid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNews = sprintf("SELECT * FROM news WHERE newsid = %s", GetSQLValueString($colname_rsNews, "int"));
$rsNews = mysql_query($query_rsNews, $connEABP2) or die(mysql_error());
$row_rsNews = mysql_fetch_assoc($rsNews);
$totalRows_rsNews = mysql_num_rows($rsNews);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit a news item</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>Edit a news item</h1>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmEdit" id="frmEdit" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,PNG',false,'',150,150,150,150,'','');return document.MM_returnValue">
<table>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Title:</td>
    <td><input name="title" type="text" class="input400" value="<?php echo $row_rsNews['title']; ?>" size="70" maxlength="70" /></td>
  </tr>
  <tr valign="baseline">
    <td align="right" valign="top" nowrap="nowrap">Text:</td>
    <td><textarea name="textblock" cols="32" rows="3"><?php echo $row_rsNews['textblock']; ?></textarea></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Image file:</td>
    <td><input name="imagefile" type="file" id="imagefile" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,PNG',false,'',150,150,150,150,'','')" />
    must be 150 x 150 pixels</td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Link text:</td>
    <td><input name="linktext" type="text" value="<?php echo $row_rsNews['linktext']; ?>" size="32" maxlength="50" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Link url:</td>
    <td><input name="linkurl" type="text" value="<?php echo $row_rsNews['linkurl']; ?>" size="32" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Live:</td>
    <td><input <?php if (!(strcmp($row_rsNews['live'],1))) {echo "checked=\"checked\"";} ?> name="live" type="checkbox" class="input50" value="1" checked="checked" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right"><input name="newsid" type="hidden" id="newsid" value="<?php echo $row_rsNews['newsid']; ?>" /></td>
    <td><input type="submit" class="btnAdd" value="Update" />
   <?php if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmEdit")) { ?>
      <img src="../images/tick.gif" alt="" width="16" height="16" />
    <?php } ?>  
      </td>
  </tr>
</table>
<input type="hidden" name="MM_update" value="frmEdit" />
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input type="submit" name="btnDelete" id="btnDelete" value="Delete this item" />
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsNews['newsid']; ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsNews);

mysql_free_result($rsNews);
?>
