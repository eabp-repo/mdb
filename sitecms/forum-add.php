<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/logos";
$ppu->extensions = "GIF,JPG,JPEG,PNG";
$ppu->formName = "form1";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO forum (id, orgname, logofile, address, city, accredited, reaccredited, contact, contact2, contact3, telephone, email, email2, email3, fax, website, legalstructure, typeid, totaltrained, ethics, trainers, listtrainees, numbertrainees, cid, doa, EABPreps, orgfee, notes, showonsite, username, password) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id'], "int"),
                       GetSQLValueString($_POST['orgname'], "text"),
                       GetSQLValueString($_POST['logofile'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['city'], "text"),
                       GetSQLValueString($_POST['accredited'], "text"),
                       GetSQLValueString($_POST['reaccredited'], "text"),
                       GetSQLValueString($_POST['contact'], "text"),
                       GetSQLValueString($_POST['contact2'], "text"),
                       GetSQLValueString($_POST['contact3'], "text"),
                       GetSQLValueString($_POST['telephone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['email2'], "text"),
                       GetSQLValueString($_POST['email3'], "text"),
                       GetSQLValueString($_POST['fax'], "text"),
                       GetSQLValueString($_POST['website'], "text"),
                       GetSQLValueString($_POST['legalstructure'], "text"),
                       GetSQLValueString($_POST['typeid'], "int"),
                       GetSQLValueString($_POST['totaltrained'], "text"),
                       GetSQLValueString($_POST['ethics'], "text"),
                       GetSQLValueString($_POST['trainers'], "text"),
                       GetSQLValueString($_POST['listtrainees'], "text"),
                       GetSQLValueString($_POST['numbertrainees'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['doa'], "text"),
                       GetSQLValueString($_POST['EABPreps'], "text"),
                       GetSQLValueString($_POST['orgfee'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['password'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

$colname_rsForum = "-1";
if (isset($_GET['id'])) {
  $colname_rsForum = $_GET['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsForum = sprintf("SELECT * FROM forum WHERE id = %s", GetSQLValueString($colname_rsForum, "int"));
$rsForum = mysql_query($query_rsForum, $connEABP2) or die(mysql_error());
$row_rsForum = mysql_fetch_assoc($rsForum);
$totalRows_rsForum = mysql_num_rows($rsForum);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsType = "SELECT * FROM forumtypes WHERE forumtypeid <= 3";
$rsType = mysql_query($query_rsType, $connEABP2) or die(mysql_error());
$row_rsType = mysql_fetch_assoc($rsType);
$totalRows_rsType = mysql_num_rows($rsType);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Forum member add</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>Add a Forum member </h1>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,PNG',false,'','','','','','','');return document.MM_returnValue">
  <table cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Name:</td>
      <td><input name="orgname" type="text" class="input400" /> 
        required</td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Logo file:</td>
      <td><input name="logofile" type="file" id="logofile" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,PNG',false,'','','','','','','')" />
        (optional)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Address:</td>
      <td><input type="text" name="address" size="32" />
      required</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">City:</td>
      <td><input type="text" name="city" size="32" />
      required</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Country:</td>
      <td><select name="cid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCountries['cid']?>"><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
              </select>
        required </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Accredited:</td>
      <td><input name="accredited" type="text" id="accredited" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Re-accredited:</td>
      <td><input name="reaccredited" type="text" id="reaccredited" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact 1:</td>
      <td><input type="text" name="contact" size="32" />
      required</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact 2:</td>
      <td><input name="contact2" type="text" id="contact2" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact 3:</td>
      <td><input name="contact3" type="text" id="contact3" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Telephone:</td>
      <td><input type="text" name="telephone" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email 1:</td>
      <td><input type="text" name="email" size="32" />
      required</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email2:</td>
      <td><input type="text" name="email2" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email3:</td>
      <td><input type="text" name="email3" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Fax:</td>
      <td><input type="text" name="fax" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Website:</td>
      <td><input type="text" name="website" size="32" /> 
        NO http://</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Legal structure:</td>
      <td><input type="text" name="legalstructure" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Type:</td>
      <td><select name="typeid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsType['forumtypeid']?>"><?php echo $row_rsType['forumtype']?></option>
        <?php
} while ($row_rsType = mysql_fetch_assoc($rsType));
  $rows = mysql_num_rows($rsType);
  if($rows > 0) {
      mysql_data_seek($rsType, 0);
	  $row_rsType = mysql_fetch_assoc($rsType);
  }
?>
      </select>
      required </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Total trained:</td>
      <td><input type="text" name="totaltrained" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Ethics:</td>
      <td><input type="text" name="ethics" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Trainers:</td>
      <td><input type="text" name="trainers" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">List trainees:</td>
      <td><input type="text" name="listtrainees" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Number trainees:</td>
      <td><input type="text" name="numbertrainees" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Doa:</td>
      <td><input type="text" name="doa" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">EABP reps:</td>
      <td><input name="EABPreps" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Org fee:</td>
      <td><input type="text" name="orgfee" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Notes:</td>
      <td><input type="text" name="notes" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Login username:</td>
      <td><input type="text" name="username" size="32" />
      required - use email 1</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Login password:</td>
      <td><input name="password" type="text" value="password" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Show on site:</td>
      <td><input name="showonsite" type="checkbox" class="input50" value="" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Add" /></td>
    </tr>
  </table>
  
  <input type="hidden" name="id" />
  <input type="hidden" name="MM_insert" value="form1" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsForum);

mysql_free_result($rsType);

mysql_free_result($rsCountries);
?>
