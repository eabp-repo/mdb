<?php require_once('../common.php');

// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['username'])) {
  $loginUsername=$_POST['username'];
  $password=$_POST['password'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "index.php";
  $MM_redirectLoginFailed = "login.php";
  $MM_redirecttoReferrer = false;
//  mysqli_select_db($connEABP2,$database_connEABP2);
  mysql_select_db($database_connEABP2, $connEABP2);

  $LoginRS__query=sprintf("SELECT username, password FROM `admin` WHERE username=%s AND password=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $connEABP2) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $loginStrGroup = "";
    
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP Membership login</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p><img src="../images/logo-20.gif" alt="" width="47" height="20" align="absmiddle" /> membership admin | <a href="https://www.eabp.org" target="_blank">go to website</a></p>
<h1>Admin login</h1>
<form ACTION="<?php echo $loginFormAction; ?>" METHOD="POST" name="frmLogin" id="frmLogin">
<table border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>Username: </td>
    <td><input type="text" name="username" id="username" /></td>
  </tr>
  <tr>
    <td>Password: </td>
    <td><input type="password" name="password" id="password" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="button" type="submit" class="btnAdd" id="button" value="Login" /></td>
  </tr>
</table>
</form>
<hr />
<p><a href="../na-admin/login.php">Go to national association login</a></p>
</body>
</html>
