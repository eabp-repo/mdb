<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../docs";
$ppu->extensions = "pdf,doc";
$ppu->formName = "form1";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO events (cid, etid, organiser, orgemail, orgwebsite, eventname, eventsubject, eventdate, datetext, venue, `description`, furtherinfodoc, showonsite) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['etid'], "int"),
                       GetSQLValueString($_POST['organiser'], "text"),
                       GetSQLValueString($_POST['orgemail'], "text"),
                       GetSQLValueString($_POST['orgwebsite'], "text"),
                       GetSQLValueString($_POST['eventname'], "text"),
                       GetSQLValueString($_POST['eventsubject'], "text"),
                       GetSQLValueString($_POST['eventdate'], "date"),
                       GetSQLValueString($_POST['datetext'], "text"),
                       GetSQLValueString($_POST['venue'], "text"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['furtherinfodoc'], "text"),
                       GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined","1","0"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTypes = "SELECT * FROM eventtypes";
$rsTypes = mysql_query($query_rsTypes, $connEABP2) or die(mysql_error());
$row_rsTypes = mysql_fetch_assoc($rsTypes);
$totalRows_rsTypes = mysql_num_rows($rsTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT cid, country FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add event</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h2>Add an event</h2>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="checkFileUpload(this,'pdf,doc',false,'','','','','','','');return document.MM_returnValue">
  <table cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Country:</td>
      <td><select name="cid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCountries['cid']?>"><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
      </select>      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Type:</td>
      <td><select name="etid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsTypes['etid']?>"><?php echo $row_rsTypes['eventtype']?></option>
        <?php
} while ($row_rsTypes = mysql_fetch_assoc($rsTypes));
  $rows = mysql_num_rows($rsTypes);
  if($rows > 0) {
      mysql_data_seek($rsTypes, 0);
	  $row_rsTypes = mysql_fetch_assoc($rsTypes);
  }
?>
      </select>      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Event name:</td>
      <td><input name="eventname" type="text" class="input500" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Event subject:</td>
      <td><input type="text" name="eventsubject" id="eventsubject" /> 
        <span class="smallText">optional</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Start date (not shown on site):</td>
      <td><input name="eventdate" type="text" class="input100" value="" size="32" />
        <span class="smallText">enter as yyyy-mm-dd - optional</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Dates, times etc:</td>
      <td><input name="datetext" type="text" class="input500" id="datetext" value="" size="32" />
      <span class="smallText">optional</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Venue:</td>
      <td><input name="venue" type="text" class="input500" id="venue" value="" size="32" /> 
        <span class="smallText">do not include country</span></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap">Description text:<br />
        <span class="smallText">maximum 250 characters</span></td>
      <td><textarea name="description" cols="45" rows="5" class="input500" id="description" maxlength="250"></textarea>
      <span class="smallText">optional</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Organiser:</td>
      <td><input type="text" name="organiser" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Organiser's email:</td>
      <td><input type="text" name="orgemail" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Organiser's website:</td>
      <td><input type="text" name="orgwebsite" value="" size="32" /> 
        <span class="smallText">NO http://</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Upload further info document:</td>
      <td><input name="furtherinfodoc" type="file" id="furtherinfodoc" onchange="checkOneFileUpload(this,'pdf,doc',false,'','','','','','','')" />
        <span class="smallText">optional</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Show on site:</td>
      <td><input name="showonsite" type="checkbox" class="input50" value="" checked="checked" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Add" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>
</body>
</html>
<?php
mysql_free_result($rsTypes);

mysql_free_result($rsCountries);
?>
