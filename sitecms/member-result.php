<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsResult = "-1";
if (isset($_POST['q'])) {
  $colname_rsResult = $_POST['q'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsResult = sprintf("SELECT mid, firstname, lastname, school, type FROM member INNER JOIN type ON type.tid = member.tid WHERE lastname LIKE %s OR school LIKE %s ORDER BY type", GetSQLValueString($colname_rsResult . "%", "text"),GetSQLValueString("%" . $colname_rsResult . "%", "text"));
$rsResult = mysql_query($query_rsResult, $connEABP2) or die(mysql_error());
$row_rsResult = mysql_fetch_assoc($rsResult);
$totalRows_rsResult = mysql_num_rows($rsResult);
?>
<?php  $lastTFM_nest = "";?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Member search</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>Member search result</h1>
<p>Search for  <span class="redText"><?php echo $_POST['q']; ?></span></p>

<?php if ($totalRows_rsResult == 0) { // Show if recordset empty ?>
  <form id="frmMemberSearch" name="frmMemberSearch" method="post" action="member-result.php">
  <p>No match found - please try again.</p>
    <input name="q" type="text" class="input50" id="q" size="12" />
    <input name="btnGo" type="submit" class="btnGo" id="btnGo" value="Go" />
  </form>
  <?php } // Show if recordset empty ?>
<?php if ($totalRows_rsResult > 0) { // Show if recordset not empty ?>
  <?php do { ?>
      <?php $TFM_nest = $row_rsResult['type'];
if ($lastTFM_nest != $TFM_nest) { 
	$lastTFM_nest = $TFM_nest; ?><h2><?php echo $row_rsResult['type']; ?></h2>
    <?php } //End of Basic-UltraDev Simulated Nested Repeat?>
    <p><a href="member-edit-login.php?mid=<?php echo $row_rsResult['mid']; ?>"><?php echo $row_rsResult['lastname']; ?>, <?php echo $row_rsResult['firstname']; ?></a> <?php echo $row_rsResult['school']; ?></p>
    <?php } while ($row_rsResult = mysql_fetch_assoc($rsResult)); ?>
<?php } // Show if recordset not empty ?>

</body>
</html>
<?php
mysql_free_result($rsResult);
?>
