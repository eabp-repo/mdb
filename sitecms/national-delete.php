<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM `national` WHERE naid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsNA = "-1";
if (isset($_GET['naid'])) {
  $colname_rsNA = $_GET['naid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = sprintf("SELECT naid, fullname FROM `national` WHERE naid = %s", GetSQLValueString($colname_rsNA, "int"));
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Delete NA</title>
<link href="cms.css" rel="stylesheet" type="text/css">
</head>

<body>
<p><a href="national-edit.php?naid=<?php echo $row_rsNA['naid']; ?>">&lt; back</a></p>
<h1>Delete <em><?php echo $row_rsNA['fullname']; ?></em> ?
</h1>
<p>  The information will be removed from the membership database.
</p>
<p>This is permanent - you will not be able to undo this action.</p>
<form action="" method="post" name="frmDelete" id="frmDelete">
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsNA['naid']; ?>">
  <input type="submit" name="btnDelete" id="btnDelete" value="Yes, delete">
</form>
</body>
</html>
<?php
mysql_free_result($rsNA);
?>
