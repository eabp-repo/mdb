<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM member WHERE mid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsMember = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMember = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMember = sprintf("SELECT mid, firstname, lastname FROM member WHERE mid = %s", GetSQLValueString($colname_rsMember, "int"));
$rsMember = mysql_query($query_rsMember, $connEABP2) or die(mysql_error());
$row_rsMember = mysql_fetch_assoc($rsMember);
$totalRows_rsMember = mysql_num_rows($rsMember);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Delete</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>Delete <?php echo $row_rsMember['firstname']; ?>&nbsp;<?php echo $row_rsMember['lastname']; ?>?</h1>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <div align="right">
    <input type="submit" name="btnDelete" id="btnDelete" value="Yes, delete" />
    <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsMember['mid']; ?>" />
  </div>
</form>
<p>No - <a href="javascript:history.go(-1)">go back</a></p>
</body>
</html>
<?php
mysql_free_result($rsMember);
?>
