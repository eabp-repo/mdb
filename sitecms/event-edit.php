<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../docs";
$ppu->extensions = "pdf,doc";
$ppu->formName = "frmEdit";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM events WHERE eventid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmEdit")) {
  $updateSQL = sprintf("UPDATE events SET cid=%s, etid=%s, organiser=%s, orgemail=%s, orgwebsite=%s, eventname=%s, eventsubject=%s, eventdate=%s, datetext=%s, venue=%s, `description`=%s, furtherinfodoc=IFNULL(%s,furtherinfodoc), showonsite=%s WHERE eventid=%s",
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['etid'], "int"),
                       GetSQLValueString($_POST['organiser'], "text"),
                       GetSQLValueString($_POST['orgemail'], "text"),
                       GetSQLValueString($_POST['orgwebsite'], "text"),
                       GetSQLValueString($_POST['eventname'], "text"),
                       GetSQLValueString($_POST['eventsubject'], "text"),
                       GetSQLValueString($_POST['eventdate'], "date"),
                       GetSQLValueString($_POST['datetext'], "text"),
                       GetSQLValueString($_POST['venue'], "text"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['furtherinfodoc'], "text"),
                       GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['eventid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTypes = "SELECT * FROM eventtypes";
$rsTypes = mysql_query($query_rsTypes, $connEABP2) or die(mysql_error());
$row_rsTypes = mysql_fetch_assoc($rsTypes);
$totalRows_rsTypes = mysql_num_rows($rsTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT cid, country FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

$colname_rsEvent = "-1";
if (isset($_GET['eventid'])) {
  $colname_rsEvent = $_GET['eventid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsEvent = sprintf("SELECT * FROM events WHERE eventid = %s", GetSQLValueString($colname_rsEvent, "int"));
$rsEvent = mysql_query($query_rsEvent, $connEABP2) or die(mysql_error());
$row_rsEvent = mysql_fetch_assoc($rsEvent);
$totalRows_rsEvent = mysql_num_rows($rsEvent);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit an event</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h2>Edit an event <?php if (isset($_POST['processed'])) { ?> - <img src="../images/tick.gif" alt="tick" width="16" height="16" /><?php } ?></h2>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmEdit" id="frmEdit" onsubmit="checkFileUpload(this,'pdf,doc',false,'','','','','','','');return document.MM_returnValue">
<table cellpadding="3" cellspacing="0">
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Country:</td>
    <td><select name="cid">
      <?php
do {  
?>
      <option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsEvent['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option>
<?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
    </select>    </td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Type:</td>
    <td><select name="etid">
      <?php
do {  
?>
      <option value="<?php echo $row_rsTypes['etid']?>"<?php if (!(strcmp($row_rsTypes['etid'], $row_rsEvent['etid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsTypes['eventtype']?></option>
      <?php
} while ($row_rsTypes = mysql_fetch_assoc($rsTypes));
  $rows = mysql_num_rows($rsTypes);
  if($rows > 0) {
      mysql_data_seek($rsTypes, 0);
	  $row_rsTypes = mysql_fetch_assoc($rsTypes);
  }
?>
    </select>    </td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Event name:</td>
    <td><input name="eventname" type="text" class="input500" value="<?php echo $row_rsEvent['eventname']; ?>" size="32" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Event subject:</td>
    <td><input name="eventsubject" type="text" id="eventsubject" value="<?php echo $row_rsEvent['eventsubject']; ?>" />
        <span class="smallText">optional</span></td>
  </tr>
  <tr valign="baseline">
    <td align="right" nowrap="nowrap">Start date (not shown on site):</td>
    <td><input name="eventdate" type="text" class="input100" value="<?php echo $row_rsEvent['eventdate']; ?>" size="32" />
      enter as yyyy-mm-dd</td>
  </tr>
  <tr valign="baseline">
    <td align="right" nowrap="nowrap">Dates, times etc:</td>
    <td><input name="datetext" type="text" class="input500" id="datetext" value="<?php echo $row_rsEvent['datetext']; ?>" size="32" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Venue:</td>
    <td><input name="venue" type="text" class="input500" value="<?php echo $row_rsEvent['venue']; ?>" size="32" /></td>
  </tr>
  <tr valign="baseline">
    <td align="right" valign="top" nowrap="nowrap">Description text:</td>
    <td><textarea name="description" cols="45" rows="5" class="input400" id="description"><?php echo $row_rsEvent['description']; ?></textarea>
        <span class="smallText">optional</span></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Organiser:</td>
    <td><input type="text" name="organiser" value="<?php echo $row_rsEvent['organiser']; ?>" size="32" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Organiser's email:</td>
    <td><input type="text" name="orgemail" value="<?php echo $row_rsEvent['orgemail']; ?>" size="32" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Organiser's website:</td>
    <td><input type="text" name="orgwebsite" value="<?php echo $row_rsEvent['orgwebsite']; ?>" size="32" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Upload further info document: </td>
    <td><p>
          <input name="furtherinfodoc" type="file" id="furtherinfodoc" onchange="checkOneFileUpload(this,'pdf,doc',false,'','','','','','','')" />
          <span class="smallText">optional</span></p>      </td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Show on site:</td>
    <td><input <?php if (!(strcmp($row_rsEvent['showonsite'],1))) {echo "checked=\"checked\"";} ?> name="showonsite" type="checkbox" class="input50" value="" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right"><input name="processed" type="hidden" id="processed" value="1" />
      <input name="eventid" type="hidden" id="eventid" value="<?php echo $row_rsEvent['eventid']; ?>" /></td>
    <td><input type="submit" class="btnAdd" value="Update" /></td>
  </tr>
</table>

<input type="hidden" name="MM_update" value="frmEdit" />
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input name="btnDelete" type="submit" id="btnDelete" value="Delete" />
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsEvent['eventid']; ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsTypes);

mysql_free_result($rsCountries);

mysql_free_result($rsEvent);
?>
