<?php require_once('../Connections/connEABP2.php'); ?>
<?php 
if (($_POST['day'] !="") && ($_POST['month'] !="") && ($_POST['year'] !="")) {
$day=$_POST['day'];
$month=$_POST['month'];
$year=$_POST['year'];
$dob="$year-$month-$day";
$_POST['dob'] = $dob;
} else {
$_POST['dob'] = $_POST['dob'];
}
if (($_POST['day2'] !="") && ($_POST['month2'] !="") && ($_POST['year2'] !="")) {
$day2=$_POST['day2'];
$month2=$_POST['month2'];
$year2=$_POST['year2'];
$ecp="$year2-$month2-$day2";
$_POST['ecp'] = $ecp;
} else {
$_POST['ecp'] = $_POST['ecp'];
}
?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/memberphotos";
$ppu->extensions = "GIF,JPG";
$ppu->formName = "frmUpdate";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmUpdate")) {
  $updateSQL = sprintf("UPDATE member SET dob=%s, ecp=%s, email=%s, firstname=%s, lastname=%s, modality=%s, speciality=%s, langs=%s, skype=%s, title=%s, tid=%s, imagefile=IFNULL(%s,imagefile), showonsite=%s WHERE mid=%s",
                       GetSQLValueString($_POST['dob'], "text"),
                       GetSQLValueString($_POST['ecp'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['firstname'], "text"),
                       GetSQLValueString($_POST['lastname'], "text"),
                       GetSQLValueString($_POST['modality'], "text"),
                       GetSQLValueString($_POST['speciality'], "text"),
                       GetSQLValueString($_POST['langs'], "text"),
                       GetSQLValueString($_POST['skype'], "text"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['tid'], "int"),
                       GetSQLValueString($_POST['imagefile'], "text"),
                       GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9 AND tid <> 11 AND tid <> 6";
$rsMemTypes = mysql_query($query_rsMemTypes, $connEABP2) or die(mysql_error());
$row_rsMemTypes = mysql_fetch_assoc($rsMemTypes);
$totalRows_rsMemTypes = mysql_num_rows($rsMemTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9";
$rsTypes = mysql_query($query_rsTypes, $connEABP2) or die(mysql_error());
$row_rsTypes = mysql_fetch_assoc($rsTypes);
$totalRows_rsTypes = mysql_num_rows($rsTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = "SELECT naid, acronym FROM `national`";
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>EABP individual membership database update: General</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<style type="text/css">
<!--
.dateList {	width: 55px;
	font-size: 10px;
}
-->
</style>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p class="leftAlign"><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" />  admin home</a></p>
<!-- Google Translate -->
<div id="google_translate_element"></div>
<p class="googleTranslateText">
  <script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    includedLanguages: 'bg,da,nl,en,fi,fr,de,el,iw,it,ja,no,pl,pt,ru,sr,sl,es,sv',
    gaTrack: true,
    gaId: 'UA-224881-50',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script>
  <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  The Google Translator is here to give you some help but it is not perfect!
  <!-- End -->
</p>
<p class="clear">EABP individual member update:</p>
<p><a href="member-edit-login.php?mid=<?php echo $row_rsMembers['mid']; ?>">Login details</a> | General | <a href="member-edit-contact.php?mid=<?php echo $row_rsMembers['mid']; ?>">Contact details</a> | <a href="member-edit-membership.php?mid=<?php echo $row_rsMembers['mid']; ?>">Membership</a> | <a href="member-edit-training.php?mid=<?php echo $row_rsMembers['mid']; ?>">Training</a> | <a href="member-edit-work.php?mid=<?php echo $row_rsMembers['mid']; ?>">Current Work</a> |  <a href="member-delete.php?mid=<?php echo $row_rsMembers['mid']; ?>">Delete this member</a> (you will be asked to confirm)</p>
<hr size="1" noshade="noshade" />
<h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
<h2>General info<?php if (isset($_POST['processed'])) { ?> - <img src="../images/tick.gif" alt="tick" width="16" height="16" /><?php } ?></h2>
<hr size="1" noshade="noshade" />
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmUpdate" id="frmUpdate" onsubmit="checkFileUpload(this,'GIF,JPG',false,'','','','','','','');return document.MM_returnValue">
  <table border="0" align="left" cellpadding="3" cellspacing="0" id="tblDetails">
    <tr>
      <td colspan="2" class="smallText">Filemaker Pro membership ID:<strong> <?php echo $row_rsMembers['membershipid']; ?></strong></td>
      <td class="smallText">&nbsp;</td>
    </tr>
    <tr>
      <td class="tdWhiteBg">Membership ID number:</td>
      <td class="tdWhiteBg"><?php echo $row_rsMembers['mid']; ?></td>
      <td align="right" class="tdWhiteBg">&nbsp;</td>
    </tr>
    <tr>
      <td>Show on website</td>
      <td><input <?php if (!(strcmp($row_rsMembers['showonsite'],1))) {echo "checked=\"checked\"";} ?> type="checkbox" name="showonsite" id="showonsite" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td width="200">Last Name</td>
      <td><input name="lastname" type="text" class="largerText" id="lastname" value="<?php echo $row_rsMembers['lastname']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>First Name</td>
      <td><input name="firstname" type="text" id="firstname" value="<?php echo $row_rsMembers['firstname']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Title</td>
      <td><input name="post" type="text" id="post" value="<?php echo $row_rsMembers['title']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Email</td>
      <td><input name="email" type="text" id="email" value="<?php echo $row_rsMembers['email']; ?>" /></td>
      <td align="center"><?php if ($row_rsMembers['email'] !="") { ?><a href="mailto:<?php echo $row_rsMembers['email']; ?>">email now</a><?php } ?></td>
    </tr>
    <tr>
      <td>Skype name</td>
      <td><input name="skype" type="text" id="skype" value="<?php echo $row_rsMembers['skype']; ?>" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Photo
          <?php if ($row_rsMembers['imagefile'] !="") { ?>
          <span class="smallText"> - currently</span> <a href="../images/memberphotos/<?php echo $row_rsMembers['imagefile']; ?>"><img src="../images/memberphotos/<?php echo $row_rsMembers['imagefile']; ?>" alt="Click to view full size" width="24" height="24" border="0" align="absmiddle" /></a>
      <?php } ?></td>
      <td><input name="imagefile" type="file" id="imagefile" onchange="checkOneFileUpload(this,'GIF,JPG',false,'','','','','','','')" /></td>
      <td align="center" class="smallText">Optional - 50KB max</td>
    </tr>
    
    <tr>
      <td>Main modalities</td>
      <td><input name="modality" type="text" id="modality" value="<?php echo $row_rsMembers['modality']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Speciality</td>
      <td><input name="speciality" type="text" id="speciality" value="<?php echo $row_rsMembers['speciality']; ?>" /></td>
      <td class="smallText">&nbsp;</td>
    </tr>
    <tr>
      <td>Languages spoken:</td>
      <td><input name="langs" type="text" id="langs" value="<?php echo $row_rsMembers['langs']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    
    <tr>
      <td>Type of membership</td>
      <td><select name="tid" id="tid">
          <?php
do {  
?>
          <option value="<?php echo $row_rsTypes['tid']?>"<?php if (!(strcmp($row_rsTypes['tid'], $row_rsMembers['tid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsTypes['type']?></option>
          <?php
} while ($row_rsTypes = mysql_fetch_assoc($rsTypes));
  $rows = mysql_num_rows($rsTypes);
  if($rows > 0) {
      mysql_data_seek($rsTypes, 0);
	  $row_rsTypes = mysql_fetch_assoc($rsTypes);
  }
?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Date of Birth <span class="smallText">(yyyy-mm-dd)</span></td>
      <td><input name="dob" type="text" id="dob" value="<?php echo $row_rsMembers['dob']; ?>" maxlength="10" />
        <span class="smallText">
        <?php if ($row_rsMembers['dob'] !="") { ?>
        UPDATE (all required)
        <?php } else { ?>
        ENTER
        <?php } ?>
        : day
        <select name="day" class="dateList" id="day">
          <option value="" selected="selected"></option>
          <option value='01'>01</option>
          <option value='02'>02</option>
          <option value='03'>03</option>
          <option value='04'>04</option>
          <option value='05'>05</option>
          <option value='06'>06</option>
          <option value='07'>07</option>
          <option value='08'>08</option>
          <option value='09'>09</option>
          <option value='10'>10</option>
          <option value='11'>11</option>
          <option value='12'>12</option>
          <option value='13'>13</option>
          <option value='14'>14</option>
          <option value='15'>15</option>
          <option value='16'>16</option>
          <option value='17'>17</option>
          <option value='18'>18</option>
          <option value='19'>19</option>
          <option value='20'>20</option>
          <option value='21'>21</option>
          <option value='22'>22</option>
          <option value='23'>23</option>
          <option value='24'>24</option>
          <option value='25'>25</option>
          <option value='26'>26</option>
          <option value='27'>27</option>
          <option value='28'>28</option>
          <option value='29'>29</option>
          <option value='30'>30</option>
          <option value='31'>31</option>
        </select>
month
<select name="month" class="dateList" id="month">
  <option value='' selected="selected"></option>
  <option value='01'>01</option>
  <option value='02'>02</option>
  <option value='03'>03</option>
  <option value='04'>04</option>
  <option value='05'>05</option>
  <option value='06'>06</option>
  <option value='07'>07</option>
  <option value='08'>08</option>
  <option value='09'>09</option>
  <option value='10'>10</option>
  <option value='11'>11</option>
  <option value='12'>12</option>
</select>
year
<select name="year" id="year"  class="dateList">
  <option value="" selected="selected"></option>
  <option value="2015">2015</option>
  <option value="2014">2014</option>
  <option value="2013">2013</option>
  <option value="2012">2012</option> 
  <option value="2011">2011</option>
  <option value="2010">2010</option>
  <option value="2009">2009</option>
  <option value="2008">2008</option>
  <option value="2007">2007</option>
  <option value="2006">2006</option>
  <option value="2005">2005</option>
  <option value="2004">2004</option>
  <option value="2003">2003</option>
  <option value="2002">2002</option>
  <option value="2001">2001</option>
  <option value="2000">2000</option>
  <option value="1999">1999</option>
  <option value="1998">1998</option>
  <option value="1997">1997</option>
  <option value="1996">1996</option>
  <option value="1995">1995</option>
  <option value="1994">1994</option>
  <option value="1993">1993</option>
  <option value="1992">1992</option>
  <option value="1991">1991</option>
  <option value="1990">1990</option>
  <option value="1989">1989</option>
  <option value="1988">1988</option>
  <option value="1987">1987</option>
  <option value="1986">1986</option>
  <option value="1985">1985</option>
  <option value="1984">1984</option>
  <option value="1983">1983</option>
  <option value="1982">1982</option>
  <option value="1981">1981</option>
  <option value="1980">1980</option>
  <option value="1979">1979</option>
  <option value="1978">1978</option>
  <option value="1977">1977</option>
  <option value="1976">1976</option>
  <option value="1975">1975</option>
  <option value="1974">1974</option>
  <option value="1973">1973</option>
  <option value="1972">1972</option>
  <option value="1971">1971</option>
  <option value="1970">1970</option>
  <option value="1969">1969</option>
  <option value="1968">1968</option>
  <option value="1967">1967</option>
  <option value="1966">1966</option>
  <option value="1965">1965</option>
  <option value="1964">1964</option>
  <option value="1963">1963</option>
  <option value="1962">1962</option>
  <option value="1961">1961</option>
  <option value="1960">1960</option>
  <option value="1959">1959</option>
  <option value="1958">1958</option>
  <option value="1957">1957</option>
  <option value="1956">1956</option>
  <option value="1955">1955</option>
  <option value="1954">1954</option>
  <option value="1953">1953</option>
  <option value="1952">1952</option>
  <option value="1951">1951</option>
  <option value="1950">1950</option>
  <option value="1949">1949</option>
  <option value="1948">1948</option>
  <option value="1947">1947</option>
  <option value="1946">1946</option>
  <option value="1945">1945</option>
  <option value="1944">1944</option>
  <option value="1943">1943</option>
  <option value="1942">1942</option>
  <option value="1941">1941</option>
  <option value="1940">1940</option>
  <option value="1939">1939</option>
  <option value="1938">1938</option>
  <option value="1937">1937</option>
  <option value="1936">1936</option>
  <option value="1935">1935</option>
  <option value="1934">1934</option>
  <option value="1933">1933</option>
  <option value="1932">1932</option>
  <option value="1931">1931</option>
  <option value="1930">1930</option>
  <option value="1929">1929</option>
  <option value="1928">1928</option>
  <option value="1927">1927</option>
  <option value="1926">1926</option>
  <option value="1925">1925</option>
  <option value="1924">1924</option>
  <option value="1923">1923</option>
  <option value="1922">1922</option>
  <option value="1921">1921</option>
  <option value="1920">1920</option>
  <option value="1919">1919</option>
  <option value="1918">1918</option>
  <option value="1917">1917</option>
  <option value="1916">1916</option>
  <option value="1915">1915</option>
  <option value="1914">1914</option>
  <option value="1913">1913</option>
  <option value="1912">1912</option>
  <option value="1911">1911</option>
  <option value="1910">1910</option>
  <option value="1909">1909</option>
  <option value="1908">1908</option>
  <option value="1907">1907</option>
  <option value="1906">1906</option>
  <option value="1905">1905</option>
</select>
      </span></td>
      <td align="center" class="smallText">&nbsp;</td>
    </tr>
    <tr>
      <td>ECP <span class="smallText">(yyyy-mm-dd)</span></td>
      <td><input name="ecp" type="text" id="ecp" value="<?php echo $row_rsMembers['ecp']; ?>" maxlength="10" />
        <span class="smallText">
        <?php if ($row_rsMembers['ecp'] !="") { ?>
UPDATE (all required)
<?php } else { ?>
ENTER
<?php } ?>
: day
<select name="day2" class="dateList" id="day2">
  <option value="" selected="selected"></option>
  <option value='01'>01</option>
  <option value='02'>02</option>
  <option value='03'>03</option>
  <option value='04'>04</option>
  <option value='05'>05</option>
  <option value='06'>06</option>
  <option value='07'>07</option>
  <option value='08'>08</option>
  <option value='09'>09</option>
  <option value='10'>10</option>
  <option value='11'>11</option>
  <option value='12'>12</option>
  <option value='13'>13</option>
  <option value='14'>14</option>
  <option value='15'>15</option>
  <option value='16'>16</option>
  <option value='17'>17</option>
  <option value='18'>18</option>
  <option value='19'>19</option>
  <option value='20'>20</option>
  <option value='21'>21</option>
  <option value='22'>22</option>
  <option value='23'>23</option>
  <option value='24'>24</option>
  <option value='25'>25</option>
  <option value='26'>26</option>
  <option value='27'>27</option>
  <option value='28'>28</option>
  <option value='29'>29</option>
  <option value='30'>30</option>
  <option value='31'>31</option>
</select>
month
<select name="month2" class="dateList" id="month2">
  <option value='' selected="selected"></option>
  <option value='01'>01</option>
  <option value='02'>02</option>
  <option value='03'>03</option>
  <option value='04'>04</option>
  <option value='05'>05</option>
  <option value='06'>06</option>
  <option value='07'>07</option>
  <option value='08'>08</option>
  <option value='09'>09</option>
  <option value='10'>10</option>
  <option value='11'>11</option>
  <option value='12'>12</option>
</select>
year
<select name="year2" id="year2"  class="dateList">
  <option value="" selected="selected"></option>
  <option value="2015">2015</option>
  <option value="2014">2014</option>
  <option value="2013">2013</option>
  <option value="2012">2012</option> 
  <option value="2011">2011</option>
  <option value="2010">2010</option>
  <option value="2009">2009</option>
  <option value="2008">2008</option>
  <option value="2007">2007</option>
  <option value="2006">2006</option>
  <option value="2005">2005</option>
  <option value="2004">2004</option>
  <option value="2003">2003</option>
  <option value="2002">2002</option>
  <option value="2001">2001</option>
  <option value="2000">2000</option>
  <option value="1999">1999</option>
  <option value="1998">1998</option>
  <option value="1997">1997</option>
  <option value="1996">1996</option>
  <option value="1995">1995</option>
  <option value="1994">1994</option>
  <option value="1993">1993</option>
  <option value="1992">1992</option>
  <option value="1991">1991</option>
  <option value="1990">1990</option>
  <option value="1989">1989</option>
  <option value="1988">1988</option>
  <option value="1987">1987</option>
  <option value="1986">1986</option>
  <option value="1985">1985</option>
  <option value="1984">1984</option>
  <option value="1983">1983</option>
  <option value="1982">1982</option>
  <option value="1981">1981</option>
  <option value="1980">1980</option>
  <option value="1979">1979</option>
  <option value="1978">1978</option>
  <option value="1977">1977</option>
  <option value="1976">1976</option>
  <option value="1975">1975</option>
  <option value="1974">1974</option>
  <option value="1973">1973</option>
  <option value="1972">1972</option>
  <option value="1971">1971</option>
  <option value="1970">1970</option>
  <option value="1969">1969</option>
  <option value="1968">1968</option>
  <option value="1967">1967</option>
  <option value="1966">1966</option>
  <option value="1965">1965</option>
  <option value="1964">1964</option>
  <option value="1963">1963</option>
  <option value="1962">1962</option>
  <option value="1961">1961</option>
  <option value="1960">1960</option>
  <option value="1959">1959</option>
  <option value="1958">1958</option>
  <option value="1957">1957</option>
  <option value="1956">1956</option>
  <option value="1955">1955</option>
  <option value="1954">1954</option>
  <option value="1953">1953</option>
  <option value="1952">1952</option>
  <option value="1951">1951</option>
  <option value="1950">1950</option>
  <option value="1949">1949</option>
  <option value="1948">1948</option>
  <option value="1947">1947</option>
  <option value="1946">1946</option>
  <option value="1945">1945</option>
  <option value="1944">1944</option>
  <option value="1943">1943</option>
  <option value="1942">1942</option>
  <option value="1941">1941</option>
  <option value="1940">1940</option>
  <option value="1939">1939</option>
  <option value="1938">1938</option>
  <option value="1937">1937</option>
  <option value="1936">1936</option>
  <option value="1935">1935</option>
  <option value="1934">1934</option>
  <option value="1933">1933</option>
  <option value="1932">1932</option>
  <option value="1931">1931</option>
  <option value="1930">1930</option>
  <option value="1929">1929</option>
  <option value="1928">1928</option>
  <option value="1927">1927</option>
  <option value="1926">1926</option>
  <option value="1925">1925</option>
  <option value="1924">1924</option>
  <option value="1923">1923</option>
  <option value="1922">1922</option>
  <option value="1921">1921</option>
  <option value="1920">1920</option>
  <option value="1919">1919</option>
  <option value="1918">1918</option>
  <option value="1917">1917</option>
  <option value="1916">1916</option>
  <option value="1915">1915</option>
  <option value="1914">1914</option>
  <option value="1913">1913</option>
  <option value="1912">1912</option>
  <option value="1911">1911</option>
  <option value="1910">1910</option>
  <option value="1909">1909</option>
  <option value="1908">1908</option>
  <option value="1907">1907</option>
  <option value="1906">1906</option>
  <option value="1905">1905</option>
</select>
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>National association</td>
      <td><select name="naid" id="naid">
          <?php
do {  
?>
          <option value="<?php echo $row_rsNA['naid']?>"<?php if (!(strcmp($row_rsNA['naid'], $row_rsMembers['nid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsNA['acronym']?></option>
          <?php
} while ($row_rsNA = mysql_fetch_assoc($rsNA));
  $rows = mysql_num_rows($rsNA);
  if($rows > 0) {
      mysql_data_seek($rsNA, 0);
	  $row_rsNA = mysql_fetch_assoc($rsNA);
  }
?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td align="center" class="tdWhiteBg"><?php if (isset($_POST['processed'])) { ?>
      <img src="../images/tick.gif" alt="tick" width="16" height="16" />
      <?php } ?></td>
      <td class="tdWhiteBg"><input name="mid" type="hidden" id="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
        <input name="btnUpdate2" type="submit" class="btnAdd" id="btnUpdate2" value="Update" />
        <input name="MM_update" type="hidden" id="MM_update" value="frmUpdate" /></td>
      <td align="right" class="tdWhiteBg">&nbsp;</td>
    </tr>
  </table>
  <input name="processed" type="hidden" id="processed" value="1" />
  <input type="hidden" name="MM_update" value="frmUpdate" />
</form>
</body>
</html>
<?php
mysql_free_result($rsMembers);

mysql_free_result($rsMemTypes);

mysql_free_result($rsTypes);

mysql_free_result($rsCountries);

mysql_free_result($rsNA);
?>
