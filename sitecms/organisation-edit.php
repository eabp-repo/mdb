<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/logos";
$ppu->extensions = "GIF,JPG,JPEG,PNG";
$ppu->formName = "form1";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE orgsnew SET orgname=%s, logofile=IFNULL(%s,logofile), address=%s, postcode=%s, city=%s, forummember=%s, exmember=%s, accredited=%s, reaccredited=%s, contact=%s, contact2=%s, contact3=%s, telephone=%s, email=%s, email2=%s, email3=%s, fax=%s, website=%s, skype=%s, invoiceto=%s, invoiceemail=%s, legalstructure=%s, typeid=%s, ethics=%s, trainers=%s, numbertrainees=%s, cid=%s, nid=%s, doa=%s, EABPreps=%s, notes=%s, showonsite=%s WHERE id=%s",
                       GetSQLValueString($_POST['orgname'], "text"),
                       GetSQLValueString($_POST['logofile'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['postcode'], "text"),
                       GetSQLValueString($_POST['city'], "text"),
                       GetSQLValueString(isset($_POST['forummember']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString(isset($_POST['exmember']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['accredited'], "text"),
                       GetSQLValueString($_POST['reaccredited'], "text"),
                       GetSQLValueString($_POST['contact'], "text"),
                       GetSQLValueString($_POST['contact2'], "text"),
                       GetSQLValueString($_POST['contact3'], "text"),
                       GetSQLValueString($_POST['telephone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['email2'], "text"),
                       GetSQLValueString($_POST['email3'], "text"),
                       GetSQLValueString($_POST['fax'], "text"),
                       GetSQLValueString($_POST['website'], "text"),
                       GetSQLValueString($_POST['skype'], "text"),
                       GetSQLValueString($_POST['invoiceto'], "text"),
                       GetSQLValueString($_POST['invoiceemail'], "text"),
                       GetSQLValueString($_POST['legalstructure'], "text"),
                       GetSQLValueString($_POST['typeid'], "int"),
                       GetSQLValueString($_POST['ethics'], "text"),
                       GetSQLValueString($_POST['trainers'], "text"),
                       GetSQLValueString($_POST['numbertrainees'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['nid'], "int"),
                       GetSQLValueString($_POST['doa'], "text"),
                       GetSQLValueString($_POST['EABPreps'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

$colname_rsOrg = "-1";
if (isset($_GET['id'])) {
  $colname_rsOrg = $_GET['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrg = sprintf("SELECT * FROM orgsnew WHERE id = %s", GetSQLValueString($colname_rsOrg, "int"));
$rsOrg = mysql_query($query_rsOrg, $connEABP2) or die(mysql_error());
$row_rsOrg = mysql_fetch_assoc($rsOrg);
$totalRows_rsOrg = mysql_num_rows($rsOrg);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsType = "SELECT * FROM forumtypes WHERE forumtypeid <= 3";
$rsType = mysql_query($query_rsType, $connEABP2) or die(mysql_error());
$row_rsType = mysql_fetch_assoc($rsType);
$totalRows_rsType = mysql_num_rows($rsType);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNAs = "SELECT naid, acronym FROM `national` WHERE fullname <> '' OR naid = 12 ORDER BY acronym";
$rsNAs = mysql_query($query_rsNAs, $connEABP2) or die(mysql_error());
$row_rsNAs = mysql_fetch_assoc($rsNAs);
$totalRows_rsNAs = mysql_num_rows($rsNAs);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgs = "SELECT id, orgname FROM orgsnew ORDER BY orgname ASC";
$rsOrgs = mysql_query($query_rsOrgs, $connEABP2) or die(mysql_error());
$row_rsOrgs = mysql_fetch_assoc($rsOrgs);
$totalRows_rsOrgs = mysql_num_rows($rsOrgs);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>organisation member edit</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<style type="text/css">
input, select {
	background-color: #FFFFFF;
	width: 700px;
	font-size: 1em;
}
#logofile {
	width: 250px;
}
#form1 table {
	background-color: #EFEFEF;
}
</style>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<form id="frmOrgs" name="frmOrgs" method="get" action="organisation-edit.php">
  <select name="id" id="id">
    <option value="">Choose another organisation:</option>
    <?php
do {  
?>
    <option value="<?php echo $row_rsOrgs['id']?>"><?php echo $row_rsOrgs['orgname']?></option>
    <?php
} while ($row_rsOrgs = mysql_fetch_assoc($rsOrgs));
  $rows = mysql_num_rows($rsOrgs);
  if($rows > 0) {
      mysql_data_seek($rsOrgs, 0);
	  $row_rsOrgs = mysql_fetch_assoc($rsOrgs);
  }
?>
  </select>
  <input name="button7" type="submit" class="btnGo" id="button7" value="Go" />
</form>
<p>&nbsp;</p>
<h1>Edit organisation member </h1>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,PNG',false,'','','','','','','');return document.MM_returnValue">
  <table cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Name:</td>
      <td><input name="orgname" type="text" value="<?php echo ($row_rsOrg['orgname']); ?>" size="32" /></td>
    </tr>
    <tr>
      <td align="right" nowrap="nowrap" bgcolor="#FFFFFF">Logo file:</td>
      <td bgcolor="#FFFFFF"><input name="logofile" type="file" id="logofile" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,PNG',false,'','','','','','','')" />
        <span class="smallText">current:</span> <img src="../images/logos/<?php echo $row_rsOrg['logofile']; ?>" alt="" align="absmiddle" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Address:</td>
      <td><input type="text" name="address" value="<?php echo htmlentities($row_rsOrg['address'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Post code: </td>
      <td><input name="postcode" type="text" class="input100" id="postcode" value="<?php echo $row_rsOrg['postcode']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">City:</td>
      <td><input name="city" type="text" class="input400" value="<?php echo htmlentities($row_rsOrg['city'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Country:</td>
      <td><select name="cid" class="input100">
        <?php
do {  
?><option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsOrg['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
              </select>      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Forum member:</td>
      <td><input name="forummember" type="checkbox" class="input50" id="forummember" <?php if (!(strcmp($row_rsOrg['forummember'],1))) {echo "checked=\"checked\"";} ?> /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Ex-member:</td>
      <td><input <?php if (!(strcmp($row_rsOrg['exmember'],1))) {echo "checked=\"checked\"";} ?> name="exmember" type="checkbox" class="input50" id="exmember" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Accredited:</td>
      <td><input name="accredited" type="text" id="accredited" value="<?php echo htmlentities($row_rsOrg['accredited'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Re-accredited:</td>
      <td><input name="reaccredited" type="text" id="reaccredited" value="<?php echo htmlentities($row_rsOrg['reaccredited']); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact:</td>
      <td><input type="text" name="contact" value="<?php echo htmlentities($row_rsOrg['contact'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact 2:</td>
      <td><input name="contact2" type="text" id="contact2" value="<?php echo $row_rsOrg['contact2']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Contact 3:</td>
      <td><input name="contact3" type="text" id="contact3" value="<?php echo $row_rsOrg['contact3']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Telephone:</td>
      <td><input type="text" name="telephone" value="<?php echo htmlentities($row_rsOrg['telephone'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email:</td>
      <td><input type="text" name="email" value="<?php echo htmlentities($row_rsOrg['email'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email2:</td>
      <td><input type="text" name="email2" value="<?php echo htmlentities($row_rsOrg['email2'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email3:</td>
      <td><input type="text" name="email3" value="<?php echo htmlentities($row_rsOrg['email3'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Fax:</td>
      <td><input name="fax" type="text" class="input400" value="<?php echo htmlentities($row_rsOrg['fax'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Website:</td>
      <td><input name="website" type="text" class="input400" value="<?php echo htmlentities($row_rsOrg['website'], ENT_COMPAT, 'utf-8'); ?>" size="32" /> 
        NO http://</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Skype:</td>
      <td valign="baseline"><input name="skype" type="text" class="input400" id="skype" value="<?php echo $row_rsOrg['skype']; ?>" /></td>
    </tr>
    <tr valign="baseline" bgcolor="#CCCCCC">
      <td nowrap="nowrap">Invoices</td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Invoice to:</td>
      <td bgcolor="#CCCCCC"><input name="invoiceto" type="text" class="input400" id="invoiceto" value="<?php echo $row_rsOrg['invoiceto']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Email to:</td>
      <td bgcolor="#CCCCCC"><input name="invoiceemail" type="text" class="input400" id="invoiceemail" value="<?php echo $row_rsOrg['invoiceemail']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Legal structure:</td>
      <td><input type="text" name="legalstructure" value="<?php echo htmlentities($row_rsOrg['legalstructure'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Type:</td>
      <td><select name="typeid" class="input400">
        <?php
do {  
?>
        <option value="<?php echo $row_rsType['forumtypeid']?>"<?php if (!(strcmp($row_rsType['forumtypeid'], $row_rsOrg['typeid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsType['forumtype']?></option>
        <?php
} while ($row_rsType = mysql_fetch_assoc($rsType));
  $rows = mysql_num_rows($rsType);
  if($rows > 0) {
      mysql_data_seek($rsType, 0);
	  $row_rsType = mysql_fetch_assoc($rsType);
  }
?>
      </select>      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Ethics module:</td>
      <td><input type="text" name="ethics" value="<?php echo htmlentities($row_rsOrg['ethics'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Number trainers:</td>
      <td><input type="text" name="trainers" value="<?php echo htmlentities($row_rsOrg['trainers'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Current number trainees:</td>
      <td><input type="text" name="numbertrainees" value="<?php echo htmlentities($row_rsOrg['numbertrainees'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Date formed:</td>
      <td><input name="doa" type="text" class="input100" value="<?php echo htmlentities($row_rsOrg['doa'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">EABP members:</td>
      <td><input name="EABPreps" type="text" class="input500" value="<?php echo htmlentities($row_rsOrg['EABPreps'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">National Association:</td>
      <td><select name="nid" class="input400" id="nid">
        <option value="0" <?php if (!(strcmp(0, $row_rsOrg['nid']))) {echo "selected=\"selected\"";} ?>>Please choose:</option>
        <?php
do {  
?>
<option value="<?php echo $row_rsNAs['naid']?>"<?php if (!(strcmp($row_rsNAs['naid'], $row_rsOrg['nid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsNAs['acronym']?></option>
        <?php
} while ($row_rsNAs = mysql_fetch_assoc($rsNAs));
  $rows = mysql_num_rows($rsNAs);
  if($rows > 0) {
      mysql_data_seek($rsNAs, 0);
	  $row_rsNAs = mysql_fetch_assoc($rsNAs);
  }
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap">Notes:</td>
      <td><input name="notes" type="text" value="<?php echo htmlentities($row_rsOrg['notes'], ENT_COMPAT, 'utf-8'); ?>" size="75" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Show on site:</td>
      <td><input name="showonsite" type="checkbox" class="input50" value=""  <?php if (!(strcmp(htmlentities($row_rsOrg['showonsite'], ENT_COMPAT, 'utf-8'),1))) {echo "checked=\"checked\"";} ?> /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?php echo $row_rsOrg['id']; ?>" />
  <input type="hidden" name="MM_update" value="form1" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsOrg);

mysql_free_result($rsType);

mysql_free_result($rsCountries);

mysql_free_result($rsNAs);

mysql_free_result($rsOrgs);
?>
