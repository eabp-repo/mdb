<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM allorgs WHERE id=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsOrg = "-1";
if (isset($_GET['id'])) {
  $colname_rsOrg = $_GET['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrg = sprintf("SELECT id, orgname FROM allorgs WHERE id = %s", GetSQLValueString($colname_rsOrg, "int"));
$rsOrg = mysql_query($query_rsOrg, $connEABP2) or die(mysql_error());
$row_rsOrg = mysql_fetch_assoc($rsOrg);
$totalRows_rsOrg = mysql_num_rows($rsOrg);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Org delete</title>
<link href="cms.css" rel="stylesheet" type="text/css">
</head>

<body>
<p><a href="all-orgs-edit.php?id=<?php echo $row_rsOrg['id']; ?>">&lt; back</a></p>
<h1>Delete  <em><?php echo $row_rsOrg['orgname']; ?></em>? </h1>
<p> The information will be removed from the membership database. </p>
<p>This is permanent - you will not be able to undo this action.</p>
<form action="" method="post" name="frmdelete" id="frmdelete">
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsOrg['id']; ?>">
  <input type="submit" name="btnDelete" id="btnDelete" value="Yes, delete">
</form>
</body>
</html>
<?php
mysql_free_result($rsOrg);
?>
