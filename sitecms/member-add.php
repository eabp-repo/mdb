<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/memberphotos";
$ppu->extensions = "GIF,JPG,JPEG,BMP,PNG";
$ppu->formName = "frmAdd";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmAdd")) {
  $insertSQL = sprintf("INSERT INTO member (doa, dob, doj, dor, ecp, email, fax, firstname, forum, gender, homecity, homephone, homepostcode, homeadd1, lastname, mobile, modality, notes, post, school, skype, title, webpage, workphone, cid, nid, tid, webpassword, workadd1, workcity, workpostcode, workcid, showworkinfo, showhomeinfo, imagefile, education, currentwork, employmenthistory, bio, blogaddress, workshops, showonsite) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['doa'], "text"),
                       GetSQLValueString($_POST['dob'], "text"),
                       GetSQLValueString($_POST['doj'], "text"),
                       GetSQLValueString($_POST['dor'], "text"),
                       GetSQLValueString($_POST['ecp'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['fax'], "text"),
                       GetSQLValueString($_POST['firstname'], "text"),
                       GetSQLValueString($_POST['forum'], "text"),
                       GetSQLValueString($_POST['gender'], "text"),
                       GetSQLValueString($_POST['homecity'], "text"),
                       GetSQLValueString($_POST['homephone'], "text"),
                       GetSQLValueString($_POST['homepostcode'], "text"),
                       GetSQLValueString($_POST['homeadd1'], "text"),
                       GetSQLValueString($_POST['lastname'], "text"),
                       GetSQLValueString($_POST['mobile'], "text"),
                       GetSQLValueString($_POST['modality'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString($_POST['post'], "text"),
                       GetSQLValueString($_POST['school'], "text"),
                       GetSQLValueString($_POST['skype'], "text"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['webpage'], "text"),
                       GetSQLValueString($_POST['workphone'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['naid'], "int"),
                       GetSQLValueString($_POST['tid'], "int"),
                       GetSQLValueString($_POST['webpassword'], "text"),
                       GetSQLValueString($_POST['workadd1'], "text"),
                       GetSQLValueString($_POST['workcity'], "text"),
                       GetSQLValueString($_POST['workpostcode'], "text"),
                       GetSQLValueString($_POST['workcid'], "int"),
                       GetSQLValueString(isset($_POST['showworkinfo']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString(isset($_POST['showhomeinfo']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['imagefile'], "text"),
                       GetSQLValueString($_POST['education'], "text"),
                       GetSQLValueString($_POST['currentwork'], "text"),
                       GetSQLValueString($_POST['employmenthistory'], "text"),
                       GetSQLValueString($_POST['bio'], "text"),
                       GetSQLValueString($_POST['blogaddress'], "text"),
                       GetSQLValueString($_POST['workshops'], "text"),
                       GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined","1","0"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9 AND tid <> 11 AND tid <> 6";
$rsMemTypes = mysql_query($query_rsMemTypes, $connEABP2) or die(mysql_error());
$row_rsMemTypes = mysql_fetch_assoc($rsMemTypes);
$totalRows_rsMemTypes = mysql_num_rows($rsMemTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9 AND tid <> 11";
$rsTypes = mysql_query($query_rsTypes, $connEABP2) or die(mysql_error());
$row_rsTypes = mysql_fetch_assoc($rsTypes);
$totalRows_rsTypes = mysql_num_rows($rsTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = "SELECT naid, acronym FROM `national`";
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>EABP admin | Add a member</title>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>

<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" />  admin home</a></p>
<h1>Add a member</h1>
<hr size="1" noshade="noshade" />
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmAdd" id="frmAdd" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,BMP,PNG',false,'','','','','','','');return document.MM_returnValue">
  <table border="0" cellpadding="3" cellspacing="0" id="tblDetails">
    <?php if ($_GET['tid'] == 11) { ?>
    <?php } ?>
    <tr>
      <td>Show on website</td>
      <td><input name="showonsite" type="checkbox" id="showonsite" value="1" /></td>
      <td rowspan="7" align="right">&nbsp;</td>
    </tr>
    <tr>
      <td width="190">Last Name</td>
      <td><input name="lastname" type="text" class="largerText" id="lastname" /></td>
    </tr>
    <tr>
      <td>First Name</td>
      <td><input name="firstname" type="text" id="firstname" /></td>
    </tr>
    <tr>
      <td>Title</td>
      <td><input name="post" type="text" id="post" /></td>
    </tr>
    <tr>
      <td>Letters after name</td>
      <td><input name="title" type="text" id="title" /></td>
    </tr>
    <tr>
      <td>Gender</td>
      <td><select name="gender" id="gender">
        <option value="Female">Female</option>
        <option value="Male">Male</option>
      </select></td>
    </tr>
    <tr>
      <td>Type of membership</td>
      <td><select name="tid" id="tid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsMemTypes['tid']?>"><?php echo $row_rsMemTypes['type']?></option>
        <?php
} while ($row_rsMemTypes = mysql_fetch_assoc($rsMemTypes));
  $rows = mysql_num_rows($rsMemTypes);
  if($rows > 0) {
      mysql_data_seek($rsMemTypes, 0);
	  $row_rsMemTypes = mysql_fetch_assoc($rsMemTypes);
  }
?>
            </select></td>
    </tr>
    <tr>
      <td>Organisation</td>
      <td><input name="school" type="text" id="school" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Web password</td>
      <td><input name="webpassword" type="text" id="webpassword" value="eabppassword" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Photo</td>
      <td><input name="imagefile" type="file" id="imagefile" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,BMP,PNG',false,'','','','','','','')" /></td>
      <td align="center" class="smallText">Optional - 50KB max</td>
    </tr>
    <tr>
      <td>Date of Birth</td>
      <td><input name="dob" type="text" id="dob" /></td>
      <td align="center" class="smallText">enter as yyyy-mm-dd</td>
    </tr>
    <tr>
      <td>Email</td>
      <td><input name="email" type="text" id="email" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Fax</td>
      <td><input name="fax" type="text" id="fax" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Mobile Phone</td>
      <td><input name="mobile" type="text" id="mobile" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Work Phone</td>
      <td><input name="workphone" type="text" id="workphone" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Work Street</td>
      <td><input name="workadd1" type="text" id="workadd1" /></td>
      <td rowspan="10" align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Work City</td>
      <td><input name="workcity" type="text" id="workcity" /></td>
    </tr>
    <tr>
      <td>Work Postal Code</td>
      <td><input name="workpostcode" type="text" id="workpostcode" /></td>
    </tr>
    <tr>
      <td>Work Country</td>
      <td><select name="workcid" id="workcid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCountries['cid']?>"><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
            </select></td>
    </tr>
    <tr>
      <td>Show work info on website</td>
      <td><input name="showworkinfo" type="checkbox" id="showworkinfo" value="1" /></td>
    </tr>
    <tr>
      <td>Home Phone</td>
      <td><input name="homephone" type="text" id="homephone" /></td>
    </tr>
    <tr>
      <td>Home Street</td>
      <td><input name="homeadd1" type="text" id="homeadd1" /></td>
    </tr>
    <tr>
      <td>Home City</td>
      <td><input name="homecity" type="text" id="homecity" /></td>
    </tr>
    <tr>
      <td>Home Postal Code</td>
      <td><input name="homepostcode" type="text" id="homepostcode" /></td>
    </tr>
    <tr>
      <td>Home Country</td>
      <td><select name="cid" id="cid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCountries['cid']?>"><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
            </select></td>
    </tr>
    <tr>
      <td>Show home info on website</td>
      <td><input name="showhomeinfo" type="checkbox" id="showhomeinfo" value="1" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Skype</td>
      <td><input name="skype" type="text" id="skype" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top">Education</td>
      <td><textarea name="education" id="education" cols="45" rows="5"></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top">Current work</td>
      <td><textarea name="currentwork" id="currentwork" cols="45" rows="5"></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top">Employment history</td>
      <td><textarea name="employmenthistory" id="employmenthistory" cols="45" rows="5"></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top">Bio</td>
      <td><textarea name="bio" id="bio" cols="45" rows="5"></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top">Workshops</td>
      <td><textarea name="workshops" id="workshops" cols="45" rows="5"></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Speciality</td>
      <td>To be added to database</td>
      <td class="smallText">&nbsp;</td>
    </tr>
    <tr>
      <td>Blog address</td>
      <td><input name="blogaddress" type="text" id="blogaddress" /></td>
      <td class="smallText">NO http://</td>
    </tr>
    <tr>
      <td>Website address</td>
      <td><input name="webpage" type="text" id="webpage" /></td>
      <td class="smallText">NO http://</td>
    </tr>
    <tr>
      <td>ECP</td>
      <td><input name="ecp" type="text" id="ecp" /></td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td>Modality</td>
      <td><input name="modality" type="text" id="modality" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Forum</td>
      <td><input name="forum" type="text" id="forum" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>National association</td>
      <td><select name="naid" id="naid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsNA['naid']?>"><?php echo $row_rsNA['acronym']?></option>
        <?php
} while ($row_rsNA = mysql_fetch_assoc($rsNA));
  $rows = mysql_num_rows($rsNA);
  if($rows > 0) {
      mysql_data_seek($rsNA, 0);
	  $row_rsNA = mysql_fetch_assoc($rsNA);
  }
?>
            </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td valign="top">Notes</td>
      <td><textarea name="notes" id="notes" cols="45" rows="3"></textarea></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Membership Declaration</td>
      <td><input name="declaration" type="text" id="declaration" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Date of Application</td>
      <td><input name="doa" type="text" id="doa" /></td>
      <td align="center" class="smallText">enter as yyyy-mm-dd</td>
    </tr>
    <tr>
      <td>Date of Joining</td>
      <td><input name="doj" type="text" id="doj" /></td>
      <td align="center" class="smallText">enter as yyyy-mm-dd</td>
    </tr>
    <tr>
      <td>Date of resignation</td>
      <td><input name="dor" type="text" id="dor" /></td>
      <td align="center" class="smallText">enter as yyyy-mm-dd</td>
    </tr>

    <tr>
      <td>&nbsp;</td>
      <td><input name="btnAdd" type="submit" class="btnAdd" id="btnAdd" value="Add to database" /></td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <input name="processed" type="hidden" id="processed" value="1" />
  <input type="hidden" name="MM_insert" value="frmAdd" />
</form>
</body>
</html>
<?php
mysql_free_result($rsMembers);

mysql_free_result($rsMemTypes);

mysql_free_result($rsTypes);

mysql_free_result($rsCountries);

mysql_free_result($rsNA);
?>
