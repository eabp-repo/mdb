<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['delid'])) && ($_GET['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM membercongresses WHERE mcid=%s",
                       GetSQLValueString($_GET['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmAddCongress")) {
  $insertSQL = sprintf("INSERT INTO membercongresses (mid, congressid) VALUES (%s, %s)",
                       GetSQLValueString($_POST['mid'], "int"),
                       GetSQLValueString($_POST['congressid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCongresses = "SELECT * FROM congresses ORDER BY year DESC";
$rsCongresses = mysql_query($query_rsCongresses, $connEABP2) or die(mysql_error());
$row_rsCongresses = mysql_fetch_assoc($rsCongresses);
$totalRows_rsCongresses = mysql_num_rows($rsCongresses);

$colname_rsMemCongresses = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMemCongresses = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemCongresses = sprintf("SELECT mcid, congress FROM membercongresses INNER JOIN congresses ON membercongresses.congressid = congresses.congressid WHERE mid = %s", GetSQLValueString($colname_rsMemCongresses, "int"));
$rsMemCongresses = mysql_query($query_rsMemCongresses, $connEABP2) or die(mysql_error());
$row_rsMemCongresses = mysql_fetch_assoc($rsMemCongresses);
$totalRows_rsMemCongresses = mysql_num_rows($rsMemCongresses);

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT mid, firstname, lastname FROM member WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

$colname_rsCPD = "-1";
if (isset($_GET['mid'])) {
  $colname_rsCPD = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCPD = sprintf("SELECT * FROM membercpd WHERE mid = %s", GetSQLValueString($colname_rsCPD, "int"));
$rsCPD = mysql_query($query_rsCPD, $connEABP2) or die(mysql_error());
$row_rsCPD = mysql_fetch_assoc($rsCPD);
$totalRows_rsCPD = mysql_num_rows($rsCPD);

$colname_rsMemCPD = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMemCPD = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemCPD = sprintf("SELECT * FROM membercpd WHERE mid = %s", GetSQLValueString($colname_rsMemCPD, "int"));
$rsMemCPD = mysql_query($query_rsMemCPD, $connEABP2) or die(mysql_error());
$row_rsMemCPD = mysql_fetch_assoc($rsMemCPD);
$totalRows_rsMemCPD = mysql_num_rows($rsMemCPD);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP individual membership database update</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#frmCPD {
	float: left;
	width: 600px;
}
#rightBar {
	margin-top: 50px;
}
.input20 {
	width: 25px;
	font-size: 1.2em;
	float: left;
}
#frmCV label {
	clear: both;
	float: left;
}
-->
</style>
    <SCRIPT language="JavaScript">
function popUp(URL) {
eval("page" + " = window.open(URL, '" + "', 'toolbars=0,scrollbars=1,location=0,statusbars=0,menubars=0,resizable=0,width=500,height=400,left=50,top=100');");
}
</script>
    <script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
    <script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p class="leftAlign"><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" />  admin home</a></p>
<!-- Google Translate -->
<div id="google_translate_element"></div>
<p class="googleTranslateText">
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    includedLanguages: 'bg,da,nl,en,fi,fr,de,el,iw,it,ja,no,pl,pt,ru,sr,sl,es,sv',
    gaTrack: true,
    gaId: 'UA-224881-50',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  The Google Translator is here to give you some help but it is not perfect!</p>
<!-- End -->
<p class="clear">EABP individual member update:</p>
<p><a href="member-edit-login.php?mid=<?php echo $row_rsMembers['mid']; ?>">Login details</a> | <a href="member-edit-general.php?mid=<?php echo $row_rsMembers['mid']; ?>">General</a> | <a href="member-edit-contact.php?mid=<?php echo $row_rsMembers['mid']; ?>">Contact details</a> | <a href="member-edit-membership.php?mid=<?php echo $row_rsMembers['mid']; ?>">Membership</a> | <a href="member-edit-training.php?mid=<?php echo $row_rsMembers['mid']; ?>">Training</a> | <a href="member-edit-work.php?mid=<?php echo $row_rsMembers['mid']; ?>">Current Work</a> | Congresses attended | <a href="member-delete.php?mid=<?php echo $row_rsMembers['mid']; ?>">Delete this member</a> (you will be asked to confirm)</p>
<hr size="1" noshade="noshade" />
  <h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
  <h2>Congresses attended</h2>
  <hr size="1" noshade="noshade" />
<?php if ($totalRows_rsMemCPD == 0) { // Show if recordset empty ?>
  <h2><a href="member-cpd-add.php?mid=<?php echo $row_rsMembers['mid']; ?>">To activate this section click here</a></h2>
  <?php } // Show if recordset empty ?>

  <?php if ($totalRows_rsMemCPD > 0) { // Show if recordset not empty ?>
 
    
    <h2>Congresses attended</h2>
    <?php if ($totalRows_rsMemCongresses == 0) { // Show if recordset empty ?>
      <p>No congresses attended.</p>
      <?php } // Show if recordset empty ?>
    <?php if ($totalRows_rsMemCongresses > 0) { // Show if recordset not empty ?>
      <ul>
        <?php do { ?>
          <li><?php echo $row_rsMemCongresses['congress']; ?> - <a href="member-edit-cpd.php?delid=<?php echo $row_rsMemCongresses['mcid']; ?>&amp;mid=<?php echo $_GET['mid']; ?>">delete</a></li>
          <?php } while ($row_rsMemCongresses = mysql_fetch_assoc($rsMemCongresses)); ?>
      </ul>
      <?php } // Show if recordset not empty ?>
    <form id="frmAddCongress" name="frmAddCongress" method="POST" action="<?php echo $editFormAction; ?>">
      <select name="congressid" id="congressid">
        <option value="">Add a congress: </option>
        <?php
do {  
?>
        <option value="<?php echo $row_rsCongresses['congressid']?>"><?php echo $row_rsCongresses['congress']?></option>
        <?php
} while ($row_rsCongresses = mysql_fetch_assoc($rsCongresses));
  $rows = mysql_num_rows($rsCongresses);
  if($rows > 0) {
      mysql_data_seek($rsCongresses, 0);
	  $row_rsCongresses = mysql_fetch_assoc($rsCongresses);
  }
?>
      </select>
      <input name="button" type="submit" class="btnGo" id="button" value="Go" />
      <input name="mid" type="hidden" id="mid" value="<?php echo $_GET['mid']; ?>" />
      <input type="hidden" name="MM_insert" value="frmAddCongress" />
    </form>
  
  <?php } // Show if recordset not empty ?>
</body>
</html>
<?php
mysql_free_result($rsCongresses);

mysql_free_result($rsMemCongresses);

mysql_free_result($rsMembers);

mysql_free_result($rsCPD);

mysql_free_result($rsMemCPD);
?>
