<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM forum WHERE id=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsForum = "-1";
if (isset($_GET['id'])) {
  $colname_rsForum = $_GET['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsForum = sprintf("SELECT id, orgname FROM forum WHERE id = %s", GetSQLValueString($colname_rsForum, "int"));
$rsForum = mysql_query($query_rsForum, $connEABP2) or die(mysql_error());
$row_rsForum = mysql_fetch_assoc($rsForum);
$totalRows_rsForum = mysql_num_rows($rsForum);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Delete forum</title>
</head>

<body>
<h1><?php echo $row_rsForum['orgname']; ?></h1>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsForum['id']; ?>" />
  <input type="submit" name="btnDelete" id="btnDelete" value="Yes, delete" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsForum);
?>
