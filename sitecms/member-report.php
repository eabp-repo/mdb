<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = "SELECT CASE WHEN acronym IS NULL THEN 'TOTAL' ELSE acronym END  AS 'National Association', country, Count(CASE WHEN member.tid =7 THEN mid END) AS 'Full', Count(CASE WHEN member.tid =4 THEN mid END) AS 'Candidate', Count(CASE WHEN member.tid =3 THEN mid END) AS 'Associate', Count(CASE WHEN member.tid =13 THEN mid END) AS 'Student', Count(CASE WHEN member.tid =8 THEN mid END) AS 'Honorary', Count(*) AS Total FROM (national INNER JOIN country ON national.cid = country.cid) INNER JOIN (type INNER JOIN member ON type.tid = member.tid) ON national.naid = member.nid WHERE national.naid <> 12 AND national.naid <> 1 AND type.tid <> 1 AND type.tid <> 2 AND type.tid <> 5 AND type.tid <> 6 AND type.tid <> 9 AND type.tid <> 11  AND type.tid <> 12   AND showonsite <> 0 GROUP BY acronym WITH ROLLUP";
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsEABP = "SELECT CASE WHEN country IS NULL THEN 'Total' ELSE country END  AS 'Country', Count(CASE WHEN member.tid =7 THEN mid END) AS 'Full', Count(CASE WHEN member.tid =4 THEN mid END) AS 'Candidate', Count(CASE WHEN member.tid =3 THEN mid END) AS 'Associate', Count(CASE WHEN member.tid =13 THEN mid END) AS 'Student', Count(CASE WHEN member.tid =8 THEN mid END) AS 'Honorary', Count(*) AS Total FROM type INNER JOIN (country INNER JOIN member ON country.cid = member.cid) ON type.tid = member.tid WHERE member.nid = 12 AND type.tid <> 1 AND type.tid <> 2 AND type.tid <> 5 AND type.tid <> 6 AND type.tid <> 9 AND type.tid <> 11  AND type.tid <> 12  AND showonsite <> 0 GROUP BY country WITH ROLLUP";
$rsEABP = mysql_query($query_rsEABP, $connEABP2) or die(mysql_error());
$row_rsEABP = mysql_fetch_assoc($rsEABP);
$totalRows_rsEABP = mysql_num_rows($rsEABP);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgs = "SELECT acronym, CASE WHEN country.country IS NULL THEN 'Total' ELSE country.country END  AS 'Country',  Count(*) AS Organisation FROM (country RIGHT JOIN national ON country.cid = national.cid) LEFT JOIN orgsnew ON country.cid = orgsnew.cid    WHERE national.naid <> 12 AND acronym <> country.country   AND showonsite <> 0 GROUP BY acronym WITH ROLLUP";
$rsOrgs = mysql_query($query_rsOrgs, $connEABP2) or die(mysql_error());
$row_rsOrgs = mysql_fetch_assoc($rsOrgs);
$totalRows_rsOrgs = mysql_num_rows($rsOrgs);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgs2 = "SELECT country.country,  Count(*) AS Organisation FROM (country LEFT JOIN national ON country.cid = national.cid) RIGHT JOIN orgsnew ON country.cid = orgsnew.cid    WHERE orgsnew.nid = 12 GROUP BY country.country WITH ROLLUP";
$rsOrgs2 = mysql_query($query_rsOrgs2, $connEABP2) or die(mysql_error());
$row_rsOrgs2 = mysql_fetch_assoc($rsOrgs2);
$totalRows_rsOrgs2 = mysql_num_rows($rsOrgs2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Current Membership</title>
<style type="text/css">
.total {
	font-weight: bold;
	font-size: 20px;
}
tr:nth-child(odd)		{ background-color:#fff; }
tr:nth-child(even)		{ background-color:#eee; }
h2 {
	clear: both;
	float: left;
}
.clear {
	clear: both;
}
#tblOrgs {
	margin-left: 20px;
}
#tblOrgs2 {
	margin-left: 20px;
}
</style>
</head>

<body>
<h1>Membership Reports</h1>
<p>Shows current data.</p>
<h2>National Associations</h2>
<table border="1" align="left" cellpadding="6" cellspacing="0" class="clear">
  <tr>
    <th>&nbsp;</th>
    <th colspan="6">Individuals - <a href="export-members-eabp-by-na.php">download spreadsheet</a></th>
  </tr>
  <tr>
    <th width="200">National Association</th>
    <th width="60">Full</th>
    <th width="60">Candidate</th>
    <th width="60">Associate</th>
    <th width="60">Student</th>
    <th width="60">Honorary</th>
    <th width="60"><strong>Total</strong></th>
  </tr>
  <?php do { ?>
    <tr align="left" <?php if ($row_rsMembers['National Association'] == "TOTAL") { ?> class="total"<?php }?>>
      <td align="left"><?php echo $row_rsMembers['National Association']; ?><?php if($row_rsMembers['National Association'] != "TOTAL") echo(' - '.$row_rsMembers['country']); ?></td>
      <td align="center"><?php echo $row_rsMembers['Full']; ?></td>
      <td align="center"><?php echo $row_rsMembers['Candidate']; ?></td>
      <td align="center"><?php echo $row_rsMembers['Associate']; ?></td>
      <td align="center"><?php echo $row_rsMembers['Student']; ?></td>
      <td align="center"><?php echo $row_rsMembers['Honorary']; ?></td>
      <td align="center"><strong><?php echo $row_rsMembers['Total']; ?></strong></td>
    </tr>
    <?php } while ($row_rsMembers = mysql_fetch_assoc($rsMembers)); ?>
</table>
<table border="1" align="left" cellpadding="6" cellspacing="0" id="tblOrgs">
  <tr>
    <th>&nbsp;</th>
    <th>Organisations</th>
  </tr>
  <tr>
    <th width="60">&nbsp;</th>
    <th width="60">Total</th>
  </tr>
  <?php do { ?>
    <tr <?php if ($row_rsOrgs['acronym'] == "") { ?> class="total"<?php }?>>
      <td align="center"><?php echo $row_rsOrgs['acronym']; ?></td>
      <td align="center"><strong><?php echo $row_rsOrgs['Organisation']; ?></strong></td>
    </tr>
    <?php } while ($row_rsOrgs = mysql_fetch_assoc($rsOrgs)); ?>
</table>
<h2>Membership directly through EABP </h2>
<table border="1" align="left" cellpadding="6" cellspacing="0" class="clear">
  <tr>
    <th>&nbsp;</th>
    <th colspan="6">Individuals - <a href="export-members-eabp-as-na.php">download spreadsheet</a></th>
  </tr>
  <tr>
    <th width="200">Country</th>
    <th width="60">Full</th>
    <th width="60">Candidate</th>
    <th width="60">Associate</th>
    <th width="60">Student</th>
    <th width="60">Honorary</th>
    <th width="60"><strong>Total</strong></th>
  </tr>
  <?php do { ?>
    <tr <?php if($row_rsEABP['Country'] =="") {  ?>class="total"<?php }?>>
      <td><?php if($row_rsEABP['Country'] !="") {  ?><?php echo $row_rsEABP['Country']; ?><?php } else { ?>TOTAL<?php }?> </td>
      <td align="center"><?php echo $row_rsEABP['Full']; ?></td>
      <td align="center"><?php echo $row_rsEABP['Candidate']; ?></td>
      <td align="center"><?php echo $row_rsEABP['Associate']; ?></td>
      <td align="center"><?php echo $row_rsEABP['Student']; ?></td>
      <td align="center"><?php echo $row_rsEABP['Honorary']; ?></td>
      <td align="center"><strong><?php echo $row_rsEABP['Total']; ?></strong></td>
    </tr>
    <?php } while ($row_rsEABP = mysql_fetch_assoc($rsEABP)); ?>
</table>
<table border="1" align="left" cellpadding="6" cellspacing="0" id="tblOrgs2">
  <tr>
    <th>&nbsp;</th>
    <th>Organisations</th>
  </tr>
  <tr>
    <th>&nbsp;</th>
    <th>Total</th>
  </tr>
    <?php do { ?>
      <tr>
        <td width="130" align="center"><?php echo $row_rsOrgs2['country']; ?></td>
        <td align="center"><strong><?php echo $row_rsOrgs2['Organisation']; ?></strong></td>
    </tr>
      <?php } while ($row_rsOrgs2 = mysql_fetch_assoc($rsOrgs2)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($rsMembers);

mysql_free_result($rsEABP);

mysql_free_result($rsOrgs);

mysql_free_result($rsOrgs2);
?>
