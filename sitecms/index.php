<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNews = "SELECT newsid, title FROM news ORDER BY newsid DESC";
$rsNews = mysql_query($query_rsNews, $connEABP2) or die(mysql_error());
$row_rsNews = mysql_fetch_assoc($rsNews);
$totalRows_rsNews = mysql_num_rows($rsNews);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsForums = "SELECT id, orgname FROM forum ORDER BY orgname";
$rsForums = mysql_query($query_rsForums, $connEABP2) or die(mysql_error());
$row_rsForums = mysql_fetch_assoc($rsForums);
$totalRows_rsForums = mysql_num_rows($rsForums);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNAs = "SELECT naid, acronym, fullname FROM `national`WHERE fullname <> '' ORDER BY acronym ASC";
$rsNAs = mysql_query($query_rsNAs, $connEABP2) or die(mysql_error());
$row_rsNAs = mysql_fetch_assoc($rsNAs);
$totalRows_rsNAs = mysql_num_rows($rsNAs);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsEvents = "SELECT eventid, eventname, eventtype, showonsite FROM events INNER JOIN eventtypes ON events.etid = eventtypes.etid ORDER BY showonsite, eventtype, eventid DESC";
$rsEvents = mysql_query($query_rsEvents, $connEABP2) or die(mysql_error());
$row_rsEvents = mysql_fetch_assoc($rsEvents);
$totalRows_rsEvents = mysql_num_rows($rsEvents);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocs = "SELECT docid, acronym, country, title FROM country INNER JOIN ((documents INNER JOIN doctypes ON documents.doctypeid = doctypes.doctypeid) INNER JOIN national ON documents.naid = national.naid) ON country.cid = national.cid ORDER BY country, doctype";
$rsDocs = mysql_query($query_rsDocs, $connEABP2) or die(mysql_error());
$row_rsDocs = mysql_fetch_assoc($rsDocs);
$totalRows_rsDocs = mysql_num_rows($rsDocs);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsBooks = "SELECT bookid, title FROM books2 ORDER BY cid DESC";
$rsBooks = mysql_query($query_rsBooks, $connEABP2) or die(mysql_error());
$row_rsBooks = mysql_fetch_assoc($rsBooks);
$totalRows_rsBooks = mysql_num_rows($rsBooks);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrgs = "SELECT id, orgname FROM allorgs ORDER BY orgname ASC";
$rsOrgs = mysql_query($query_rsOrgs, $connEABP2) or die(mysql_error());
$row_rsOrgs = mysql_fetch_assoc($rsOrgs);
$totalRows_rsOrgs = mysql_num_rows($rsOrgs);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>EABP membership admin</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
label {
	font-weight: bold;
}
.hideForum {
	display: none;
}
-->
</style>
</head>

<body>
<p><img src="../images/logo-20.gif" alt="" width="47" height="20" align="absmiddle" />  membership admin | <a href="logout.php">logout</a> | <a href="https://www.eabp.org" target="_blank">go to website</a></p>
<hr size="1" noshade="noshade" />
<p><a href="intranet/index.php">Intranet/eNewsletter</a></p>
<hr size="1" noshade="noshade" />
<h2>Members</h2>
<p><a href="applications/index.php">Online applications</a></p>
<p><a href="member-report.php">Analysis - tables and spreadsheets</a></p>
<p><a href="members-eabp.php">List of members registered through the EABP</a></p>
<p><a href="member-add.php">Add a member</a></p>
<form id="frmMemberSearch" name="frmMemberSearch" method="post" action="member-result.php">
<label>Edit a member:</label>
    <input name="q" type="text" class="input50" id="q" size="12" />
  <input name="button1" type="submit" class="btnGo" id="button1" value="Go" /> 
  (search by last name or first few letters of last name OR part of organisation name)
</form>
<p><a href="address-lists.php">Address lists</a></p>
<p><a href="members-noshow.php">Not showing but not resigned</a></p>
<hr size="1" noshade="noshade" />
<h2>Organisations / FORUM</h2>
<p><a href="all-orgs-add.php">Add an organisation</a></p>
<form id="frmOrgs" name="frmOrgs" method="get" action="all-orgs-edit.php">
  <label>Edit/Delete an organisation:</label>
  <select name="id" id="id">
    <?php
do {  
?>
    <option value="<?php echo $row_rsOrgs['id']?>"><?php echo utf8_encode($row_rsOrgs['orgname'])?></option>
    <?php
} while ($row_rsOrgs = mysql_fetch_assoc($rsOrgs));
  $rows = mysql_num_rows($rsOrgs);
  if($rows > 0) {
      mysql_data_seek($rsOrgs, 0);
	  $row_rsOrgs = mysql_fetch_assoc($rsOrgs);
  }
?>
  </select>
  <input name="button7" type="submit" class="btnGo" id="button7" value="Go" />
</form>
<hr size="1" noshade="noshade" />
<h2>National Associations</h2>
<p><a href="national-add.php">Add a national association</a></p>
<form id="frmNAs" name="frmNAs" method="get" action="national-edit.php">
  <label>Edit/Delete a national association:</label>
  <select name="naid" id="naid">
    <?php
do {  
?>
    <option value="<?php echo $row_rsNAs['naid']?>"><?php echo $row_rsNAs['acronym']?> - <?php echo $row_rsNAs['fullname']; ?></option>
    <?php
} while ($row_rsNAs = mysql_fetch_assoc($rsNAs));
  $rows = mysql_num_rows($rsNAs);
  if($rows > 0) {
      mysql_data_seek($rsNAs, 0);
	  $row_rsNAs = mysql_fetch_assoc($rsNAs);
  }
?>
  </select>
  <input name="button3" type="submit" class="btnGo" id="button3" value="Go" />
</form>
<hr size="1" noshade="noshade" />
<div class="hideForum">
<h2>Forum members</h2>
<p><a href="forum-add.php">Add a Forum member</a></p>
<form id="frmForum" name="frmForum" method="get" action="forum-edit.php">
  <label>Edit a Forum member:</label>

  <select name="id" class="input500" id="id">
    <?php
do {  
?>
    <option value="<?php echo $row_rsForums['id']?>"><?php echo $row_rsForums['orgname']?></option>
    <?php
} while ($row_rsForums = mysql_fetch_assoc($rsForums));
  $rows = mysql_num_rows($rsForums);
  if($rows > 0) {
      mysql_data_seek($rsForums, 0);
	  $row_rsForums = mysql_fetch_assoc($rsForums);
  }
?>
  </select>
  <input name="button2" type="submit" class="btnGo" id="button2" value="Go" />
</form>
<p>Forum meetings and attendance (to follow)</p>
<hr size="1" noshade="noshade" />
</div>
<h2>Documents <span class="normalText">(application forms, guidance etc)</span></h2>
<p><a href="applications/forms.php">Application forms</a> (blank)</p> 

<p><a href="160607 REGULATIONS and HOUSE RULES[1].doc">House Rules and Regulations</a></p>
<p><a href="doc-add.php">Upload a document</a></p>
<form action="doc-edit.php" method="get" name="frmDocs" id="frmDocs">
  <label>Edit/Delete a document:</label>

  <select name="docid" class="input500" id="docid">
    <?php
do {  
?>
    <option value="<?php echo $row_rsDocs['docid']?>"><?php echo $row_rsDocs['country']?> - <?php echo $row_rsDocs['acronym']?> - <?php echo $row_rsDocs['title']?></option>
    <?php
} while ($row_rsDocs = mysql_fetch_assoc($rsDocs));
  $rows = mysql_num_rows($rsDocs);
  if($rows > 0) {
      mysql_data_seek($rsDocs, 0);
	  $row_rsDocs = mysql_fetch_assoc($rsDocs);
  }
?>
  </select>
  <input name="button5" type="submit" class="btnGo" id="button5" value="Go" />
</form>
<hr size="1" noshade="noshade" />
<h2>News</h2>
<p><a href="news-add.php">Add an item</a></p>

<form id="frmNews" name="frmNews" method="get" action="news-edit.php">
  
  <label>Edit/Delete an item:</label>
  
  <select name="newsid" id="newsid">
    <?php
do {  
?>
    <option value="<?php echo $row_rsNews['newsid']?>"><?php echo $row_rsNews['title']?></option>
    <?php
} while ($row_rsNews = mysql_fetch_assoc($rsNews));
  $rows = mysql_num_rows($rsNews);
  if($rows > 0) {
      mysql_data_seek($rsNews, 0);
	  $row_rsNews = mysql_fetch_assoc($rsNews);
  }
?>
  </select>
  <input name="button" type="submit" class="btnGo" id="button" value="Go" />
</form>
<p><a href="newsletter.php">Newsletter</a></p>
<hr size="1" noshade="noshade" />
<h2>Events</h2>
<p><a href="event-add.php">Add an event</a></p>
<form action="event-edit.php" method="get" name="frmEvents" id="frmEvents">
  <label>Edit/Delete an event: </label>
  <select name="eventid" class="input500" id="eventid">
    <?php
do {  
?>
    <option value="<?php echo $row_rsEvents['eventid']?>"><?php if ($row_rsEvents['showonsite'] !=1) echo("pending review: "); ?><?php echo $row_rsEvents['eventtype']; ?> - <?php echo $row_rsEvents['eventname']?></option>
    <?php
} while ($row_rsEvents = mysql_fetch_assoc($rsEvents));
  $rows = mysql_num_rows($rsEvents);
  if($rows > 0) {
      mysql_data_seek($rsEvents, 0);
	  $row_rsEvents = mysql_fetch_assoc($rsEvents);
  }
?>
  </select>
  <input name="button4" type="submit" class="btnGo" id="button4" value="Go" />
</form>
<hr size="1" noshade="noshade" />
<h2>Publications</h2>
<p><a href="book-add.php">Add a book</a></p>

<form id="frmBooks" name="frmBooks" method="get" action="book-edit.php">
<label>Edit/delete a book:</label>
<select name="bookid" id="bookid">
  <?php
do {  
?>
  <option value="<?php echo $row_rsBooks['bookid']?>"><?php echo $row_rsBooks['title']?></option>
  <?php
} while ($row_rsBooks = mysql_fetch_assoc($rsBooks));
  $rows = mysql_num_rows($rsBooks);
  if($rows > 0) {
      mysql_data_seek($rsBooks, 0);
	  $row_rsBooks = mysql_fetch_assoc($rsBooks);
  }
?>
</select>
<input name="button6" type="submit" class="btnGo" id="button6" value="Go" />
</form>
<hr size="1" noshade="noshade" />
<h2>Links</h2>
<p>Add a link (to follow)</p>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsNews);

mysql_free_result($rsForums);

mysql_free_result($rsNAs);

mysql_free_result($rsEvents);

mysql_free_result($rsDocs);

mysql_free_result($rsBooks);

mysql_free_result($rsOrgs);
?>
