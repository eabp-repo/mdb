<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['delid'])) && ($_GET['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM modalitymembers WHERE modmemid=%s",
                       GetSQLValueString($_GET['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE membertrainingwork SET academicqualification=%s, bpqualification=%s, mainmodid=%s WHERE mid=%s",
                       GetSQLValueString($_POST['academicqualification'], "text"),
                       GetSQLValueString($_POST['bpqualification'], "text"),
                       GetSQLValueString($_POST['mainmodid'], "int"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmAdd")) {
  $insertSQL = sprintf("INSERT INTO modalitymembers (mid, modid) VALUES (%s, %s)",
                       GetSQLValueString($_POST['mid'], "int"),
                       GetSQLValueString($_POST['modid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT mid, firstname, lastname FROM member WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

$colname_rsModalities = "-1";
if (isset($_GET['mid'])) {
  $colname_rsModalities = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsModalities = sprintf("SELECT modmemid, modality FROM `modalitymembers` INNER JOIN modalities ON modalitymembers.modid = modalities.modid WHERE mid = %s", GetSQLValueString($colname_rsModalities, "int"));
$rsModalities = mysql_query($query_rsModalities, $connEABP2) or die(mysql_error());
$row_rsModalities = mysql_fetch_assoc($rsModalities);
$totalRows_rsModalities = mysql_num_rows($rsModalities);

$colname_rsTrainWork = "-1";
if (isset($_GET['mid'])) {
  $colname_rsTrainWork = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTrainWork = sprintf("SELECT * FROM membertrainingwork WHERE mid = %s", GetSQLValueString($colname_rsTrainWork, "int"));
$rsTrainWork = mysql_query($query_rsTrainWork, $connEABP2) or die(mysql_error());
$row_rsTrainWork = mysql_fetch_assoc($rsTrainWork);
$totalRows_rsTrainWork = mysql_num_rows($rsTrainWork);

if ($row_rsTrainWork['mainmodid'] !="") {
$colname_rsModalityList = $row_rsTrainWork['mainmodid'];
} else {
$colname_rsModalityList = "-1";
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsModalityList = sprintf("SELECT * FROM modalities WHERE modid <> %s ORDER BY modality", GetSQLValueString($colname_rsModalityList, "int"));
$rsModalityList = mysql_query($query_rsModalityList, $connEABP2) or die(mysql_error());
$row_rsModalityList = mysql_fetch_assoc($rsModalityList);
$totalRows_rsModalityList = mysql_num_rows($rsModalityList);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsModalityList2 = "SELECT * FROM modalities";
$rsModalityList2 = mysql_query($query_rsModalityList2, $connEABP2) or die(mysql_error());
$row_rsModalityList2 = mysql_fetch_assoc($rsModalityList2);
$totalRows_rsModalityList2 = mysql_num_rows($rsModalityList2);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP individual membership database update</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p class="leftAlign"><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" />  admin home</a></p>
<!-- Google Translate -->
<div id="google_translate_element"></div>
<p class="googleTranslateText">
  <script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    includedLanguages: 'bg,da,nl,en,fi,fr,de,el,iw,it,ja,no,pl,pt,ru,sr,sl,es,sv',
    gaTrack: true,
    gaId: 'UA-224881-50',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script>
  <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  The Google Translator is here to give you some help but it is not perfect!
  <!-- End -->
</p>
<p class="clear">EABP individual member update:</p>
<p><a href="member-edit-login.php?mid=<?php echo $row_rsMembers['mid']; ?>">Login details</a> | <a href="member-edit-general.php?mid=<?php echo $row_rsMembers['mid']; ?>">General</a> | <a href="member-edit-contact.php?mid=<?php echo $row_rsMembers['mid']; ?>">Contact details</a> | <a href="member-edit-membership.php?mid=<?php echo $row_rsMembers['mid']; ?>">Membership</a> | Training | <a href="member-edit-work.php?mid=<?php echo $row_rsMembers['mid']; ?>">Current Work</a> |  <a href="member-delete.php?mid=<?php echo $row_rsMembers['mid']; ?>">Delete this member</a> (you will be asked to confirm)</p>
<hr size="1" noshade="noshade" />
<h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
<h2>Training</h2>
<hr size="1" noshade="noshade" />
<?php if ($totalRows_rsTrainWork == 0) { // Show if recordset empty ?>
  <h2><a href="member-training-add.php?mid=<?php echo $row_rsMembers['mid']; ?>">To activate this section click here</a></h2>
  <?php } // Show if recordset empty ?>
<?php if ($totalRows_rsTrainWork > 0) { // Show if recordset not empty ?>
  <form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">
    <table align="left" cellpadding="3" cellspacing="0">
      <tr valign="baseline">
        <td nowrap="nowrap" align="right" valign="top">Pre-education:</td>
        <td><textarea name="academicqualification" rows="10" class="input500"><?php echo $row_rsTrainWork['academicqualification']; ?></textarea></td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right" valign="top">BP qualifications:</td>
        <td><textarea name="bpqualification" rows="10" class="input500"><?php echo $row_rsTrainWork['bpqualification']; ?></textarea></td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right" valign="top">Main modality: </td>
        <td><select name="mainmodid" id="mainmodid">
          <option value="" <?php if (!(strcmp("", $row_rsTrainWork['mainmodid']))) {echo "selected=\"selected\"";} ?>>Choose:</option>
          <?php
do {  
?>
          <option value="<?php echo $row_rsModalityList2['modid']?>"<?php if (!(strcmp($row_rsModalityList2['modid'], $row_rsTrainWork['mainmodid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsModalityList2['modality']?></option>
          <?php
} while ($row_rsModalityList2 = mysql_fetch_assoc($rsModalityList2));
  $rows = mysql_num_rows($rsModalityList2);
  if($rows > 0) {
      mysql_data_seek($rsModalityList2, 0);
	  $row_rsModalityList2 = mysql_fetch_assoc($rsModalityList2);
  }
?>
        </select></td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">&nbsp;</td>
        <td><input type="submit" class="btnAdd" value="Update" /></td>
      </tr>
    </table>
    <input type="hidden" name="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
    <input type="hidden" name="MM_update" value="form1" />
  </form>
  <?php } // Show if recordset not empty ?>
<?php if ($totalRows_rsTrainWork > 0) { // Show if recordset not empty ?>
  <div id="rightBar">
    <h3>Other Modalities</h3>
    <ul>
      <?php if ($totalRows_rsModalities > 0) { // Show if recordset not empty ?>
        <?php do { ?>
          <li><?php echo $row_rsModalities['modality']; ?> - <a href="member-edit-training.php?delid=<?php echo $row_rsModalities['modmemid']; ?>&amp;mid=<?php echo $_GET['mid']; ?>">delete</a></li>
          <?php } while ($row_rsModalities = mysql_fetch_assoc($rsModalities)); ?>
        <?php } // Show if recordset not empty ?>
    </ul>
    <?php if ($totalRows_rsModalities == 0) { // Show if recordset empty ?>
      <p>No modalities entered</p>
      <?php } // Show if recordset empty ?>
    <hr size="1" noshade="noshade" />
    <h3>Add modality</h3>
    <form id="frmAdd" name="frmAdd" method="POST" action="<?php echo $editFormAction; ?>">
      <select name="modid" id="modid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsModalityList['modid']?>"><?php echo $row_rsModalityList['modality']?></option>
        <?php
} while ($row_rsModalityList = mysql_fetch_assoc($rsModalityList));
  $rows = mysql_num_rows($rsModalityList);
  if($rows > 0) {
      mysql_data_seek($rsModalityList, 0);
	  $row_rsModalityList = mysql_fetch_assoc($rsModalityList);
  }
?>
      </select>
      <br />
      <input name="button" type="submit" class="btnGo" id="button" value="Add" />
      <input name="mid" type="hidden" id="mid" value="<?php echo $_GET['mid']; ?>" />
      <input type="hidden" name="MM_insert" value="frmAdd" />
    </form>
  </div>
  <?php } // Show if recordset not empty ?>
</body>
</html>
<?php
mysql_free_result($rsMembers);

mysql_free_result($rsModalities);

mysql_free_result($rsModalityList);

mysql_free_result($rsModalityList2);

mysql_free_result($rsTrainWork);
?>
