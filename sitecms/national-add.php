<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO national (cid, live, acronym, fullname, logofile, webaddress, president, presdetails, presemail, secretary, secdetails, secemail, treasurer, treasdetails, treasemail, rep, repdetails, repemail, secretariatname, secretariatemail, vicepres, vicepresemail, memsec, memsecemail, intername, interemail, otherdetails, username, password) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString(isset($_POST['live']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['acronym'], "text"),
                       GetSQLValueString($_POST['fullname'], "text"),
                       GetSQLValueString($_POST['logofile'], "text"),
                       GetSQLValueString($_POST['webaddress'], "text"),
                       GetSQLValueString($_POST['president'], "text"),
                       GetSQLValueString($_POST['presdetails'], "text"),
                       GetSQLValueString($_POST['presemail'], "text"),
                       GetSQLValueString($_POST['secretary'], "text"),
                       GetSQLValueString($_POST['secdetails'], "text"),
                       GetSQLValueString($_POST['secemail'], "text"),
                       GetSQLValueString($_POST['treasurer'], "text"),
                       GetSQLValueString($_POST['treasdetails'], "text"),
                       GetSQLValueString($_POST['treasemail'], "text"),
                       GetSQLValueString($_POST['rep'], "text"),
                       GetSQLValueString($_POST['repdetails'], "text"),
                       GetSQLValueString($_POST['repemail'], "text"),
                       GetSQLValueString($_POST['secretariatname'], "text"),
                       GetSQLValueString($_POST['secretariatemail'], "text"),
                       GetSQLValueString($_POST['vicepres'], "text"),
                       GetSQLValueString($_POST['vicepresemail'], "text"),
                       GetSQLValueString($_POST['memsec'], "text"),
                       GetSQLValueString($_POST['memsecemail'], "text"),
                       GetSQLValueString($_POST['intername'], "text"),
                       GetSQLValueString($_POST['interemail'], "text"),
                       GetSQLValueString($_POST['otherdetails'], "text"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['password'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT cid, country FROM country WHERE country <> '' ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = "SELECT naid, fullname FROM `national`";
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add national association</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>Add national association</h1>
<p>You can add the basic details now and edit later including making live.</p>

<form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">
  <table cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Live:</td>
      <td><input name="live" type="checkbox" class="input50" id="live" value="1" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Country:</td>
      <td><select name="cid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCountries['cid']?>"><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
      </select>      </td>
    </tr>
    <tr> </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Acronym:</td>
      <td><input type="text" name="acronym" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Full name:</td>
      <td><input name="fullname" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Logo file:</td>
      <td><input type="text" name="logofile" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Web address:</td>
      <td><input type="text" name="webaddress" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">President:</td>
      <td><input type="text" name="president" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">President details:</td>
      <td><input name="presdetails" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">President email:</td>
      <td><input type="text" name="presemail" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretary:</td>
      <td><input type="text" name="secretary" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretary details:</td>
      <td><input name="secdetails" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretary email:</td>
      <td><input type="text" name="secemail" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Treasurer:</td>
      <td><input type="text" name="treasurer" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Treasurer details:</td>
      <td><input name="treasdetails" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Treasurer email:</td>
      <td><input type="text" name="treasemail" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Rep:</td>
      <td><input type="text" name="rep" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Rep details:</td>
      <td><input name="repdetails" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Rep email:</td>
      <td><input type="text" name="repemail" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretariat name:</td>
      <td><input type="text" name="secretariatname" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretariat email:</td>
      <td><input type="text" name="secretariatemail" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Vice President name:</td>
      <td><input name="vicepres" type="text" id="vicepres" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Vice President email:</td>
      <td><input name="vicepresemail" type="text" id="vicepresemail" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Membership Secretary name:</td>
      <td><input name="memsec" type="text" id="memsec" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Membership Secretary email:</td>
      <td><input name="memsecemail" type="text" id="memsecemail" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">International name:</td>
      <td><input type="text" name="intername" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">International email:</td>
      <td><input type="text" name="interemail" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Other details:</td>
      <td><textarea name="otherdetails" cols="32" rows="3"></textarea></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Login username:</td>
      <td><input type="text" name="username" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Login password:</td>
      <td><input name="password" type="text" value="eabppassword" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><input name="processed" type="hidden" id="processed" value="1" /></td>
      <td><input type="submit" class="btnAdd" value="Add" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsCountries);

mysql_free_result($rsNA);
?>
