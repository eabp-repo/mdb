<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM allorgs WHERE id=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsOrg = "-1";
if (isset($_GET['id'])) {
  $colname_rsOrg = $_GET['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrg = sprintf("SELECT id, orgname, city FROM allorgs WHERE id = %s", GetSQLValueString($colname_rsOrg, "int"));
$rsOrg = mysql_query($query_rsOrg, $connEABP2) or die(mysql_error());
$row_rsOrg = mysql_fetch_assoc($rsOrg);
$totalRows_rsOrg = mysql_num_rows($rsOrg);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Delete org</title>
</head>

<body>
<p>Delete organisation/Forum member from database?</p>
<p><strong><?php echo $row_rsOrg['orgname']; ?></strong><br />
<?php echo $row_rsOrg['city']; ?></p>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsOrg['id']; ?>" />
  <input type="submit" name="btnDelete" id="btnDelete" value="Yes, delete" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsOrg);
?>
