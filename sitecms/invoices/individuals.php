<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
// sort options
if(isset($_GET['so'])) {
 $invoiceorder = $_GET['so'];
} else {
$invoiceorder = "paymentdate";	
}
if(isset($_GET['ad'])) {
	$ascdesc = $_GET['ad'];
} else {
	$ascdesc = "DESC";
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmPayment")) {
  $insertSQL = sprintf("INSERT INTO payments (mid, sessionid, amountdue, payment, paymentdate, received) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['mid'], "int"),
                       GetSQLValueString($_POST['sessionid'], "text"),
                       GetSQLValueString($_POST['amountdue'], "double"),
                       GetSQLValueString($_POST['payment'], "double"),
                       GetSQLValueString($_POST['paymentdate'], "date"),
                       GetSQLValueString($_POST['received'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "individuals.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPaymentsReceived = "SELECT DISTINCTROW paymentid, member.mid, firstname, lastname, foryear, amountdue, payment, paymentdate, received FROM payments INNER JOIN member ON member.mid = payments.mid WHERE YEAR(paymentdate) = YEAR(CURDATE()) ORDER BY ".$invoiceorder." ".$ascdesc."";
$rsPaymentsReceived = mysql_query($query_rsPaymentsReceived, $connEABP2) or die(mysql_error());
$row_rsPaymentsReceived = mysql_fetch_assoc($rsPaymentsReceived);
$totalRows_rsPaymentsReceived = mysql_num_rows($rsPaymentsReceived);

$colname_rsMembers = "-1";
if (isset($_GET['q'])) {
  $colname_rsMembers = $_GET['q'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT mid, cid, firstname, lastname, fee, type, email FROM member INNER JOIN type ON member.tid = type.tid WHERE (lastname LIKE %s OR firstname LIKE %s)  AND `email` <> ''", GetSQLValueString("%" . $colname_rsMembers . "%", "text"),GetSQLValueString("%" . $colname_rsMembers . "%", "text"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

$colname_rsPayment = "-1";
if (isset($_GET['q'])) {
  $colname_rsPayment = $_GET['q'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPayment = sprintf("SELECT mid, firstname, lastname, fee, type, email FROM member INNER JOIN type ON member.tid = type.tid WHERE lastname LIKE %s  AND `email` <> ''", GetSQLValueString("%" . $colname_rsPayment . "%", "text"));
$rsPayment = mysql_query($query_rsPayment, $connEABP2) or die(mysql_error());
$row_rsPayment = mysql_fetch_assoc($rsPayment);
$totalRows_rsPayment = mysql_num_rows($rsPayment);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Invoicing - Individual members</title>
<style type="text/css">
tr:nth-child(even){
	background-color:#F5F5F5;
}
tr:nth-child(odd){
	background-color:#FFFFFF;
}
.smaller {
	font-size: 75%;
	font-weight: normal;
}
</style>
</head>

<body>
<p><a href="index.php">Invoices home</a></p>
<h1>Individual members - invoicing and manual payments - <?php echo date('Y') ; ?></h1>
<p><a href="individuals-archive.php">Archive &gt;</a></p>
<form id="frmSearch" name="frmSearch" method="get" action="">
  Find member
  <input name="q" type="text" id="q" value="enter last name" onclick="this.value='';" />
  <input type="submit" value="Search" />
</form>
<?php if ($totalRows_rsMembers > 0 && isset($_GET['q'])) { // Show if recordset empty ?>
<hr />
<h2>Email Invoice</h2>
<p>Search results for last name including <strong><?php echo $_GET['q']; ?></strong></p>

<?php do { ?>
  <p><?php if ($row_rsMembers['cid'] !=16) { ?>
  <a href="mailer.php?mid=<?php echo $row_rsMembers['mid']; ?>">Click here</a>
  <?php } else { ?>
  <a href="mailer-special.php?mid=<?php echo $row_rsMembers['mid']; ?>">Click here</a>
  <?php } ?>
   to send an email invoice for 2013 to <strong><?php echo $row_rsMembers['firstname']; ?> <?php echo $row_rsMembers['lastname']; ?> </strong>(<?php echo $row_rsMembers['type']; ?> - &#8364;<?php if($row_rsMembers['cid'] !=16) echo $row_rsMembers['fee']; else echo(30.00); ?>) using <em><?php echo $row_rsMembers['email']; ?></em></p>
  <hr size="1" noshade="noshade" />
  <p><?php if ($row_rsMembers['cid'] !=16) { ?>
  <a href="mailer-reminder.php?mid=<?php echo $row_rsMembers['mid']; ?>">Click here</a>
  <?php } else { ?>
  <a href="mailer-reminder-special.php?mid=<?php echo $row_rsMembers['mid']; ?>">Click here</a>
  <?php } ?>  
   to send an invoice REMINDER for 2013 to <strong><?php echo $row_rsMembers['firstname']; ?> <?php echo $row_rsMembers['lastname']; ?> </strong>(<?php echo $row_rsMembers['type']; ?> - &#8364;<?php if($row_rsMembers['cid'] !=16) echo $row_rsMembers['fee']; else echo(30.00); ?>) using <em><?php echo $row_rsMembers['email']; ?></em></p>
  <?php } while ($row_rsMembers = mysql_fetch_assoc($rsMembers)); ?>
<?php } ?>
<?php if ($totalRows_rsMembers == 0 && isset($_GET['q'])) { // Show if recordset empty ?>
<p>No match found.</p>
  <?php } // Show if recordset empty ?>
  <?php if ($totalRows_rsPayment > 0) { // Show if recordset not empty ?>
<hr />
<h2>Manual Payments</h2>
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <?php do { ?>
      <tr>
        <td><?php echo $row_rsPayment['firstname']; ?> <?php echo $row_rsPayment['lastname']; ?> (<?php echo $row_rsPayment['type']; ?>)</td>
        <td><form id="frmPayment" name="frmPayment" method="POST" action="<?php echo $editFormAction; ?>">
          <input name="mid" type="hidden" id="mid" value="<?php echo $row_rsPayment['mid']; ?>" />
          <input name="received" type="hidden" id="received" value="1" />
          <label>Payment date:
            <input name="paymentdate" type="text" id="paymentdate" value="<?php echo date("Y-m-d"); ?>" size="12" />
            </label>
          <label>Amount due:
            <input name="amountdue" type="text" id="amountdue" value="<?php echo $row_rsPayment['fee']; ?>" size="8" />
            Amount paid: 
            <input name="payment" type="text" id="payment" value="<?php echo $row_rsPayment['fee']; ?>" size="12" />
            </label>
          <input type="submit" name="btnPay" id="btnPay" value="Record Payment" />
          <input type="hidden" name="MM_insert" value="frmPayment" />
          <input name="sessionid" type="hidden" id="sessionid" value="manualpayment-<?php echo $row_rsPayment['mid']; ?>" />
        </form></td>
      </tr>
      <?php } while ($row_rsPayment = mysql_fetch_assoc($rsPayment)); ?>
  </table>
  <?php } // Show if recordset not empty ?>
  <hr />  
<?php if ($totalRows_rsPaymentsReceived > 0) { // Show if recordset not empty ?>
  <h3>Members who have responded to emailed invoice<br />
    <span class="smaller">(payment confirmed via PayPal or manually, above):</span></h3>
    <table width="80%" border="0" cellpadding="5" cellspacing="0">
    <tr>
      <th><a href="individuals.php?so=lastname" title="Sort alphabetically">Name</a></th>
      <th>
     <?php if (isset($_GET['so']) || isset($_GET['ad'])) { ?> 
     <?php if ($_GET['ad'] =="DESC") {?>
      <a href="individuals.php?so=<?php echo $_GET['so']; ?>&amp;ad=ASC">reverse order</a>
      <?php } else if(!isset($_GET['ad'])) { ?>
      <a href="individuals.php?so=<?php echo $_GET['so']; ?>&amp;ad=ASC">reverse order</a>
      <?php }
	  }?>
      </th>
      <th><a href="individuals.php?so=paymentdate" title="Sort by date">Year</a></th>
      <th>Amount due</th>
      <th>Amount paid</th>
      <th>Date paid</th>
      <th>Payment received</th>
    </tr>
    <?php do { ?>
      <tr>
        <td colspan="2"><a href="payment-edit.php?paymentid=<?php echo $row_rsPaymentsReceived['paymentid']; ?>"><?php echo $row_rsPaymentsReceived['firstname']; ?> <strong><?php echo $row_rsPaymentsReceived['lastname']; ?></strong></a></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['foryear']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['amountdue']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['payment']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['paymentdate']; ?></td>
        <td align="center"><?php if ($row_rsPaymentsReceived['received'] == 1) echo("YES"); else echo("NO"); ?></td>
      </tr>
      <?php } while ($row_rsPaymentsReceived = mysql_fetch_assoc($rsPaymentsReceived)); ?>
      <tr>
      <th><a href="individuals.php?so=lastname">Name</a></th>
      <th>
     <?php if (isset($_GET['so']) || isset($_GET['ad'])) { ?> 
      <a href="individuals.php?so=<?php echo $_GET['so']; ?>&amp;ad=ASC">reverse order</a>
      <?php }?>
      </th>
      <th><a href="individuals.php?so=paymentdate">Year</a> (Recent first)</th>
      <th>Amount due</th>
      <th>Amount paid</th>
      <th>Date paid</th>
      <th>Payment received</th>
    </tr>
  </table>
  <?php } // Show if recordset not empty ?>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsPaymentsReceived);

mysql_free_result($rsMembers);

mysql_free_result($rsPayment);
?>
