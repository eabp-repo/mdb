<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmSend")) {
  $updateSQL = sprintf("UPDATE invoices SET sent=%s, sentdate=%s WHERE invoiceid=%s",
                       GetSQLValueString($_POST['sent'], "int"),
                       GetSQLValueString($_POST['sentdate'], "date"),
                       GetSQLValueString($_POST['invoiceid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsInvoice = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsInvoice = $_GET['invoiceid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoice = sprintf("SELECT invoiceid, invoicedate, orgname, invoiceto, invoiceemail, address, country, city, type, sent, sentdate FROM invoices INNER JOIN forum ON invoices.id = forum.id WHERE invoiceid = %s", GetSQLValueString($colname_rsInvoice, "int"));
$rsInvoice = mysql_query($query_rsInvoice, $connEABP2) or die(mysql_error());
$row_rsInvoice = mysql_fetch_assoc($rsInvoice);
$totalRows_rsInvoice = mysql_num_rows($rsInvoice);

$colname_rsDetails = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsDetails = $_GET['invoiceid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDetails = sprintf("SELECT amount, `description`, notes FROM invoicedetails WHERE invoiceid = %s", GetSQLValueString($colname_rsDetails, "int"));
$rsDetails = mysql_query($query_rsDetails, $connEABP2) or die(mysql_error());
$row_rsDetails = mysql_fetch_assoc($rsDetails);
$totalRows_rsDetails = mysql_num_rows($rsDetails);

$colname_rsTotal = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsTotal = $_GET['invoiceid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTotal = sprintf("SELECT Sum(amount) AS SumOfamount FROM invoicedetails WHERE invoiceid = %s GROUP BY invoicedetails.invoiceid", GetSQLValueString($colname_rsTotal, "int"));
$rsTotal = mysql_query($query_rsTotal, $connEABP2) or die(mysql_error());
$row_rsTotal = mysql_fetch_assoc($rsTotal);
$totalRows_rsTotal = mysql_num_rows($rsTotal);
?>
<?php require_once("../../webassist/email/mail_php.php"); ?>
<?php require_once("../../webassist/email/mailformatting_php.php"); ?>
<?php
if (!isset($_SESSION))session_start();
if ((($_SERVER["REQUEST_METHOD"] == "POST") && (isset($_SERVER["HTTP_REFERER"]) && strpos(urldecode($_SERVER["HTTP_REFERER"]), urldecode($_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"])) > 0) && isset($_POST)))     {
  //WA Universal Email object="mail"
  set_time_limit(0);
  $EmailRef = "waue_forum-invoice-preview_1";
  $BurstSize = 200;
  $BurstTime = 1;
  $WaitTime = 1;
  $GoToPage = "forum-invoice-sent.php";
  $RecipArray = array();
  $StartBurst = time();
  $LoopCount = 0;
  $TotalEmails = 0;
  $RecipIndex = 0;
  // build up recipients array
  $CurIndex = sizeof($RecipArray);
  $RecipArray[$CurIndex] = array();
  $RecipArray[$CurIndex ][] = "".$row_rsInvoice['invoiceemail']  ."";
  $TotalEmails += sizeof($RecipArray[$CurIndex]);
  $RealWait = ($WaitTime<0.25)?0.25:($WaitTime+0.1);
  $TimeTracker = Array();
  $TotalBursts = floor($TotalEmails/$BurstSize);
  $AfterBursts = $TotalEmails % $BurstSize;
  $TimeRemaining = ($TotalBursts * $BurstTime) + ($AfterBursts*$RealWait);
  if ($TimeRemaining < ($TotalEmails*$RealWait) )  {
    $TimeRemaining = $TotalEmails*$RealWait;
  }
  $_SESSION[$EmailRef."_Total"] = $TotalEmails;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = $TimeRemaining;
  while ($RecipIndex < sizeof($RecipArray))  {
    $EnteredValue = is_string($RecipArray[$RecipIndex][0]);
    $CurIndex = 0;
    while (($EnteredValue && $CurIndex < sizeof($RecipArray[$RecipIndex])) || (!$EnteredValue && $RecipArray[$RecipIndex][0])) {
      $starttime = microtime_float();
      if ($EnteredValue)  {
        $RecipientEmail = $RecipArray[$RecipIndex][$CurIndex];
      }  else  {
        $RecipientEmail = $RecipArray[$RecipIndex][0][$RecipArray[$RecipIndex][2]];
      }
      $EmailsRemaining = ($TotalEmails- $LoopCount);
      $BurstsRemaining = ceil(($EmailsRemaining-$AfterBursts)/$BurstSize);
      $IntoBurst = ($EmailsRemaining-$AfterBursts) % $BurstSize;
      if ($AfterBursts<$EmailsRemaining) $IntoBurst = 0;
      $TimeRemaining = ($BurstsRemaining * $BurstTime * 60) + ((($AfterBursts<$EmailsRemaining)?$AfterBursts:$EmailsRemaining)*$RealWait) - (($AfterBursts>$EmailsRemaining)?0:($IntoBurst*$RealWait));
      if ($TimeRemaining < ($EmailsRemaining*$RealWait) )  {
        $TimeRemaining = $EmailsRemaining*$RealWait;
      }
      $CurIndex ++;
      $LoopCount ++;
      session_commit();
      session_start();
      $_SESSION[$EmailRef."_Index"] = $LoopCount;
      $_SESSION[$EmailRef."_Remaining"] = round($TimeRemaining);
      session_commit();
      wa_sleep($WaitTime);
      include("../../webassist/email/waue_forum-invoice-preview_1.php");
      $endtime = microtime_float();
      $TimeTracker[] =$endtime - $starttime;
      $RealWait = array_sum($TimeTracker)/sizeof($TimeTracker);
      if ($LoopCount % $BurstSize == 0 && $CurIndex < sizeof($RecipArray[$RecipIndex]))  {
        $TimePassed = (time() - $StartBurst);
        if ($TimePassed < ($BurstTime*60))  {
          $WaitBurst = ($BurstTime*60) -$TimePassed;
          wa_sleep($WaitBurst);
        }
        else  {
          $TimeRemaining = ($TotalEmails- $LoopCount)*$RealWait;
        }
        $StartBurst = time();
      }
      if (!$EnteredValue)  {
        $RecipArray[$RecipIndex][0] =  mysql_fetch_assoc($RecipArray[$RecipIndex][1]);
      }
    }
    $RecipIndex ++;
  }
  $_SESSION[$EmailRef."_Total"] = 0;
  $_SESSION[$EmailRef."_Index"] = 0;
  $_SESSION[$EmailRef."_Remaining"] = 0;
  session_commit();
  session_start();
  if ($GoToPage!="")     {
    header("Location: ".$GoToPage);
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Preview Invoice</title>
<style type="text/css">
#fees {
	font-size: 12px;
}
#fees ul {
	margin: 0px;
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 0px;
	padding-left: 15px;
}
body {
	font-family: Arial, Helvetica, sans-serif;
}
</style>
</head>

<body>
<p> <a href="forum.php">Forum Invoices</a></p>
<table width="600" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td bgcolor="#E6E6E6">Invoice number: FF-<?php echo $row_rsInvoice['invoiceid']; ?>-2013</td>
    <td width="200" align="right" bgcolor="#E6E6E6">Invoice date: <?php echo  date("d M Y",strtotime($row_rsInvoice['invoicedate'])); ?></td>
  </tr>
  <tr>
    <td valign="top">For:</td>
    <td align="right" valign="top">Attention of: </td>
  </tr>
  <tr>
    <td valign="top"><strong><?php echo $row_rsInvoice['orgname']; ?><br />
    </strong><?php echo $row_rsInvoice['address']; ?><br />
    <?php echo $row_rsInvoice['city']; ?>    <br />
    <?php echo $row_rsInvoice['country']; ?>    </td>
    <td align="right" valign="top"><?php echo $row_rsInvoice['invoiceto']; ?></td>
  </tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="3">
  <tr>
  <td bgcolor="#E6E6E6">Details</td>
  <td bgcolor="#E6E6E6">&nbsp;</td></tr>
    <?php do { ?>
  <tr>

      <td><?php echo $row_rsDetails['description']; ?></td>
      <td align="right"><?php echo $row_rsDetails['amount']; ?></td>

  </tr>
      <?php } while ($row_rsDetails = mysql_fetch_assoc($rsDetails)); ?>  
  <tr>
    <td align="right">Total: </td>
    <td align="right">&#8364;<?php echo $row_rsTotal['SumOfamount']; ?></td>
  </tr>
</table>
<hr align="left" width="600" size="1" noshade="noshade" />
<div id="fees">
  <?php if ($row_rsInvoice['type'] == "Training Institute") { ?>
  <strong>Fee Structure for Training Institutes:</strong>
  <ul>
  <li>For       up to 25 trainees/members - &euro;80 </li>
  <li>For       26-60 trainees/members - &euro;180 </li>
  <li>For       61-100 trainees/members - &euro;270 </li>
  <li>For       more than 100 trainees/members - &euro;360</li>
</ul>
<?php } ?>
<?php if ($row_rsInvoice['type'] == "Professional Association") { ?>
<strong>Fee Structure for Professional Associations:</strong>
<ul>
  <li>Up       to 25 trainees - &euro;80 </li>
  <li>From       25 - 100 trainees - &euro;120 </li>
  <li>More       than 100 - &euro;160 </li>
</ul>
<?php } ?>
</div>
<hr align="left" width="600" size="1" noshade="noshade" />
<?php if ($totalRows_rsDetails == 0) { // Show if recordset empty ?>
  <p>This invoice has no items so cannot be sent :)</p>
  <?php } // Show if recordset empty ?>
<?php if ($totalRows_rsDetails > 0) { // Show if recordset not empty ?>
  <?php if ($row_rsInvoice['sent'] !=1) { ?>
  <p>Email this invoice to <?php echo $row_rsInvoice['invoiceto']; ?></p>
  <form id="frmSend" name="frmSend" method="POST" action="<?php echo $editFormAction; ?>">
    <input name="sent" type="hidden" id="sent" value="1" />
    <input name="invoiceid" type="hidden" id="invoiceid" value="<?php echo $row_rsInvoice['invoiceid']; ?>" />
  <input type="submit" name="btnSend" id="btnSend" value="Send" />
  <input type="hidden" name="MM_update" value="frmSend" />
  <input name="sentdate" type="hidden" id="sentdate" value="<?php echo date('Y-m-d'); ?>" />
  </form>
  <?php } else { ?>
  <p>Invoice emailed on <?php echo date("d M Y",strtotime($row_rsInvoice['sentdate'])); ?></p>
  <?php } ?>
  <?php } // Show if recordset not empty ?>
</body>
</html>
<?php
mysql_free_result($rsInvoice);

mysql_free_result($rsDetails);

mysql_free_result($rsTotal);
?>
