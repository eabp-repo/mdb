<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmForum")) {
  $insertSQL = sprintf("INSERT INTO invoices (id, invoicedate, forummember) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['id'], "int"),
                       GetSQLValueString($_POST['invoicedate'], "date"),
                       GetSQLValueString($_POST['forummember'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "forum-invoice-details.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsForum = "SELECT id, orgname FROM forum WHERE showonsite = 1 ORDER BY orgname ASC";
$rsForum = mysql_query($query_rsForum, $connEABP2) or die(mysql_error());
$row_rsForum = mysql_fetch_assoc($rsForum);
$totalRows_rsForum = mysql_num_rows($rsForum);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoices = "SELECT invoices.invoiceid, invoicedate, orgname, sent, received, paymentid FROM (invoices LEFT JOIN forum ON invoices.id = forum.id) LEFT JOIN paymentsforum ON invoices.invoiceid = paymentsforum.invoiceid WHERE forummember = 1 AND YEAR(invoicedate) = YEAR(CURDATE()) ORDER BY orgname";
$rsInvoices = mysql_query($query_rsInvoices, $connEABP2) or die(mysql_error());
$row_rsInvoices = mysql_fetch_assoc($rsInvoices);
$totalRows_rsInvoices = mysql_num_rows($rsInvoices);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>FORUM invoices</title>
<style type="text/css">
#tblInvoices tr:nth-child(even){
	background-color:#F5F5F5;
}
#tblInvoices tr:nth-child(odd){
	background-color:#FFFFFF;
}
</style>
</head>

<body>
<p><a href="index.php">Invoices home</a></p>
<h2>FORUM fees invoices - 
<?php echo date('Y') ; ?></h2>
<p><a href="forum-archive.php">Archive &gt;</a></p>
<form id="frmForum" name="frmForum" method="POST" action="<?php echo $editFormAction; ?>">
  Create new invoice: for:
  <select name="id" id="id">
    <?php
do {  
?>
    <option value="<?php echo $row_rsForum['id']?>"><?php echo $row_rsForum['orgname']?></option>
    <?php
} while ($row_rsForum = mysql_fetch_assoc($rsForum));
  $rows = mysql_num_rows($rsForum);
  if($rows > 0) {
      mysql_data_seek($rsForum, 0);
	  $row_rsForum = mysql_fetch_assoc($rsForum);
  }
?>
  </select>
  
  Date
  <input name="invoicedate" type="text" id="invoicedate" value="<?php echo date('Y-m-d'); ?>" size="12" />
  <input type="submit" name="btnGo" id="btnGo" value="Go" />
  <input name="forummember" type="hidden" id="forummember" value="1" />
  <input type="hidden" name="MM_insert" value="frmForum" />
</form>
<hr />
<?php if ($totalRows_rsInvoices > 0) { // Show if recordset not empty ?>
  <table border="0" cellpadding="3" cellspacing="0" id="tblInvoices">
    <tr>
      <th>Invoice No:</th>
      <th>&nbsp;</th>
      <th>Invoice Date</th>
      <th width="100">Sent</th>
      <th>Payment Confirmed</th>
    </tr>
    <?php do { ?>
      <tr>
        <td align="center"><a href="forum-invoice-details.php?invoiceid=<?php echo $row_rsInvoices['invoiceid']; ?>" title="Edit/Delete/Preview/Send"><?php echo $row_rsInvoices['invoiceid']; ?></a></td>
        <td><?php echo $row_rsInvoices['orgname']; ?></td>
        <td><?php echo $row_rsInvoices['invoicedate']; ?></td>
        <td align="center"><?php if ($row_rsInvoices['sent'] !=1) { ?>
          NO
        <?php } else { ?>YES<?php } ?></td>
        <td align="left" nowrap="nowrap"><?php if($row_rsInvoices['paymentid'] !="") {  ?><a href="forum-payment-edit.php?paymentid=<?php echo $row_rsInvoices['paymentid']; ?>" title="Edit payment"><?php if ($row_rsInvoices['received'] ==1) echo("YES"); else echo("Read - no payment. Click to update"); ?></a><?php } ?></td>
      </tr>
      <?php } while ($row_rsInvoices = mysql_fetch_assoc($rsInvoices)); ?>
  </table>
  <?php } // Show if recordset not empty ?>
  <?php if ($totalRows_rsInvoices == 0) { // Show if recordset empty ?>
  <p>No invoices created for <?php echo date('Y') ; ?>.</p>
  <?php } // Show if recordset empty ?>
</body>
</html>
<?php
mysql_free_result($rsForum);

mysql_free_result($rsInvoices);
?>
