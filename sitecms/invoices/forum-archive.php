<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoices = "SELECT invoices.invoiceid, invoicedate, orgname, sent, received, paymentid FROM (invoices LEFT JOIN forum ON invoices.id = forum.id) LEFT JOIN paymentsforum ON invoices.invoiceid = paymentsforum.invoiceid WHERE forummember = 1 ORDER BY orgname";
$rsInvoices = mysql_query($query_rsInvoices, $connEABP2) or die(mysql_error());
$row_rsInvoices = mysql_fetch_assoc($rsInvoices);
$totalRows_rsInvoices = mysql_num_rows($rsInvoices);mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoices = "SELECT invoices.invoiceid, invoicedate, orgname, sent, received, paymentid FROM (invoices LEFT JOIN forum ON invoices.id = forum.id) LEFT JOIN paymentsforum ON invoices.invoiceid = paymentsforum.invoiceid WHERE forummember = 1 AND YEAR(invoicedate) < YEAR(CURDATE()) ORDER BY orgname";
$rsInvoices = mysql_query($query_rsInvoices, $connEABP2) or die(mysql_error());
$row_rsInvoices = mysql_fetch_assoc($rsInvoices);
$totalRows_rsInvoices = mysql_num_rows($rsInvoices);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>FORUM invoices archive</title>
<style type="text/css">
#tblInvoices tr:nth-child(even){
	background-color:#F5F5F5;
}
#tblInvoices tr:nth-child(odd){
	background-color:#FFFFFF;
}
</style>
</head>

<body>
<p><a href="index.php">Invoices home</a></p>
<h2>FORUM fees invoices - archive
</h2>
<hr />
<table border="0" cellpadding="3" cellspacing="0" id="tblInvoices">
  <tr>
    <th>Invoice No:</th>
    <th>&nbsp;</th>
    <th>Invoice Date</th>
    <th width="100">Sent</th>
    <th>Payment Confirmed</th>
  </tr>
  <?php do { ?>
    <tr>
      <td align="center"><a href="forum-invoice-details.php?invoiceid=<?php echo $row_rsInvoices['invoiceid']; ?>" title="Edit/Delete/Preview/Send"><?php echo $row_rsInvoices['invoiceid']; ?></a></td>
      <td><?php echo $row_rsInvoices['orgname']; ?></td>
      <td><?php echo $row_rsInvoices['invoicedate']; ?></td>
      <td align="center"><?php if ($row_rsInvoices['sent'] !=1) { ?>
        NO
      <?php } else { ?>YES<?php } ?></td>
      <td align="left" nowrap="nowrap"><?php if($row_rsInvoices['paymentid'] !="") {  ?><a href="forum-payment-edit.php?paymentid=<?php echo $row_rsInvoices['paymentid']; ?>" title="Edit payment"><?php if ($row_rsInvoices['received'] ==1) echo("YES"); else echo("Read - no payment. Click to update"); ?></a><?php } ?></td>
    </tr>
    <?php } while ($row_rsInvoices = mysql_fetch_assoc($rsInvoices)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($rsInvoices);
?>
