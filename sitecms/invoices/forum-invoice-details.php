<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmDetails")) {
  $insertSQL = sprintf("INSERT INTO invoicedetails (invoiceid, amount, `description`, notes) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['invoiceid'], "int"),
                       GetSQLValueString($_POST['amount'], "double"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['notes'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmEdit")) {
  $updateSQL = sprintf("UPDATE invoicedetails SET amount=%s, `description`=%s, notes=%s WHERE detailid=%s",
                       GetSQLValueString($_POST['amount'], "double"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString($_POST['detailid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM invoicedetails WHERE detailid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());
}
$colname_rsInvoice = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsInvoice = $_GET['invoiceid']; 


mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoice = sprintf("SELECT invoiceid, forum.id, invoicedate, invoiceto, invoiceemail, orgname, forummember, contact, email, sent, sentdate FROM invoices LEFT JOIN forum ON invoices.id = forum.id WHERE invoiceid = %s", GetSQLValueString($colname_rsInvoice, "int"));
$rsInvoice = mysql_query($query_rsInvoice, $connEABP2) or die(mysql_error());
$row_rsInvoice = mysql_fetch_assoc($rsInvoice);
$totalRows_rsInvoice = mysql_num_rows($rsInvoice);

} else {

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoice = "SELECT invoiceid, forum.id, invoicedate, invoiceto, invoiceemail, orgname, forummember, contact, email, sent, sentdate FROM invoices LEFT JOIN forum ON invoices.id = forum.id ORDER BY invoiceid DESC Limit 0,1";
$rsInvoice = mysql_query($query_rsInvoice, $connEABP2) or die(mysql_error());
$row_rsInvoice = mysql_fetch_assoc($rsInvoice);
$totalRows_rsInvoice = mysql_num_rows($rsInvoice);

}

//$colname_rsDetails = $row_rsInvoice['invoiceid'];
$colname_rsDetails = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsDetails = $_GET['invoiceid'];
}else {
$colname_rsDetails = $row_rsInvoice['invoiceid'];  
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDetails = sprintf("SELECT * FROM invoicedetails WHERE invoiceid = %s", GetSQLValueString($colname_rsDetails, "int"));
$rsDetails = mysql_query($query_rsDetails, $connEABP2) or die(mysql_error());
$row_rsDetails = mysql_fetch_assoc($rsDetails);
$totalRows_rsDetails = mysql_num_rows($rsDetails);

$colname_rsList = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsList = $_GET['invoiceid'];
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsList = sprintf("SELECT invoiceid, invoicedate, orgname FROM invoices INNER JOIN forum ON invoices.id = forum.id WHERE invoiceid <> %s AND invoices.forummember = 1 ORDER BY invoiceid DESC", GetSQLValueString($colname_rsList, "int"));
$rsList = mysql_query($query_rsList, $connEABP2) or die(mysql_error());
$row_rsList = mysql_fetch_assoc($rsList);
$totalRows_rsList = mysql_num_rows($rsIList);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Forum Invoice details</title>
<style type="text/css">
h3 {
	font-weight: normal;
}
.redText {
	color: #F00;
}
</style>
</head>

<body>
<p><a href="index.php">Invoice home</a> | FORUM Invoice: FF-<?php echo $row_rsInvoice['invoiceid']; ?>-2013</p>
<form id="frmList" name="frmList" method="get" action="forum-invoice-details.php">
  Edit another invoice: 
    <select name="invoiceid" id="invoiceid">
      <?php
do {  
?>
      <option value="<?php echo $row_rsList['invoiceid']?>">Invoice no:<?php echo $row_rsList['invoiceid']?> - <?php echo $row_rsList['orgname']?> - <?php echo $row_rsList['invoicedate']?></option>
      <?php
} while ($row_rsList = mysql_fetch_assoc($rsList));
  $rows = mysql_num_rows($rsList);
  if($rows > 0) {
      mysql_data_seek($rsList, 0);
	  $row_rsList = mysql_fetch_assoc($rsList);
  }
?>
    </select>
    <input type="submit" name="btnGoList" id="btnGoList" value="Go" />
</form>
<hr />
<table border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td align="right">Invoice number:</td>
    <td bgcolor="#EBEBEB"><strong>FF-<?php echo $row_rsInvoice['invoiceid']; ?>-2013</strong></td>
    <td rowspan="4"><img src="../../images/spacer.gif" width="20" height="1" alt="" /></td>
    <td width="200" rowspan="4">If the membership details are incorrect <a href="../forum-edit.php?id=<?php echo $row_rsInvoice['id']; ?>">change them now</a> and then return to edit this invoice</td>
  </tr>
  <tr>
    <td align="right">Invoice date:</td>
    <td bgcolor="#EBEBEB"><strong><?php echo $row_rsInvoice['invoicedate']; ?></strong></td>
  </tr>
  <tr>
    <td align="right">For:</td>
    <td bgcolor="#EBEBEB"><strong><?php echo $row_rsInvoice['orgname']; ?></strong></td>
  </tr>
  <tr>
    <td align="right">Attention of:</td>
    <td bgcolor="#EBEBEB"><strong><?php echo $row_rsInvoice['invoiceto']; ?> (<?php echo $row_rsInvoice['invoiceemail']; ?>)</strong></td>
  </tr>
</table>
<hr />
<p><a href="invoice-delete.php?invoiceid=<?php echo $row_rsInvoice['invoiceid']; ?>">Delete this invoice</a> (you will be asked to confirm) | 
<?php if ($totalRows_rsDetails > 0) { // Show if recordset not empty ?>
  <a href="forum-invoice-preview.php?invoiceid=<?php echo $row_rsInvoice['invoiceid']; ?>">Preview invoice and then send</a>
<?php if ($row_rsInvoice['sent'] !=0) { ?>
(emailed on <?php echo date("d M Y",strtotime($row_rsInvoice['sentdate'])); ?>)
<?php } ?>
  <?php } // Show if recordset not empty ?></p>
  
<h2>Forum invoice details</h2>
<?php if ($totalRows_rsDetails == 0) { // Show if recordset empty ?>
  <p class="redText">Invoice incomplete - no items added.</p>
  <?php } // Show if recordset empty ?>
      <?php if ($totalRows_rsDetails > 0) { // Show if recordset not empty ?>  
<table border="0" cellspacing="0" cellpadding="3">
  <?php do { ?>
    <tr>

        <td><form id="frmEdit" name="frmEdit" method="POST" action="<?php echo $editFormAction; ?>">
          <table width="600" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>Amount:</td>
              <td><input name="amount" type="text" id="amount" value="<?php echo $row_rsDetails['amount']; ?>" size="8" /></td>
              <td>Description:
                <input name="description" type="text" id="description" value="<?php echo $row_rsDetails['description']; ?>" size="50" maxlength="250" /></td>
            </tr>
            <tr>
              <td valign="top">Notes:</td>
              <td colspan="2"><textarea name="notes" id="notes" cols="55" rows="3"><?php echo $row_rsDetails['notes']; ?></textarea></td>
            </tr>
            <tr>
              <td><input name="detailid" type="hidden" id="detailid" value="<?php echo $row_rsDetails['detailid']; ?>" /></td>
              <td colspan="2"><input type="submit" name="btnAdd2" id="btnAdd2" value="Update" /></td>
            </tr>
          </table>
          <input type="hidden" name="MM_update" value="frmEdit" />
        </form></td>
        <td valign="top"><form id="frmDelete" name="frmDelete" method="post" action="">
          <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsDetails['detailid']; ?>" />
          <input type="submit" name="btnDelete" id="btnDelete" value="Delete this item" />
        </form></td>

    </tr>
    <tr>
      <td colspan="2"><hr /></td>
    </tr>
    <?php } while ($row_rsDetails = mysql_fetch_assoc($rsDetails)); ?>
</table>
        <?php } // Show if recordset not empty ?>
<h3>Add item</h3>
<form method="POST" action="<?php echo $editFormAction; ?>" name="frmDetails" id="frmDetails">
<table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Amount:</td>
    <td><input name="amount" type="text" id="amount" value="80.00" size="8" /></td>
    <td>Description:      
      <select name="description" id="description">
<option value="FORUM fee 2013">FORUM fee 2013</option>      
<option value="FORUM fee 2012">FORUM fee 2012</option>
<option value="FORUM fee 2011">FORUM fee 2011</option>
<option value="Organisation fee 2013">Organisation fee 2013</option>      
<option value="Organisation fee 2012">Organisation fee 2012</option>
        <option value="Other">Other - edit after adding</option>
      </select></td>
  </tr>
  <tr>
    <td valign="top">Notes:</td>
    <td colspan="2"><textarea name="notes" id="notes" cols="55" rows="3"></textarea></td>
  </tr>
  <tr>
    <td><input name="invoiceid" type="hidden" id="invoiceid" value="<?php echo $row_rsInvoice['invoiceid']; ?>" /></td>
    <td colspan="2"><input type="submit" name="btnAdd" id="btnAdd" value="Add" /></td>
  </tr>
</table>
<input type="hidden" name="MM_insert" value="frmDetails" />
</form>


</body>
</html>
<?php
mysql_free_result($rsInvoice);

mysql_free_result($rsDetails);

mysql_free_result($rsList);
?>
