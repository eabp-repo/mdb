<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmInvoice")) {
  $insertSQL = sprintf("INSERT INTO invoices (id, invoicedate) VALUES (%s, %s)",
                       GetSQLValueString($_POST['id'], "int"),
                       GetSQLValueString($_POST['invoicedate'], "date"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "invoice-details.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsOrg = "SELECT id, orgname FROM orgsnew WHERE nid = 12 ORDER BY orgname ASC";
$rsOrg = mysql_query($query_rsOrg, $connEABP2) or die(mysql_error());
$row_rsOrg = mysql_fetch_assoc($rsOrg);
$totalRows_rsOrg = mysql_num_rows($rsOrg);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoices = "SELECT invoiceid, invoicedate, orgname FROM invoices INNER JOIN orgsnew ON invoices.id = orgsnew.id WHERE invoices.forummember = 0 ORDER BY invoiceid DESC";
$rsInvoices = mysql_query($query_rsInvoices, $connEABP2) or die(mysql_error());
$row_rsInvoices = mysql_fetch_assoc($rsInvoices);
$totalRows_rsInvoices = mysql_num_rows($rsInvoices);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPaymentsReceived = "SELECT DISTINCTROW paymentid, orgsnew.id, orgname, foryear, amountdue, payment, paymentdate, received FROM paymentsorgs INNER JOIN orgsnew ON orgsnew.id = paymentsorgs.id WHERE paymentdate >='2012-04-05' ORDER BY paymentdate DESC";
$rsPaymentsReceived = mysql_query($query_rsPaymentsReceived, $connEABP2) or die(mysql_error());
$row_rsPaymentsReceived = mysql_fetch_assoc($rsPaymentsReceived);
$totalRows_rsPaymentsReceived = mysql_num_rows($rsPaymentsReceived);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Invoicing Orgs</title>
</head>

<body>
<p><a href="index.php">Invoices Home</a></p>
<h1>Invoicing Organisations (where NA is EABP)</h1>
<h2>Create invoice</h2>
<form id="frmInvoice" name="frmInvoice" method="POST" action="<?php echo $editFormAction; ?>">
  <p>Create new invoice: for:
    <select name="id" id="id">
      <?php
do {  
?>
      <option value="<?php echo $row_rsOrg['id']?>"><?php echo htmlentities($row_rsOrg['orgname'])?></option>
      <?php
} while ($row_rsOrg = mysql_fetch_assoc($rsOrg));
  $rows = mysql_num_rows($rsOrg);
  if($rows > 0) {
      mysql_data_seek($rsOrg, 0);
	  $row_rsOrg = mysql_fetch_assoc($rsOrg);
  }
?>
    </select>
  </p>
  <p> 
    date: 
  <input name="invoicedate" type="text" id="invoicedate" value="<?php echo date('Y-m-d'); ?>" size="12" />
  <input type="submit" name="btnInvoiceGo" id="btnInvoiceGo" value="Go" />
  <input type="hidden" name="MM_insert" value="frmInvoice" />
  </p>
</form>
<h2>Edit/Send invoice/reminder</h2>
<?php if ($totalRows_rsInvoices > 0) { // Show if recordset not empty ?>
  <form id="frmEdit" name="frmEdit" method="get" action="invoice-details.php">
    Edit/Send an invoice/reminder:
      <select name="invoiceid" id="invoiceid">
      <?php
do {  
?>
      <option value="<?php echo $row_rsInvoices['invoiceid']?>">Invoice no: <?php echo $row_rsInvoices['invoiceid']?> - <?php echo htmlentities($row_rsInvoices['orgname'])?> - <?php echo $row_rsInvoices['invoicedate']?></option>
      <?php
} while ($row_rsInvoices = mysql_fetch_assoc($rsInvoices));
  $rows = mysql_num_rows($rsInvoices);
  if($rows > 0) {
      mysql_data_seek($rsInvoices, 0);
	  $row_rsInvoices = mysql_fetch_assoc($rsInvoices);
  }
?>
      </select>
    <input type="submit" name="btnInvoiceEditGo" id="btnInvoiceEditGo" value="Go" />
  </form>
  <?php } // Show if recordset not empty ?>
    <hr />  
    <h2>Invoices sent:</h2>
    <?php if ($totalRows_rsPaymentsReceived > 0) { // Show if recordset not empty ?>
    <p>Members who have responded to emailed invoice for organisation fees      <span class="smaller">- payment confirmed via PayPal or manually (click link below):</span></p>
    <table width="80%" border="0" cellpadding="5" cellspacing="0">
    <tr>
      <th>Name</th>
      <th>Year</th>
      <th>Amount due</th>
      <th>Amount paid</th>
      <th>Date paid</th>
      <th>Payment confirmed</th>
    </tr>
    <?php do { ?>
      <tr>
        <td><a href="orgs-payment-edit.php?paymentid=<?php echo $row_rsPaymentsReceived['paymentid']; ?>"><?php echo htmlentities($row_rsPaymentsReceived['orgname']); ?></a></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['foryear']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['amountdue']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['payment']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['paymentdate']; ?></td>
        <td align="center"><?php if ($row_rsPaymentsReceived['received'] == 1) echo("YES"); else echo("NO"); ?></td>
      </tr>
      <?php } while ($row_rsPaymentsReceived = mysql_fetch_assoc($rsPaymentsReceived)); ?>
  </table>
  <?php } // Show if recordset not empty ?>
</body>
</html>
<?php
mysql_free_result($rsOrg);

mysql_free_result($rsInvoices);

mysql_free_result($rsPaymentsReceived);
?>
