<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmDetails")) {
  $insertSQL = sprintf("INSERT INTO invoicedetails (invoiceid, amount, `description`, notes) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['invoiceid'], "int"),
                       GetSQLValueString($_POST['amount'], "double"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['notes'], "text"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmEdit")) {
  $updateSQL = sprintf("UPDATE invoicedetails SET amount=%s, `description`=%s, notes=%s WHERE detailid=%s",
                       GetSQLValueString($_POST['amount'], "double"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString($_POST['detailid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM invoicedetails WHERE detailid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());
}
$colname_rsInvoice = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsInvoice = $_GET['invoiceid']; 


mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoice = sprintf("SELECT invoiceid, invoicedate, orgname, orgsnew.forummember FROM invoices INNER JOIN orgsnew ON invoices.id = orgsnew.id WHERE invoiceid = %s", GetSQLValueString($colname_rsInvoice, "int"));
$rsInvoice = mysql_query($query_rsInvoice, $connEABP2) or die(mysql_error());
$row_rsInvoice = mysql_fetch_assoc($rsInvoice);
$totalRows_rsInvoice = mysql_num_rows($rsInvoice);

} else {

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoice = "SELECT invoiceid, invoicedate, orgname, orgsnew.forummember FROM invoices INNER JOIN orgsnew ON invoices.id = orgsnew.id ORDER BY invoiceid DESC Limit 0,1";
$rsInvoice = mysql_query($query_rsInvoice, $connEABP2) or die(mysql_error());
$row_rsInvoice = mysql_fetch_assoc($rsInvoice);
$totalRows_rsInvoice = mysql_num_rows($rsInvoice);

}

//$colname_rsDetails = $row_rsInvoice['invoiceid'];
$colname_rsDetails = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsDetails = $_GET['invoiceid'];
}else {
$colname_rsDetails = $row_rsInvoice['invoiceid'];  
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDetails = sprintf("SELECT * FROM invoicedetails WHERE invoiceid = %s", GetSQLValueString($colname_rsDetails, "int"));
$rsDetails = mysql_query($query_rsDetails, $connEABP2) or die(mysql_error());
$row_rsDetails = mysql_fetch_assoc($rsDetails);
$totalRows_rsDetails = mysql_num_rows($rsDetails);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Invoice details</title>
<style type="text/css">
.red {
	color: #F00;
}
</style>
</head>

<body>
<p><a href="index.php">Invoice home</a></p>
<p>Invoice number: 2013-<?php echo $row_rsInvoice['invoiceid']; ?><br />
  Invoice date: <?php echo $row_rsInvoice['invoicedate']; ?><br />
For: <?php echo htmlentities($row_rsInvoice['orgname']); ?></p>
<h2>Organisation invoice details</h2>
<?php if ($totalRows_rsDetails > 0) { // Show if recordset not empty ?>
  <p><a href="invoice-preview.php?invoiceid=<?php echo $row_rsInvoice['invoiceid']; ?>">Preview invoice and send (and reminder) ...</a></p>
  <?php } // Show if recordset not empty ?>

      <?php if ($totalRows_rsDetails > 0) { // Show if recordset not empty ?>
<h3>Edit/Delete item</h3>
<table border="0" cellspacing="0" cellpadding="3">
  <?php do { ?>
    <tr>

        <td><form id="frmEdit" name="frmEdit" method="POST" action="<?php echo $editFormAction; ?>">
          <table width="600" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td>Amount:</td>
              <td><input name="amount" type="text" id="amount" value="<?php echo $row_rsDetails['amount']; ?>" size="8" /></td>
              <td>Description:
                <input name="description" type="text" id="description" value="<?php echo $row_rsDetails['description']; ?>" size="50" maxlength="250" /></td>
            </tr>
            <tr>
              <td valign="top">Notes:</td>
              <td colspan="2"><textarea name="notes" id="notes" cols="55" rows="3"><?php echo $row_rsDetails['notes']; ?></textarea></td>
            </tr>
            <tr>
              <td><input name="detailid" type="hidden" id="detailid" value="<?php echo $row_rsDetails['detailid']; ?>" /></td>
              <td colspan="2"><input type="submit" name="btnAdd2" id="btnAdd2" value="Update" /></td>
            </tr>
          </table>
          <input type="hidden" name="MM_update" value="frmEdit" />
        </form></td>
        <td><form id="frmDelete" name="frmDelete" method="post" action="">
          <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsDetails['detailid']; ?>" />
          <input type="submit" name="btnDelete" id="btnDelete" value="Delete" />
        </form></td>

    </tr>
    <?php } while ($row_rsDetails = mysql_fetch_assoc($rsDetails)); ?>
</table>
        <?php } // Show if recordset not empty ?>
<?php if ($totalRows_rsDetails == 0) { // Show if recordset empty ?>
  <p class="red">This invoice is incomplete - please add an item (below).</p>
  <?php } // Show if recordset empty ?>
  <hr />
  <h3>Add item</h3>
<form method="POST" action="<?php echo $editFormAction; ?>" name="frmDetails" id="frmDetails">
<table width="600" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>Amount:</td>
    <td><input name="amount" type="text" id="amount" value="190.00" size="8" /></td>
    <td>Description:      
      <select name="description" id="description">
        <option value="EABP Organisational Fee 2013">EABP Organisational Fee 2013</option>
        <option value="EABP Organisational Fee 2012">EABP Organisational Fee 2012</option>
        <option value="Other">Other - edit after adding</option>
      </select></td>
  </tr>
  <tr>
    <td valign="top">Notes:</td>
    <td colspan="2"><textarea name="notes" id="notes" cols="55" rows="3"></textarea></td>
  </tr>
  <tr>
    <td><input name="invoiceid" type="hidden" id="invoiceid" value="<?php echo $row_rsInvoice['invoiceid']; ?>" /></td>
    <td colspan="2"><input type="submit" name="btnAdd" id="btnAdd" value="Add" /></td>
  </tr>
</table>
<input type="hidden" name="MM_insert" value="frmDetails" />
</form>

</body>
</html>
<?php
mysql_free_result($rsInvoice);

mysql_free_result($rsDetails);
?>
