<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsInvoice = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsInvoice = $_GET['invoiceid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsInvoice = sprintf("SELECT invoiceid, invoicedate, orgname, contact, address, postcode, city, invoiceemail FROM invoices INNER JOIN orgsnew ON invoices.id = orgsnew.id WHERE invoiceid = %s", GetSQLValueString($colname_rsInvoice, "int"));
$rsInvoice = mysql_query($query_rsInvoice, $connEABP2) or die(mysql_error());
$row_rsInvoice = mysql_fetch_assoc($rsInvoice);
$totalRows_rsInvoice = mysql_num_rows($rsInvoice);

$colname_rsDetails = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsDetails = $_GET['invoiceid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDetails = sprintf("SELECT amount, `description`, notes FROM invoicedetails WHERE invoiceid = %s", GetSQLValueString($colname_rsDetails, "int"));
$rsDetails = mysql_query($query_rsDetails, $connEABP2) or die(mysql_error());
$row_rsDetails = mysql_fetch_assoc($rsDetails);
$totalRows_rsDetails = mysql_num_rows($rsDetails);

$colname_rsTotal = "-1";
if (isset($_GET['invoiceid'])) {
  $colname_rsTotal = $_GET['invoiceid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTotal = sprintf("SELECT Sum(amount) AS SumOfamount FROM invoicedetails WHERE invoiceid = %s GROUP BY invoicedetails.invoiceid", GetSQLValueString($colname_rsTotal, "int"));
$rsTotal = mysql_query($query_rsTotal, $connEABP2) or die(mysql_error());
$row_rsTotal = mysql_fetch_assoc($rsTotal);
$totalRows_rsTotal = mysql_num_rows($rsTotal);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Preview Invoice</title>
</head>

<body>
<p> <a href="index.php">Invoice home</a></p>
<hr align="left" width="600" />
<table width="600" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>Invoice number: 2013-<?php echo $row_rsInvoice['invoiceid']; ?></td>
    <td width="200" align="right">Invoice date: <?php echo  date("d M Y",strtotime($row_rsInvoice['invoicedate'])); ?></td>
  </tr>
  <tr>
    <td valign="top">For:</td>
    <td align="right" valign="top">Contact: </td>
  </tr>
  <tr>
    <td valign="top"><strong><?php echo $row_rsInvoice['orgname']; ?><br />
    </strong><?php echo $row_rsInvoice['address']; ?><br />
    <?php echo $row_rsInvoice['city']; ?>    <br />
    <?php echo $row_rsInvoice['postcode']; ?>    </td>
    <td align="right" valign="top"><?php echo $row_rsInvoice['contact']; ?></td>
  </tr>
</table>
<hr align="left" width="600" />
<table width="600" border="0" cellspacing="0" cellpadding="3">
  <tr>
  <td><strong>Details</strong></td>
  <td>&nbsp;</td></tr>
    <?php do { ?>
  <tr>

      <td valign="top"><?php echo $row_rsDetails['description']; ?><br />
      <?php echo $row_rsDetails['notes']; ?></td>
      <td align="right" valign="top"><?php echo $row_rsDetails['amount']; ?></td>

  </tr>
      <?php } while ($row_rsDetails = mysql_fetch_assoc($rsDetails)); ?>  
  <tr>
    <td align="right">Total: </td>
    <td align="right">&#8364;<?php echo $row_rsTotal['SumOfamount']; ?></td>
  </tr>
</table>
<hr align="left" width="600" />
<p>Email this invoice to <a href="mailer-organisations.php?invoiceid=<?php echo $row_rsInvoice['invoiceid']; ?>"><?php echo $row_rsInvoice['invoiceemail']; ?></a></p>
<p>Email reminder to <a href="mailer-organisations-reminder.php?invoiceid=<?php echo $row_rsInvoice['invoiceid']; ?>"><?php echo $row_rsInvoice['invoiceemail']; ?></a></p>
</body>
</html>
<?php
mysql_free_result($rsInvoice);

mysql_free_result($rsDetails);

mysql_free_result($rsTotal);
?>
