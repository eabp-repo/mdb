<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmPayment")) {
  $insertSQL = sprintf("INSERT INTO payments (mid, sessionid, amountdue, payment, paymentdate, received) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['mid'], "int"),
                       GetSQLValueString($_POST['sessionid'], "text"),
                       GetSQLValueString($_POST['amountdue'], "double"),
                       GetSQLValueString($_POST['payment'], "double"),
                       GetSQLValueString($_POST['paymentdate'], "date"),
                       GetSQLValueString($_POST['received'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "individuals.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPaymentsReceived = "SELECT DISTINCTROW paymentid, member.mid, firstname, lastname, foryear, amountdue, payment, paymentdate, received FROM payments INNER JOIN member ON member.mid = payments.mid WHERE paymentdate >='2012-04-05' ORDER BY paymentdate DESC";
$rsPaymentsReceived = mysql_query($query_rsPaymentsReceived, $connEABP2) or die(mysql_error());
$row_rsPaymentsReceived = mysql_fetch_assoc($rsPaymentsReceived);
$totalRows_rsPaymentsReceived = mysql_num_rows($rsPaymentsReceived);

$colname_rsMembers = "-1";
if (isset($_GET['q'])) {
  $colname_rsMembers = $_GET['q'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT mid, firstname, lastname, fee, type FROM member INNER JOIN type ON member.tid = type.tid WHERE lastname = %s AND `email` <> '' AND `nid` =12", GetSQLValueString($colname_rsMembers, "text"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Invoicing - Individual members</title>
<style type="text/css">
tr:nth-child(even){
	background-color:#F5F5F5;
}
tr:nth-child(odd){
	background-color:#FFFFFF;
}
.smaller {
	font-size: 75%;
	font-weight: normal;
}
</style>
</head>

<body>
<p><a href="index.php">Invoices home</a></p>
<h1>Individual members</h1>
<p>Emailed on 5 April 2012 to full, associate and candidate members who come under EABP directly.</p>
<form id="frmSearch" name="frmSearch" method="get" action="">
  <h3>Make manual payment for
  <input name="q" type="text" id="q" value="enter last name" onclick="this.value='';" />
    <input type="submit" name="btnGo" id="btnGo" value="Search" />
  </h3>
</form>
<?php if ($totalRows_rsMembers == 0 && isset($_GET['q'])) { // Show if recordset empty ?>
  <p>No match found.</p>
  <?php } // Show if recordset empty ?>
<?php if ($totalRows_rsMembers > 0) { // Show if recordset not empty ?>
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?php echo $row_rsMembers['firstname']; ?> <?php echo $row_rsMembers['lastname']; ?> (<?php echo $row_rsMembers['type']; ?>)</td>
      <td><form id="frmPayment" name="frmPayment" method="POST" action="<?php echo $editFormAction; ?>">
        <input name="mid" type="hidden" id="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
        <input name="received" type="hidden" id="received" value="1" />
        <label>Payment date:
          <input name="paymentdate" type="text" id="paymentdate" value="<?php echo date("Y-m-d"); ?>" size="12" />
          </label>
        <label>Amount due:
          <input name="amountdue" type="text" id="amountdue" value="<?php echo $row_rsMembers['fee']; ?>" size="8" />
          Amount paid: 
          <input name="payment" type="text" id="payment" value="<?php echo $row_rsMembers['fee']; ?>" size="12" />
          </label>
        <input type="submit" name="btnPay" id="btnPay" value="Submit" />
        <input type="hidden" name="MM_insert" value="frmPayment" />
        <input name="sessionid" type="hidden" id="sessionid" value="manualpayment-<?php echo $row_rsMembers['mid']; ?>" />
      </form></td>
    </tr>
  </table>
  <?php } // Show if recordset not empty ?>
  <hr />  
<?php if ($totalRows_rsPaymentsReceived > 0) { // Show if recordset not empty ?>
  <h2>Members who have responded to emailed invoice<br />
  <span class="smaller">(payment confirmed via PayPal or manually, above):</span></h2>
    <table width="80%" border="0" cellpadding="5" cellspacing="0">
    <tr>
      <th>Name</th>
      <th>Year</th>
      <th>Amount due</th>
      <th>Amount paid</th>
      <th>Date paid</th>
      <th>Payment confirmed</th>
    </tr>
    <?php do { ?>
      <tr>
        <td><a href="payment-edit.php?paymentid=<?php echo $row_rsPaymentsReceived['paymentid']; ?>"><?php echo $row_rsPaymentsReceived['firstname']; ?> <?php echo $row_rsPaymentsReceived['lastname']; ?></a></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['foryear']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['amountdue']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['payment']; ?></td>
        <td align="center"><?php echo $row_rsPaymentsReceived['paymentdate']; ?></td>
        <td align="center"><?php if ($row_rsPaymentsReceived['received'] == 1) echo("YES"); else echo("NO"); ?></td>
      </tr>
      <?php } while ($row_rsPaymentsReceived = mysql_fetch_assoc($rsPaymentsReceived)); ?>
  </table>
  <?php } // Show if recordset not empty ?>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsPaymentsReceived);

mysql_free_result($rsMembers);
?>
