<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE payments SET foryear=%s, amountdue=%s, payment=%s, paymentdate=%s, received=%s WHERE paymentid=%s",
                       GetSQLValueString($_POST['foryear'], "int"),
                       GetSQLValueString($_POST['amountdue'], "double"),
                       GetSQLValueString($_POST['payment'], "double"),
                       GetSQLValueString($_POST['paymentdate'], "date"),
                       GetSQLValueString($_POST['received'], "int"),
                       GetSQLValueString($_POST['paymentid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "individuals.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM payments WHERE paymentid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "individuals.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsPaymentsReceived = "-1";
if (isset($_GET['paymentid'])) {
  $colname_rsPaymentsReceived = $_GET['paymentid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPaymentsReceived = sprintf("SELECT member.mid, firstname, lastname, foryear, amountdue, payment, paymentdate, received FROM payments INNER JOIN member ON member.mid = payments.mid WHERE paymentid = %s", GetSQLValueString($colname_rsPaymentsReceived, "int"));
$rsPaymentsReceived = mysql_query($query_rsPaymentsReceived, $connEABP2) or die(mysql_error());
$row_rsPaymentsReceived = mysql_fetch_assoc($rsPaymentsReceived);
$totalRows_rsPaymentsReceived = mysql_num_rows($rsPaymentsReceived);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit invoice payment</title>
<style type="text/css">
#frmDelete {
	float: right;
	background-color: #F00;
	padding: 2px;
	margin-top: 20px;
}
</style>
</head>

<body>
<h3>Edit invoice payment for <?php echo $row_rsPaymentsReceived['firstname']; ?>
  <?php echo $row_rsPaymentsReceived['lastname']; ?>
</h3>
<form id="form1" name="form1" method="POST" action="<?php echo $editFormAction; ?>">
  <table width="500" border="0" cellspacing="0" cellpadding="2">
    <tr>
      <td align="right" valign="baseline">For year:</td>
      <td><input name="foryear" type="text" id="foryear" value="<?php echo $row_rsPaymentsReceived['foryear']; ?>" /></td>
    </tr>
    <tr>
      <td align="right" valign="baseline">Amount due:</td>
      <td><input name="amountdue" type="text" id="amountdue" value="<?php echo $row_rsPaymentsReceived['amountdue']; ?>" /></td>
    </tr>
    <tr>
      <td align="right" valign="baseline">Payment made:</td>
      <td><input name="payment" type="text" id="payment" value="<?php echo $row_rsPaymentsReceived['payment']; ?>" /></td>
    </tr>
    <tr>
      <td align="right" valign="baseline">Payment date:</td>
      <td><input name="paymentdate" type="text" id="paymentdate" value="<?php echo $row_rsPaymentsReceived['paymentdate']; ?>" /></td>
    </tr>
    <tr>
      <td align="right" valign="baseline">Received:</td>
      <td><label>
          <input <?php if (!(strcmp($row_rsPaymentsReceived['received'],"1"))) {echo "checked=\"checked\"";} ?> type="radio" name="received" value="1" id="received_0" />
          Yes</label>
        <label>
          <input <?php if (!(strcmp($row_rsPaymentsReceived['received'],"0"))) {echo "checked=\"checked\"";} ?> type="radio" name="received" value="0" id="received_1" />
        No</label>      </td>
    </tr>
    <tr>
      <td valign="top"><input name="paymentid" type="hidden" id="paymentid" value="<?php echo $_GET['paymentid']; ?>" /></td>
      <td><input type="submit" name="btnUpdate" id="btnUpdate" value="Update" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
</form>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input name="delid" type="hidden" id="delid" value="<?php echo $_GET['paymentid']; ?>" />
  <input type="submit" name="btnDelete" id="btnDelete" value="Delete" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsPaymentsReceived);
?>
