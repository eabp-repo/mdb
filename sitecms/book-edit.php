<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/books";
$ppu->extensions = "GIF,JPG,JPEG";
$ppu->formName = "form1";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE books2 SET cid=%s, title=%s, author=%s, `description`=%s, linktext=%s, linkurl=%s, imagefile=IFNULL(%s,imagefile), live=%s WHERE bookid=%s",
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['author'], "text"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['linktext'], "text"),
                       GetSQLValueString($_POST['linkurl'], "text"),
                       GetSQLValueString($_POST['imagefile'], "text"),
                       GetSQLValueString(isset($_POST['live']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['bookid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM books2 WHERE bookid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsLanguage = "SELECT cid, country, language1 FROM country";
$rsLanguage = mysql_query($query_rsLanguage, $connEABP2) or die(mysql_error());
$row_rsLanguage = mysql_fetch_assoc($rsLanguage);
$totalRows_rsLanguage = mysql_num_rows($rsLanguage);

$colname_rsBook = "-1";
if (isset($_GET['bookid'])) {
  $colname_rsBook = $_GET['bookid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsBook = sprintf("SELECT * FROM books2 WHERE bookid = %s", GetSQLValueString($colname_rsBook, "int"));
$rsBook = mysql_query($query_rsBook, $connEABP2) or die(mysql_error());
$row_rsBook = mysql_fetch_assoc($rsBook);
$totalRows_rsBook = mysql_num_rows($rsBook);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit a book</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h2>Edit a book</h2>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG',false,'','','','','','','');return document.MM_returnValue">
  <table cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Language:</td>
      <td><select name="cid">
        <?php
do {  
?><option value="<?php echo $row_rsLanguage['cid']?>"<?php if (!(strcmp($row_rsLanguage['cid'], $row_rsBook['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsLanguage['language1']?></option>
        <?php
} while ($row_rsLanguage = mysql_fetch_assoc($rsLanguage));
  $rows = mysql_num_rows($rsLanguage);
  if($rows > 0) {
      mysql_data_seek($rsLanguage, 0);
	  $row_rsLanguage = mysql_fetch_assoc($rsLanguage);
  }
?>
                </select>        </td>
    </tr>
    <tr> </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Title:</td>
      <td><input name="title" type="text" class="input500" value="<?php echo $row_rsBook['title']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Author:</td>
      <td><input name="author" type="text" class="input500" value="<?php echo $row_rsBook['author']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Image file:<br />
      <?php if ($row_rsBook['imagefile'] !="") { ?>
      <img src="../images/books/<?php echo $row_rsBook['imagefile']; ?>" width="50" />
      <?php }?>
      </td>
      <td><input name="imagefile" type="file" id="imagefile" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG',false,'','','','','','','')" />
        <span class="smallText">optional</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right" valign="top">Description:</td>
      <td><textarea name="description" cols="50" rows="5"><?php echo $row_rsBook['description']; ?></textarea>
      <span class="smallText">optional</span> </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Link text:</td>
      <td><input name="linktext" type="text" class="input500" value="<?php echo $row_rsBook['linktext']; ?>" size="32" />
      <span class="smallText">optional</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Link url:</td>
      <td><input name="linkurl" type="text" class="input500" value="<?php echo $row_rsBook['linkurl']; ?>" size="32" />
      <span class="smallText">optional</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Make live:</td>
      <td><input <?php if (!(strcmp($row_rsBook['live'],1))) {echo "checked=\"checked\"";} ?> name="live" type="checkbox" class="input20" id="live" value="1" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><input name="bookid" type="hidden" id="bookid" value="<?php echo $row_rsBook['bookid']; ?>" /></td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    </table>
  
  <input type="hidden" name="MM_update" value="form1" />
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsBook['bookid']; ?>" />
  <input type="submit" name="btnDelete" id="btnDelete" value="Delete" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsLanguage);

mysql_free_result($rsBook);
?>
