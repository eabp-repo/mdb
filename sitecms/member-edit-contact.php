<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmUpdate")) {
  $updateSQL = sprintf("UPDATE member SET email=%s, homecity=%s, homephone=%s, homepostcode=%s, homeadd1=%s, mobile=%s, skype=%s, webpage=%s, workphone=%s, cid=%s, workadd1=%s, workcity=%s, workpostcode=%s, workcid=%s, showworkinfo=%s, showhomeinfo=%s, blogaddress=%s WHERE mid=%s",
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['homecity'], "text"),
                       GetSQLValueString($_POST['homephone'], "text"),
                       GetSQLValueString($_POST['homepostcode'], "text"),
                       GetSQLValueString($_POST['homeadd1'], "text"),
                       GetSQLValueString($_POST['mobile'], "text"),
                       GetSQLValueString($_POST['skype'], "text"),
                       GetSQLValueString($_POST['webpage'], "text"),
                       GetSQLValueString($_POST['workphone'], "text"),
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString($_POST['workadd1'], "text"),
                       GetSQLValueString($_POST['workcity'], "text"),
                       GetSQLValueString($_POST['workpostcode'], "text"),
                       GetSQLValueString($_POST['workcid'], "int"),
                       GetSQLValueString(isset($_POST['showworkinfo']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString(isset($_POST['showhomeinfo']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['blogaddress'], "text"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9 AND tid <> 11 AND tid <> 6";
$rsMemTypes = mysql_query($query_rsMemTypes, $connEABP2) or die(mysql_error());
$row_rsMemTypes = mysql_fetch_assoc($rsMemTypes);
$totalRows_rsMemTypes = mysql_num_rows($rsMemTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9";
$rsTypes = mysql_query($query_rsTypes, $connEABP2) or die(mysql_error());
$row_rsTypes = mysql_fetch_assoc($rsTypes);
$totalRows_rsTypes = mysql_num_rows($rsTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = "SELECT naid, acronym FROM `national`";
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>EABP individual membership database update</title>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>

<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p class="leftAlign"><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" />  admin home</a></p>
<!-- Google Translate -->
<div id="google_translate_element"></div><script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    includedLanguages: 'bg,da,nl,en,fi,fr,de,el,iw,it,ja,no,pl,pt,ru,sr,sl,es,sv',
    gaTrack: true,
    gaId: 'UA-224881-50',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<p class="googleTranslateText">The Google Translator is here to give you some help but it is not perfect!</p>
<!-- End -->
<p class="clear">EABP individual member update:</p>
<p><a href="member-edit-login.php?mid=<?php echo $row_rsMembers['mid']; ?>">Login details</a> | <a href="member-edit-general.php?mid=<?php echo $row_rsMembers['mid']; ?>">General</a> | Contact details | <a href="member-edit-membership.php?mid=<?php echo $row_rsMembers['mid']; ?>">Membership</a> | <a href="member-edit-training.php?mid=<?php echo $row_rsMembers['mid']; ?>">Training</a> | <a href="member-edit-work.php?mid=<?php echo $row_rsMembers['mid']; ?>">Current Work</a> |  <a href="member-delete.php?mid=<?php echo $row_rsMembers['mid']; ?>">Delete this member</a> (you will be asked to confirm)</p>
<hr size="1" noshade="noshade" />
<h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
<h2>Contact details<?php if (isset($_POST['processed'])) { ?> - <img src="../images/tick.gif" alt="tick" width="16" height="16" /><?php } ?></h2>
<hr size="1" noshade="noshade" />
<form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="frmUpdate" id="frmUpdate" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,PNG',false,50,'','','','','','');return document.MM_returnValue">
  <table border="0" cellpadding="3" cellspacing="0" id="tblDetails">
    <tr>
      <td colspan="2" class="smallText">Filemaker Pro membership ID:<strong> <?php echo $row_rsMembers['membershipid']; ?></strong></td>
      <td class="smallText">&nbsp;</td>
    </tr>
    <tr>
      <td width="200">Email</td>
      <td><input name="email" type="text" id="email" value="<?php echo $row_rsMembers['email']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Mobile Phone</td>
      <td><input name="mobile" type="text" id="mobile" value="<?php echo $row_rsMembers['mobile']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Skype name</td>
      <td><input name="skype" type="text" id="skype" value="<?php echo $row_rsMembers['skype']; ?>" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Website address</td>
      <td><input name="webpage" type="text" id="webpage" value="<?php echo $row_rsMembers['webpage']; ?>" /></td>
      <td class="smallText">NO http://</td>
    </tr>
    <tr>
      <td>Blog address</td>
      <td><input name="blogaddress" type="text" id="blogaddress" value="<?php echo $row_rsMembers['blogaddress']; ?>" /></td>
      <td class="smallText">NO http://</td>
    </tr>
    <tr>
      <td class="tdWhiteBg"><strong>Work</strong></td>
      <td class="tdWhiteBg">&nbsp;</td>
      <td align="center" class="tdWhiteBg">&nbsp;</td>
    </tr>
    <tr>
      <td>Work Phone</td>
      <td><input name="workphone" type="text" id="workphone" value="<?php echo $row_rsMembers['workphone']; ?>" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td>Work Street</td>
      <td><input name="workadd1" type="text" id="workadd1" value="<?php echo $row_rsMembers['workadd1']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Work City</td>
      <td><input name="workcity" type="text" id="workcity" value="<?php echo $row_rsMembers['workcity']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Work Postal Code</td>
      <td><input name="workpostcode" type="text" id="workpostcode" value="<?php echo $row_rsMembers['workpostcode']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Work Country</td>
      <td><select name="workcid" id="workcid">
          <?php
do {  
?><option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsMembers['workcid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option><?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Show work info on website</td>
      <td><input <?php if (!(strcmp($row_rsMembers['showworkinfo'],1))) {echo "checked=\"checked\"";} ?> name="showworkinfo" type="checkbox" id="showworkinfo" value="1" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td class="tdWhiteBg"><strong>Home</strong></td>
      <td class="tdWhiteBg">&nbsp;</td>
      <td align="right" class="tdWhiteBg">&nbsp;</td>
    </tr>
    <tr>
      <td>Home Phone</td>
      <td><input name="homephone" type="text" id="homephone" value="<?php echo $row_rsMembers['homephone']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Home Street</td>
      <td><input name="homeadd1" type="text" id="homeadd1" value="<?php echo $row_rsMembers['homeadd1']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Home City</td>
      <td><input name="homecity" type="text" id="homecity" value="<?php echo $row_rsMembers['homecity']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Home Postal Code</td>
      <td><input name="homepostcode" type="text" id="homepostcode" value="<?php echo $row_rsMembers['homepostcode']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Home Country</td>
      <td><select name="cid" id="cid">
          <?php
do {  
?>
          <option value="<?php echo $row_rsCountries['cid']?>"<?php if (!(strcmp($row_rsCountries['cid'], $row_rsMembers['cid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCountries['country']?></option>
          <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
  $rows = mysql_num_rows($rsCountries);
  if($rows > 0) {
      mysql_data_seek($rsCountries, 0);
	  $row_rsCountries = mysql_fetch_assoc($rsCountries);
  }
?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Show home info on website</td>
      <td><input <?php if (!(strcmp($row_rsMembers['showhomeinfo'],1))) {echo "checked=\"checked\"";} ?> name="showhomeinfo" type="checkbox" id="showhomeinfo" value="1" /></td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFFF" class="tdWhiteBg"><?php if (isset($_POST['processed'])) { ?>
          <img src="../images/tick.gif" alt="tick" width="16" height="16" />
      <?php } ?></td>
      <td class="tdWhiteBg"><input name="mid" type="hidden" id="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
        <input type="submit" name="btnUpdate" id="btnUpdate" value="Update" />
        <input name="MM_update" type="hidden" id="MM_update" value="frmUpdate" /></td>
      <td class="tdWhiteBg">&nbsp;</td>
    </tr>
  </table>
  <input name="processed" type="hidden" id="processed" value="1" />
  <input type="hidden" name="MM_update" value="frmUpdate" />
</form>
</body>
</html>
<?php
mysql_free_result($rsMembers);

mysql_free_result($rsMemTypes);

mysql_free_result($rsTypes);

mysql_free_result($rsCountries);

mysql_free_result($rsNA);
?>
