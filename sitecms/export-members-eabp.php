<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembersExportCSV = "SELECT mid, firstname, lastname, email, homephone, workphone, IF(workadd1 ='' OR workadd1 IS NULL,homeadd1,workadd1) AS address, IF(workadd1 ='' OR workadd1 IS NULL,homecity,workcity) AS city, IF(workadd1 ='' OR workadd1 IS NULL,homepostcode,workpostcode) AS postcode, IF(workadd1 ='' OR workadd1 IS NULL,country.country,country_1.country) AS country,  acronym AS NA, type FROM (type INNER JOIN ((country INNER JOIN member ON country.cid = member.cid) INNER JOIN national ON member.nid = national.naid) ON type.tid = member.tid) INNER JOIN country AS country_1 ON member.workcid = country_1.cid WHERE nid = 12 AND member.tid = 7 ORDER BY country,lastname";
$rsMembersExportCSV = mysql_query($query_rsMembersExportCSV, $connEABP2) or die(mysql_error());
$row_rsMembersExportCSV = mysql_fetch_assoc($rsMembersExportCSV);
$totalRows_rsMembersExportCSV = mysql_num_rows($rsMembersExportCSV);

?>
<?php 
csvToExcelDownloadFromResult($rsMembersExportCSV);

// setup the headers
function setExcelContentType() {
    if(headers_sent())
        return false;

    header('Content-type: application/vnd.ms-excel');
    return true;
}

function setDownloadAsHeader($filename) {
    if(headers_sent())
        return false;

    header('Content-disposition: attachment; filename=' . $filename);
    return true;
}
// send a CSV to a stream using a mysql result
function csvFromResult($stream, $rsMembersExportCSV, $showColumnHeaders = true) {
    if($showColumnHeaders) {
        $columnHeaders = array();
        $nfields = mysql_num_fields($rsMembersExportCSV);
        for($i = 0; $i < $nfields; $i++) {
            $field = mysql_fetch_field($rsMembersExportCSV, $i);
            $columnHeaders[] = $field->name;
        }
        fputcsv($stream, $columnHeaders);
    }

    $nrows = 0;
    while($row = mysql_fetch_row($rsMembersExportCSV)) {
        fputcsv($stream, $row);
        $nrows++;
    }

    return $nrows;
}
// use the above function to write a CSV to a file, given by $filename
function csvFileFromResult($filename, $rsMembersExportCSV, $showColumnHeaders = true) {
    $fp = fopen($filename, 'w');
    $rc = csvFromResult($fp, $rsMembersExportCSV, $showColumnHeaders);
    fclose($fp);
    return $rc;
}
// output
function csvToExcelDownloadFromResult($rsMembersExportCSV, $showColumnHeaders = true, $asFilename = 'members-eabp.csv') {
    setExcelContentType();
    setDownloadAsHeader($asFilename);
    return csvFileFromResult('php://output', $rsMembersExportCSV, $showColumnHeaders);
}

?>
<?php
mysql_free_result($rsMembersExportCSV);
?>