<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsSections = "SELECT sectionid, `section` FROM intranetsections WHERE sectionid <> 2 AND sectionid <> 8";
$rsSections = mysql_query($query_rsSections, $connEABP2) or die(mysql_error());
$row_rsSections = mysql_fetch_assoc($rsSections);
$totalRows_rsSections = mysql_num_rows($rsSections);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPages = "SELECT pageid, title, section FROM intranetpages INNER JOIN intranetsections ON intranetsections.sectionid = intranetpages.sectionid ORDER BY section, intranetpages.rankorder, intranetpages.title  Limit 0,50";
$rsPages = mysql_query($query_rsPages, $connEABP2) or die(mysql_error());
$row_rsPages = mysql_fetch_assoc($rsPages);
$totalRows_rsPages = mysql_num_rows($rsPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNewsletters = "SELECT id, monthissue FROM enewsletter ORDER BY `date` DESC";
$rsNewsletters = mysql_query($query_rsNewsletters, $connEABP2) or die(mysql_error());
$row_rsNewsletters = mysql_fetch_assoc($rsNewsletters);
$totalRows_rsNewsletters = mysql_num_rows($rsNewsletters);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Intranet and eNewsletter</title>
<link href="../cms.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
h3 {
	margin-bottom: 2px;
}
-->
</style>
</head>

<body>
<p><img src="../../images/logo-20.gif" alt="" width="47" height="20" align="absmiddle" /> <a href="../index.php">website admin home</a> | <a href="../logout.php">logout</a> | <a href="../../intranet/index.php">go to intranet</a></p>
<h1>Intranet and eNewsletter</h1>
<hr />
<h2>Intranet Pages</h2>
<form id="frmAddPage" name="frmAddPage" method="get" action="page-add.php">
  <h3>Add a page for:</h3> 
    <select name="sectionid" id="sectionid">
      <?php
do {  
?>
      <option value="<?php echo $row_rsSections['sectionid']?>"><?php echo $row_rsSections['section']?></option>
      <?php
} while ($row_rsSections = mysql_fetch_assoc($rsSections));
  $rows = mysql_num_rows($rsSections);
  if($rows > 0) {
      mysql_data_seek($rsSections, 0);
	  $row_rsSections = mysql_fetch_assoc($rsSections);
  }
?>
    </select>
    <input name="button2" type="submit" class="btnGo" id="button2" value="Go" />
  
</form>
<form action="page-edit.php" method="post" name="frmEditPage" id="frmEditPage">
<h3>Edit/Delete a page:</h3>
  <select name="pageid" class="input500" id="pageid">
    <?php
do {  
?>
    <option value="<?php echo $row_rsPages['pageid']?>"><?php echo $row_rsPages['section']?> - <?php echo $row_rsPages['title']?></option>
    <?php
} while ($row_rsPages = mysql_fetch_assoc($rsPages));
  $rows = mysql_num_rows($rsPages);
  if($rows > 0) {
      mysql_data_seek($rsPages, 0);
	  $row_rsPages = mysql_fetch_assoc($rsPages);
  }
?>
  </select>
  <input name="button3" type="submit" class="btnGo" id="button3" value="Go" />
</form>
<hr />
<h2>Intranet Sections</h2>
<h3><a href="section-add.php">Add a section</a></h3>
<form id="frmSections" name="frmSections" method="post" action="section-edit.php">
  <h3>Edit/delete a section:</h3>
    <select name="sectionid" id="sectionid">
      <?php
do {  
?>
      <option value="<?php echo $row_rsSections['sectionid']?>"><?php echo $row_rsSections['section']?></option>
      <?php
} while ($row_rsSections = mysql_fetch_assoc($rsSections));
  $rows = mysql_num_rows($rsSections);
  if($rows > 0) {
      mysql_data_seek($rsSections, 0);
	  $row_rsSections = mysql_fetch_assoc($rsSections);
  }
?>
  </select>
    <input name="button" type="submit" class="btnGo" id="button" value="Go" />
  
</form>
<hr />

<h2>eNewsletter</h2>
<p><a href="newsletter-add.php">Add a newsletter</a></p>

<form id="frmNewsletters" name="frmNewsletters" method="post" action="newsletter-edit.php">
<h3>Edit/Delete a newsletter:</h3>
  <select name="id" id="id">
    <?php
do {  
?>
    <option value="<?php echo $row_rsNewsletters['id']?>"><?php echo $row_rsNewsletters['monthissue']?></option>
    <?php
} while ($row_rsNewsletters = mysql_fetch_assoc($rsNewsletters));
  $rows = mysql_num_rows($rsNewsletters);
  if($rows > 0) {
      mysql_data_seek($rsNewsletters, 0);
	  $row_rsNewsletters = mysql_fetch_assoc($rsNewsletters);
  }
?>
  </select>
  <input name="button4" type="submit" class="btnGo" id="button4" value="Go" />
</form>
</body>
</html>
<?php
mysql_free_result($rsSections);

mysql_free_result($rsPages);

mysql_free_result($rsNewsletters);
?>
