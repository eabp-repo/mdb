<?php require_once('../../Connections/connEABP2.php'); ?>
<?php require_once('../../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../../intranet/images";
$ppu->extensions = "GIF,JPG,JPEG,PNG";
$ppu->formName = "frmEdit";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "150";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();
?>
<?php require_once("../../WA_iRite/WARichEditorPHP.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmEdit")) {
  $updateSQL = sprintf("UPDATE intranetpages SET sectionid=%s, title=%s, summary=%s, pagetext=%s, linktext=%s, linkurl=%s, pageimage=IFNULL(%s,pageimage), rankorder=%s WHERE pageid=%s",
                       GetSQLValueString($_POST['sectionid'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['summary'], "text"),
                       GetSQLValueString($_POST['pagetext'], "text"),
                       GetSQLValueString($_POST['linktext'], "text"),
                       GetSQLValueString($_POST['linkurl'], "text"),
                       GetSQLValueString($_POST['pageimage'], "text"),
                       GetSQLValueString($_POST['rankorder'], "int"),
                       GetSQLValueString($_POST['pageid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM intranetpages WHERE pageid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsSection = "SELECT sectionid, `section` FROM intranetsections WHERE sectionid <> 2 AND sectionid <> 8";
$rsSection = mysql_query($query_rsSection, $connEABP2) or die(mysql_error());
$row_rsSection = mysql_fetch_assoc($rsSection);
$totalRows_rsSection = mysql_num_rows($rsSection);

$colname_rsPage = "-1";
if (isset($_POST['pageid'])) {
  $colname_rsPage = $_POST['pageid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPage = sprintf("SELECT * FROM intranetpages WHERE pageid = %s", GetSQLValueString($colname_rsPage, "int"));
$rsPage = mysql_query($query_rsPage, $connEABP2) or die(mysql_error());
$row_rsPage = mysql_fetch_assoc($rsPage);
$totalRows_rsPage = mysql_num_rows($rsPage);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Intranet - edit page</title>
<link href="../cms.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#frmDelete {
	margin-top: 25px;
	float: right;
}
#frmEdit select {
	font-weight: bold;
	color: #AC3556;
}
-->
</style>
<script language='JavaScript' src='../../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php">Home</a></p>
<h1>Intranet - edit page</h1>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsPage['pageid']; ?>" />
  <input name="btnDelete" type="submit" id="btnDelete" value="Delete this page" />
</form>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmEdit" id="frmEdit" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,PNG',false,'','','',150,'','','');return document.MM_returnValue">
<table width="800" cellpadding="3" cellspacing="0">
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Section:</td>
    <td><select name="sectionid">
      <?php
do {  
?>
      <option value="<?php echo $row_rsSection['sectionid']?>"<?php if (!(strcmp($row_rsSection['sectionid'], $row_rsPage['sectionid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsSection['section']?></option>
      <?php
} while ($row_rsSection = mysql_fetch_assoc($rsSection));
  $rows = mysql_num_rows($rsSection);
  if($rows > 0) {
      mysql_data_seek($rsSection, 0);
	  $row_rsSection = mysql_fetch_assoc($rsSection);
  }
?>
    </select>    </td>
  </tr>
  <tr> </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Title:</td>
    <td><input name="title" type="text" class="input400" value="<?php echo $row_rsPage['title']; ?>" size="32" /></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right" valign="top">Summary:</td>
    <td><textarea name="summary" cols="50" rows="5" class="input400"><?php echo $row_rsPage['summary']; ?></textarea>    </td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right" valign="top">Rank order:</td>
    <td><input name="rankorder" type="text" class="input50" id="rankorder" value="<?php echo $row_rsPage['rankorder']; ?>" maxlength="2" />
      Use numbers 1 to 99 - a 1 will put the page at the top of the list</td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right" valign="top">Page text:</td>
    <td><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_1 = CreateRichTextEditor ("pagetext", "../../WA_iRite/", "100%", "700px", "Custom", "../custom/page-edit_pagetext1.js", "".$row_rsPage['pagetext']  ."");
?></td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Image:</td>
    <td><input name="pageimage" type="file" id="pageimage" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,PNG',false,'','','',150,'','','')" />
      optional - max width 150 px</td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Link text:</td>
    <td><input name="linktext" type="text" id="linktext" value="<?php echo $row_rsPage['linktext']; ?>" />
      can be changed - optional</td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right">Link URL:</td>
    <td><input name="linkurl" type="text" id="linkurl" value="<?php echo $row_rsPage['linkurl']; ?>" />
      must begin with http:// - optional</td>
  </tr>
  <tr valign="baseline">
    <td nowrap="nowrap" align="right"><input name="pageid" type="hidden" id="pageid" value="<?php echo $row_rsPage['pageid']; ?>" /></td>
    <td><input type="submit" class="btnAdd" value="Update" /></td>
  </tr>
</table>
<input type="hidden" name="MM_update" value="frmEdit" />
</form>


</body>
</html>
<?php
mysql_free_result($rsSection);

mysql_free_result($rsPage);
?>
