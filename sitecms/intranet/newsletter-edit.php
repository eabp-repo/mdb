<?php require_once('../../Connections/connEABP2.php'); ?>
<?php require_once("../../WA_iRite/WARichEditorPHP.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM enewsletter WHERE id=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE enewsletter SET live=%s, `date`=%s, monthissue=%s, headline=%s, intro=%s, boardtitle=%s, boardsummary=%s, boardpageid=%s, counciltitle=%s, councilsummary=%s, councilpageid=%s, forumtitle=%s, forumsummary=%s, forumpageid=%s, committeestitle=%s, committeessummary=%s, committeespageid=%s, congresstitle=%s, congresssummary=%s, congresspageid=%s, publicationstitle=%s, publicationssummary=%s, publicationspageid=%s, calendartitle=%s, calendarsummary=%s, calendarpageid=%s, letterstitle=%s, letterssummary=%s, letterspageid=%s WHERE id=%s",
                       GetSQLValueString(isset($_POST['live']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['date'], "date"),
                       GetSQLValueString($_POST['monthissue'], "text"),
                       GetSQLValueString($_POST['headline'], "text"),
                       GetSQLValueString($_POST['intro'], "text"),
                       GetSQLValueString($_POST['boardtitle'], "text"),
                       GetSQLValueString($_POST['boardsummary'], "text"),
                       GetSQLValueString($_POST['boardpageid'], "int"),
                       GetSQLValueString($_POST['counciltitle'], "text"),
                       GetSQLValueString($_POST['councilsummary'], "text"),
                       GetSQLValueString($_POST['councilpageid'], "int"),
                       GetSQLValueString($_POST['forumtitle'], "text"),
                       GetSQLValueString($_POST['forumsummary'], "text"),
                       GetSQLValueString($_POST['forumpageid'], "int"),
                       GetSQLValueString($_POST['committeestitle'], "text"),
                       GetSQLValueString($_POST['committeessummary'], "text"),
                       GetSQLValueString($_POST['committeespageid'], "int"),
                       GetSQLValueString($_POST['congresstitle'], "text"),
                       GetSQLValueString($_POST['congresssummary'], "text"),
                       GetSQLValueString($_POST['congresspageid'], "int"),
                       GetSQLValueString($_POST['publicationstitle'], "text"),
                       GetSQLValueString($_POST['publicationssummary'], "text"),
                       GetSQLValueString($_POST['publicationspageid'], "int"),
                       GetSQLValueString($_POST['calendartitle'], "text"),
                       GetSQLValueString($_POST['calendarsummary'], "text"),
                       GetSQLValueString($_POST['calendarpageid'], "int"),
                       GetSQLValueString($_POST['letterstitle'], "text"),
                       GetSQLValueString($_POST['letterssummary'], "text"),
                       GetSQLValueString($_POST['letterspageid'], "int"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsNewsletter = "-1";
if (isset($_POST['id'])) {
  $colname_rsNewsletter = $_POST['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNewsletter = sprintf("SELECT * FROM enewsletter WHERE id = %s", GetSQLValueString($colname_rsNewsletter, "int"));
$rsNewsletter = mysql_query($query_rsNewsletter, $connEABP2) or die(mysql_error());
$row_rsNewsletter = mysql_fetch_assoc($rsNewsletter);
$totalRows_rsNewsletter = mysql_num_rows($rsNewsletter);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsBoardPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 1 ORDER BY pageid DESC";
$rsBoardPages = mysql_query($query_rsBoardPages, $connEABP2) or die(mysql_error());
$row_rsBoardPages = mysql_fetch_assoc($rsBoardPages);
$totalRows_rsBoardPages = mysql_num_rows($rsBoardPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCouncilPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 3 ORDER BY pageid DESC";
$rsCouncilPages = mysql_query($query_rsCouncilPages, $connEABP2) or die(mysql_error());
$row_rsCouncilPages = mysql_fetch_assoc($rsCouncilPages);
$totalRows_rsCouncilPages = mysql_num_rows($rsCouncilPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsForumPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 4 ORDER BY pageid DESC";
$rsForumPages = mysql_query($query_rsForumPages, $connEABP2) or die(mysql_error());
$row_rsForumPages = mysql_fetch_assoc($rsForumPages);
$totalRows_rsForumPages = mysql_num_rows($rsForumPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCommitteesPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 5 ORDER BY pageid DESC";
$rsCommitteesPages = mysql_query($query_rsCommitteesPages, $connEABP2) or die(mysql_error());
$row_rsCommitteesPages = mysql_fetch_assoc($rsCommitteesPages);
$totalRows_rsCommitteesPages = mysql_num_rows($rsCommitteesPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCongressPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 6 ORDER BY pageid DESC";
$rsCongressPages = mysql_query($query_rsCongressPages, $connEABP2) or die(mysql_error());
$row_rsCongressPages = mysql_fetch_assoc($rsCongressPages);
$totalRows_rsCongressPages = mysql_num_rows($rsCongressPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPublicationsPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 7 ORDER BY pageid DESC";
$rsPublicationsPages = mysql_query($query_rsPublicationsPages, $connEABP2) or die(mysql_error());
$row_rsPublicationsPages = mysql_fetch_assoc($rsPublicationsPages);
$totalRows_rsPublicationsPages = mysql_num_rows($rsPublicationsPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsArticlesPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 8 ORDER BY pageid DESC";
$rsArticlesPages = mysql_query($query_rsArticlesPages, $connEABP2) or die(mysql_error());
$row_rsArticlesPages = mysql_fetch_assoc($rsArticlesPages);
$totalRows_rsArticlesPages = mysql_num_rows($rsArticlesPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCalendarPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 9 ORDER BY pageid DESC";
$rsCalendarPages = mysql_query($query_rsCalendarPages, $connEABP2) or die(mysql_error());
$row_rsCalendarPages = mysql_fetch_assoc($rsCalendarPages);
$totalRows_rsCalendarPages = mysql_num_rows($rsCalendarPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsLettersPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 10 ORDER BY pageid DESC";
$rsLettersPages = mysql_query($query_rsLettersPages, $connEABP2) or die(mysql_error());
$row_rsLettersPages = mysql_fetch_assoc($rsLettersPages);
$totalRows_rsLettersPages = mysql_num_rows($rsLettersPages);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit a newsletter</title>
<link href="../cms.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
td {
	font-size: 1em;
	margin: 0px;
	padding: 4px;
}
.input16 {
	width: 16px;
	font-size: 1.2em;
}
textarea {
	width: 500px;
}
#frmDelete {
	margin-top: 25px;
	float: right;
}
-->
</style>

</head>

<body>
<p><a href="index.php">Home</a></p>
<h1>Edit newsletter issue: 
<?php echo $row_rsNewsletter['monthissue']; ?></h1>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input type="submit" name="btnDelete" id="btnDelete" value="Delete this newsletter" />
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsNewsletter['id']; ?>" />
</form>
<form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">
  <table align="left" cellpadding="5" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Live:</td>
      <td><input <?php if (!(strcmp($row_rsNewsletter['live'],1))) {echo "checked=\"checked\"";} ?> name="live" type="checkbox" class="input16" value="1" />
        tick if you want this newsletter to show on the intranet NOW</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Date:</td>
      <td><input type="text" name="date" value="<?php echo $row_rsNewsletter['date']; ?>" size="32" />
        <span class="required">required</span> - enter as YYYY-MM-DD</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Month/issue:</td>
      <td><input type="text" name="monthissue" value="<?php echo $row_rsNewsletter['monthissue']; ?>" size="32" />
        <span class="required">required</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Headline:</td>
      <td><input type="text" name="headline" value="<?php echo $row_rsNewsletter['headline']; ?>" size="32" />
        <span class="required">required</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right" valign="top">Intro:</td>
      <td><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_1 = CreateRichTextEditor ("intro", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_intro1.js", "".$row_rsNewsletter['intro']  ."");
?><span class="required">required</span> </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Board</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC"> title:</td>
      <td bgcolor="#CCCCCC"><input name="boardtitle" type="text" class="input500" value="<?php echo $row_rsNewsletter['boardtitle']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC"> summary:</td>
      <td bgcolor="#CCCCCC"><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_2 = CreateRichTextEditor ("boardsummary", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_boardsummary1.js", "".$row_rsNewsletter['boardsummary']  ."");
?></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC"> Related Intranet/Board page:</td>
      <td bgcolor="#CCCCCC"><select name="boardpageid" class="input400" id="boardpageid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsBoardPages['pageid']?>"<?php if (!(strcmp($row_rsBoardPages['pageid'], $row_rsNewsletter['boardpageid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsBoardPages['title']?></option>
        <?php
} while ($row_rsBoardPages = mysql_fetch_assoc($rsBoardPages));
  $rows = mysql_num_rows($rsBoardPages);
  if($rows > 0) {
      mysql_data_seek($rsBoardPages, 0);
	  $row_rsBoardPages = mysql_fetch_assoc($rsBoardPages);
  }
?>
            </select>
(for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Council</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC"> title:</td>
      <td bgcolor="#CCCCCC"><input name="counciltitle" type="text" class="input500" value="<?php echo $row_rsNewsletter['counciltitle']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC"> summary:</td>
      <td bgcolor="#CCCCCC"><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_3 = CreateRichTextEditor ("councilsummary", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_councilsummary1.js", "".$row_rsNewsletter['councilsummary']  ."");
?></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Council page:</td>
      <td bgcolor="#CCCCCC"><select name="councilpageid" class="input400" id="councilpageid">
        <?php
do {  
?><option value="<?php echo $row_rsCouncilPages['pageid']?>"<?php if (!(strcmp($row_rsCouncilPages['pageid'], $row_rsNewsletter['councilpageid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCouncilPages['title']?></option>
        <?php
} while ($row_rsCouncilPages = mysql_fetch_assoc($rsCouncilPages));
  $rows = mysql_num_rows($rsCouncilPages);
  if($rows > 0) {
      mysql_data_seek($rsCouncilPages, 0);
	  $row_rsCouncilPages = mysql_fetch_assoc($rsCouncilPages);
  }
?>
      </select>
(for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#FFFFCC"><strong>Forum</strong></td>
      <td bgcolor="#FFFFCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">title:</td>
      <td bgcolor="#FFFFCC"><input name="forumtitle" type="text" class="input500" value="<?php echo $row_rsNewsletter['forumtitle']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#FFFFCC">summary:</td>
      <td bgcolor="#FFFFCC"><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_4 = CreateRichTextEditor ("forumsummary", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_forumsummary1.js", "".$row_rsNewsletter['forumsummary']  ."");
?></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Forum page:</td>
      <td bgcolor="#CCCCCC"><select name="forumpageid" class="input400" id="forumpageid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsForumPages['pageid']?>"<?php if (!(strcmp($row_rsForumPages['pageid'], $row_rsNewsletter['forumpageid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsForumPages['title']?></option>
        <?php
} while ($row_rsForumPages = mysql_fetch_assoc($rsForumPages));
  $rows = mysql_num_rows($rsForumPages);
  if($rows > 0) {
      mysql_data_seek($rsForumPages, 0);
	  $row_rsForumPages = mysql_fetch_assoc($rsForumPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Committees</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">title:</td>
      <td bgcolor="#CCCCCC"><input name="committeestitle" type="text" class="input500" value="<?php echo $row_rsNewsletter['committeestitle']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC">summary:</td>
      <td bgcolor="#CCCCCC"><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_5 = CreateRichTextEditor ("committeessummary", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_committeessummary1.js", "".$row_rsNewsletter['committeessummary']  ."");
?></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Committees page:</td>
      <td bgcolor="#CCCCCC"><select name="committeespageid" class="input400" id="committeespageid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCommitteesPages['pageid']?>"<?php if (!(strcmp($row_rsCommitteesPages['pageid'], $row_rsNewsletter['committeespageid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCommitteesPages['title']?></option>
        <?php
} while ($row_rsCommitteesPages = mysql_fetch_assoc($rsCommitteesPages));
  $rows = mysql_num_rows($rsCommitteesPages);
  if($rows > 0) {
      mysql_data_seek($rsCommitteesPages, 0);
	  $row_rsCommitteesPages = mysql_fetch_assoc($rsCommitteesPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#FFFFCC"><strong>Congress</strong></td>
      <td bgcolor="#FFFFCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">title:</td>
      <td bgcolor="#FFFFCC"><input name="congresstitle" type="text" class="input500" value="<?php echo $row_rsNewsletter['congresstitle']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#FFFFCC">summary:</td>
      <td bgcolor="#FFFFCC"><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_6 = CreateRichTextEditor ("congresssummary", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_congresssummary1.js", "".$row_rsNewsletter['congresssummary']  ."");
?></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Congress page:</td>
      <td bgcolor="#CCCCCC"><select name="congresspageid" class="input400" id="congresspageid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCongressPages['pageid']?>"<?php if (!(strcmp($row_rsCongressPages['pageid'], $row_rsNewsletter['congresspageid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCongressPages['title']?></option>
        <?php
} while ($row_rsCongressPages = mysql_fetch_assoc($rsCongressPages));
  $rows = mysql_num_rows($rsCongressPages);
  if($rows > 0) {
      mysql_data_seek($rsCongressPages, 0);
	  $row_rsCongressPages = mysql_fetch_assoc($rsCongressPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Publications</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">title:</td>
      <td bgcolor="#CCCCCC"><input name="publicationstitle" type="text" class="input500" value="<?php echo $row_rsNewsletter['publicationstitle']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC">summary:</td>
      <td bgcolor="#CCCCCC"><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_7 = CreateRichTextEditor ("publicationssummary", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_publicationssummary1.js", "".$row_rsNewsletter['publicationssummary']  ."");
?></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Publications page:</td>
      <td bgcolor="#CCCCCC"><select name="publicationspageid" class="input400" id="publicationspageid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsPublicationsPages['pageid']?>"<?php if (!(strcmp($row_rsPublicationsPages['pageid'], $row_rsNewsletter['publicationspageid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsPublicationsPages['title']?></option>
        <?php
} while ($row_rsPublicationsPages = mysql_fetch_assoc($rsPublicationsPages));
  $rows = mysql_num_rows($rsPublicationsPages);
  if($rows > 0) {
      mysql_data_seek($rsPublicationsPages, 0);
	  $row_rsPublicationsPages = mysql_fetch_assoc($rsPublicationsPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#FFFFCC"><strong>Calendar</strong></td>
      <td bgcolor="#FFFFCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">title:</td>
      <td bgcolor="#FFFFCC"><input name="calendartitle" type="text" class="input500" value="<?php echo $row_rsNewsletter['calendartitle']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#FFFFCC">summary:</td>
      <td bgcolor="#FFFFCC"><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_8 = CreateRichTextEditor ("calendarsummary", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_calendarsummary1.js", "".$row_rsNewsletter['calendarsummary']  ."");
?></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">Related Intranet/Calendar page:</td>
      <td bgcolor="#FFFFCC"><select name="calendarpageid" class="input400" id="calendarpageid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCalendarPages['pageid']?>"<?php if (!(strcmp($row_rsCalendarPages['pageid'], $row_rsNewsletter['calendarpageid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCalendarPages['title']?></option>
        <?php
} while ($row_rsCalendarPages = mysql_fetch_assoc($rsCalendarPages));
  $rows = mysql_num_rows($rsCalendarPages);
  if($rows > 0) {
      mysql_data_seek($rsCalendarPages, 0);
	  $row_rsCalendarPages = mysql_fetch_assoc($rsCalendarPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap="nowrap" bgcolor="#CCCCCC"><strong>Letters to the Editor</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">title:</td>
      <td bgcolor="#CCCCCC"><input name="letterstitle" type="text" class="input500" id="letterstitle" value="<?php echo $row_rsNewsletter['letterstitle']; ?>" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC">summary:</td>
      <td bgcolor="#CCCCCC"><?php
// WebAssist iRite: Rich Text Editor for Dreamweaver
$WARichTextEditor_9 = CreateRichTextEditor ("letterssummary", "../../WA_iRite/", "100%", "200px", "Custom", "../custom/newsletter-edit_letterssummary1.js", "".$row_rsNewsletter['letterssummary']  ."");
?></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Letters page:</td>
      <td bgcolor="#CCCCCC"><select name="letterspageid" class="input400" id="letterspageid">
        <?php
do {  
?><option value="<?php echo $row_rsLettersPages['pageid']?>"<?php if (!(strcmp($row_rsLettersPages['pageid'], $row_rsNewsletter['letterspageid']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsLettersPages['title']?></option>
        <?php
} while ($row_rsLettersPages = mysql_fetch_assoc($rsLettersPages));
  $rows = mysql_num_rows($rsLettersPages);
  if($rows > 0) {
      mysql_data_seek($rsLettersPages, 0);
	  $row_rsLettersPages = mysql_fetch_assoc($rsLettersPages);
  }
?>
        </select>
      (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><input name="id" type="hidden" id="id" value="<?php echo $row_rsNewsletter['id']; ?>" /></td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  
    
  
  <input type="hidden" name="MM_update" value="form1" />
</form>
</body>
</html>
<?php
mysql_free_result($rsNewsletter);

mysql_free_result($rsBoardPages);

mysql_free_result($rsCouncilPages);

mysql_free_result($rsForumPages);

mysql_free_result($rsCommitteesPages);

mysql_free_result($rsCongressPages);

mysql_free_result($rsPublicationsPages);

mysql_free_result($rsArticlesPages);

mysql_free_result($rsCalendarPages);

mysql_free_result($rsLettersPages);
?>
