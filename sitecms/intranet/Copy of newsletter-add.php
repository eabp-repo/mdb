<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO enewsletter (live, `date`, monthissue, headline, intro, boardtitle, boardsummary, boardpageid, membershiptitle, membershipsummary, membershippageid, counciltitle, councilsummary, councilpageid, forumtitle, forumsummary, forumpageid, committeestitle, committeessummary, committeespageid, congresstitle, congresssummary, congresspageid, publicationstitle, publicationssummary, publicationspageid, articlestitle, articlessummary, articlespageid, calendartitle, calendarsummary, calendarpageid) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString(isset($_POST['live']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['date'], "date"),
                       GetSQLValueString($_POST['monthissue'], "text"),
                       GetSQLValueString($_POST['headline'], "text"),
                       GetSQLValueString($_POST['intro'], "text"),
                       GetSQLValueString($_POST['boardtitle'], "text"),
                       GetSQLValueString($_POST['boardsummary'], "text"),
                       GetSQLValueString($_POST['boardpageid'], "int"),
                       GetSQLValueString($_POST['membershiptitle'], "text"),
                       GetSQLValueString($_POST['membershipsummary'], "text"),
                       GetSQLValueString($_POST['membershippageid'], "int"),
                       GetSQLValueString($_POST['counciltitle'], "text"),
                       GetSQLValueString($_POST['councilsummary'], "text"),
                       GetSQLValueString($_POST['councilpageid'], "int"),
                       GetSQLValueString($_POST['forumtitle'], "text"),
                       GetSQLValueString($_POST['forumsummary'], "text"),
                       GetSQLValueString($_POST['forumpageid'], "int"),
                       GetSQLValueString($_POST['committeestitle'], "text"),
                       GetSQLValueString($_POST['committeessummary'], "text"),
                       GetSQLValueString($_POST['committeespageid'], "int"),
                       GetSQLValueString($_POST['congresstitle'], "text"),
                       GetSQLValueString($_POST['congresssummary'], "text"),
                       GetSQLValueString($_POST['congresspageid'], "int"),
                       GetSQLValueString($_POST['publicationstitle'], "text"),
                       GetSQLValueString($_POST['publicationssummary'], "text"),
                       GetSQLValueString($_POST['publicationspageid'], "int"),
                       GetSQLValueString($_POST['articlestitle'], "text"),
                       GetSQLValueString($_POST['articlessummary'], "text"),
                       GetSQLValueString($_POST['articlespageid'], "int"),
                       GetSQLValueString($_POST['calendartitle'], "text"),
                       GetSQLValueString($_POST['calendarsummary'], "text"),
                       GetSQLValueString($_POST['calendarpageid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());

  $insertGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

$colname_rsNewsletter = "-1";
if (isset($_POST['id'])) {
  $colname_rsNewsletter = $_POST['id'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNewsletter = sprintf("SELECT * FROM enewsletter WHERE id = %s", GetSQLValueString($colname_rsNewsletter, "int"));
$rsNewsletter = mysql_query($query_rsNewsletter, $connEABP2) or die(mysql_error());
$row_rsNewsletter = mysql_fetch_assoc($rsNewsletter);
$totalRows_rsNewsletter = mysql_num_rows($rsNewsletter);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsBoardPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 1 ORDER BY pageid DESC";
$rsBoardPages = mysql_query($query_rsBoardPages, $connEABP2) or die(mysql_error());
$row_rsBoardPages = mysql_fetch_assoc($rsBoardPages);
$totalRows_rsBoardPages = mysql_num_rows($rsBoardPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembershipPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 2 ORDER BY pageid DESC";
$rsMembershipPages = mysql_query($query_rsMembershipPages, $connEABP2) or die(mysql_error());
$row_rsMembershipPages = mysql_fetch_assoc($rsMembershipPages);
$totalRows_rsMembershipPages = mysql_num_rows($rsMembershipPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCouncilPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 3 ORDER BY pageid DESC";
$rsCouncilPages = mysql_query($query_rsCouncilPages, $connEABP2) or die(mysql_error());
$row_rsCouncilPages = mysql_fetch_assoc($rsCouncilPages);
$totalRows_rsCouncilPages = mysql_num_rows($rsCouncilPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsForumPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 4 ORDER BY pageid DESC";
$rsForumPages = mysql_query($query_rsForumPages, $connEABP2) or die(mysql_error());
$row_rsForumPages = mysql_fetch_assoc($rsForumPages);
$totalRows_rsForumPages = mysql_num_rows($rsForumPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCommitteesPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 5 ORDER BY pageid DESC";
$rsCommitteesPages = mysql_query($query_rsCommitteesPages, $connEABP2) or die(mysql_error());
$row_rsCommitteesPages = mysql_fetch_assoc($rsCommitteesPages);
$totalRows_rsCommitteesPages = mysql_num_rows($rsCommitteesPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCongressPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 6 ORDER BY pageid DESC";
$rsCongressPages = mysql_query($query_rsCongressPages, $connEABP2) or die(mysql_error());
$row_rsCongressPages = mysql_fetch_assoc($rsCongressPages);
$totalRows_rsCongressPages = mysql_num_rows($rsCongressPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsPublicationsPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 7 ORDER BY pageid DESC";
$rsPublicationsPages = mysql_query($query_rsPublicationsPages, $connEABP2) or die(mysql_error());
$row_rsPublicationsPages = mysql_fetch_assoc($rsPublicationsPages);
$totalRows_rsPublicationsPages = mysql_num_rows($rsPublicationsPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsArticlesPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 8 ORDER BY pageid DESC";
$rsArticlesPages = mysql_query($query_rsArticlesPages, $connEABP2) or die(mysql_error());
$row_rsArticlesPages = mysql_fetch_assoc($rsArticlesPages);
$totalRows_rsArticlesPages = mysql_num_rows($rsArticlesPages);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCalendarPages = "SELECT pageid, title FROM intranetpages WHERE sectionid = 9 ORDER BY pageid DESC";
$rsCalendarPages = mysql_query($query_rsCalendarPages, $connEABP2) or die(mysql_error());
$row_rsCalendarPages = mysql_fetch_assoc($rsCalendarPages);
$totalRows_rsCalendarPages = mysql_num_rows($rsCalendarPages);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add newsletter</title>
<link href="../cms.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
td {
	font-size: 1em;
	margin: 0px;
	padding: 4px;
}
.input16 {
	width: 16px;
	font-size: 1.2em;
}
textarea {
	width: 500px;
}
#frmDelete {
	margin-top: 25px;
	float: right;
}
-->
</style>

</head>

<body>
<p><a href="index.php">Home</a></p>
<h1>Add a newsletter</h1>
<form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">
  <table align="left" cellpadding="5" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Live:</td>
      <td><input name="live" type="checkbox" class="input16" value="1" />
        tick if you want this newsletter to show on the intranet NOW</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Date:</td>
      <td><input type="text" name="date" size="32" />
        <span class="required">required</span> - enter as YYYY-MM-DD</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Month/issue:</td>
      <td><input type="text" name="monthissue" size="32" />
        <span class="required">required</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Headline:</td>
      <td><input type="text" name="headline" size="32" />
        <span class="required">required</span></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right" valign="top">Intro:</td>
      <td><textarea name="intro" rows="5" class="input500"></textarea>
        <span class="required">required</span> </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Board</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC"> title:</td>
      <td bgcolor="#CCCCCC"><input name="boardtitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC"> summary:</td>
      <td bgcolor="#CCCCCC"><textarea name="boardsummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC"> Related Intranet/Board page:</td>
      <td bgcolor="#CCCCCC"><select name="boardpageid" class="input400" id="boardpageid">
          <?php
do {  
?><option value="<?php echo $row_rsBoardPages['pageid']?>"><?php echo $row_rsBoardPages['title']?></option>
          <?php
} while ($row_rsBoardPages = mysql_fetch_assoc($rsBoardPages));
  $rows = mysql_num_rows($rsBoardPages);
  if($rows > 0) {
      mysql_data_seek($rsBoardPages, 0);
	  $row_rsBoardPages = mysql_fetch_assoc($rsBoardPages);
  }
?>
            </select>
(for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#FFFFCC"><strong>Membership</strong></td>
      <td bgcolor="#FFFFCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC"> title:</td>
      <td bgcolor="#FFFFCC"><input name="membershiptitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#FFFFCC">summary:</td>
      <td bgcolor="#FFFFCC"><textarea name="membershipsummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">Related Intranet/Membership page:</td>
      <td bgcolor="#FFFFCC"><select name="membershippageid" class="input400" id="membershippageid">
          <?php
do {  
?><option value="<?php echo $row_rsMembershipPages['pageid']?>"><?php echo $row_rsMembershipPages['title']?></option>
          <?php
} while ($row_rsMembershipPages = mysql_fetch_assoc($rsMembershipPages));
  $rows = mysql_num_rows($rsMembershipPages);
  if($rows > 0) {
      mysql_data_seek($rsMembershipPages, 0);
	  $row_rsMembershipPages = mysql_fetch_assoc($rsMembershipPages);
  }
?>
      </select> 
      (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Council</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC"> title:</td>
      <td bgcolor="#CCCCCC"><input name="counciltitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC"> summary:</td>
      <td bgcolor="#CCCCCC"><textarea name="councilsummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Council page:</td>
      <td bgcolor="#CCCCCC"><select name="councilpageid" class="input400" id="councilpageid">
          <?php
do {  
?><option value="<?php echo $row_rsCouncilPages['pageid']?>"><?php echo $row_rsCouncilPages['title']?></option>
          <?php
} while ($row_rsCouncilPages = mysql_fetch_assoc($rsCouncilPages));
  $rows = mysql_num_rows($rsCouncilPages);
  if($rows > 0) {
      mysql_data_seek($rsCouncilPages, 0);
	  $row_rsCouncilPages = mysql_fetch_assoc($rsCouncilPages);
  }
?>
      </select>
(for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#FFFFCC"><strong>Forum</strong></td>
      <td bgcolor="#FFFFCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">title:</td>
      <td bgcolor="#FFFFCC"><input name="forumtitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#FFFFCC">summary:</td>
      <td bgcolor="#FFFFCC"><textarea name="forumsummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">Related Intranet/Forum page:</td>
      <td bgcolor="#FFFFCC"><select name="forumpageid" class="input400" id="forumpageid">
          <?php
do {  
?><option value="<?php echo $row_rsForumPages['pageid']?>"><?php echo $row_rsForumPages['title']?></option>
          <?php
} while ($row_rsForumPages = mysql_fetch_assoc($rsForumPages));
  $rows = mysql_num_rows($rsForumPages);
  if($rows > 0) {
      mysql_data_seek($rsForumPages, 0);
	  $row_rsForumPages = mysql_fetch_assoc($rsForumPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Committees</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">title:</td>
      <td bgcolor="#CCCCCC"><input name="committeestitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC">summary:</td>
      <td bgcolor="#CCCCCC"><textarea name="committeessummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Committees page:</td>
      <td bgcolor="#CCCCCC"><select name="committeespageid" class="input400" id="committeespageid">
          <?php
do {  
?><option value="<?php echo $row_rsCommitteesPages['pageid']?>"><?php echo $row_rsCommitteesPages['title']?></option>
          <?php
} while ($row_rsCommitteesPages = mysql_fetch_assoc($rsCommitteesPages));
  $rows = mysql_num_rows($rsCommitteesPages);
  if($rows > 0) {
      mysql_data_seek($rsCommitteesPages, 0);
	  $row_rsCommitteesPages = mysql_fetch_assoc($rsCommitteesPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#FFFFCC"><strong>Congress</strong></td>
      <td bgcolor="#FFFFCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">title:</td>
      <td bgcolor="#FFFFCC"><input name="congresstitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#FFFFCC">summary:</td>
      <td bgcolor="#FFFFCC"><textarea name="congresssummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">Related Intranet/Congress page:</td>
      <td bgcolor="#FFFFCC"><select name="congresspageid" class="input400" id="congresspageid">
          <?php
do {  
?><option value="<?php echo $row_rsCongressPages['pageid']?>"><?php echo $row_rsCongressPages['title']?></option>
          <?php
} while ($row_rsCongressPages = mysql_fetch_assoc($rsCongressPages));
  $rows = mysql_num_rows($rsCongressPages);
  if($rows > 0) {
      mysql_data_seek($rsCongressPages, 0);
	  $row_rsCongressPages = mysql_fetch_assoc($rsCongressPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Publications</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">title:</td>
      <td bgcolor="#CCCCCC"><input name="publicationstitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC">summary:</td>
      <td bgcolor="#CCCCCC"><textarea name="publicationssummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Publications page:</td>
      <td bgcolor="#CCCCCC"><select name="publicationspageid" class="input400" id="publicationspageid">
          <?php
do {  
?><option value="<?php echo $row_rsPublicationsPages['pageid']?>"><?php echo $row_rsPublicationsPages['title']?></option>
          <?php
} while ($row_rsPublicationsPages = mysql_fetch_assoc($rsPublicationsPages));
  $rows = mysql_num_rows($rsPublicationsPages);
  if($rows > 0) {
      mysql_data_seek($rsPublicationsPages, 0);
	  $row_rsPublicationsPages = mysql_fetch_assoc($rsPublicationsPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#FFFFCC"><strong>Articles</strong></td>
      <td bgcolor="#FFFFCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">title:</td>
      <td bgcolor="#FFFFCC"><input name="articlestitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#FFFFCC">summary:</td>
      <td bgcolor="#FFFFCC"><textarea name="articlessummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC">Related Intranet/Articles page:</td>
      <td bgcolor="#FFFFCC"><select name="articlespageid" class="input400" id="articlespageid">
          <?php
do {  
?><option value="<?php echo $row_rsArticlesPages['pageid']?>"><?php echo $row_rsArticlesPages['title']?></option>
          <?php
} while ($row_rsArticlesPages = mysql_fetch_assoc($rsArticlesPages));
  $rows = mysql_num_rows($rsArticlesPages);
  if($rows > 0) {
      mysql_data_seek($rsArticlesPages, 0);
	  $row_rsArticlesPages = mysql_fetch_assoc($rsArticlesPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" bgcolor="#CCCCCC"><strong>Calendar</strong></td>
      <td bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">title:</td>
      <td bgcolor="#CCCCCC"><input name="calendartitle" type="text" class="input500" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" bgcolor="#CCCCCC">summary:</td>
      <td bgcolor="#CCCCCC"><textarea name="calendarsummary" cols="50" rows="5"></textarea>      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" bgcolor="#CCCCCC">Related Intranet/Calendar page:</td>
      <td bgcolor="#CCCCCC"><select name="calendarpageid" class="input400" id="calendarpageid">
        <?php
do {  
?>
        <option value="<?php echo $row_rsCalendarPages['pageid']?>"><?php echo $row_rsCalendarPages['title']?></option>
<?php
} while ($row_rsCalendarPages = mysql_fetch_assoc($rsCalendarPages));
  $rows = mysql_num_rows($rsCalendarPages);
  if($rows > 0) {
      mysql_data_seek($rsCalendarPages, 0);
	  $row_rsCalendarPages = mysql_fetch_assoc($rsCalendarPages);
  }
?>
              </select>
        (for Read more &gt; link)</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap">&nbsp;</td>
      <td><input name="button" type="submit" class="btnAdd" id="button" value="Add" /></td>
    </tr>
  </table>
  
    
  
  <input type="hidden" name="MM_insert" value="form1" />
</form>
</body>
</html>
<?php
mysql_free_result($rsNewsletter);

mysql_free_result($rsBoardPages);

mysql_free_result($rsMembershipPages);

mysql_free_result($rsCouncilPages);

mysql_free_result($rsForumPages);

mysql_free_result($rsCommitteesPages);

mysql_free_result($rsCongressPages);

mysql_free_result($rsPublicationsPages);

mysql_free_result($rsArticlesPages);

mysql_free_result($rsCalendarPages);
?>
