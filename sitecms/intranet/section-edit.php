<?php require_once('../../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE intranetsections SET `section`=%s, rankorder=%s WHERE sectionid=%s",
                       GetSQLValueString($_POST['section'], "text"),
                       GetSQLValueString($_POST['rankorder'], "int"),
                       GetSQLValueString($_POST['sectionid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());

  $updateGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsSection = "-1";
if (isset($_POST['sectionid'])) {
  $colname_rsSection = $_POST['sectionid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsSection = sprintf("SELECT * FROM intranetsections WHERE sectionid = %s", GetSQLValueString($colname_rsSection, "int"));
$rsSection = mysql_query($query_rsSection, $connEABP2) or die(mysql_error());
$row_rsSection = mysql_fetch_assoc($rsSection);
$totalRows_rsSection = mysql_num_rows($rsSection);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit an intranet section</title>
<link href="../cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p><a href="index.php">Home</a></p>
<h1>Edit an intranet section</h1>
  <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
    <table cellpadding="3" cellspacing="0">
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">Section:</td>
        <td><input type="text" name="section" value="<?php echo htmlentities($row_rsSection['section'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">Rank order:</td>
        <td><input name="rankorder" type="text" class="input50" value="<?php echo htmlentities($row_rsSection['rankorder'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">&nbsp;</td>
        <td><input type="submit" class="btnAdd" value="Update" /></td>
      </tr>
    </table>
    <input type="hidden" name="MM_update" value="form1" />
    <input type="hidden" name="sectionid" value="<?php echo $row_rsSection['sectionid']; ?>" />
  </form>


</body>
</html>
<?php
mysql_free_result($rsSection);
?>
