<?php require_once('../Connections/connEABP2.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php
// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../docs";
$ppu->extensions = "pdf,doc";
$ppu->formName = "form1";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {
	  if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE documents SET naid=%s, doctypeid=%s, title=%s, docfile=IFNULL(%s,docfile) WHERE docid=%s",
                       GetSQLValueString($_POST['naid'], "int"),
                       GetSQLValueString($_POST['doctypeid'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['docfile'], "text"),
                       GetSQLValueString($_POST['docid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST['delid'])) && ($_POST['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM documents WHERE docid=%s",
                       GetSQLValueString($_POST['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());

  $deleteGoTo = "confirmed.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDocTypes = "SELECT * FROM doctypes";
$rsDocTypes = mysql_query($query_rsDocTypes, $connEABP2) or die(mysql_error());
$row_rsDocTypes = mysql_fetch_assoc($rsDocTypes);
$totalRows_rsDocTypes = mysql_num_rows($rsDocTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNAs = "SELECT acronym, country, national.naid FROM national INNER JOIN country ON national.cid = country.cid";
$rsNAs = mysql_query($query_rsNAs, $connEABP2) or die(mysql_error());
$row_rsNAs = mysql_fetch_assoc($rsNAs);
$totalRows_rsNAs = mysql_num_rows($rsNAs);

$colname_rsDoc = "-1";
if (isset($_GET['docid'])) {
  $colname_rsDoc = $_GET['docid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsDoc = sprintf("SELECT * FROM documents WHERE docid = %s", GetSQLValueString($colname_rsDoc, "int"));
$rsDoc = mysql_query($query_rsDoc, $connEABP2) or die(mysql_error());
$row_rsDoc = mysql_fetch_assoc($rsDoc);
$totalRows_rsDoc = mysql_num_rows($rsDoc);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Edit a document</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>Edit a document <?php if (isset($_POST['processed'])) { ?> - <img src="../images/tick.gif" alt="tick" width="16" height="16" /><?php } ?></h1>
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="checkFileUpload(this,'pdf,doc',false,'','','','','','','');return document.MM_returnValue">
  <table cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nation association:</td>
      <td><select name="naid">
        <?php 
do {  
?>
        <option value="<?php echo $row_rsNAs['naid']?>" <?php if (!(strcmp($row_rsNAs['naid'], $row_rsDoc['naid']))) {echo "SELECTED";} ?>><?php echo $row_rsNAs['acronym']?></option>
        <?php
} while ($row_rsNAs = mysql_fetch_assoc($rsNAs));
?>
      </select>
      </td>
    </tr>
    <tr> </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Doctype:</td>
      <td><select name="doctypeid">
        <?php 
do {  
?>
        <option value="<?php echo $row_rsDocTypes['doctypeid']?>" <?php if (!(strcmp($row_rsDocTypes['doctypeid'], $row_rsDoc['doctypeid']))) {echo "SELECTED";} ?>><?php echo $row_rsDocTypes['doctype']?></option>
        <?php
} while ($row_rsDocTypes = mysql_fetch_assoc($rsDocTypes));
?>
      </select>
      </td>
    </tr>
    <tr> </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Title:</td>
      <td><input name="title" type="text" class="input500" value="<?php echo $row_rsDoc['title']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Docfile:</td>
      <td><input name="docfile" type="file" id="docfile" onchange="checkOneFileUpload(this,'pdf,doc',false,'','','','','','','')" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><input name="processed" type="hidden" id="processed" value="1" /></td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="docid" value="<?php echo $row_rsDoc['docid']; ?>" />
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<form id="frmDelete" name="frmDelete" method="post" action="">
  <input type="submit" name="btnDelete" id="btnDelete" value="Delete" />
  <input name="delid" type="hidden" id="delid" value="<?php echo $row_rsDoc['docid']; ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsDocTypes);

mysql_free_result($rsNAs);

mysql_free_result($rsDoc);
?>
