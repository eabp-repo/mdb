<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE `national` SET cid=%s, live=%s, acronym=%s, fullname=%s, logofile=%s, webaddress=%s, president=%s, presdetails=%s, presemail=%s, secretary=%s, secdetails=%s, secemail=%s, treasurer=%s, treasdetails=%s, treasemail=%s, rep=%s, repdetails=%s, repemail=%s, secretariatname=%s, secretariatemail=%s, vicepres=%s, vicepresemail=%s, memsec=%s, memsecemail=%s, intername=%s, interemail=%s, otherdetails=%s, username=%s, password=%s WHERE naid=%s",
                       GetSQLValueString($_POST['cid'], "int"),
                       GetSQLValueString(isset($_POST['live']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['acronym'], "text"),
                       GetSQLValueString($_POST['fullname'], "text"),
                       GetSQLValueString($_POST['logofile'], "text"),
                       GetSQLValueString($_POST['webaddress'], "text"),
                       GetSQLValueString($_POST['president'], "text"),
                       GetSQLValueString($_POST['presdetails'], "text"),
                       GetSQLValueString($_POST['presemail'], "text"),
                       GetSQLValueString($_POST['secretary'], "text"),
                       GetSQLValueString($_POST['secdetails'], "text"),
                       GetSQLValueString($_POST['secemail'], "text"),
                       GetSQLValueString($_POST['treasurer'], "text"),
                       GetSQLValueString($_POST['treasdetails'], "text"),
                       GetSQLValueString($_POST['treasemail'], "text"),
                       GetSQLValueString($_POST['rep'], "text"),
                       GetSQLValueString($_POST['repdetails'], "text"),
                       GetSQLValueString($_POST['repemail'], "text"),
                       GetSQLValueString($_POST['secretariatname'], "text"),
                       GetSQLValueString($_POST['secretariatemail'], "text"),
                       GetSQLValueString($_POST['vicepres'], "text"),
                       GetSQLValueString($_POST['vicepresemail'], "text"),
                       GetSQLValueString($_POST['memsec'], "text"),
                       GetSQLValueString($_POST['memsecemail'], "text"),
                       GetSQLValueString($_POST['intername'], "text"),
                       GetSQLValueString($_POST['interemail'], "text"),
                       GetSQLValueString($_POST['otherdetails'], "text"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['password'], "text"),
                       GetSQLValueString($_POST['naid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

$colname_rsNA = "-1";
if (isset($_GET['naid'])) {
  $colname_rsNA = $_GET['naid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = sprintf("SELECT * FROM `national` WHERE naid = %s", GetSQLValueString($colname_rsNA, "int"));
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit national association</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p><a href="index.php"><img src="../images/logo-20.gif" alt="" width="47" height="20" border="0" align="absmiddle" /> admin home</a></p>
<h1>Edit national association
  <?php if (isset($_POST['processed'])) { ?>
- <img src="../images/tick.gif" alt="tick" width="16" height="16" />
<?php } ?>
</h1>
<p class="alignRight"><a href="national-delete.php?naid=<?php echo $row_rsNA['naid']; ?>" class="smallText">Delete this national association - you will be asked to confirm</a></p> 

<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
  <table cellpadding="3" cellspacing="0">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Live:</td>
      <td><input name="live" type="checkbox" class="input50" id="live" value="1" <?php if (!(strcmp($row_rsNA['live'],1))) {echo "checked=\"checked\"";} ?> /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Country:</td>
      <td><select name="cid">
        <?php 
do {  
?>
        <option value="<?php echo $row_rsCountries['cid']?>" <?php if (!(strcmp($row_rsCountries['cid'], $row_rsNA['cid']))) {echo "SELECTED";} ?>><?php echo $row_rsCountries['country']?></option>
        <?php
} while ($row_rsCountries = mysql_fetch_assoc($rsCountries));
?>
      </select>      </td>
    </tr>
    <tr> </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Acronym:</td>
      <td><input type="text" name="acronym" value="<?php echo $row_rsNA['acronym']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Full name:</td>
      <td><input name="fullname" type="text" class="input500" value="<?php echo $row_rsNA['fullname']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Logo file:</td>
      <td><input type="text" name="logofile" value="<?php echo $row_rsNA['logofile']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Web address:</td>
      <td><input type="text" name="webaddress" value="<?php echo $row_rsNA['webaddress']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">President:</td>
      <td><input type="text" name="president" value="<?php echo $row_rsNA['president']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">President details:</td>
      <td><input name="presdetails" type="text" class="input500" value="<?php echo $row_rsNA['presdetails']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">President email:</td>
      <td><input type="text" name="presemail" value="<?php echo $row_rsNA['presemail']; ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretary:</td>
      <td><input type="text" name="secretary" value="<?php echo htmlentities($row_rsNA['secretary'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretary details:</td>
      <td><input name="secdetails" type="text" class="input500" value="<?php echo htmlentities($row_rsNA['secdetails'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretary email:</td>
      <td><input type="text" name="secemail" value="<?php echo htmlentities($row_rsNA['secemail'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Treasurer:</td>
      <td><input type="text" name="treasurer" value="<?php echo htmlentities($row_rsNA['treasurer'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Treasurer details:</td>
      <td><input name="treasdetails" type="text" class="input500" value="<?php echo htmlentities($row_rsNA['treasdetails'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Treasurer email:</td>
      <td><input type="text" name="treasemail" value="<?php echo htmlentities($row_rsNA['treasemail'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Rep:</td>
      <td><input type="text" name="rep" value="<?php echo htmlentities($row_rsNA['rep'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Rep details:</td>
      <td><input name="repdetails" type="text" class="input500" value="<?php echo htmlentities($row_rsNA['repdetails'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Rep email:</td>
      <td><input type="text" name="repemail" value="<?php echo htmlentities($row_rsNA['repemail'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretariat name:</td>
      <td><input type="text" name="secretariatname" value="<?php echo htmlentities($row_rsNA['secretariatname'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Secretariat email:</td>
      <td><input type="text" name="secretariatemail" value="<?php echo htmlentities($row_rsNA['secretariatemail'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Vice President name:</td>
      <td><input name="vicepres" type="text" id="vicepres" value="<?php echo htmlentities($row_rsNA['vicepres']); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Vice President email:</td>
      <td><input name="vicepresemail" type="text" id="vicepresemail" value="<?php echo htmlentities($row_rsNA['vicepresemail']); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Membership Secretary name:</td>
      <td><input name="memsec" type="text" id="memsec" value="<?php echo htmlentities($row_rsNA['memsec']); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Membership Secretary email:</td>
      <td><input name="memsecemail" type="text" id="memsecemail" value="<?php echo htmlentities($row_rsNA['memsecemail']); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">International name:</td>
      <td><input type="text" name="intername" value="<?php echo htmlentities($row_rsNA['intername'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">International email:</td>
      <td><input type="text" name="interemail" value="<?php echo htmlentities($row_rsNA['interemail'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Other details:</td>
      <td><textarea name="otherdetails" cols="32" rows="3"><?php echo htmlentities($row_rsNA['otherdetails'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Login username:</td>
      <td><input type="text" name="username" value="<?php echo htmlentities($row_rsNA['username'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Login password:</td>
      <td><input type="text" name="password" value="<?php echo htmlentities($row_rsNA['password'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><input name="processed" type="hidden" id="processed" value="1" /></td>
      <td><input type="submit" class="btnAdd" value="Update" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="naid" value="<?php echo $row_rsNA['naid']; ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsCountries);

mysql_free_result($rsNA);
?>
