<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsNaMembers = "-1";
if (isset($_GET['na'])) {
  $colname_rsNaMembers = $_GET['na'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNaMembers = sprintf("SELECT mid AS eabp_membership_number, firstname, lastname, email, homephone, workphone, IF(workadd1 ='' OR workadd1 IS NULL,homeadd1,workadd1) AS address, IF(workadd1 ='' OR workadd1 IS NULL,homecity,workcity) AS city, IF(workadd1 ='' OR workadd1 IS NULL,homepostcode,workpostcode) AS postcode, IF(workadd1 ='' OR workadd1 IS NULL,country.country,country_1.country) AS country,  acronym AS NA, type FROM (type INNER JOIN ((country INNER JOIN member ON country.cid = member.cid) INNER JOIN national ON member.nid = national.naid) ON type.tid = member.tid) INNER JOIN country AS country_1 ON member.workcid = country_1.cid WHERE acronym = %s AND lastname <> '' AND (member.tid=7 OR member.tid=8) ORDER BY lastname", GetSQLValueString($colname_rsNaMembers, "text"));
$rsNaMembers = mysql_query($query_rsNaMembers, $connEABP2) or die(mysql_error());
//$row_rsNaMembers = mysql_fetch_assoc($rsNaMembers);
//$totalRows_rsNaMembers = mysql_num_rows($rsNaMembers);
?>

<?php 
csvToExcelDownloadFromResult($rsNaMembers);

// setup the headers
function setExcelContentType() {
    if(headers_sent())
        return false;

    header('Content-type: application/vnd.ms-excel');
    return true;
}

function setDownloadAsHeader($filename) {
    if(headers_sent())
        return false;

    header('Content-disposition: attachment; filename=' . $filename);
    return true;
}
// send a CSV to a stream using a mysql result
function csvFromResult($stream, $rsNaMembers, $showColumnHeaders = true) {
    if($showColumnHeaders) {
        $columnHeaders = array();
        $nfields = mysql_num_fields($rsNaMembers);
        for($i = 0; $i < $nfields; $i++) {
            $field = mysql_fetch_field($rsNaMembers, $i);
            $columnHeaders[] = $field->name;
        }
        fputcsv($stream, $columnHeaders);
    }

    $nrows = 0;
    while($row = mysql_fetch_row($rsNaMembers)) {
        fputcsv($stream, $row);
        $nrows++;
    }

    return $nrows;
}
// use the above function to write a CSV to a file, given by $filename
function csvFileFromResult($filename, $rsNaMembers, $showColumnHeaders = true) {
    $fp = fopen($filename, 'w');
    $rc = csvFromResult($fp, $rsNaMembers, $showColumnHeaders);
    fclose($fp);
    return $rc;
}
// output
function csvToExcelDownloadFromResult($rsNaMembers, $showColumnHeaders = true, $asFilename = 'members-by-na') {
	$myvalue = '-'.$_GET['na'];
	$asFilename = $asFilename.$myvalue;	
	$asFilename = $asFilename. '.csv';
    setExcelContentType();
    setDownloadAsHeader($asFilename);
    return csvFileFromResult('php://output', $rsNaMembers, $showColumnHeaders);
}
?>
<?php 
mysql_free_result($rsNaMembers);
?>
