<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = "SELECT CASE WHEN country IS NULL THEN 'Total' ELSE country END  AS 'Country', Count(CASE WHEN member.tid =7 THEN mid END) AS 'Full', Count(CASE WHEN member.tid =4 THEN mid END) AS 'Candidate', Count(CASE WHEN member.tid =3 THEN mid END) AS 'Associate', Count(CASE WHEN member.tid =13 THEN mid END) AS 'Student', Count(CASE WHEN member.tid =8 THEN mid END) AS 'Honorary', Count(*) AS Total FROM type INNER JOIN (country INNER JOIN member ON country.cid = member.cid) ON type.tid = member.tid WHERE member.nid = 12 AND type.tid <> 1 AND type.tid <> 2 AND type.tid <> 5 AND type.tid <> 6 AND type.tid <> 9 AND type.tid <> 11  AND type.tid <> 12  AND showonsite <> 0 GROUP BY country WITH ROLLUP";
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

?>
<?php 
csvToExcelDownloadFromResult($rsMembers);

// setup the headers
function setExcelContentType() {
    if(headers_sent())
        return false;

    header('Content-type: application/vnd.ms-excel');
    return true;
}

function setDownloadAsHeader($filename) {
    if(headers_sent())
        return false;

    header('Content-disposition: attachment; filename=' . $filename);
    return true;
}
// send a CSV to a stream using a mysql result
function csvFromResult($stream, $rsMembers, $showColumnHeaders = true) {
    if($showColumnHeaders) {
        $columnHeaders = array();
        $nfields = mysql_num_fields($rsMembers);
        for($i = 0; $i < $nfields; $i++) {
            $field = mysql_fetch_field($rsMembers, $i);
            $columnHeaders[] = $field->name;
        }
        fputcsv($stream, $columnHeaders);
    }

    $nrows = 0;
    while($row = mysql_fetch_row($rsMembers)) {
        fputcsv($stream, $row);
        $nrows++;
    }

    return $nrows;
}
// use the above function to write a CSV to a file, given by $filename
function csvFileFromResult($filename, $rsMembers, $showColumnHeaders = true) {
    $fp = fopen($filename, 'w');
    $rc = csvFromResult($fp, $rsMembers, $showColumnHeaders);
    fclose($fp);
    return $rc;
}
// output
function csvToExcelDownloadFromResult($rsMembers, $showColumnHeaders = true, $asFilename = 'members-eabp-as-na.csv') {
    setExcelContentType();
    setDownloadAsHeader($asFilename);
    return csvFileFromResult('php://output', $rsMembers, $showColumnHeaders);
}

?>
<?php
mysql_free_result($rsMembers);
?>