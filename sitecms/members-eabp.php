<?php require_once('../Connections/connEABP2.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembersEABP = "SELECT mid, firstname, lastname, type FROM member INNER JOIN type ON member.tid = type.tid WHERE nid = 12 AND type.tid <> 12 AND type.tid <> 8 ORDER BY  type.tid DESC, type.type, lastname ASC";
$rsMembersEABP = mysql_query($query_rsMembersEABP, $connEABP2) or die(mysql_error());
$row_rsMembersEABP = mysql_fetch_assoc($rsMembersEABP);
$totalRows_rsMembersEABP = mysql_num_rows($rsMembersEABP);
?>
<?php  $lastTFM_nest = "";?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>registered through the EABP</title>
<style type="text/css">
h3 {
	margin: 0px;
	padding: 0px;
}
</style>
</head>

<body>
<p>Members registered through the EABP </p>
<table border="0" cellspacing="0" cellpadding="5">
  <?php do { ?>
    <tr>
      <td valign="top" scope="col"><?php $TFM_nest = $row_rsMembersEABP['type'];
if ($lastTFM_nest != $TFM_nest) { 
	$lastTFM_nest = $TFM_nest; ?>
        <h3><?php echo $row_rsMembersEABP['type']; ?></h3>
      <?php } //End of Basic-UltraDev Simulated Nested Repeat?></td>
      <td scope="col"><a href="member-edit-login.php?mid=<?php echo $row_rsMembersEABP['mid']; ?>"><?php echo htmlentities($row_rsMembersEABP['lastname']); ?>, <?php echo htmlentities($row_rsMembersEABP['firstname']); ?></a></td>
    </tr>
    <?php } while ($row_rsMembersEABP = mysql_fetch_assoc($rsMembersEABP)); ?>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsMembersEABP);
?>
