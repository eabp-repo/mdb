<?php
/*
Common functions used repeteadly throughout the site
 */
require_once 'Connections/connEABP2.php';

if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
		case "text":
			$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			break;
		case "long":
		case "int":
			$theValue = ($theValue != "") ? intval($theValue) : "NULL";
			break;
		case "double":
			$theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
			break;
		case "date":
			$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			break;
		case "defined":
			$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
			break;
		}
		return $theValue;
	}
}

$cpd_email = 'cpd.eabp@eabp.org';
$river_email = 'river.cpd@eabp.org';

if (!function_exists("do_login")) {
	function do_login($loginUsername, $password, $LoginRS__query, $MM_fldUserAuthorization, $MM_redirectLoginSuccess, $MM_redirectLoginFailed, $MM_redirecttoReferrer) {
		global $cpd_email;
		global $river_email;
		$loginFoundUser = false;

		if ($loginUsername == $cpd_email) {
			$loginFoundUser = ($password == '191122committee');
		} elseif ($loginUsername == $river_email) {
			$loginFoundUser = ($password == 'gHQNV3DfPDHc9AMwOILb');		
		} else {
			global $database_connEABP2, $connEABP2;
			mysql_select_db($database_connEABP2, $connEABP2);

			$LoginRS = mysql_query($LoginRS__query, $connEABP2) or die(mysql_error());
			$loginFoundUser = mysql_num_rows($LoginRS);
		}
		if ($loginFoundUser) {
			$loginStrGroup = "";

			//declare two session variables and assign them
			$_SESSION['MM_Username'] = $loginUsername;
			$_SESSION['MM_UserGroup'] = $loginStrGroup;

			if (isset($_SESSION['PrevUrl']) && $MM_redirecttoReferrer) {
				$MM_redirectLoginSuccess = $_SESSION['PrevUrl'];
			}
			header("Location: " . $MM_redirectLoginSuccess);
		} else {
			header("Location: " . $MM_redirectLoginFailed);
		}
	}

}

if (!function_exists("isAuthorized")) {

// *** Restrict Access To Page: Grant or deny access to this page
	function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
		// For security, start by assuming the visitor is NOT authorized.
		$isValid = false;

		// When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
		// Therefore, we know that a user is NOT logged in if that Session variable is blank.
		if (!empty($UserName)) {
			// Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
			// Parse the strings into arrays.
			$arrUsers = Explode(",", $strUsers);
			$arrGroups = Explode(",", $strGroups);
			if (in_array($UserName, $arrUsers)) {
				$isValid = true;
			}
			// Or, you may restrict access to only certain users based on their username.
			if (in_array($UserGroup, $arrGroups)) {
				$isValid = true;
			}
			if (($strUsers == "") && true) {
				$isValid = true;
			}
		}
		return $isValid;
	}
}

if (!function_exists("google_translate")) {
	function google_translate() {
		require_once "gTranslate.php";
	}
}

if (!function_exists("member_menu")) {
	function member_menu($current_page, $mid) {
		$pages = [
			'login' => "Login details",
			'general' => "General",
			'contact' => "Contact details",
			'training' => "Training",
			'work' => "Current Work",
			'cpd' => "CPD",
			//'cpd-log' => "CPD Log",
		];

		echo '<p class="clear">EABP individual member update:</p>';
		$menu = [];
		foreach ($pages as $k => $lbl) {
			if ($current_page == $k) {
				$menu[] = $lbl;
			} else {
				$url = 'member-edit-' . $k . '.php?mid=' . $mid;
				if ($k == 'cpd-log') {
					$url = 'https://eabp.rivercpd.com';
				}
				$menu[] = '<a href="' . $url . '">' . $lbl . '</a>';
			}
		}

		echo '<p>' . implode(' | ', $menu) . '</p>';

		echo '<hr size="1" noshade="noshade" />';
	}
}

if (!function_exists("pv")) {
	function pv($var, $key) {
		if (isset($var)) {
			if ($var) {
				if (array_key_exists($key, $var)) {
					return $var[$key];
				}
			}
		}
		return '';
	}
}

if (!function_exists("print_nums")) {
	function print_nums($end_num, $start_num = 1, $num_length = 2) {
		echo '<option value="" selected="selected"></option>';
		$fmt = '%0' . $num_length . 's';
		if ($end_num >= $start_num) {
			for ($i = $start_num; $i <= $end_num; $i++) {
				$nr = sprintf($fmt, $i);
				echo '<option value="' . $nr . '">' . $nr . '</option>';
			}
		} else {
			for ($i = $start_num; $i >= $end_num; $i--) {
				$nr = sprintf($fmt, $i);
				echo '<option value="' . $nr . '">' . $nr . '</option>';
			}
		}
	}
}
