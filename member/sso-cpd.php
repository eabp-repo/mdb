<?php
require_once '../common.php';

if (!isset($_SESSION)) {
    session_start();
}

if ($_SESSION && array_key_exists('MM_Username', $_SESSION)) {
  $secret = 'a62f2225bf70bfaccbc7f1ef2a397836717377de';

    /*
    We will redirect the user via a GET request the SSOUrl provided, the following query string parameters will be appended to the request
    - returnUrl: you should redirect the user back to this url when they have authenticated on your website successfully.
    - code: this is a SHA256 hash of the returnUrl + '&' + SsoSecret. You can use this to verify that the request originates from us
    -callbackUrl: you will also need to pass this back to us on the return request
    https://mdb.eabp.org/members/sso-cpd.php?returnUrl=https%3A%2F%2Fregister.rivercpd.com%2FAccount%2FVerifySso%3FReturnUrl%3D%252Fconnect%252Fauthorize%252Fcallback%253Fclient_id%253Dcpd-eabp%2526response_type%253Dcode%252520id_token%2526scope%253Dopenid%252520profile%2526state%253DOpenIdConnect.AuthenticationProperties%25253DKDW_0987c9tzKQeSpLWJ5ce4N-1lBcnBnLnG4jjarGpiCQsNVB_o3rCgADcO94pB8U7trxc2bWVUPcmVLeGLhV2BDpRXkNNPB2aZ3p6fV3OWVp-5-WxqGeNodlIYGece%2526response_mode%253Dform_post%2526nonce%253D637457034431833448.NzE1OTI5NWUtYTY1YS00NWM3LTk5NGUtNDgwZmU1NjlkMmJkNTdjMjgxNmQtYmQzZC00MGNhLThlODctYzIxNGI2MzM3YmZi%2526redirect_uri%253Dhttps%25253A%25252F%25252Feabp.rivercpd.com%25252Fsignin-oidc%2526post_logout_redirect_uri%253Dhttps%25253A%25252F%25252Feabp.rivercpd.com%25252Fsignout-oidc%2526x-client-SKU%253DID_NET461%2526x-client-ver%253D6.8.0.0
    -
    https://mdb.eabp.org/member/sso-cpd.php?returnUrl=https%3A%2F%2Fregister.rivercpd.com%2FAccount%2FVerifySso&code=c1b9dc34c4396d9ebf503d3eddac7605a7cc8a992062c2e3796cec9d1315df8e&callback=%2Fconnect%2Fauthorize%2Fcallback%3Fclient_id%3Dcpd-eabp%26response_type%3Dcode%2520id_token%26scope%3Dopenid%2520profile%26state%3DOpenIdConnect.AuthenticationProperties%253DuhqvgJtHF3b82gBnACYl7e7CHxGo7Eh9Oz2kypy927uiRUx0Quhj8ynsU47CdkMLxDvASuPdb0deLifdXpI3FlPyd85Lwgp8mXwNR9Rf7AyXPgbtSuKKAPjXJPlqY7qT%26response_mode%3Dform_post%26nonce%3D637481183494989343.OGYyY2Y2ZjYtZDUwNi00Y2MxLWFlODAtZTM5NmIwZTg1MWI2NjIwYTQ1NDItZDNhMS00MDU4LWI5YjItZDkwMDc4YmNjNTRm%26redirect_uri%3Dhttps%253A%252F%252Feabp.rivercpd.com%252Fsignin-oidc%26post_logout_redirect_uri%3Dhttps%253A%252F%252Feabp.rivercpd.com%252Fsignout-oidc%26x-client-SKU%3DID_NET461%26x-client-ver%3D6.8.0.0

    */
    
    // verify the required QueryString parameters
    //$query_string_keys = ['returnUrl', 'code'];
    $query_string_keys = ['returnUrl','code','callback'];

    foreach ($query_string_keys as $k) {
      if (isset($_GET[$k])) {
        $params[$k] = $_GET[$k];
//        echo $k.'='.$_GET[$k].'<br/>';
      } else {
        die("<b>CPD LOG - ERROR</b><br/>Please contact EABP support and paste this message:<br/>Missing River CPD parameter [".$k.", ".$_SESSION['MM_Username'].", ".date("Y-m-d H:i:s")."]<br/>Thank you.");
      }
    }    
    
    global $cpd_email;
    global $river_email;

    if ($_SESSION['MM_Username'] == $cpd_email) {
      $row_rsMembers= [
        'dor' => null, 
        'email' => $cpd_email,
        'firstname' => 'EABP',
        'lastname' => 'CPD committee',
        'type' => 'CPD Admin',
        'mid' => 50500
      ];
    } elseif ($_SESSION['MM_Username'] == $river_email) {
      $row_rsMembers= [
        'dor' => null, 
        'email' => $river_email,
        'firstname' => 'EABP',
        'lastname' => 'River',
        'type' => 'Test Account',
        'mid' => 50510
      ];
    
    } else {
      mysql_select_db($database_connEABP2, $connEABP2);
      $query_rsMembers = sprintf("SELECT m.*,t.type FROM member m, type t WHERE m.email = %s and m.tid=t.tid", GetSQLValueString($_SESSION['MM_Username'], "text"));
      $rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
      $row_rsMembers = mysql_fetch_assoc($rsMembers);
    }
    $returnUrl = $params['returnUrl'];
    $parts = explode('?', $returnUrl);
    $cpd_url = $parts[0];
    //<returnUrl>?AccessExpires=&Email=&FirstName=&LastName=&ReturnUrl=&Roles=&UserId&state=
    $access_expires = date('d/M/Y', strtotime('+1 year'));
    if (!$row_rsMembers['dor'] == null){
      $access_expires = date('d/M/Y', strtotime('-1 year')); 
    }

    $params_out_unencoded=[
      "accessexpires=".$access_expires,
      "email=".$row_rsMembers['email'],
      "firstname=".$row_rsMembers['firstname'],
      "lastname=".$row_rsMembers['lastname'],
      //"ReturnUrl=".explode('=',$parts[1])[1],
      "returnurl=".urlencode($params['callback']),
      "roles=".$row_rsMembers['type'], 
      "userid=".$row_rsMembers['mid']
    ];

    $params_out=[
      "accessexpires=".urlencode($access_expires),
      "email=".urlencode($row_rsMembers['email']),
      "firstname=".urlencode($row_rsMembers['firstname']),
      "lastname=".urlencode($row_rsMembers['lastname']),
      //"ReturnUrl=".explode('=',$parts[1])[1],
      "returnurl=".urlencode($params['callback']),
      "roles=".urlencode($row_rsMembers['type']), 
      "userid=".urlencode($row_rsMembers['mid'])
    ];

    $qs = implode('&', $params_out);
    $qs_secret= implode('&', $params_out_unencoded);
    //echo 'ValueToHash:<br>'.$qs_secret.'<br>';

    $state = hash('sha256', $qs_secret.'&'.$secret);
    $qs .= "&state=".$state;

    $cpd_url .= '?'.$qs;
    //echo 'RiverCPD url:<br>'.$cpd_url.'<br>';
    header("Location: " . $cpd_url);
} else {

    // Not autheticated

    $MM_restrictGoTo = "login.php";
    $MM_qsChar = "?";
    $MM_referrer = $_SERVER['PHP_SELF'];
    if (strpos($MM_restrictGoTo, "?")) {
        $MM_qsChar = "&";
    }

    if (isset($_SERVER['QUERY_STRING'])) {
      $QUERY_STRING = $_SERVER['QUERY_STRING'];
    }

    if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) {
        $MM_referrer .= "?" . $QUERY_STRING;
    }

    $MM_restrictGoTo = $MM_restrictGoTo . $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
    header("Location: " . $MM_restrictGoTo);
    exit;
}
