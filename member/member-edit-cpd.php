<?php 
require_once '../Connections/connEABP2.php'; 
require_once '../common.php';

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP - CPD Log</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
<!--
#cpd-text {
	float: left;
	width: 600px;
}
#rightBar {
	margin-top: 50px;
}
li{
  line-height:1.4em;
  margin-bottom:0.6em;
}
dt{
  font-weight: bold;
  margin-top:1em;
}
dd{
  margin-bottom:0.7em;
}
/*
dd::before {
  content: "•"
}*/

#rightBar li{
font-size:1em;
}

-->
</style>
<body>
<?php if (strtolower($_SESSION['MM_Username']) == strtolower($row_rsMembers['email'])) : ?>
<p class="leftAlign"><a href="https://www.eabp.org">EABP website</a> |   | <a href="logout.php">Logout</a></p>
<?php 
  google_translate();
  member_menu('login', $row_rsMembers['mid']);
?>
<h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
<h2>Continuous Professional Development (CPD)</h2>
<hr size="1" noshade="noshade" />
<div id="cpd-text">
<p><strong>Welcome to the CPD log!</strong></p>
<p>
EABP requires its Full Members to undertake regular Continuous Professional Development (CPD). The CPD policy, adopted by the General Assembly in 2018, regulates this in more detail as follows:
</p>
<ul>
  <li><strong>Every Full Member must complete a total of 250 hours of CPD every 5 years.</strong> This means an average of 50 hours per year, but it can be distributed unevenly over the 5 years. We recommend that every member should do ma minimum of 20 hours of CPD in any one year.</li>
  <li>CPD activities come under several different categories (specified below), and the 5-yearly total <strong>needs to consist of activities in several categories</strong>.</li>
  <li>The categories are:
    <ol type="A">
      <li><strong>Work-based Learning Activities</strong> (for example, supervision and intervision, writing case studies, discussing work with colleagues, offering supervision to other practitioners, etc.)</li>
      <li><strong>Professional Activities</strong> (for example, fulfilling a role in a professional association such as EABP, training or lecturing, being an examiner, organising journal clubs or seminars, developing specialist skills, etc.)</li>
      <li><strong>Formal Educational Activities</strong> (for example, attending further education courses or additional trainings, conducting research, writing articles or papers, planning and running courses, etc.)</li>
      <li><strong>Self-directed Learning</strong> (for example having therapy for yourself, reading books, reviewing books, watching relevant films and TV programmes, etc.)</li>
      <li><strong>Other Aspects of CDP</strong> (this could for example be public service, delivering voluntary work, doing creative work related to body psychotherapy, etc.)</li>
    </ol>
  </li>
  <li>In order to document your CPD activities, you need to log them on CPD Website. You do this by clicking the link "CPD Log" on the right.</li>
  <li>You should log your activities and the number of hours together with a brief reflection on how this is developing you professionally. You should also upload some evidence for your CPD activities. The CPD log can accept most formats of computer files including pictures taken on phones.</li>
  <li>It should be easy to find your way around the CPD log. We have tried to make it self-explaining. However if you have difficulties, you can look at the explanatory manual on the right, or you can contact the CPD Committee on cpd.eabp@eabp.org. A member of the CPD committee will then contact you and see how we can help. <strong>Please be patient with us while we learn how to help you keep your CPD log (and make all possible mistakes)!</strong></li>
  <li>On the right you can find the full text of the EABP CPD Policy as voted in by the 2018 GA as well as a simplified version but please be aware that if anything is not clear, the full version applies.</li>

</ul>

<p>Below are some <strong>Frequently Asked Questions</strong> about CPD:</p>
<dl>
  <dt>1. What is CPD and why do I need it?</dt>
  <dd>Continuing Professional Development (CPD) is anything that supports your development as a Body Psychotherapist. If you have worked in this field for any length of time, you will know that when you finish training you are not just ‘finished’ but continue to learn all the time. We also know that being a body psychotherapist is not something that is just part of our identity, but rather it is a process. Like all processes this one has to be nurtured and supported, and CPD is an excellent way of doing that. This is one reason for doing CPD.</dd>
  <dd>Another reason is that EABP keeps a register of body psychotherapists in Europe. People consult this register in order to find a therapist. So by having your name on this register, EABP gives a certain guarantee that you practice to a high professional standard. EABP can do this initially because you fill in a detailed application form. But after a while EABP wants some evidence that you are still developing as a professional so that it can still give this guarantee to the general public and to other professional bodies.</dd>
  <dd>We are aware that you are probably doing enough of this continued professional development already. There are surely many things that you do that contribute to developing you as a professional. What you need to do here is to document this using our CPD log system.</dd>

  <dt>2. What do I need to do?</dt>
  <dd>Nowadays it is normal for events that you attend to give you a CPD Certificate – a piece of paper that says you have completed X number of hours. You need to start collecting these pieces of paper. You may need a confirmation from your supervisor about how many sessions you have had in the last year or the last Y years. If you meet with peers for peer supervision (Intervision), the same applies. If you learn from reading, write down what the reading material is and guess how many hours you have spent reading it. It may be best if you keep a file somewhere for your CPD. Once you start doing this, you will probably be surprised to see how much you actually do!</dd>
  <dd>Once every year at least, you need to log into the EABP Website members’ area and click the ‘CPD’ link. This takes you to your personal CPD log. Enter all your CPD events and hours there. The Website is set up so that you can go and enter any CPD activity as soon as you have done it if you prefer that.</dd>
  <dd>The Secretariat will send you a reminder when it is time to update your CPD record, so you don’t have to remember how often it needs updating.</dd>
  
  <dt>3. What happens if I don’t do it?</dt>
  <dd>The CPD Committee will conduct random checks on the Website. If they check your records and you haven’t kept them up to date, they will remind you to do it. You will be given a certain amount of time (usually weeks or months) to complete the records. If for any reason you are unable to keep this timeframe and need an extension, you need to contact the committee and inform them of this before the deadline.</dd>
  <dd>Your EABP accreditation is a guarantee that EABP undertakes to say that you are a good body psychotherapist, and if you don’t keep up your CPD, you make it difficult for EABP to continue to give this guarantee. Ultimately you may be excluded from Full EABP membership if EABP has no CPD records from you for too long.</dd>

  <dt>4. What if my CPD already gets monitored by a different professional body (for instance, a national psychotherapy association or medical association)?</dt>
  <dd>It needs to be clear that your CPD has to be in body psychotherapy. For example, if you are a medical practitioner, you will have to decide how much of your medical CPD can be counted for your professional development as a body psychotherapist.</dd>
  <dd>CPD records from other organisations can be imported into your CPD log. This should be straightforward and you can see how to do it once you are in your CPD log page. If you have problems doing this, the CPD committee will help.</dd>

  <dt>5. I have been working for a lot of years and am nearing retirement. Surely I don’t have to do this any more?</dt>
  <dd>You will find on the CPD log that we are looking at several categories of CPD, which represent different types of development, such as supervision, further training, teaching etc. If you think that you no longer need to complete some of these categories, you can contact us and let us know which categories you want to be exempted from and why.</dd>

  <dt>6. What about data protection?</dt>
  <dd>The records that you are logging on the CPD log are part of our members’ area and therefore password protected. We suggest you make sure you are using a strong password and change it from time to time.</dd>
  <dd>What you log on this site can only be seen by members of the CPD Committee and the EABP General Secretary. They will make random checks on whether EABP members are keeping their logs up to date, and to this end they need access to your records from the Website back office. They are Full Members of EABP and understand that all CPD information on this site is strictly confidential and that a breach of confidentiality would have severe consequences for themselves.</dd>  
</dl>

</div>
<div id="rightBar">
<h2>Links</h2>
<ul>
<li><a href="https://eabp.rivercpd.com">CPD Log</a></li>
<li><a href="/docs/EABP RiverCPD manual.pdf">Manual - How to use the CPD Log</a></li>
<li><a href="/docs/The EABP CPD Framework and Guidelines.pdf">EABP CPD Policy - Full verison</a></li>
<li><a href="/docs/EABP CPD framework_Main points.pdf">EABP CPD Policy - Simplified verison</a></li>
</ul>
</div>
<?php else :?>
<?php
   unset($_SESSION['MM_Username']);
   unset($_SESSION['PrevUrl']);
?>
<p>Sorry there is a problem - please email the <a href="mailto:secretariat@eabp.org">EABP Secretariat</a>.</p>
<?php endif; ?>
</body>
</html>
<?php
mysql_free_result($rsMembers);
?>