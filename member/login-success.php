<?php
require_once '../common.php';

if (!isset($_SESSION)) {
	session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("", $MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
	$MM_qsChar = "?";
	$MM_referrer = $_SERVER['PHP_SELF'];
	if (strpos($MM_restrictGoTo, "?")) {
		$MM_qsChar = "&";
	}

	if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) {
		$MM_referrer .= "?" . $QUERY_STRING;
	}

	$MM_restrictGoTo = $MM_restrictGoTo . $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
	header("Location: " . $MM_restrictGoTo);
	exit;
}

$colname_rsMembers = "-1";
if (isset($_SESSION['MM_Username'])) {
	$colname_rsMembers = $_SESSION['MM_Username'];
}
global $cpd_email;
if ($colname_rsMembers == $cpd_email) {
	header("Location: https://eabp.rivercpd.com");
	exit;
}
global $river_email;
if ($colname_rsMembers == $river_email) {
	header("Location: https://eabp.rivercpd.com");
	exit;
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid and member.tid=7 WHERE email = %s", GetSQLValueString($colname_rsMembers, "text"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP member login success</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php // guest
if ($row_rsMembers['mid'] != 961) {?>
<p class="leftAlign"><a href="https://www.eabp.org">EABP website</a> |  | <a href="logout.php">Logout</a></p>
<p>&nbsp;</p>
<p class="clear">EABP individual member update</p>
<h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
<h2>&nbsp;</h2>
<p>You have logged in successfully. <a href="member-edit-login.php?mid=<?php echo $row_rsMembers['mid']; ?>">Continue &gt;</a></p>
<?php if ($row_rsMembers['webpassword'] == "eabppassword") {?>
<p>Your password is still set as the default <em><strong>eabppassword</strong></em>. Please <a href="member-edit-login.php?mid=<?php echo $row_rsMembers['mid']; ?>">update it now</a>.</p>
<p>
  <?php }?>
</p>

  <?php // end guest
}?>
<?php if ($row_rsMembers['mid'] == 961) {?>
<p>
  <span class="leftAlign"><a href="logout.php">Logout</a></span>
</p>
<?php }?>
<hr size="1" noshade="noshade" />
<?php
mysql_free_result($rsMembers);
?>

<h2>International Body Psychotherapy Journal</h2>
<p>Download issues:</p>
<?php 
	$issues = json_decode(file_get_contents('https://ibpj.org/get_issues.php')); 	
	foreach ($issues as $issue) {
		$url='https://ibpj.org/issues/'.$issue->pdf;
		$title = $issue->issue;
		echo '<h2><a target="_blank" rel="noreferrer" href="'.$url.'">'.$title.'</a></h2>';
	}
?>
</body>
</html>
