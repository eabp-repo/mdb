<?php
 require_once '../common.php';
 require_once '../ScriptLibrary/incPureUpload.php';

if (!isset($_SESSION)) {
    session_start();
}

if ($_POST) {
if ($_POST['day'] != "" && $_POST['month'] != "" && $_POST['year'] != "") {
    $day = $_POST['day'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    $dob = "$year-$month-$day";
    $_POST['dob'] = $dob;
} else {
    $_POST['dob'] = $_POST['dob'];
}
if (($_POST['day2'] != "") && ($_POST['month2'] != "") && ($_POST['year2'] != "")) {
    $day2 = $_POST['day2'];
    $month2 = $_POST['month2'];
    $year2 = $_POST['year2'];
    $ecp = "$year2-$month2-$day2";
    $_POST['ecp'] = $ecp;
} else {
    $_POST['ecp'] = $_POST['ecp'];
}
}

// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "../images/memberphotos";
$ppu->extensions = "GIF,JPG";
$ppu->formName = "frmUpdate";
$ppu->storeType = "file";
$ppu->sizeLimit = "";
$ppu->nameConflict = "over";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
    $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
    if (isset($_SERVER['QUERY_STRING'])) {
        if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
            $editFormAction .= "&GP_upload=true";
        }
    } else {
        $editFormAction .= "?GP_upload=true";
    }
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmUpdate")) {
    $updateSQL = sprintf("UPDATE member SET dob=%s, ecp=%s, firstname=%s, lastname=%s, modality=%s, speciality=%s, langs=%s, skype=%s, title=%s, tid=%s, imagefile=IFNULL(%s,imagefile), showonsite=%s WHERE mid=%s",
        GetSQLValueString($_POST['dob'], "text"),
        GetSQLValueString($_POST['ecp'], "text"),
        GetSQLValueString($_POST['firstname'], "text"),
        GetSQLValueString($_POST['lastname'], "text"),
        GetSQLValueString($_POST['modality'], "text"),
        GetSQLValueString($_POST['speciality'], "text"),
        GetSQLValueString($_POST['langs'], "text"),
        GetSQLValueString($_POST['skype'], "text"),
        GetSQLValueString($_POST['title'], "text"),
        GetSQLValueString($_POST['tid'], "int"),
        GetSQLValueString($_POST['imagefile'], "text"),
        GetSQLValueString(isset($_POST['showonsite']) ? "true" : "", "defined", "1", "0"),
        GetSQLValueString($_POST['mid'], "int"));

    mysql_select_db($database_connEABP2, $connEABP2);
    $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
    $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9 AND tid <> 11 AND tid <> 6";
$rsMemTypes = mysql_query($query_rsMemTypes, $connEABP2) or die(mysql_error());
$row_rsMemTypes = mysql_fetch_assoc($rsMemTypes);
$totalRows_rsMemTypes = mysql_num_rows($rsMemTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsTypes = "SELECT * FROM type WHERE tid <> 1 AND tid <> 9";
$rsTypes = mysql_query($query_rsTypes, $connEABP2) or die(mysql_error());
$row_rsTypes = mysql_fetch_assoc($rsTypes);
$totalRows_rsTypes = mysql_num_rows($rsTypes);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCountries = "SELECT * FROM country ORDER BY country ASC";
$rsCountries = mysql_query($query_rsCountries, $connEABP2) or die(mysql_error());
$row_rsCountries = mysql_fetch_assoc($rsCountries);
$totalRows_rsCountries = mysql_num_rows($rsCountries);

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsNA = "SELECT naid, acronym FROM `national`";
$rsNA = mysql_query($query_rsNA, $connEABP2) or die(mysql_error());
$row_rsNA = mysql_fetch_assoc($rsNA);
$totalRows_rsNA = mysql_num_rows($rsNA);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>EABP individual membership database update: General</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
<style type="text/css">
<!--
.dateList {	width: 55px;
	font-size: 10px;
}
-->
</style>
<script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>
<?php if ($_SESSION && array_key_exists('MM_Username', $_SESSION) && $row_rsMembers && $_SESSION['MM_Username'] == $row_rsMembers['email']) {?>
<p class="leftAlign"><a href="https://www.eabp.org">EABP  website</a>   | <a href="logout.php">Logout</a></p>
<?php 
google_translate();
member_menu('general',$row_rsMembers['mid']);
?>
<h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
<h2>General info<?php if (isset($_POST['processed'])) {?> - <img src="../images/tick.gif" alt="tick" width="16" height="16" /><?php }?></h2>
<hr size="1" noshade="noshade" />
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmUpdate" id="frmUpdate" onsubmit="checkFileUpload(this,'GIF,JPG',false,'','','','','','','');return document.MM_returnValue">
  <table border="0" align="left" cellpadding="3" cellspacing="0" id="tblDetails">
    <tr>
      <td class="tdWhiteBg">Membership ID number:</td>
      <td class="tdWhiteBg"><?php echo $row_rsMembers['mid']; ?></td>
      <td align="right" class="tdWhiteBg">&nbsp;</td>
    </tr>
    <tr>
      <td>Show on website</td>
      <td><input <?php if (!(strcmp($row_rsMembers['showonsite'], 1))) {echo "checked=\"checked\"";}?> type="checkbox" name="showonsite" id="showonsite" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td width="200">Last Name</td>
      <td><input name="lastname" type="text" class="largerText" id="lastname" value="<?php echo $row_rsMembers['lastname']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>First Name</td>
      <td><input name="firstname" type="text" id="firstname" value="<?php echo $row_rsMembers['firstname']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Title</td>
      <td><input name="title" type="text" id="title" value="<?php echo $row_rsMembers['title']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Skype name</td>
      <td><input name="skype" type="text" id="skype" value="<?php echo $row_rsMembers['skype']; ?>" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Photo
          <?php if ($row_rsMembers['imagefile'] != "") {?>
          <span class="smallText"> - currently</span> <a href="../images/memberphotos/<?php echo $row_rsMembers['imagefile']; ?>"><img src="../images/memberphotos/<?php echo $row_rsMembers['imagefile']; ?>" alt="Click to view full size" width="24" height="24" border="0" align="absmiddle" /></a>
          <?php }?></td>
      <td><input name="imagefile" type="file" id="imagefile" onchange="checkOneFileUpload(this,'GIF,JPG',false,'','','','','','','')" /></td>
      <td align="center" class="smallText">Optional - 50KB max</td>
    </tr>

    <tr>
      <td>Main modalities</td>
      <td><input name="modality" type="text" id="modality" value="<?php echo $row_rsMembers['modality']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Speciality</td>
      <td><input name="speciality" type="text" id="speciality" value="<?php echo $row_rsMembers['speciality']; ?>" /></td>
      <td class="smallText">&nbsp;</td>
    </tr>

    <tr>
      <td>Languages spoken:</td>
      <td><input name="langs" type="text" id="langs" value="<?php echo $row_rsMembers['langs']; ?>" /></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Type of membership</td>
      <td><select name="tid" id="tid">
          <?php
do {
    ?>
          <option value="<?php echo $row_rsTypes['tid'] ?>"<?php if (!(strcmp($row_rsTypes['tid'], $row_rsMembers['tid']))) {echo "selected=\"selected\"";}?>><?php echo $row_rsTypes['type'] ?></option>
          <?php
} while ($row_rsTypes = mysql_fetch_assoc($rsTypes));
    $rows = mysql_num_rows($rsTypes);
    if ($rows > 0) {
        mysql_data_seek($rsTypes, 0);
        $row_rsTypes = mysql_fetch_assoc($rsTypes);
    }
    ?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>Date of Birth <span class="smallText">(yyyy-mm-dd)</span></td>
      <td><input name="dob" type="text" id="dob" value="<?php echo $row_rsMembers['dob']; ?>" maxlength="10" />
        <span class="smallText">
        <?php if ($row_rsMembers['dob'] != "") {?>
        UPDATE (all required)
        <?php } else {?>
        ENTER
        <?php }?>
        : day
        <select name="day" class="dateList" id="day">
        <?php print_nums(31) ?>
        </select>
month
<select name="month" class="dateList" id="month">
<?php print_nums(12) ?>
</select>
year
<select name="year" id="year"  class="dateList">
<?php print_nums(1905, 2011, 4) ?>
</select>
      </span></td>
      <td align="center" class="smallText">&nbsp;</td>
    </tr>
    <tr>
      <td>ECP <span class="smallText">(yyyy-mm-dd)</span></td>
      <td><input name="ecp" type="text" id="ecp" value="<?php echo $row_rsMembers['ecp']; ?>" maxlength="10" />
        <span class="smallText">
        <?php if ($row_rsMembers['ecp'] != "") {?>
UPDATE (all required)
<?php } else {?>
ENTER
<?php }?>
: day
<select name="day2" class="dateList" id="day2">
<?php print_nums(31) ?>
</select>
month
<select name="month2" class="dateList" id="month2">
<?php print_nums(12) ?>
</select>
year
<select name="year2" id="year2"  class="dateList">
<?php print_nums(1905, 2011,4) ?>
</select>
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>National association</td>
      <td><select name="naid" id="naid">
          <?php
do {
        ?>
          <option value="<?php echo $row_rsNA['naid'] ?>"<?php if (!(strcmp($row_rsNA['naid'], $row_rsMembers['nid']))) {echo "selected=\"selected\"";}?>><?php echo $row_rsNA['acronym'] ?></option>
          <?php
} while ($row_rsNA = mysql_fetch_assoc($rsNA));
    $rows = mysql_num_rows($rsNA);
    if ($rows > 0) {
        mysql_data_seek($rsNA, 0);
        $row_rsNA = mysql_fetch_assoc($rsNA);
    }
    ?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td align="center" class="tdWhiteBg"><?php if (isset($_POST['processed'])) {?>
      <img src="../images/tick.gif" alt="tick" width="16" height="16" />
      <?php }?></td>
      <td class="tdWhiteBg"><input name="mid" type="hidden" id="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
        <input name="btnUpdate2" type="submit" class="btnAdd" id="btnUpdate2" value="Update" />
        <input name="MM_update" type="hidden" id="MM_update" value="frmUpdate" /></td>
      <td align="right" class="tdWhiteBg">&nbsp;</td>
    </tr>
  </table>
  <input name="processed" type="hidden" id="processed" value="1" />
  <input type="hidden" name="MM_update" value="frmUpdate" />
</form>
<?php }?>
</body>
</html>
<?php
mysql_free_result($rsMembers);

mysql_free_result($rsMemTypes);

mysql_free_result($rsTypes);

mysql_free_result($rsCountries);

mysql_free_result($rsNA);
?>
