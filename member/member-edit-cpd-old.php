<?php 
  require_once '../common.php';
  require_once '../ScriptLibrary/incPureUpload.php'; 

// Pure PHP Upload 2.1.12
$ppu = new pureFileUpload();
$ppu->path = "cvs";
$ppu->extensions = "doc,docx,pdf";
$ppu->formName = "frmCV";
$ppu->storeType = "file";
$ppu->sizeLimit = "1000";
$ppu->nameConflict = "uniq";
$ppu->nameToLower = false;
$ppu->requireUpload = false;
$ppu->minWidth = "";
$ppu->minHeight = "";
$ppu->maxWidth = "";
$ppu->maxHeight = "";
$ppu->saveWidth = "";
$ppu->saveHeight = "";
$ppu->timeout = "600";
$ppu->progressBar = "";
$ppu->progressWidth = "300";
$ppu->progressHeight = "100";
$ppu->redirectURL = "";
$ppu->checkVersion("2.1.12");
$ppu->doUpload();
 
if (!isset($_SESSION)) {
  session_start();
  }

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($editFormAction)) {
  if (isset($_SERVER['QUERY_STRING'])) {

    if (!stripos($_SERVER['QUERY_STRING'], "GP_upload=true")) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($_GET['delid'])) && ($_GET['delid'] != "")) {
  $deleteSQL = sprintf("DELETE FROM membercongresses WHERE mcid=%s",
                       GetSQLValueString($_GET['delid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($deleteSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmCV")) {
  $updateSQL = sprintf("UPDATE membercpd SET cv=IFNULL(%s,cv), showcv=IFNULL(%s,cv) WHERE mid=%s",
                       GetSQLValueString($_POST['cv'], "text"),
                       GetSQLValueString(isset($_POST['showcv']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmCPD")) {
  $updateSQL = sprintf("UPDATE membercpd SET cat1=%s, cat2=%s, cat3=%s, cat4=%s, cat5=%s WHERE mid=%s",
                       GetSQLValueString($_POST['cat1'], "text"),
                       GetSQLValueString($_POST['cat2'], "text"),
                       GetSQLValueString($_POST['cat3'], "text"),
                       GetSQLValueString($_POST['cat4'], "text"),
                       GetSQLValueString($_POST['cat5'], "text"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmAddCongress")) {
  $insertSQL = sprintf("INSERT INTO membercongresses (mid, congressid) VALUES (%s, %s)",
                       GetSQLValueString($_POST['mid'], "int"),
                       GetSQLValueString($_POST['congressid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($insertSQL, $connEABP2) or die(mysql_error());
}

mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCongresses = "SELECT * FROM congresses ORDER BY year DESC";
$rsCongresses = mysql_query($query_rsCongresses, $connEABP2) or die(mysql_error());
$row_rsCongresses = mysql_fetch_assoc($rsCongresses);
$totalRows_rsCongresses = mysql_num_rows($rsCongresses);

$colname_rsMemCongresses = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMemCongresses = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMemCongresses = sprintf("SELECT mcid, congress FROM membercongresses INNER JOIN congresses ON membercongresses.congressid = congresses.congressid WHERE mid = %s", GetSQLValueString($colname_rsMemCongresses, "int"));
$rsMemCongresses = mysql_query($query_rsMemCongresses, $connEABP2) or die(mysql_error());
$row_rsMemCongresses = mysql_fetch_assoc($rsMemCongresses);
$totalRows_rsMemCongresses = mysql_num_rows($rsMemCongresses);

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);

$colname_rsCPD = "-1";
if (isset($_GET['mid'])) {
  $colname_rsCPD = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsCPD = sprintf("SELECT * FROM membercpd WHERE mid = %s", GetSQLValueString($colname_rsCPD, "int"));
$rsCPD = mysql_query($query_rsCPD, $connEABP2) or die(mysql_error());
$row_rsCPD = mysql_fetch_assoc($rsCPD);
$totalRows_rsCPD = mysql_num_rows($rsCPD);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP individual membership database update</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#frmCPD {
	float: left;
	width: 600px;
}
#rightBar {
	margin-top: 50px;
}
.btnUpload {
	background-color: #CCFF99;
	border: 1px solid #006600;
	font-weight: normal;
	width: 48px;
	height: 20px;
	font-size: 12px;
}
.input20 {
	width: 25px;
	font-size: 1.2em;
	float: left;
}
#frmCV label {
	clear: both;
	float: left;
}
-->
</style>
    <SCRIPT language="JavaScript">
function popUp(URL) {
eval("page" + " = window.open(URL, '" + "', 'toolbars=0,scrollbars=1,location=0,statusbars=0,menubars=0,resizable=0,width=500,height=400,left=50,top=100');");
}
</script>
    <script language='JavaScript' src='../ScriptLibrary/incPureUpload.js' type="text/javascript"></script>
</head>

<body>

<?php if ($_SESSION && array_key_exists('MM_Username', $_SESSION) && $row_rsMembers && $_SESSION['MM_Username'] == $row_rsMembers['email']) {?>
<p class="leftAlign"><a href="https://www.eabp.org">EABP website</a> |  | <a href="logout.php">Logout</a></p>
<?php 
google_translate();
member_menu('cpd',$row_rsMembers['mid']);
?>
  <h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
  <h2>Continuing Professional Development<?php if (isset($_POST['processed'])) { ?>
    <img src="../images/tick.gif" alt="tick" width="16" height="16" />
<?php } ?></h2>
<form method="POST" action="<?php echo $editFormAction; ?>" name="frmCPD" id="frmCPD">
  <p>Up until now EABP has no requirements for members for continuing professional development. But this may be necessary in the future. For now we invite you to keep a profile of what you have done.</p>
  <p>NB. EAP requires a minimum of 250 hours over 5 years with no more than 75 hours counting in any one of their categories. </p>
  <p><em>You are advised to type up what you show here in a saved document first; then copy and paste into each section.</em></p>
  <h3>Category 1</h3>
<p>Advanced specialist training courses</p>

  <textarea name="cat1" cols="45" rows="10" class="input500" id="cat1"><?=pv($row_rsCPD,'cat1')?></textarea>
<h3>Category 2</h3>
<p>Supervision; Intervision/Professional discussion groups with peers</p>
  <textarea name="cat2" cols="45" rows="10" class="input500" id="cat2"><?=pv($row_rsCPD,'cat2')?></textarea>
<h3>Category 3</h3>
<p>Workshops followed; Workshops given; Ethics workshop </p>
  <textarea name="cat3" cols="45" rows="10" class="input500" id="cat3"><?=pv($row_rsCPD,'cat3')?></textarea>
<h3>Category 4</h3>
<p><a href="javascript:popUp('cpd-guidance-cat4.htm')">See guidance notes</a></p>
<textarea name="cat4" cols="45" rows="10" class="input500" id="cat4"><?=pv($row_rsCPD,'cat4')?></textarea>
<h3>Category 5</h3>
<p><a href="javascript:popUp('cpd-guidance-cat5.htm')">See guidance notes</a></p>
<p>
  <textarea name="cat5" cols="45" rows="10" class="input500" id="cat5"><?=pv($row_rsCPD,'cat5')?></textarea>
</p>
<p>
  <input name="button2" type="submit" class="btnAdd" id="button2" value="Update" />
  <input name="mid" type="hidden" id="mid" value="<?php echo $_GET['mid']; ?>" />
  <input name="processed" type="hidden" id="processed" value="1" />
<?php if (isset($_POST['processed'])) { ?>
          <img src="../images/tick.gif" alt="tick" width="16" height="16" />
  <?php } ?></p>
<input type="hidden" name="MM_update" value="frmCPD" />
</form>
<div id="rightBar">
  <h2>Curriculum Vitae
    <?php if (isset($_POST['cvprocessed'])) { ?>
          <img src="../images/tick.gif" alt="tick" width="16" height="16" />
<?php } ?></h2>
  <p>Can be a Word document or pdf. 100kb limit.</p>
  <form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="frmCV" id="frmCV" onsubmit="checkFileUpload(this,'doc,docx,pdf',false,100,'','','','','','');return document.MM_returnValue">
    <input name="cv" type="file" id="cv" onchange="checkOneFileUpload(this,'doc,docx,pdf',false,100,'','','','','','')" value="<?php echo $row_rsCPD['cv']; ?>" />
    <input name="btnUpload" type="submit" class="btnGo" id="btnUpload" value="Go" />
    <input name="mid" type="hidden" id="mid" value="<?php echo $_GET['mid']; ?>" />
    <input name="cvprocessed" type="hidden" id="cvprocessed" value="1" />
    <input type="hidden" name="MM_update" value="frmCV" />
<label>Show on website:</label>
<?php 
  $checked='';
  if (pv($row_rsCPD,'showcv') == 1) {
    $checked='checked="checked"';
  }
/*<input <?php if (!(strcmp($row_rsCPD['showcv'],1))) {echo "checked=\"checked\"";} ?> name="showcv" type="checkbox" class="input20" id="showcv" value="1" />
  */
  ?>
  <input <?=$checked?> name="showcv" type="checkbox" class="input20" id="showcv" value="1" />
  </form>
  <hr class="clear" />
  <h2>Congresses attended</h2>
  
  <?php if ($totalRows_rsMemCongresses == 0) { // Show if recordset empty ?>
    <p>No congresses attended.</p>
    <?php } // Show if recordset empty ?>
  <?php if ($totalRows_rsMemCongresses > 0) { // Show if recordset not empty ?>
    <ul>
      <?php do { ?>
        <li><?php echo $row_rsMemCongresses['congress']; ?> - <a href="member-edit-cpd.php?delid=<?php echo $row_rsMemCongresses['mcid']; ?>&amp;mid=<?php echo $_GET['mid']; ?>">delete</a></li>
        <?php } while ($row_rsMemCongresses = mysql_fetch_assoc($rsMemCongresses)); ?>
    </ul>
    <?php } // Show if recordset not empty ?>
  <form id="frmAddCongress" name="frmAddCongress" method="POST" action="<?php echo $editFormAction; ?>">
    <select name="congressid" id="congressid">
      <option value="">Add a congress: </option>
<?php
do {  
?>
      <option value="<?php echo $row_rsCongresses['congressid']?>"><?php echo $row_rsCongresses['congress']?></option>
      <?php
} while ($row_rsCongresses = mysql_fetch_assoc($rsCongresses));
  $rows = mysql_num_rows($rsCongresses);
  if($rows > 0) {
      mysql_data_seek($rsCongresses, 0);
	  $row_rsCongresses = mysql_fetch_assoc($rsCongresses);
  }
?>
    </select>
      <input name="button" type="submit" class="btnGo" id="button" value="Go" />
      <input name="mid" type="hidden" id="mid" value="<?php echo $_GET['mid']; ?>" />
      <input type="hidden" name="MM_insert" value="frmAddCongress" />
  </form>
</div>
<?php } ?>
</body>
</html>
<?php
mysql_free_result($rsCongresses);

mysql_free_result($rsMemCongresses);

mysql_free_result($rsMembers);

mysql_free_result($rsCPD);
?>
