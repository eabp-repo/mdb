<?php
require_once '../common.php';

// *** Validate request to login to this site.
if (!isset($_SESSION)) {
	session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];

if (isset($_GET['accesscheck'])) {
	$_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['email'])) {

	$loginUsername = $_POST['email'];
	$password = $_POST['webpassword'];

	$LoginRS__query = sprintf("SELECT email, webpassword FROM member WHERE email=%s AND webpassword=%s AND tid =7",
		GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text"));

	do_login($loginUsername, $password, $LoginRS__query, "", "login-success.php", "login.php?failed=1", true);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP member login</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<a href="https://www.eabp.org">Website</a>
<h1>EABP full member login</h1>
<form id="frmLogin" name="frmLogin" method="POST" action="<?php echo $loginFormAction; ?>">
  <table border="0" cellpadding="3" cellspacing="0" id="tblDetails">
    <tr>
      <td>Username (your email address): </td>
      <td><input name="email" type="text" class="input200" id="email" /></td>
    </tr><?php echo ((isset($_SERVER[""])) ? $_SERVER[""] : "") ?>
    <tr>
      <td>Password:</td>
      <td><input name="webpassword" type="password" class="input200" id="webpassword" /></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFFF" class="tdWhiteBg">&nbsp;</td>
      <td class="tdWhiteBg"><input name="btnUpdate" type="submit" id="btnUpdate" value="Login" /></td>
    </tr>
  </table>
</form>
<hr />
<form id="frmPassword" name="frmPassword" method="post" action="mailer-password.php">
  <label for="email"></label>
  Enter your email address to receive a password reminder:
  <input type="text" name="email" id="email" />
  <input name="btnGo" type="submit" class="btnGo" id="btnGo" value="Go" />
</form>
<?php if (isset($_GET['failed']) && $_GET['failed'] == 1) {?>
<p>Sorry, there is a problem. Please try again. <strong>Please note only full members can login</strong>.</p>

<p>If you still can't login <a href="mailto:secretariat@eabp.org">email the EABP Secretariat</a>.</p>
<?php }?>
<?php if (isset($_GET['p'])) {?>
<p>Please check your email for the password reminder.</p>
<?php }?>
</body>
</html>
