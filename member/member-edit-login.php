<?php 
require_once '../Connections/connEABP2.php'; 
require_once '../common.php';

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmUpdate")) {
  $updateSQL = sprintf("UPDATE member SET webpassword=%s WHERE mid=%s",
                       GetSQLValueString($_POST['webpassword'], "text"),
                       GetSQLValueString($_POST['mid'], "int"));

  mysql_select_db($database_connEABP2, $connEABP2);
  $Result1 = mysql_query($updateSQL, $connEABP2) or die(mysql_error());
}

$colname_rsMembers = "-1";
if (isset($_GET['mid'])) {
  $colname_rsMembers = $_GET['mid'];
}
mysql_select_db($database_connEABP2, $connEABP2);
$query_rsMembers = sprintf("SELECT member.*, country, acronym, type FROM ((member INNER JOIN country ON member.cid = country.cid) INNER JOIN national ON member.nid = national.naid) INNER JOIN type ON member.tid = type.tid WHERE mid = %s", GetSQLValueString($colname_rsMembers, "int"));
$rsMembers = mysql_query($query_rsMembers, $connEABP2) or die(mysql_error());
$row_rsMembers = mysql_fetch_assoc($rsMembers);
$totalRows_rsMembers = mysql_num_rows($rsMembers);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EABP individual membership database update</title>
<link href="cms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php if (strtolower($_SESSION['MM_Username']) == strtolower($row_rsMembers['email'])) { ?>
<p class="leftAlign"><a href="https://www.eabp.org">EABP website</a> |   | <a href="logout.php">Logout</a></p>
<?php 
  google_translate();
  member_menu('login', $row_rsMembers['mid']);
?>
<h1><?php echo $row_rsMembers['lastname']; ?>, <?php echo $row_rsMembers['firstname']; ?></h1>
<h2>Login details</h2>
<hr size="1" noshade="noshade" />
<form action="<?php echo $editFormAction; ?>" method="POST" name="frmUpdate" id="frmUpdate" >
  <table border="0" cellpadding="3" cellspacing="0" id="tblDetails">
    
    <tr>
      <td>Username (your email address):</td>
      <td><strong><?php echo $row_rsMembers['email']; ?></strong><span class="smallText"> if you wish to change this please <a href="mailto:secretariat@eabp.org">email the EABP Secretariat</a></span></td>
    </tr>
    <tr>
      <td>Password:</td>
      <td><input name="webpassword" type="text" class="input200" id="webpassword" value="<?php echo $row_rsMembers['webpassword']; ?>" /></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFFF" class="tdWhiteBg"><?php if (isset($_POST['processed'])) { ?>
          <img src="../images/tick.gif" alt="tick" width="16" height="16" />
      <?php } ?></td>
      <td class="tdWhiteBg"><input name="mid" type="hidden" id="mid" value="<?php echo $row_rsMembers['mid']; ?>" />
        <input name="btnUpdate" type="submit" id="btnUpdate" value="Update" />
        <input name="MM_update" type="hidden" id="MM_update" value="frmUpdate" /></td>
    </tr>
  </table>
  <input name="processed" type="hidden" id="processed" value="1" />
  <input type="hidden" name="MM_update" value="frmUpdate" />
</form>
<?php } else {
   unset($_SESSION['MM_Username']);
   unset($_SESSION['PrevUrl']);
    ?>
<p>Sorry there is a problem - please email the <a href="mailto:secretariat@eabp.org">EABP Secretariat</a>.</p>
<?php } ?>
</body>
</html>
<?php
mysql_free_result($rsMembers);
?>
